#include "gneiss/VioSystem.h"
#include "gneiss/executable-utils/Evaluator.h"
#include "gneiss/image/ImageMeasurement.h"
#include "gneiss/io/Dataset.h"
#include "gneiss/io/VkittiLoader.h"
#include "gneiss/parameters/parameters.h"
#include "gneiss/utils/constexpr_for.h"

#include <CLI/CLI.hpp>  // IWYU pragma: keep

#include <atomic>
#include <cstddef>
#include <exception>
#include <filesystem>
#include <iostream>
#include <memory>
#include <string>
#include <string_view>
#include <thread>
#include <utility>
#include <vector>

using namespace std::literals;

class OpticalFlowCliExe {
 public:
  OpticalFlowCliExe(const std::filesystem::path& datasetPath, const std::string_view scenarioName)
      : m_dataset(gneiss::io::load_vkitti(datasetPath, scenarioName)),
        m_evaluator(m_dataset),
        m_imageFeeder([this]() { feed_images(); }),
        m_resultFetcher([this]() { fetch_results(); }),
        m_profilingFetcher([this]() {
          if constexpr (gneiss::parameters::parameters.at("simple_profiling"sv).get<bool>()) {
            fetch_profiling();
          }
        }) {
    m_opticalFlowSystem.get_vio().shutdown();
  }

  ~OpticalFlowCliExe() {
    m_opticalFlowSystem.get_keypoints_queue().shutdown();
    m_opticalFlowSystem.get_image_queue().shutdown();
    m_opticalFlowSystem.get_profiling_queue().shutdown();
    m_shutdown = true;

    if (m_imageFeeder.joinable()) {
      m_imageFeeder.join();
    }
    if (m_resultFetcher.joinable()) {
      m_resultFetcher.join();
    }
    if (m_profilingFetcher.joinable()) {
      m_profilingFetcher.join();
    }
  }

  void join() {
    if (m_resultFetcher.joinable()) {
      m_resultFetcher.join();
    }
  }

  void summarize_optical_flow_evaluation(const std::filesystem::path& evaluationFile = std::filesystem::path()) {
    m_evaluator.summarize(evaluationFile);
  }

 private:
  void feed_images() {
    for (std::size_t frameIdx = 0; frameIdx < m_dataset->get_number_of_frames(); ++frameIdx) {
      if (m_shutdown) {
        return;
      }

      gneiss::ImageMeasurement imageMeasurement;
      gneiss::constexpr_for<0, cameras.size(), 1>([&](auto cameraIdx) {
        using ThisImage = typename gneiss::internal::ParameterizedImage<cameraIdx>::Image_;
        imageMeasurement.get<cameraIdx>() = m_dataset->cameraMeasurements.at(cameraIdx)
                                                .at(frameIdx)
                                                ->template getImage<typename ThisImage::PixelType, ThisImage::width,
                                                                    ThisImage::height, ThisImage::pitched>();
      });
      imageMeasurement.timestamp() = m_dataset->cameraTimestamps.at(frameIdx);
      auto imageMeasurementPtr = m_opticalFlowSystem.get_storage().storeImageMeasurement(imageMeasurement);
      m_opticalFlowSystem.get_image_queue().push(std::move(imageMeasurementPtr));
    }

    m_opticalFlowSystem.get_image_queue().stop();
  }

  void fetch_results() {
    while (!m_shutdown) {
      const auto optionalkeypointsEstimate = m_opticalFlowSystem.get_keypoints_queue().pop();
      if (!optionalkeypointsEstimate) {
        break;
      }
      m_evaluator.push_back(*optionalkeypointsEstimate.value());
    }
  }

  void fetch_profiling() {
    while (!m_shutdown) {
      const auto optionalProfilingStats = m_opticalFlowSystem.get_profiling_queue().pop();
      if (!optionalProfilingStats) {
        return;
      }
      m_evaluator.push_back(optionalProfilingStats.value());
    }
  }

  constexpr static auto& cameras = gneiss::parameters::parameters["cameras"sv].array_data();
  std::atomic_bool m_shutdown = false;
  std::shared_ptr<gneiss::io::Dataset> m_dataset;
  gneiss::executable_utils::Evaluator m_evaluator;
  gneiss::VioSystem m_opticalFlowSystem;
  std::thread m_imageFeeder;
  std::thread m_resultFetcher;
  std::thread m_profilingFetcher;
};

int main(int argc, char** argv) {
  try {
    // NOLINTNEXTLINE(misc-include-cleaner)
    CLI::App app{"App description"};

    std::filesystem::path datasetPath = "undefined";
    std::string scenarioName = "undefined";
    std::filesystem::path evaluationFile;
    app.add_option("-d,--dataset_path", datasetPath, "Path to the dataset root folder")
        ->required()
        ->check(CLI::ExistingDirectory);  // NOLINT(misc-include-cleaner)
    app.add_option("-s, --scenario", scenarioName, "Name of the scenario.")->required();
    app.add_option("-e, --evaluation_file", evaluationFile, "File path to write evaluation to.");

    try {
      app.parse((argc), (argv));
    }
    // NOLINTNEXTLINE(misc-include-cleaner)
    catch (const CLI::ParseError& e) {
      return app.exit(e);
    }

    OpticalFlowCliExe opticalFlowExe(datasetPath, scenarioName);
    opticalFlowExe.join();
    opticalFlowExe.summarize_optical_flow_evaluation(evaluationFile);

  } catch (const std::exception& e) {
    std::cout << e.what();
    return 1;
  }

  return 0;
}