find_package(CLI11 REQUIRED CONFIG)
find_package(fmt REQUIRED CONFIG)

add_executable(optical-flow-cli-exe src/main.cpp)

target_include_directories(optical-flow-cli-exe PRIVATE $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src>)

target_link_libraries(optical-flow-cli-exe PRIVATE CLI11::CLI11 fmt::fmt gneiss::core gneiss::io gneiss::parameters)

include(CheckIPOSupported)
check_ipo_supported(RESULT IPO_SUPPORTED OUTPUT error)

if(IPO_SUPPORTED)
  message(STATUS "IPO / LTO enabled")
  set_property(TARGET optical-flow-cli-exe PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
else()
  message(STATUS "IPO / LTO not supported: <${error}>")
endif()

# target_compile_options(optical-flow-cli-exe PRIVATE -fsanitize=address -fsanitize=leak -fsanitize=undefined)
# target_link_options(optical-flow-cli-exe PRIVATE -fsanitize=address -fsanitize=leak -fsanitize=undefined)

install(TARGETS optical-flow-cli-exe)
