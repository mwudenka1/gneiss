workspace "gneiss" "This an architecture model of the gneiss visual-inertial odometry system embedded in a robotic software stack." {

    model {
        developer = person "Developer" "A human developer of gneiss." {
            tags Development
        }

        robot = person "Robot" "A robot" {
            tags Deploy Robot
        }

        robotSoftwareStack = softwareSystem "RobotSoftwareSystem" "The software stack or development environment of some robot" {
            gneiss = container "gneiss core" "TODO desc" {
                tags Library

                storage = component "Storage" "TODO desc" {
                    properties {
                        shutdown "void shutdown()"
                        storeImageMeasurement "ImageMeasurementPtr storeImageMeasurement(const ImageMeasurement& measurement)"
                        storeKeypointsEstimate "KeypointsEstimatePtr storeKeypointsEstimate(const KeypointsEstimate& estimate)"
                    }
                }

                imageQueue = component "ImageQueue" "TODO desc" {
                    tags Queue
                    properties {
                        push "void push(ImageMeasurementConstPtr imageMeasurement)"
                        pop "std::optional<ImageMeasurementConstPtr> pop()"
                        shutdown "void shutdown()"
                        stop "void stop()"
                    }
                }

                keypointsQueue = component "KeypointsQueue" "TODO desc" {
                    tags Queue
                    properties {
                        push "void push(KeypointsEstimateConstPtr keypointsEstimate)"
                        pop "std::optional<KeypointsEstimateConstPtr> pop()"
                        shutdown "void shutdown()"
                        stop "void stop()"
                    }
                }

                vioQueue = component "VioQueue" "TODO desc" {
                    tags Queue
                    properties {
                        shutdown "void shutdown()"
                        stop "void stop()"
                    }
                }

                imuQueue = component "ImuQueue" "TODO desc" {
                    tags Queue
                    properties {
                        shutdown "void shutdown()"
                        stop "void stop()"
                    }
                }

                profilingQueue = component "ProfilingQueue" "TODO desc" {
                    tags Queue
                    properties {
                        push "void push(ProfilingStats profilingStats)"
                        pop "std::optional<ProfilingStats> pop()"
                        shutdown "void shutdown()"
                        stop "void stop()"
                    }
                }

                opticalFlowEstimator = component "OpticalFlowEstimator" "TODO desc" {
                    -> storage "request memory"
                    -> imageQueue "pull Images"
                    -> keypointsQueue "push Optical Flow Result"
                    -> profilingQueue "push profiling data"
                    properties {
                        shutdown "void shutdown()"
                    }
                }

                vio = component "Vio" "TODO desc" {
                    -> storage "request memory"
                    -> keypointsQueue "pull key points"
                    -> imuQueue "pull IMU-data"
                    -> vioQueue "push Poses"
                    -> profilingQueue "push profiling data"
                    properties {
                        shutdown "void shutdown()"
                    }
                }
            }

            dataset = container "Dataset" "A benchmark dataset stored on disk." {
                tags Data Development
            }

            datasetVio = container "Dataset Vio" "An executable that consumes benchmark datasets and evaluates the performance." {
                tags Development Executable
                -> imageQueue "Images"
                -> imuQueue "IMU-data"
                vioQueue -> this "Poses"
                dataset -> this "Images and IMU-data"
            }

            sensorDriver = container "Sensor Driver" "" {
                tags Deploy Executable NotImplemented
            }
            poseConsumer = container "Pose Consumer" "" {
                tags Deploy Executable NotImplemented
            }
            iOWrapper = container "I/O Wrapper" "An executable that uses gneiss and handles the I/O." {
                 tags Deploy Executable NotImplemented
                 -> imageQueue "Images"
                 -> imuQueue "IMU-data"
                 vioQueue -> this "Poses"
                 sensorDriver -> this "IMU-data and Poses"
                 -> poseConsumer "Poses"
            }
        }

        developer -> datasetVio "Uses"
        robot -> sensorDriver "Has Sensors"
        robot -> poseConsumer "Uses"

        deploymentEnvironment "VioDevelopment" {
            deploymentNode "Developer Computer" "" "Ubuntu 20.04" {
                containerInstance gneiss
                containerInstance datasetVio
            }
        }

        deploymentEnvironment "Robot" {
            deploymentNode "SBC" "" "Ubuntu Core 20.04" {
                containerInstance gneiss
                containerInstance iOWrapper
                containerInstance sensorDriver
                containerInstance poseConsumer
            }
        }
    }

    views {
        systemLandscape SystemLandscape "TODO desc" {
            include *
            exclude dataset
            autoLayout lr
        }

        systemContext robotSoftwareStack SystemContext "TODO desc" {
            include *
            exclude dataset
            autoLayout lr
        }

        container robotSoftwareStack "ContainerViewFull" "Container view of gneiss full" {
            include *
            autolayout lr
        }

        container robotSoftwareStack "ContainerViewDevelopment" "Container view of gneiss development" {
            include ->element.tag==Development->
            autolayout lr
        }

        container robotSoftwareStack "ContainerViewDeploy" "Container view of gneiss deploy" {
            include ->element.tag==Deploy->
            autolayout lr
        }

        component gneiss "ComponentView" "TODO desc" {
            include *
            exclude datasetVio
            autolayout lr
        }

        deployment robotSoftwareStack "VioDevelopment" "VioDevelopmentDeployment" {
            include *
            autoLayout
        }

        deployment robotSoftwareStack "Robot" "RobotDeployment" {
            include *
            autoLayout
        }

        theme default

        styles {
            element Executable {
                background #2980b9
                color #efefef
            }
            element Library {
                background #16a085
                color #efefef
            }
            element "Person" {
                shape person
                background #08427b
                color #efefef
            }
            element Robot {
                shape Robot
            }
            element Queue {
                background #e67e22
                color #efefef
            }
            element Data {
                shape Cylinder
                background #8e44ad
                color #efefef
            }
            element NotImplemented {
                background #3990c9
                opacity 75
            }
        }
    }

}
