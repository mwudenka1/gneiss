#ifndef GNEISS_MAKE_VIEW_H
#define GNEISS_MAKE_VIEW_H
#pragma once
/** @file */

#include "gneiss/fields-2d/Reference.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/fields-2d/makeViewClosure.h"
#include "gneiss/utils/ignore.h"

#include <type_traits>

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

struct MakeViewFunction {
 private:
  // if view pass through
  template <typename Field2d_>
  static constexpr auto fromField2d(Field2d_&& field2d, std::true_type, ignore_type) {
    return static_cast<Field2d_&&>(field2d);
  }

  template <typename Field2d_>
  static constexpr auto fromField2d(Field2d_&& field2d, std::false_type, std::true_type) {
    return Reference(field2d);
  }

 public:
  template <typename Field2d_>
    requires field_2d<std::remove_reference_t<Field2d_>>
  constexpr auto operator()(Field2d_&& field2d) const {
    return MakeViewFunction::fromField2d(
        static_cast<Field2d_&&>(field2d),
        std::integral_constant<bool, field_2d_view<std::remove_reference_t<Field2d_>>>{},
        std::is_lvalue_reference<Field2d_>{});
  }
};

namespace view {
inline constexpr auto makeView = MakeViewFunction{};

template <typename Field2d_>
using makeView_t = decltype(makeView(std::declval<Field2d_>()));
}  // namespace view

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d
#endif  // GNEISS_MAKE_VIEW_H
