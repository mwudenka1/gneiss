#ifndef GNEISS_ROWVIEW_H
#define GNEISS_ROWVIEW_H
#pragma once

/** @file */

#include "gneiss/fields-2d/ViewBase.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/fields-2d/makeView.h"
#include "gneiss/fields-2d/makeViewClosure.h"
#include "gneiss/utils/assert.h"
#include "gneiss/utils/ranges.h"

#include <functional>
#include <utility>

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

template <field_2d InField2d_>
struct RowView : public gneiss::ranges::view_interface<RowView<InField2d_>>, public ViewBase {
  using scalar_type = typename InField2d_::scalar_type;
  constexpr static std::size_t width = InField2d_::width;
  constexpr static std::size_t height = 1;

  RowView() = default;
  constexpr RowView(RowView const& rhs) noexcept = default;
  constexpr RowView(RowView&& rhs) noexcept = default;
  constexpr RowView& operator=(RowView const& rhs) noexcept {
    if (rhs != this) {
      m_inField2d = rhs.m_inField2d;
      m_yIdx = rhs.m_yIdx;
    }
    return *this;
  };
  constexpr RowView& operator=(RowView&& rhs) noexcept {
    if (rhs != this) {
      m_inField2d = std::move(rhs.m_inField2d);
      m_yIdx = std::move(rhs.m_yIdx);
    }
    return *this;
  };
  ~RowView() = default;

  constexpr RowView(std::size_t yIdx, InField2d_ inField2d) : m_inField2d(std::move(inField2d)), m_yIdx(yIdx) {}

  [[nodiscard]] constexpr std::size_t size() const noexcept { return width; }

  constexpr scalar_type operator()(const std::size_t xIdx) const { return m_inField2d({xIdx, m_yIdx}); }

  template <typename IteratorRowView_>
    requires std::same_as<std::remove_cv_t<IteratorRowView_>, RowView>
  struct iterator_type {
    using iterator_category = std::random_access_iterator_tag;
    using difference_type = int64_t;
    using value_type = std::remove_cvref_t<typename InField2d_::scalar_type>;
    using reference_type = std::add_lvalue_reference_t<value_type>;

    iterator_type() = default;
    constexpr iterator_type(iterator_type const& rhs) noexcept = default;
    constexpr iterator_type(iterator_type&& rhs) noexcept = default;
    constexpr iterator_type& operator=(iterator_type const& rhs) noexcept = default;
    constexpr iterator_type& operator=(iterator_type&& rhs) noexcept = default;

    constexpr iterator_type(IteratorRowView_* rowView, std::size_t xIdx) : m_rowView(rowView), m_xIdx(xIdx) {}

    [[maybe_unused]] constexpr iterator_type& operator++() {
      ++m_xIdx;
      return *this;
    }

    // NOLINTNEXTLINE(cert-dcl21-cpp)
    [[maybe_unused]] constexpr iterator_type operator++(int) & {
      const auto tmp = *this;
      ++(*this);
      return tmp;
    }

    [[maybe_unused]] constexpr iterator_type& operator--() {
      --m_xIdx;
      return *this;
    }

    // NOLINTNEXTLINE(cert-dcl21-cpp)
    [[maybe_unused]] constexpr iterator_type operator--(int) & {
      const auto tmp = *this;
      --(*this);
      return tmp;
    }

    [[maybe_unused]] constexpr iterator_type& operator+=(const difference_type diff) {
      const auto tmp = static_cast<difference_type>(m_xIdx) + diff;
      m_xIdx = static_cast<std::size_t>(tmp);
      return *this;
    }

    [[maybe_unused]] constexpr iterator_type& operator-=(const difference_type diff) {
      const auto tmp = static_cast<difference_type>(m_xIdx) - diff;
      m_xIdx = static_cast<std::size_t>(tmp);
      return *this;
    }

    [[nodiscard]] constexpr iterator_type operator+(const difference_type diff) const {
      return iterator_type(m_rowView, static_cast<std::size_t>(static_cast<difference_type>(m_xIdx) + diff));
    }

    [[nodiscard]] friend constexpr iterator_type operator+(const difference_type diff,
                                                           const iterator_type& iter) noexcept {
      return iter + diff;
    }

    [[nodiscard]] constexpr iterator_type operator-(const difference_type diff) const {
      return iterator_type(m_rowView, static_cast<std::size_t>(static_cast<difference_type>(m_xIdx) - diff));
    }

    [[nodiscard]] constexpr difference_type operator-(const iterator_type& other) const {
      return static_cast<difference_type>(m_xIdx) - static_cast<difference_type>(other.m_xIdx);
    }

    [[nodiscard]] auto operator<=>(const iterator_type& other) const { return m_xIdx <=> other.m_xIdx; };

    [[nodiscard]] constexpr typename InField2d_::scalar_type operator*() const {
      GNEISS_ASSERT(m_rowView);
      GNEISS_ASSERT(m_xIdx < InField2d_::width);
      return (*m_rowView)(m_xIdx);
    }

    [[nodiscard]] constexpr typename InField2d_::scalar_type operator[](std::size_t idx) const {
      GNEISS_ASSERT(m_rowView);
      GNEISS_ASSERT(m_xIdx + idx < InField2d_::width);
      return (*m_rowView)(m_xIdx + idx);
    }

    friend constexpr bool operator==(const iterator_type& leftIter, const iterator_type& rightIter) {
      GNEISS_ASSERT(leftIter.m_rowView->getYIdx() == rightIter.m_rowView->getYIdx());
      return leftIter.m_rowView->getYIdx() == rightIter.m_rowView->getYIdx() && leftIter.m_xIdx == rightIter.m_xIdx;
    }
    friend constexpr bool operator!=(const iterator_type& leftIter, const iterator_type& rightIter) {
      GNEISS_ASSERT(leftIter.m_rowView->getYIdx() == rightIter.m_rowView->getYIdx());
      return leftIter.m_rowView->getYIdx() != rightIter.m_rowView->getYIdx() || leftIter.m_xIdx != rightIter.m_xIdx;
    }

   private:
    IteratorRowView_* m_rowView;
    std::size_t m_xIdx = 0;
  };

  using const_iterator = iterator_type<const RowView>;
  using iterator = iterator_type<RowView>;
  // using const_reverse_iterator = std::reverse_iterator<const_iterator>;
  // using reverse_iterator = std::reverse_iterator<iterator>;

  [[nodiscard]] constexpr auto begin() noexcept { return iterator(this, 0); }
  [[nodiscard]] constexpr auto end() noexcept { return iterator(this, width); }
  [[nodiscard]] constexpr auto begin() const noexcept { return const_iterator(this, 0); }
  [[nodiscard]] constexpr auto end() const noexcept { return const_iterator(this, width); }
  [[nodiscard]] constexpr auto cbegin() const noexcept { return const_iterator(this, 0); }
  [[nodiscard]] constexpr auto cend() const noexcept { return const_iterator(this, width); }
  // Does not work with libstdc++
  // [[nodiscard]] constexpr auto rbegin() { return reverse_iterator(end()); }
  // [[nodiscard]] constexpr auto rend() { return reverse_iterator(begin()); }
  // [[nodiscard]] constexpr auto rbegin() const { return const_reverse_iterator(end()); }
  // [[nodiscard]] constexpr auto rend() const { return const_reverse_iterator(begin()); }
  // [[nodiscard]] constexpr auto crbegin() const { return const_reverse_iterator(cend()); }
  // [[nodiscard]] constexpr auto crend() const { return const_reverse_iterator(cbegin()); }

  [[nodiscard]] constexpr const InField2d_& getField2d() const noexcept { return m_inField2d; }

  [[nodiscard]] constexpr std::size_t getYIdx() const noexcept { return m_yIdx; }

 private:
  InField2d_ m_inField2d;
  std::size_t m_yIdx{};
};

struct RowFunctionBase {
  template <typename InField2d_>
    requires field_2d<std::remove_reference_t<InField2d_>>
  constexpr auto operator()(std::size_t yIdx, InField2d_&& inField2d) const {
    return RowView<std::remove_cvref_t<view::makeView_t<InField2d_>>>(
        yIdx, view::makeView(static_cast<InField2d_&&>(inField2d)));
  }
};

struct RowFunction : RowFunctionBase {
  using RowFunctionBase::operator();

  constexpr auto operator()(std::size_t yIdx) const {
    return makeViewClosure(std::bind_front(RowFunctionBase{}, yIdx));
  }
};

namespace view {
inline constexpr RowFunction Row{};
}  // namespace view

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d

#endif  // GNEISS_ROWVIEW_H
