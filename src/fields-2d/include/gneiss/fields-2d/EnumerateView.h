#ifndef GNEISS_ENUMERATEVIEW_H
#define GNEISS_ENUMERATEVIEW_H
#pragma once
/** @file */

#include "gneiss/fields-2d/FieldCoordinate.h"
#include "gneiss/fields-2d/ViewBase.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/fields-2d/makeView.h"
#include "gneiss/fields-2d/makeViewClosure.h"

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

template <field_2d InField2d_>
struct EnumerateView : public ViewBase {
  using scalar_type = std::tuple<FieldCoordinate<std::size_t>, typename InField2d_::scalar_type>;
  constexpr static std::size_t width = InField2d_::width;
  constexpr static std::size_t height = InField2d_::height;

  EnumerateView() = default;
  constexpr EnumerateView(EnumerateView const& rhs) noexcept = default;
  constexpr EnumerateView(EnumerateView&& rhs) noexcept = default;
  constexpr EnumerateView& operator=(EnumerateView const& rhs) noexcept = default;
  constexpr EnumerateView& operator=(EnumerateView&& rhs) noexcept = default;
  ~EnumerateView() = default;

  explicit constexpr EnumerateView(InField2d_ inField2d) : m_inField2d(std::move(inField2d)) {}

  [[nodiscard]] inline constexpr scalar_type operator()(const FieldCoordinate<std::size_t>& coordinate) const {
    return scalar_type(coordinate, m_inField2d(coordinate));
  }
  [[nodiscard]] inline constexpr scalar_type operator()(const std::size_t xIdx, const std::size_t yIdx) const {
    return operator()(FieldCoordinate<std::size_t>{xIdx, yIdx});
  }

 private:
  InField2d_ m_inField2d;
};

struct EnumerateFunctionBase {
  template <typename InField2d_>
    requires field_2d<std::remove_reference_t<InField2d_>>
  constexpr auto operator()(InField2d_&& inField2d) const noexcept {
    return EnumerateView<std::remove_cvref_t<view::makeView_t<InField2d_>>>(
        view::makeView(static_cast<InField2d_&&>(inField2d)));
  }
};

namespace view {
inline constexpr auto Enumerate = makeViewClosure(EnumerateFunctionBase{});
}  // namespace view

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d
#endif  // GNEISS_ENUMERATEVIEW_H
