#ifndef GNEISS_CONCEPTS_H
#define GNEISS_CONCEPTS_H
#pragma once
/** @file */

#include "gneiss/fields-2d/FieldCoordinate.h"
#include "gneiss/fields-2d/ViewBase.h"

#include <concepts>
#include <cstddef>

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

template <typename Type_>
struct add_const_to_reference {
  using type = std::add_lvalue_reference_t<std::add_const_t<std::remove_reference_t<Type_>>>;
};

template <typename Type_>
using add_const_to_reference_t = typename add_const_to_reference<Type_>::type;

template <typename Type_>
struct add_const_if_reference {
  using type = std::conditional_t<std::is_lvalue_reference_v<Type_>, add_const_to_reference_t<Type_>, Type_>;
};

template <typename Type_>
using add_const_if_reference_t = typename add_const_if_reference<Type_>::type;

template <typename Type_>
struct add_const_or_const_to_ref {
  using type =
      std::conditional_t<std::is_lvalue_reference_v<Type_>, add_const_to_reference_t<Type_>, std::add_const_t<Type_>>;
};

template <typename Type_>
using add_const_or_const_to_ref_t = typename add_const_or_const_to_ref<Type_>::type;

template <typename Type_>
concept field_call_operator = requires(Type_ field, std::size_t xIdx, std::size_t yIdx) {
  { field(FieldCoordinate<std::size_t>{xIdx, yIdx}) };
  { field(xIdx, yIdx) };
};

template <typename Field2D_>
concept field_2d = std::same_as<decltype(Field2D_::width), const std::size_t> &&
                   std::same_as<decltype(Field2D_::height), const std::size_t> && field_call_operator<Field2D_>;

template <typename Field2D_>
concept field_2d_view = field_2d<Field2D_> && std::is_base_of_v<ViewBase, Field2D_>;

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d

#endif  // GNEISS_CONCEPTS_H
