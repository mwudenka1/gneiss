#ifndef GNEISS_RANGEVIEW_H
#define GNEISS_RANGEVIEW_H
#pragma once

/** @file */

#include "gneiss/fields-2d/ViewBase.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/fields-2d/makeView.h"
#include "gneiss/fields-2d/makeViewClosure.h"
#include "gneiss/utils/assert.h"
#include "gneiss/utils/ranges.h"

#include <functional>
#include <utility>

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

namespace internal {
class EmptyForRangesView {};
}  // namespace internal

template <field_2d InField2d_>
struct RangeView : public gneiss::ranges::view_interface<RangeView<InField2d_>>,
                   public std::conditional_t<std_ranges_used, ::ranges::view_interface<RangeView<InField2d_>>,
                                             internal::EmptyForRangesView> {
  RangeView() = default;
  constexpr RangeView(RangeView const& rhs) noexcept = default;
  constexpr RangeView(RangeView&& rhs) noexcept = default;
  constexpr RangeView& operator=(RangeView const& rhs) noexcept {
    if (rhs != this) {
      m_inField2d = rhs.m_inField2d;
    }
    return *this;
  };
  constexpr RangeView& operator=(RangeView&& rhs) noexcept {
    if (rhs != this) {
      m_inField2d = std::move(rhs.m_inField2d);
    }
    return *this;
  };
  ~RangeView() = default;

  explicit constexpr RangeView(InField2d_ inField2d) : m_inField2d(std::move(inField2d)) {}

  [[nodiscard]] constexpr std::size_t size() const noexcept { return InField2d_::height * InField2d_::width; }

  [[nodiscard]] constexpr InField2d_::scalar_type operator[](const int64_t idx) const { return begin()[idx]; }

  template <typename IteratorRangeView_>
    requires std::same_as<std::remove_cv_t<IteratorRangeView_>, RangeView>
  struct iterator_type {
    using iterator_category = std::random_access_iterator_tag;
    using difference_type = int64_t;
    using value_type = std::remove_cvref_t<typename InField2d_::scalar_type>;
    using reference_type = std::add_lvalue_reference_t<value_type>;

    iterator_type() = default;
    constexpr iterator_type(iterator_type const& rhs) noexcept = default;
    constexpr iterator_type(iterator_type&& rhs) noexcept = default;
    constexpr iterator_type& operator=(iterator_type const& rhs) noexcept = default;
    constexpr iterator_type& operator=(iterator_type&& rhs) noexcept = default;

    constexpr iterator_type(IteratorRangeView_* rangeView, FieldCoordinate<size_t> coordinate)
        : m_rangeView(rangeView), m_coordinate(coordinate) {}

    [[maybe_unused]] constexpr iterator_type& operator++() {
      ++m_coordinate.x;
      if (m_coordinate.x >= InField2d_::width) {
        m_coordinate.x = 0;
        ++m_coordinate.y;
      }
      return *this;
    }

    // NOLINTNEXTLINE(cert-dcl21-cpp)
    [[maybe_unused]] constexpr iterator_type operator++(int) & {
      const auto tmp = *this;
      ++(*this);
      return tmp;
    }

    [[maybe_unused]] constexpr iterator_type& operator--() {
      if (m_coordinate.x == 0) {
        m_coordinate.x = InField2d_::width - 1;
        --m_coordinate.y;
      } else {
        --m_coordinate.x;
      }
      return *this;
    }

    // NOLINTNEXTLINE(cert-dcl21-cpp)
    [[maybe_unused]] constexpr iterator_type operator--(int) & {
      const auto tmp = *this;
      --(*this);
      return tmp;
    }

    [[maybe_unused]] constexpr iterator_type& operator+=(const difference_type diff) {
      difference_type delta_y =
          (static_cast<difference_type>(m_coordinate.x) + diff) / static_cast<difference_type>(InField2d_::width);
      if (diff < 0 && m_coordinate.x != 0) {
        --delta_y;
      }
      m_coordinate.x =
          static_cast<std::size_t>(diff) - (static_cast<std::size_t>(delta_y) * InField2d_::width - m_coordinate.x);
      m_coordinate.y = static_cast<std::size_t>(static_cast<difference_type>(m_coordinate.y) + delta_y);

      return *this;
    }

    [[maybe_unused]] constexpr iterator_type& operator-=(const difference_type diff) {
      operator+=(-diff);
      return *this;
    }

    [[nodiscard]] constexpr iterator_type operator+(const difference_type diff) const {
      auto newIter = *this;
      newIter += diff;

      return newIter;
    }

    [[nodiscard]] friend constexpr iterator_type operator+(const difference_type diff,
                                                           const iterator_type& iter) noexcept {
      return iter + diff;
    }

    [[nodiscard]] constexpr iterator_type operator-(const difference_type diff) const {
      auto newIter = *this;
      newIter -= diff;

      return newIter;
    }

    [[nodiscard]] constexpr difference_type operator-(const iterator_type& other) const {
      return static_cast<difference_type>(m_coordinate.y * InField2d_::width + m_coordinate.x) -
             static_cast<difference_type>(other.m_coordinate.y * InField2d_::width + other.m_coordinate.x);
    }

    [[nodiscard]] auto operator<=>(const iterator_type& other) const noexcept {
      return (m_coordinate.y * InField2d_::width + m_coordinate.x) <=>
             (other.m_coordinate.y * InField2d_::width + other.m_coordinate.x);
    };

    [[nodiscard]] constexpr typename InField2d_::scalar_type operator*() const {
      GNEISS_ASSERT(m_rangeView);
      GNEISS_ASSERT(m_coordinate.x < InField2d_::width);
      GNEISS_ASSERT(m_coordinate.y < InField2d_::height);
      return m_rangeView->getField2d()(m_coordinate);
    }

    [[nodiscard]] constexpr typename InField2d_::scalar_type operator[](difference_type idx) const {
      GNEISS_ASSERT(m_rangeView);
      const auto newIter = *this + idx;
      return *newIter;
    }

    friend constexpr bool operator==(const iterator_type& leftIter, const iterator_type& rightIter) {
      return leftIter.m_coordinate == rightIter.m_coordinate;
    }
    friend constexpr bool operator!=(const iterator_type& leftIter, const iterator_type& rightIter) {
      return leftIter.m_coordinate != rightIter.m_coordinate;
    }

   private:
    IteratorRangeView_* m_rangeView;
    FieldCoordinate<size_t> m_coordinate{};
  };

  using const_iterator = iterator_type<const RangeView>;
  using iterator = iterator_type<RangeView>;
  // using const_reverse_iterator = std::reverse_iterator<const_iterator>;
  // using reverse_iterator = std::reverse_iterator<iterator>;

  [[nodiscard]] constexpr auto begin() noexcept { return iterator(this, {0, 0}); }
  [[nodiscard]] constexpr auto end() noexcept { return iterator(this, {0, InField2d_::height}); }
  [[nodiscard]] constexpr auto begin() const noexcept { return const_iterator(this, {0, 0}); }
  [[nodiscard]] constexpr auto end() const noexcept { return const_iterator(this, {0, InField2d_::height}); }
  [[nodiscard]] constexpr auto cbegin() const noexcept { return const_iterator(this, {0, 0}); }
  [[nodiscard]] constexpr auto cend() const noexcept { return const_iterator(this, {0, InField2d_::height}); }
  // Does not work with libstdc++
  // [[nodiscard]] constexpr auto rbegin() { return reverse_iterator(end()); }
  // [[nodiscard]] constexpr auto rend() { return reverse_iterator(begin()); }
  // [[nodiscard]] constexpr auto rbegin() const { return const_reverse_iterator(end()); }
  // [[nodiscard]] constexpr auto rend() const { return const_reverse_iterator(begin()); }
  // [[nodiscard]] constexpr auto crbegin() const { return const_reverse_iterator(cend()); }
  // [[nodiscard]] constexpr auto crend() const { return const_reverse_iterator(cbegin()); }

  [[nodiscard]] constexpr const InField2d_& getField2d() const noexcept { return m_inField2d; }

 private:
  InField2d_ m_inField2d;
};

struct RangeFunctionBase {
  template <typename InField2d_>
    requires field_2d<std::remove_reference_t<InField2d_>>
  constexpr auto operator()(InField2d_&& inField2d) const {
    return RangeView<std::remove_cvref_t<view::makeView_t<InField2d_>>>(
        view::makeView(static_cast<InField2d_&&>(inField2d)));
  }
};

namespace view {
inline constexpr auto Range = makeViewClosure(RangeFunctionBase{});
}  // namespace view

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d
#endif  // GNEISS_RANGEVIEW_H
