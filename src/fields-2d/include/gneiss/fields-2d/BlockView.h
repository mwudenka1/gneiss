#ifndef GNEISS_BLOCKVIEW_H
#define GNEISS_BLOCKVIEW_H
#pragma once
/** @file */

#include "gneiss/fields-2d/FieldCoordinate.h"
#include "gneiss/fields-2d/ViewBase.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/fields-2d/makeView.h"
#include "gneiss/fields-2d/makeViewClosure.h"

#include <functional>
#include <utility>

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

template <field_2d InField2d_, std::size_t width_, std::size_t height_>
struct BlockView : public ViewBase {
  static_assert(width_ <= InField2d_::width, "Block width must be smaller or equal than field width");
  static_assert(height_ <= InField2d_::height, "Block height must be smaller or equal than field height");
  using scalar_type = typename InField2d_::scalar_type;
  constexpr static std::size_t width = width_;
  constexpr static std::size_t height = height_;

  BlockView() = default;
  constexpr BlockView(BlockView const& rhs) noexcept = default;
  constexpr BlockView(BlockView&& rhs) noexcept = default;
  constexpr BlockView& operator=(BlockView const& rhs) noexcept = default;
  constexpr BlockView& operator=(BlockView&& rhs) noexcept = default;
  ~BlockView() = default;

  explicit constexpr BlockView(FieldCoordinate<std::size_t> topLeft, InField2d_ inField2d)
      : m_inField2d(std::move(inField2d)), m_topLeft(topLeft) {}

  [[nodiscard]] inline constexpr scalar_type operator()(const FieldCoordinate<std::size_t>& coordinate) const {
    return m_inField2d(m_topLeft + coordinate);
  }
  [[nodiscard]] inline constexpr scalar_type operator()(const std::size_t xIdx, const std::size_t yIdx) const {
    return operator()(FieldCoordinate<std::size_t>{xIdx, yIdx});
  }

 private:
  InField2d_ m_inField2d;
  FieldCoordinate<std::size_t> m_topLeft;
};

template <std::size_t width_, std::size_t height_>
struct BlockFunctionBase {
  template <typename InField2d_>
    requires field_2d<std::remove_reference_t<InField2d_>>
  constexpr auto operator()(FieldCoordinate<std::size_t> topLeft, InField2d_&& inField2d) const {
    return BlockView<std::remove_cvref_t<view::makeView_t<InField2d_>>, width_, height_>(
        topLeft, view::makeView(static_cast<InField2d_&&>(inField2d)));
  }
};

template <std::size_t width_, std::size_t height_>
struct BlockFunction : BlockFunctionBase<width_, height_> {
  using BlockFunctionBase<width_, height_>::operator();

  constexpr auto operator()(FieldCoordinate<std::size_t> topLeft) const {
    return makeViewClosure(std::bind_front(BlockFunctionBase<width_, height_>{}, topLeft));
  }
};

namespace view {
template <std::size_t width_, std::size_t height_>
inline constexpr auto Block = makeViewClosure(BlockFunction<width_, height_>{});
}  // namespace view

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d

#endif  // GNEISS_BLOCKVIEW_H
