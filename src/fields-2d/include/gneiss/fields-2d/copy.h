#ifndef GNEISS_COPY_H
#define GNEISS_COPY_H
#pragma once
/** @file */

#include "gneiss/fields-2d/ColumnsView.h"
#include "gneiss/fields-2d/RangeView.h"
#include "gneiss/fields-2d/ZipView.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/utils/parallelize.h"
#include "gneiss/utils/ranges.h"

#if GNEISS_PARALLELIZATION == GNEISS_PARALLELIZATION_TBB
#include "oneapi/tbb.h"
#endif

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

template <field_2d SourceField2d_, field_2d SinkField2d_, typename Parallelize_ = do_not_parallelize>
  requires std::convertible_to<typename SourceField2d_::scalar_type,
                               std::remove_reference_t<typename SinkField2d_::scalar_type>>
constexpr void copy(const SourceField2d_& sourceField, SinkField2d_& sinkField,
                    Parallelize_ parallelize = Parallelize_()) {
  const auto zippedFields = gneiss::fields_2d::view::Zip(sourceField, sinkField);

  if consteval {
    for (const auto sourceSinkPair : zippedFields | gneiss::fields_2d::view::Range) {
      const auto& [valSource, refSink] = sourceSinkPair;
      refSink = valSource;
    }
  } else {
    if constexpr (parallelize) {
#if GNEISS_PARALLELIZATION == GNEISS_PARALLELIZATION_TBB
      const auto columns = zippedFields | gneiss::fields_2d::view::Columns;
      tbb::parallel_for(tbb::blocked_range<std::size_t>(0, columns.size()),
                        [&columns](const tbb::blocked_range<std::size_t>& columnsIdxs) {
                          for (std::size_t columnIdx = columnsIdxs.begin(); columnIdx != columnsIdxs.end();
                               ++columnIdx) {
                            for (const auto sourceSinkPair : columns[columnIdx]) {
                              const auto& [valSource, refSink] = sourceSinkPair;
                              refSink = valSource;
                            }
                          }
                        });
#else
      static_assert(!parallelize, "parallelization type not supported");
#endif
    } else {
      for (const auto sourceSinkPair : zippedFields | gneiss::fields_2d::view::Range) {
        const auto& [valSource, refSink] = sourceSinkPair;
        refSink = valSource;
      }
    }
  }
}

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d

#endif  // GNEISS_COPY_H
