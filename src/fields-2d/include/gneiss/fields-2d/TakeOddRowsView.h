#ifndef GNEISS_TAKEODDROWSVIEW_H
#define GNEISS_TAKEODDROWSVIEW_H
#pragma once
/** @file */

#include "gneiss/fields-2d/FieldCoordinate.h"
#include "gneiss/fields-2d/ViewBase.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/fields-2d/makeView.h"
#include "gneiss/fields-2d/makeViewClosure.h"

#include <utility>

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

template <field_2d InField2d_>
struct TakeOddRowsView : public ViewBase {
  using scalar_type = typename InField2d_::scalar_type;
  constexpr static std::size_t width = InField2d_::width;
  constexpr static std::size_t height = InField2d_::height / 2;

  TakeOddRowsView() = default;
  constexpr TakeOddRowsView(TakeOddRowsView const& rhs) noexcept = default;
  constexpr TakeOddRowsView(TakeOddRowsView&& rhs) noexcept = default;
  constexpr TakeOddRowsView& operator=(TakeOddRowsView const& rhs) noexcept = default;
  constexpr TakeOddRowsView& operator=(TakeOddRowsView&& rhs) noexcept = default;
  ~TakeOddRowsView() = default;

  explicit constexpr TakeOddRowsView(InField2d_ inField2d) : m_inField2d(std::move(inField2d)) {}

  [[nodiscard]] constexpr scalar_type operator()(const FieldCoordinate<std::size_t>& coordinate) const {
    return m_inField2d({coordinate.x, 2 * coordinate.y + 1});
  }
  [[nodiscard]] constexpr scalar_type operator()(const std::size_t xIdx, const std::size_t yIdx) const {
    return operator()(FieldCoordinate<std::size_t>{xIdx, yIdx});
  }

 private:
  InField2d_ m_inField2d;
};

struct TakeOddRowsFunctionBase {
  template <typename InField2d_>
    requires field_2d<std::remove_reference_t<InField2d_>>
  constexpr auto operator()(InField2d_&& inField2d) const {
    return TakeOddRowsView<std::remove_cvref_t<view::makeView_t<InField2d_>>>(
        view::makeView(static_cast<InField2d_&&>(inField2d)));
  }
};

/** @} */  // end of fields-2d

namespace view {
inline constexpr auto TakeOddRows = makeViewClosure(TakeOddRowsFunctionBase{});
}  // namespace view

}  // namespace gneiss::fields_2d

#endif  // GNEISS_TAKEODDROWSVIEW_H
