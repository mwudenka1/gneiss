#ifndef GNEISS_ROWSVIEW_H
#define GNEISS_ROWSVIEW_H
#pragma once
/** @file */

#include "gneiss/fields-2d/RowView.h"
#include "gneiss/fields-2d/ViewBase.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/fields-2d/makeView.h"
#include "gneiss/fields-2d/makeViewClosure.h"
#include "gneiss/utils/ranges.h"

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

namespace internal {
class EmptyForRowsView {};
}  // namespace internal

template <field_2d InField2d_>
struct RowsView : public gneiss::ranges::view_interface<RowsView<InField2d_>>,
                  public std::conditional_t<std_ranges_used, ::ranges::view_interface<RowsView<InField2d_>>,
                                            internal::EmptyForRowsView>,
                  public ViewBase {
  RowsView() = default;
  constexpr RowsView(RowsView const& rhs) noexcept = default;
  constexpr RowsView(RowsView&& rhs) noexcept = default;
  constexpr RowsView& operator=(RowsView const& rhs) noexcept = default;
  constexpr RowsView& operator=(RowsView&& rhs) noexcept {
    m_inField2d = std::move(rhs.m_inField2d);
    return *this;
  }
  ~RowsView() = default;

  explicit constexpr RowsView(InField2d_ inField2d) : m_inField2d(std::move(inField2d)) {}

  [[nodiscard]] constexpr std::size_t size() const noexcept { return InField2d_::height; }

  template <typename IteratorRowsView_>
    requires std::same_as<std::remove_cv_t<IteratorRowsView_>, RowsView>
  struct iterator_type {
    using iterator_category = std::random_access_iterator_tag;
    using difference_type = int64_t;
    using value_type = RowView<InField2d_>;
    using reference_type = value_type&;

    iterator_type() = default;
    constexpr iterator_type(iterator_type const& rhs) noexcept = default;
    constexpr iterator_type(iterator_type&& rhs) noexcept = default;
    constexpr iterator_type& operator=(iterator_type const& rhs) noexcept = default;
    constexpr iterator_type& operator=(iterator_type&& rhs) noexcept = default;

    constexpr iterator_type(IteratorRowsView_* rowsView, const std::size_t yIdx) : m_rowsView(rowsView), m_yIdx(yIdx) {}

    [[maybe_unused]] constexpr iterator_type& operator++() {
      ++m_yIdx;
      return *this;
    }

    // NOLINTNEXTLINE(cert-dcl21-cpp)
    [[maybe_unused]] constexpr iterator_type operator++(int) & {
      const auto tmp = *this;
      ++(*this);
      return tmp;
    }

    [[maybe_unused]] constexpr iterator_type& operator--() {
      --m_yIdx;
      return *this;
    }

    // NOLINTNEXTLINE(cert-dcl21-cpp)
    [[maybe_unused]] constexpr iterator_type operator--(int) & {
      const auto tmp = *this;
      --(*this);
      return tmp;
    }

    [[maybe_unused]] constexpr iterator_type& operator+=(const difference_type diff) {
      const auto tmp = static_cast<difference_type>(m_yIdx) + diff;
      m_yIdx = static_cast<std::size_t>(tmp);
      return *this;
    }

    [[maybe_unused]] constexpr iterator_type& operator-=(const difference_type diff) {
      const auto tmp = static_cast<difference_type>(m_yIdx) - diff;
      m_yIdx = static_cast<std::size_t>(tmp);
      return *this;
    }

    [[nodiscard]] constexpr iterator_type operator+(const difference_type diff) const {
      return iterator_type(m_rowsView, static_cast<std::size_t>(static_cast<difference_type>(m_yIdx) + diff));
    }

    [[nodiscard]] friend constexpr iterator_type operator+(const difference_type diff,
                                                           const iterator_type& iter) noexcept {
      return iter + diff;
    }

    [[nodiscard]] constexpr iterator_type operator-(const difference_type diff) const {
      return iterator_type(m_rowsView, static_cast<std::size_t>(static_cast<difference_type>(m_yIdx) - diff));
    }

    [[nodiscard]] constexpr difference_type operator-(const iterator_type& other) const {
      return static_cast<difference_type>(m_yIdx) - static_cast<difference_type>(other.m_yIdx);
    }

    [[nodiscard]] auto operator<=>(const iterator_type& other) const { return m_yIdx <=> other.m_yIdx; };

    [[nodiscard]] constexpr value_type operator*() const {
      GNEISS_ASSERT(m_rowsView);
      GNEISS_ASSERT(m_yIdx < InField2d_::height);
      return value_type(m_yIdx, m_rowsView->m_inField2d);
    }

    [[nodiscard]] constexpr value_type operator[](std::size_t idx) const {
      GNEISS_ASSERT(m_rowsView);
      GNEISS_ASSERT(m_yIdx + idx < InField2d_::height);
      return value_type(m_yIdx + idx, m_rowsView->m_inField2d);
    }

    friend constexpr bool operator==(const iterator_type& leftIter, const iterator_type& rightIter) {
      GNEISS_ASSERT(leftIter.m_rowsView == rightIter.m_rowsView);
      return leftIter.m_rowsView == rightIter.m_rowsView && leftIter.m_yIdx == rightIter.m_yIdx;
    }
    friend constexpr bool operator!=(const iterator_type& leftIter, const iterator_type& rightIter) {
      GNEISS_ASSERT(leftIter.m_rowsView == rightIter.m_rowsView);
      return leftIter.m_rowsView != rightIter.m_rowsView || leftIter.m_yIdx != rightIter.m_yIdx;
    }

   private:
    IteratorRowsView_* m_rowsView;
    std::size_t m_yIdx = 0;
  };

  using const_iterator = iterator_type<const RowsView>;
  using iterator = std::conditional_t<std::is_const_v<InField2d_>, const_iterator, iterator_type<RowsView>>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;
  using reverse_iterator = std::reverse_iterator<iterator>;

  [[nodiscard]] constexpr auto begin() noexcept { return iterator(this, 0); }
  [[nodiscard]] constexpr auto end() noexcept { return iterator(this, InField2d_::height); }
  [[nodiscard]] constexpr auto begin() const noexcept { return const_iterator(this, 0); }
  [[nodiscard]] constexpr auto end() const noexcept { return const_iterator(this, InField2d_::height); }
  [[nodiscard]] constexpr auto cbegin() const noexcept { return const_iterator(this, 0); }
  [[nodiscard]] constexpr auto cend() const noexcept { return const_iterator(this, InField2d_::height); }
  [[nodiscard]] constexpr auto rbegin() noexcept { return reverse_iterator(end()); }
  [[nodiscard]] constexpr auto rend() noexcept { return reverse_iterator(begin()); }
  [[nodiscard]] constexpr auto rbegin() const noexcept { return const_reverse_iterator(end()); }
  [[nodiscard]] constexpr auto rend() const noexcept { return const_reverse_iterator(begin()); }
  [[nodiscard]] constexpr auto crbegin() const noexcept { return const_reverse_iterator(cend()); }
  [[nodiscard]] constexpr auto crend() const noexcept { return const_reverse_iterator(cbegin()); }

  [[nodiscard]] constexpr auto operator[](std::size_t yIdx) const noexcept {
    GNEISS_ASSERT(yIdx < InField2d_::height);
    return RowView<InField2d_>(yIdx, m_inField2d);
  }

 private:
  InField2d_ m_inField2d;
};

struct RowsFunctionBase {
  template <typename InField2d_>
    requires field_2d<std::remove_reference_t<InField2d_>>
  constexpr auto operator()(InField2d_&& inField2d) const {
    return RowsView<std::remove_cvref_t<view::makeView_t<InField2d_>>>(
        view::makeView(static_cast<InField2d_&&>(inField2d)));
  }
};

namespace view {
inline constexpr auto Rows = makeViewClosure(RowsFunctionBase{});
}  // namespace view

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d

#endif  // GNEISS_ROWSVIEW_H
