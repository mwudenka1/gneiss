#ifndef GNEISS_TRANSFORMVIEW_H
#define GNEISS_TRANSFORMVIEW_H
#pragma once
/** @file */

#include "gneiss/fields-2d/FieldCoordinate.h"
#include "gneiss/fields-2d/ViewBase.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/fields-2d/makeView.h"
#include "gneiss/fields-2d/makeViewClosure.h"

#include <range/v3/utility/semiregular_box.hpp>

#include <functional>
#include <utility>

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

template <field_2d InField2d_, typename TransformCallable_>
struct TransformView : public ViewBase {
  using scalar_type =
      std::invoke_result_t<TransformCallable_, typename std::remove_reference_t<InField2d_>::scalar_type>;
  constexpr static std::size_t width = InField2d_::width;
  constexpr static std::size_t height = InField2d_::height;

  TransformView() = default;
  constexpr TransformView(TransformView const& rhs) noexcept = default;
  constexpr TransformView(TransformView&& rhs) noexcept = default;
  constexpr TransformView& operator=(TransformView const& rhs) noexcept = default;
  constexpr TransformView& operator=(TransformView&& rhs) noexcept = default;
  ~TransformView() = default;

  explicit constexpr TransformView(InField2d_ inField2d, TransformCallable_ transfomCallable)
      : m_inField2d(std::move(inField2d)), m_transfomCallable(std::move(transfomCallable)) {}

  [[nodiscard]] constexpr inline scalar_type operator()(const FieldCoordinate<std::size_t>& coordinate) const {
    return m_transfomCallable(m_inField2d(coordinate));
  }
  [[nodiscard]] constexpr inline scalar_type operator()(const std::size_t xIdx, const std::size_t yIdx) const {
    return operator()(FieldCoordinate<std::size_t>{xIdx, yIdx});
  }

 private:
  InField2d_ m_inField2d;
  ::ranges::semiregular_box_t<TransformCallable_> m_transfomCallable;
};

struct TransformFunctionBase {
  template <typename InField2d_, typename TransformCallable_>
    requires field_2d<std::remove_reference_t<InField2d_>>
  constexpr auto operator()(TransformCallable_ transformCallable, InField2d_&& inField2d) const noexcept {
    return TransformView<std::remove_cvref_t<view::makeView_t<InField2d_>>, TransformCallable_>{
        view::makeView(static_cast<InField2d_&&>(inField2d)), std::move(transformCallable)};
  }
};

struct TransformFunction : TransformFunctionBase {
  using TransformFunctionBase::operator();

  template <typename TransformCallable_>
  constexpr auto operator()(TransformCallable_ transformCallable) const noexcept {
    return makeViewClosure(std::bind_front(TransformFunctionBase{}, std::move(transformCallable)));
  }
};

namespace view {
inline constexpr TransformFunction Transform{};
}  // namespace view

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d

#endif  // GNEISS_TRANSFORMVIEW_H
