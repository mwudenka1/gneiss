#ifndef GNEISS_HORIZONTALREFLECTPADDINGVIEW_H
#define GNEISS_HORIZONTALREFLECTPADDINGVIEW_H
#pragma once
/** @file */

#include "gneiss/fields-2d/FieldCoordinate.h"
#include "gneiss/fields-2d/ViewBase.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/fields-2d/makeView.h"
#include "gneiss/fields-2d/makeViewClosure.h"
#include "gneiss/utils/abs.h"

#include <cmath>
#include <type_traits>

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

template <std::size_t padding_, field_2d InField2d_>
struct HorizontalReflectPaddingView : public ViewBase {
  using scalar_type = typename InField2d_::scalar_type;
  constexpr static std::size_t width = InField2d_::width + 2 * padding_;
  constexpr static std::size_t height = InField2d_::height;

  HorizontalReflectPaddingView() = default;
  constexpr HorizontalReflectPaddingView(HorizontalReflectPaddingView const& rhs) noexcept = default;
  constexpr HorizontalReflectPaddingView(HorizontalReflectPaddingView&& rhs) noexcept = default;
  constexpr HorizontalReflectPaddingView& operator=(HorizontalReflectPaddingView const& rhs) noexcept = default;
  constexpr HorizontalReflectPaddingView& operator=(HorizontalReflectPaddingView&& rhs) noexcept = default;
  ~HorizontalReflectPaddingView() = default;

  explicit constexpr HorizontalReflectPaddingView(InField2d_ inField2d) : m_inField2d(std::move(inField2d)) {}

  [[nodiscard]] constexpr inline scalar_type operator()(const FieldCoordinate<std::size_t>& coordinate) const {
    const std::size_t xIdxReflected = reflect(static_cast<int>(coordinate.x) - static_cast<int>(padding_));

    return m_inField2d({xIdxReflected, coordinate.y});
  }
  [[nodiscard]] constexpr inline scalar_type operator()(const std::size_t xIdx, const std::size_t yIdx) const {
    return operator()(FieldCoordinate<std::size_t>{xIdx, yIdx});
  }

 private:
  constexpr static auto reflect(const int value) {
    return value <= 0 ? static_cast<std::size_t>(gneiss::abs(value))
                      : InField2d_::width - 1 -
                            static_cast<std::size_t>(gneiss::abs(static_cast<int>(InField2d_::width) - 1 - value));
  };

  InField2d_ m_inField2d;
};

template <std::size_t padding_>
struct HorizontalReflectPaddingFunctionBase {
  template <typename InField2d_>
    requires field_2d<std::remove_reference_t<InField2d_>>
  constexpr auto operator()(InField2d_&& inField2d) const {
    return HorizontalReflectPaddingView<padding_, std::remove_cvref_t<view::makeView_t<InField2d_>>>(
        view::makeView(static_cast<InField2d_&&>(inField2d)));
  }
};

namespace view {
template <std::size_t padding_>
inline constexpr auto HorizontalReflectPadding = makeViewClosure(HorizontalReflectPaddingFunctionBase<padding_>{});
}  // namespace view

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d

#endif  // GNEISS_HORIZONTALREFLECTPADDINGVIEW_H
