#ifndef GNEISS_HORIZONTALCONVOLUTIONVIEW_H
#define GNEISS_HORIZONTALCONVOLUTIONVIEW_H
#pragma once
/** @file */

#include "gneiss/fields-2d/FieldCoordinate.h"
#include "gneiss/fields-2d/ViewBase.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/fields-2d/makeView.h"
#include "gneiss/fields-2d/makeViewClosure.h"

#include <array>
#include <cstdint>
#include <type_traits>

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

template <std::size_t halfKernelSize_, std::array<int32_t, 2 * halfKernelSize_ + 1> kernel_, field_2d InField2d_>
struct HorizontalConvolutionView : public ViewBase {
  using scalar_type = int32_t;
  constexpr static std::size_t width = InField2d_::width - 2 * halfKernelSize_;
  constexpr static std::size_t height = InField2d_::height;

  HorizontalConvolutionView() = default;
  constexpr HorizontalConvolutionView(HorizontalConvolutionView const& rhs) noexcept = default;
  constexpr HorizontalConvolutionView(HorizontalConvolutionView&& rhs) noexcept = default;
  constexpr HorizontalConvolutionView& operator=(HorizontalConvolutionView const& rhs) noexcept = default;
  constexpr HorizontalConvolutionView& operator=(HorizontalConvolutionView&& rhs) noexcept = default;
  ~HorizontalConvolutionView() = default;

  explicit constexpr HorizontalConvolutionView(InField2d_ inField2d) : m_inField2d(std::move(inField2d)) {}

  [[nodiscard]] constexpr inline scalar_type operator()(const FieldCoordinate<std::size_t>& coordinate) const {
    return kernel_.at(0) * m_inField2d(coordinate) + kernel_.at(1) * m_inField2d({coordinate.x + 1, coordinate.y}) +
           kernel_.at(2) * m_inField2d({coordinate.x + 2, coordinate.y}) +
           kernel_.at(3) * m_inField2d({coordinate.x + 3, coordinate.y}) +
           kernel_.at(4) * m_inField2d({coordinate.x + 4, coordinate.y});
  }
  [[nodiscard]] constexpr inline scalar_type operator()(const std::size_t xIdx, const std::size_t yIdx) const {
    return operator()(FieldCoordinate<std::size_t>{xIdx, yIdx});
  }

 private:
  InField2d_ m_inField2d;
};

template <std::size_t halfKernelSize_, std::array<int32_t, 2 * halfKernelSize_ + 1> kernel_>
struct HorizontalConvolutionFunctionBase {
  template <typename InField2d_>
    requires field_2d<std::remove_reference_t<view::makeView_t<InField2d_>>>
  constexpr auto operator()(InField2d_&& inField2d) const {
    return HorizontalConvolutionView<halfKernelSize_, kernel_, std::remove_cvref_t<InField2d_>>(
        view::makeView(static_cast<InField2d_&&>(inField2d)));
  }
};

namespace view {
template <std::size_t halfKernelSize_, std::array<int32_t, 2 * halfKernelSize_ + 1> kernel_>
inline constexpr auto HorizontalConvolution =
    makeViewClosure(HorizontalConvolutionFunctionBase<halfKernelSize_, kernel_>{});
}  // namespace view

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d

#endif  // GNEISS_HORIZONTALCONVOLUTIONVIEW_H
