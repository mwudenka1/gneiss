#ifndef GNEISS_COLUMNVIEW_H
#define GNEISS_COLUMNVIEW_H
#pragma once

/** @file */

#include "gneiss/fields-2d/ViewBase.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/fields-2d/makeView.h"
#include "gneiss/fields-2d/makeViewClosure.h"
#include "gneiss/utils/assert.h"
#include "gneiss/utils/ranges.h"

#include <functional>
#include <utility>

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

template <field_2d InField2d_>
struct ColumnView : public gneiss::ranges::view_interface<ColumnView<InField2d_>>, public ViewBase {
  using scalar_type = typename InField2d_::scalar_type;
  constexpr static std::size_t width = 1;
  constexpr static std::size_t height = InField2d_::height;

  ColumnView() = default;
  constexpr ColumnView(ColumnView const& rhs) noexcept = default;
  constexpr ColumnView(ColumnView&& rhs) noexcept = default;
  constexpr ColumnView& operator=(ColumnView const& rhs) noexcept {
    if (rhs != this) {
      m_inField2d = rhs.m_imageIterator;
      m_xIdx = rhs.m_xIdx;
    }
    return *this;
  };
  constexpr ColumnView& operator=(ColumnView&& rhs) noexcept {
    if (rhs != this) {
      m_inField2d = std::move(rhs.m_imageIterator);
      m_xIdx = std::move(rhs.m_xIdx);
    }
    return *this;
  };
  ~ColumnView() = default;

  constexpr ColumnView(std::size_t xIdx, InField2d_ inField2d) : m_inField2d(std::move(inField2d)), m_xIdx(xIdx) {}

  [[nodiscard]] constexpr std::size_t size() const noexcept { return height; }

  constexpr scalar_type operator()(const std::size_t yIdx) const { return m_inField2d({m_xIdx, yIdx}); }

  template <typename IteratorColumnView_>
    requires std::same_as<std::remove_cv_t<IteratorColumnView_>, ColumnView>
  struct iterator_type {
    using iterator_category = std::random_access_iterator_tag;
    using difference_type = int64_t;
    using value_type = std::remove_cvref_t<typename InField2d_::scalar_type>;
    using reference_type = std::add_lvalue_reference_t<value_type>;

    iterator_type() = default;
    constexpr iterator_type(iterator_type const& rhs) noexcept = default;
    constexpr iterator_type(iterator_type&& rhs) noexcept = default;
    constexpr iterator_type& operator=(iterator_type const& rhs) noexcept = default;
    constexpr iterator_type& operator=(iterator_type&& rhs) noexcept = default;

    constexpr iterator_type(IteratorColumnView_* columnView, std::size_t yIdx)
        : m_columnView(columnView), m_yIdx(yIdx) {}

    [[maybe_unused]] constexpr iterator_type& operator++() {
      ++m_yIdx;
      return *this;
    }

    // NOLINTNEXTLINE(cert-dcl21-cpp)
    [[maybe_unused]] constexpr iterator_type operator++(int) & {
      const auto tmp = *this;
      ++(*this);
      return tmp;
    }

    [[maybe_unused]] constexpr iterator_type& operator--() {
      --m_yIdx;
      return *this;
    }

    // NOLINTNEXTLINE(cert-dcl21-cpp)
    [[maybe_unused]] constexpr iterator_type operator--(int) & {
      const auto tmp = *this;
      --(*this);
      return tmp;
    }

    [[maybe_unused]] constexpr iterator_type& operator+=(const difference_type diff) {
      const auto tmp = static_cast<difference_type>(m_yIdx) + diff;
      m_yIdx = static_cast<std::size_t>(tmp);
      return *this;
    }

    [[maybe_unused]] constexpr iterator_type& operator-=(const difference_type diff) {
      const auto tmp = static_cast<difference_type>(m_yIdx) - diff;
      m_yIdx = static_cast<std::size_t>(tmp);
      return *this;
    }

    [[nodiscard]] constexpr iterator_type operator+(const difference_type diff) const {
      return iterator_type(m_columnView, static_cast<std::size_t>(static_cast<difference_type>(m_yIdx) + diff));
    }

    [[nodiscard]] friend constexpr iterator_type operator+(const difference_type diff,
                                                           const iterator_type& iter) noexcept {
      return iter + diff;
    }

    [[nodiscard]] constexpr iterator_type operator-(const difference_type diff) const {
      return iterator_type(m_columnView, static_cast<std::size_t>(static_cast<difference_type>(m_yIdx) - diff));
    }

    [[nodiscard]] constexpr difference_type operator-(const iterator_type& other) const {
      return static_cast<difference_type>(m_yIdx) - static_cast<difference_type>(other.m_xIdx);
    }

    [[nodiscard]] auto operator<=>(const iterator_type& other) const { return m_yIdx <=> other.m_yIdx; };

    [[nodiscard]] constexpr typename InField2d_::scalar_type operator*() const {
      GNEISS_ASSERT(m_columnView);
      GNEISS_ASSERT(m_yIdx < InField2d_::height);
      return (*m_columnView)(m_yIdx);
    }

    [[nodiscard]] constexpr typename InField2d_::scalar_type operator[](std::size_t idx) const {
      GNEISS_ASSERT(m_columnView);
      GNEISS_ASSERT(m_yIdx + idx < InField2d_::height);
      return (*m_columnView)(m_yIdx + idx);
    }

    friend constexpr bool operator==(const iterator_type& leftIter, const iterator_type& rightIter) {
      GNEISS_ASSERT(leftIter.m_columnView->getXIdx() == rightIter.m_columnView->getXIdx());
      return leftIter.m_columnView->getXIdx() == rightIter.m_columnView->getXIdx() &&
             leftIter.m_yIdx == rightIter.m_yIdx;
    }
    friend constexpr bool operator!=(const iterator_type& leftIter, const iterator_type& rightIter) {
      GNEISS_ASSERT(leftIter.m_columnView->getXIdx() == rightIter.m_columnView->getXIdx());
      return leftIter.m_columnView->getXIdx() != rightIter.m_columnView->getXIdx() ||
             leftIter.m_yIdx != rightIter.m_yIdx;
    }

   private:
    IteratorColumnView_* m_columnView;
    std::size_t m_yIdx = 0;
  };

  using const_iterator = iterator_type<const ColumnView>;
  using iterator = iterator_type<ColumnView>;
  // using const_reverse_iterator = std::reverse_iterator<const_iterator>;
  // using reverse_iterator = std::reverse_iterator<iterator>;

  [[nodiscard]] constexpr auto begin() { return iterator(this, 0); }
  [[nodiscard]] constexpr auto end() { return iterator(this, height); }
  [[nodiscard]] constexpr auto begin() const { return const_iterator(this, 0); }
  [[nodiscard]] constexpr auto end() const { return const_iterator(this, height); }
  [[nodiscard]] constexpr auto cbegin() const { return const_iterator(this, 0); }
  [[nodiscard]] constexpr auto cend() const { return const_iterator(this, height); }
  // Does not work with libstdc++
  // [[nodiscard]] constexpr auto rbegin() { return reverse_iterator(end()); }
  // [[nodiscard]] constexpr auto rend() { return reverse_iterator(begin()); }
  // [[nodiscard]] constexpr auto rbegin() const { return const_reverse_iterator(end()); }
  // [[nodiscard]] constexpr auto rend() const { return const_reverse_iterator(begin()); }
  // [[nodiscard]] constexpr auto crbegin() const { return const_reverse_iterator(cend()); }
  // [[nodiscard]] constexpr auto crend() const { return const_reverse_iterator(cbegin()); }

  [[nodiscard]] constexpr const InField2d_& getField2d() const noexcept { return m_inField2d; }

  [[nodiscard]] constexpr std::size_t getXIdx() const noexcept { return m_xIdx; }

 private:
  InField2d_ m_inField2d;
  std::size_t m_xIdx{};
};

struct ColumnFunctionBase {
  template <typename InField2d_>
    requires field_2d<std::remove_reference_t<InField2d_>>
  constexpr auto operator()(std::size_t xIdx, InField2d_&& inField2d) const {
    return ColumnView<std::remove_cvref_t<view::makeView_t<InField2d_>>>(
        xIdx, view::makeView(static_cast<InField2d_&&>(inField2d)));
  }
};

struct ColumnFunction : ColumnFunctionBase {
  using ColumnFunctionBase::operator();

  constexpr auto operator()(std::size_t xIdx) const {
    return makeViewClosure(std::bind_front(ColumnFunctionBase{}, xIdx));
  }
};

namespace view {
inline constexpr ColumnFunction Column{};
}  // namespace view

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d

#endif  // GNEISS_COLUMNVIEW_H
