#ifndef GNEISS_BLOCKSVIEW_H
#define GNEISS_BLOCKSVIEW_H
#pragma once
/** @file */

#include "gneiss/fields-2d/BlockView.h"
#include "gneiss/fields-2d/FieldCoordinate.h"
#include "gneiss/fields-2d/ViewBase.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/fields-2d/makeView.h"
#include "gneiss/fields-2d/makeViewClosure.h"

#include <functional>
#include <utility>

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

template <field_2d InField2d_, std::size_t blockWidth_, std::size_t blockHeight_, std::size_t paddingX_ = 0,
          std::size_t paddingY_ = 0, std::size_t strideX_ = blockWidth_, std::size_t strideY_ = blockHeight_>
struct BlocksView : public ViewBase {
  static_assert(blockWidth_ + 2 * paddingX_ <= InField2d_::width,
                "Block width + padding must be smaller or equal than field width");
  static_assert(blockHeight_ + 2 * paddingY_ <= InField2d_::height,
                "Block height + padding must be smaller or equal than field height");
  static_assert(strideX_ > 0, "Stride must not be 0");
  static_assert(strideY_ > 0, "Stride must not be 0");

  using scalar_type = BlockView<InField2d_, blockWidth_, blockHeight_>;
  constexpr static std::size_t width = (InField2d_::width - 2 * paddingX_ - blockWidth_) / strideX_ + 1;
  constexpr static std::size_t height = (InField2d_::height - 2 * paddingY_ - blockHeight_) / strideY_ + 1;

  BlocksView() = default;
  constexpr BlocksView(BlocksView const& rhs) noexcept = default;
  constexpr BlocksView(BlocksView&& rhs) noexcept = default;
  constexpr BlocksView& operator=(BlocksView const& rhs) noexcept = default;
  constexpr BlocksView& operator=(BlocksView&& rhs) noexcept = default;
  ~BlocksView() = default;

  explicit constexpr BlocksView(InField2d_ inField2d) : m_inField2d(std::move(inField2d)) {}

  [[nodiscard]] inline constexpr scalar_type operator()(const FieldCoordinate<std::size_t>& coordinate) const noexcept {
    return scalar_type(
        FieldCoordinate<std::size_t>{coordinate.x * strideX_ + paddingX_, coordinate.y * strideY_ + paddingY_},
        m_inField2d);
  }
  [[nodiscard]] inline constexpr scalar_type operator()(const std::size_t xIdx, const std::size_t yIdx) const noexcept {
    return operator()(FieldCoordinate<std::size_t>{xIdx, yIdx});
  }

 private:
  InField2d_ m_inField2d;
};

template <std::size_t blockWidth_, std::size_t blockHeight_, std::size_t paddingX_, std::size_t paddingY_,
          std::size_t strideX_, std::size_t strideY_>
struct BlocksFunctionBase {
  template <typename InField2d_>
    requires field_2d<std::remove_reference_t<InField2d_>>
  constexpr auto operator()(InField2d_&& inField2d) const noexcept {
    return BlocksView<std::remove_cvref_t<view::makeView_t<InField2d_>>, blockWidth_, blockHeight_, paddingX_,
                      paddingY_, strideX_, strideY_>(view::makeView(static_cast<InField2d_&&>(inField2d)));
  }
};

namespace view {
template <std::size_t blockWidth_, std::size_t blockHeight_, std::size_t paddingX_ = 0, std::size_t paddingY_ = 0,
          std::size_t strideX_ = blockWidth_, std::size_t strideY_ = blockHeight_>
inline constexpr auto Blocks =
    makeViewClosure(BlocksFunctionBase<blockWidth_, blockHeight_, paddingX_, paddingY_, strideX_, strideY_>{});
}  // namespace view

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d

#endif  // GNEISS_BLOCKSVIEW_H
