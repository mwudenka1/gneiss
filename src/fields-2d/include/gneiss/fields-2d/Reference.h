#ifndef GNEISS_REFERENCE_H
#define GNEISS_REFERENCE_H
#pragma once
/** @file */

#include "gneiss/fields-2d/FieldCoordinate.h"
#include "gneiss/fields-2d/concepts.h"

#include <memory>

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

template <field_2d WrappedField2d_>
struct Reference : public ViewBase {
  using scalar_type = std::conditional_t<std::is_const_v<WrappedField2d_>,
                                         add_const_if_reference_t<typename WrappedField2d_::scalar_type>,
                                         typename WrappedField2d_::scalar_type>;
  constexpr static std::size_t width = WrappedField2d_::width;
  constexpr static std::size_t height = WrappedField2d_::height;

  Reference() = default;
  constexpr Reference(Reference const& rhs) noexcept = default;
  constexpr Reference(Reference&& rhs) noexcept = default;
  constexpr Reference& operator=(Reference const& rhs) noexcept = default;
  constexpr Reference& operator=(Reference&& rhs) noexcept = default;
  ~Reference() = default;

  explicit constexpr Reference(WrappedField2d_& inField2d) : m_wrappedField2d(std::addressof(inField2d)) {}
  explicit constexpr Reference(WrappedField2d_ const&& inField2d) = delete;

  [[nodiscard]] constexpr inline scalar_type operator()(const FieldCoordinate<std::size_t>& coordinate) const {
    return (*m_wrappedField2d)(coordinate);
  }
  [[nodiscard]] constexpr inline scalar_type operator()(const std::size_t xIdx, const std::size_t yIdx) const {
    return operator()(FieldCoordinate<std::size_t>{xIdx, yIdx});
  }

 private:
  WrappedField2d_* m_wrappedField2d;
};

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d
#endif  // GNEISS_REFERENCE_H
