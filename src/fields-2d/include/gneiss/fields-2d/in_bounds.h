#ifndef GNEISS_IN_BOUNDS_H
#define GNEISS_IN_BOUNDS_H
#pragma once

/** @file */

#include "gneiss/fields-2d/FieldCoordinate.h"
#include "gneiss/fields-2d/concepts.h"

#include <cstddef>

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

/**
 *
 * @tparam Field_
 * @param coordinate
 * @return True if coordinate is within bounds of Field_, False otherwise
 */
template <field_2d Field_>
[[nodiscard]] constexpr bool in_bounds(FieldCoordinate<std::size_t> coordinate) {
  return coordinate.x < Field_::width && coordinate.y < Field_::height;
}

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d

#endif  // GNEISS_IN_BOUNDS_H
