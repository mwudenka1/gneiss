#ifndef GNEISS_FIELDCOORDINATE_H
#define GNEISS_FIELDCOORDINATE_H
#pragma once
/** @file */

#include <Eigen/Dense>
#include <cstddef>
#include <ostream>

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

template <typename Scalar_>
struct FieldCoordinate {
  // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
  Scalar_ x = 0, y = 0;

  FieldCoordinate() = default;
  constexpr FieldCoordinate(Scalar_ xIn, Scalar_ yIn) noexcept : x(xIn), y(yIn) {}
  // NOLINTNEXTLINE(google-explicit-constructor,hicpp-explicit-conversions)
  constexpr FieldCoordinate(Eigen::Matrix<Scalar_, 2, 1> vector) noexcept : x(vector.x()), y(vector.y()) {}

  template <typename VectorScalar_>
  [[nodiscard]] Eigen::Matrix<VectorScalar_, 2, 1> as_vector() const {
    return Eigen::Matrix<VectorScalar_, 2, 1>(x, y);
  }

  constexpr inline FieldCoordinate& operator+=(const FieldCoordinate& other) {
    x += other.x;
    y += other.y;

    return *this;
  }

  [[nodiscard]] constexpr inline FieldCoordinate operator+(const FieldCoordinate& other) const {
    FieldCoordinate copy = *this;
    copy += other;
    return copy;
  }

  constexpr inline FieldCoordinate& operator-=(const FieldCoordinate& other) {
    x -= other.x;
    y -= other.y;

    return *this;
  }

  [[nodiscard]] constexpr inline FieldCoordinate operator-(const FieldCoordinate& other) const {
    FieldCoordinate copy = *this;
    copy -= other;
    return copy;
  }

  [[nodiscard]] constexpr inline FieldCoordinate operator-() const {
    const FieldCoordinate inverted{-x, -y};
    return inverted;
  }

  constexpr inline FieldCoordinate& operator*=(const Scalar_& scalar) {
    x *= scalar;
    y *= scalar;

    return *this;
  }

  [[nodiscard]] constexpr inline FieldCoordinate operator*(const Scalar_& scalar) const {
    FieldCoordinate copy = *this;
    copy *= scalar;
    return copy;
  }

  template <typename OtherScalar_>
  [[nodiscard]] explicit constexpr inline operator FieldCoordinate<OtherScalar_>() const {
    return {static_cast<OtherScalar_>(x), static_cast<OtherScalar_>(y)};
  }

  constexpr friend std::ostream& operator<<(std::ostream& outStream, const FieldCoordinate& self) {
    outStream << "(" << self.x << ", " << self.y << ")";
    return outStream;
  }

  auto operator<=>(const FieldCoordinate&) const = default;
};

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d

#endif  // GNEISS_FIELDCOORDINATE_H
