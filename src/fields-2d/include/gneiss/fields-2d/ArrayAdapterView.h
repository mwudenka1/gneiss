#ifndef GNEISS_ARRAYADAPTER_H
#define GNEISS_ARRAYADAPTER_H
#pragma once
/** @file */

#include "gneiss/fields-2d/FieldCoordinate.h"
#include "gneiss/fields-2d/ViewBase.h"
#include "gneiss/fields-2d/concepts.h"

#include <array>
#include <cstddef>
#include <tuple>

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

template <std::size_t width_, std::size_t height_, typename Array_>
  requires(width_* height_ <= std::tuple_size_v<Array_>)
struct ArrayAdapterView : public ViewBase {
  using scalar_type = std::add_lvalue_reference_t<std::conditional_t<
      std::is_const_v<Array_>, std::add_const_t<typename Array_::value_type>, typename Array_::value_type>>;

  constexpr static std::size_t width = width_;
  constexpr static std::size_t height = height_;

  ArrayAdapterView() = default;
  constexpr ArrayAdapterView(ArrayAdapterView const& rhs) noexcept = default;
  constexpr ArrayAdapterView(ArrayAdapterView&& rhs) noexcept = default;
  constexpr ArrayAdapterView& operator=(ArrayAdapterView const& rhs) noexcept = default;
  constexpr ArrayAdapterView& operator=(ArrayAdapterView&& rhs) noexcept = default;
  ~ArrayAdapterView() = default;

  explicit constexpr ArrayAdapterView(Array_* inArray) : m_array(inArray) {}
  explicit constexpr ArrayAdapterView(Array_& inArray) : m_array(&inArray) {}
  // explicit constexpr ArrayAdapterView(Array_t&&) = delete;

  [[nodiscard]] constexpr inline scalar_type operator()(const FieldCoordinate<std::size_t>& coordinate) const {
    return m_array->at(coordinate.y * width + coordinate.x);
  }
  [[nodiscard]] constexpr inline scalar_type operator()(const std::size_t xIdx, const std::size_t yIdx) const {
    return operator()(FieldCoordinate<std::size_t>{xIdx, yIdx});
  }

 private:
  Array_* m_array;
};

template <std::size_t width_, std::size_t height_>
struct ArrayAdapterFunction {
  template <typename Array_>
  constexpr auto operator()(Array_& inArray) const {
    return ArrayAdapterView<width_, height_, Array_>(inArray);
  }

  template <typename Array_>
  friend constexpr auto operator|(Array_& inArray, const ArrayAdapterFunction&) {
    return ArrayAdapterView<width_, height_, Array_>(inArray);
  }
};

namespace view {
template <std::size_t width_, std::size_t height_>
inline constexpr ArrayAdapterFunction<width_, height_> ArrayAdapter;
}  // namespace view

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d

#endif  // GNEISS_ARRAYADAPTER_H
