#ifndef GNEISS_COLUMNSVIEW_H
#define GNEISS_COLUMNSVIEW_H
#pragma once
/** @file */

#include "gneiss/fields-2d/ColumnView.h"
#include "gneiss/fields-2d/ViewBase.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/fields-2d/makeView.h"
#include "gneiss/fields-2d/makeViewClosure.h"
#include "gneiss/utils/ranges.h"

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */
namespace internal {
class EmptyForColumnsView {};
}  // namespace internal

template <field_2d InField2d_>
struct ColumnsView : public gneiss::ranges::view_interface<ColumnsView<InField2d_>>,
                     public std::conditional_t<std_ranges_used, ::ranges::view_interface<ColumnsView<InField2d_>>,
                                               internal::EmptyForColumnsView>,
                     public ViewBase {
  ColumnsView() = default;
  constexpr ColumnsView(ColumnsView const& rhs) noexcept = default;
  constexpr ColumnsView(ColumnsView&& rhs) noexcept = default;
  constexpr ColumnsView& operator=(ColumnsView const& rhs) noexcept = default;
  constexpr ColumnsView& operator=(ColumnsView&& rhs) noexcept {
    m_inField2d = std::move(rhs.m_inField2d);
    return *this;
  }
  ~ColumnsView() = default;

  explicit constexpr ColumnsView(InField2d_ inField2d) : m_inField2d(std::move(inField2d)) {}

  [[nodiscard]] constexpr std::size_t size() const noexcept { return InField2d_::width; }

  template <typename IteratorColumnsView_>
    requires std::same_as<std::remove_cv_t<IteratorColumnsView_>, ColumnsView>
  struct iterator_type {
    using iterator_category = std::random_access_iterator_tag;
    using difference_type = int64_t;
    using value_type = ColumnView<InField2d_>;

    iterator_type() = default;
    constexpr iterator_type(iterator_type const& rhs) noexcept = default;
    constexpr iterator_type(iterator_type&& rhs) noexcept = default;
    constexpr iterator_type& operator=(iterator_type const& rhs) noexcept = default;
    constexpr iterator_type& operator=(iterator_type&& rhs) noexcept = default;

    constexpr iterator_type(IteratorColumnsView_* columnsView, const std::size_t xIdx)
        : m_columnsView(columnsView), m_xIdx(xIdx) {}

    [[maybe_unused]] constexpr iterator_type& operator++() {
      ++m_xIdx;
      return *this;
    }

    // NOLINTNEXTLINE(cert-dcl21-cpp)
    [[maybe_unused]] constexpr iterator_type operator++(int) & {
      const auto tmp = *this;
      ++(*this);
      return tmp;
    }

    [[maybe_unused]] constexpr iterator_type& operator--() {
      --m_xIdx;
      return *this;
    }

    // NOLINTNEXTLINE(cert-dcl21-cpp)
    [[maybe_unused]] constexpr iterator_type operator--(int) & {
      const auto tmp = *this;
      --(*this);
      return tmp;
    }

    [[maybe_unused]] constexpr iterator_type& operator+=(const difference_type diff) {
      const auto tmp = static_cast<difference_type>(m_xIdx) + diff;
      m_xIdx = static_cast<std::size_t>(tmp);
      return *this;
    }

    [[maybe_unused]] constexpr iterator_type& operator-=(const difference_type diff) {
      const auto tmp = static_cast<difference_type>(m_xIdx) - diff;
      m_xIdx = static_cast<std::size_t>(tmp);
      return *this;
    }

    [[nodiscard]] constexpr iterator_type operator+(const difference_type diff) const {
      return iterator_type(m_columnsView, static_cast<std::size_t>(static_cast<difference_type>(m_xIdx) + diff));
    }

    [[nodiscard]] friend constexpr iterator_type operator+(const difference_type diff,
                                                           const iterator_type& iter) noexcept {
      return iter + diff;
    }

    [[nodiscard]] constexpr iterator_type operator-(const difference_type diff) const {
      return iterator_type(m_columnsView, static_cast<std::size_t>(static_cast<difference_type>(m_xIdx) - diff));
    }

    [[nodiscard]] constexpr difference_type operator-(const iterator_type& other) const {
      return static_cast<difference_type>(m_xIdx) - static_cast<difference_type>(other.m_xIdx);
    }

    [[nodiscard]] auto operator<=>(const iterator_type& other) const { return m_xIdx <=> other.m_xIdx; };

    [[nodiscard]] constexpr value_type operator*() const {
      GNEISS_ASSERT(m_columnsView);
      GNEISS_ASSERT(m_xIdx < InField2d_::width);
      return value_type(m_xIdx, m_columnsView->m_inField2d);
    }

    [[nodiscard]] constexpr value_type operator[](std::size_t idx) const {
      GNEISS_ASSERT(m_columnsView);
      GNEISS_ASSERT(m_xIdx + idx < InField2d_::width);
      return value_type(m_xIdx + idx, m_columnsView->m_inField2d);
    }

    friend constexpr bool operator==(const iterator_type& leftIter, const iterator_type& rightIter) {
      GNEISS_ASSERT(leftIter.m_columnsView == rightIter.m_columnsView);
      return leftIter.m_columnsView == rightIter.m_columnsView && leftIter.m_xIdx == rightIter.m_xIdx;
    }
    friend constexpr bool operator!=(const iterator_type& leftIter, const iterator_type& rightIter) {
      GNEISS_ASSERT(leftIter.m_columnsView == rightIter.m_columnsView);
      return leftIter.m_columnsView != rightIter.m_columnsView || leftIter.m_xIdx != rightIter.m_xIdx;
    }

   private:
    IteratorColumnsView_* m_columnsView;
    std::size_t m_xIdx = 0;
  };

  using const_iterator = iterator_type<const ColumnsView>;
  using iterator = std::conditional_t<std::is_const_v<InField2d_>, const_iterator, iterator_type<ColumnsView>>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;
  using reverse_iterator = std::reverse_iterator<iterator>;

  [[nodiscard]] constexpr auto begin() noexcept { return iterator(this, 0); }
  [[nodiscard]] constexpr auto end() noexcept { return iterator(this, InField2d_::width); }
  [[nodiscard]] constexpr auto begin() const noexcept { return const_iterator(this, 0); }
  [[nodiscard]] constexpr auto end() const noexcept { return const_iterator(this, InField2d_::width); }
  [[nodiscard]] constexpr auto cbegin() const noexcept { return const_iterator(this, 0); }
  [[nodiscard]] constexpr auto cend() const noexcept { return const_iterator(this, InField2d_::width); }
  [[nodiscard]] constexpr auto rbegin() noexcept { return reverse_iterator(end()); }
  [[nodiscard]] constexpr auto rend() noexcept { return reverse_iterator(begin()); }
  [[nodiscard]] constexpr auto rbegin() const noexcept { return const_reverse_iterator(end()); }
  [[nodiscard]] constexpr auto rend() const noexcept { return const_reverse_iterator(begin()); }
  [[nodiscard]] constexpr auto crbegin() const noexcept { return const_reverse_iterator(cend()); }
  [[nodiscard]] constexpr auto crend() const noexcept { return const_reverse_iterator(cbegin()); }

  [[nodiscard]] constexpr auto operator[](std::size_t xIdx) const noexcept {
    GNEISS_ASSERT(xIdx < InField2d_::width);
    return ColumnView<InField2d_>(xIdx, m_inField2d);
  }

 private:
  InField2d_ m_inField2d;
};

struct ColumnsFunctionBase {
  template <typename InField2d_>
    requires field_2d<std::remove_reference_t<InField2d_>>
  constexpr auto operator()(InField2d_&& inField2d) const {
    return ColumnsView<std::remove_cvref_t<view::makeView_t<InField2d_>>>(
        view::makeView(static_cast<InField2d_&&>(inField2d)));
  }
};

namespace view {
inline constexpr auto Columns = makeViewClosure(ColumnsFunctionBase{});
}  // namespace view

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d

#endif  // GNEISS_COLUMNSVIEW_H
