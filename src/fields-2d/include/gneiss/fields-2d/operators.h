#ifndef GNEISS_OPERATORS_H
#define GNEISS_OPERATORS_H
#pragma once

/** @file */

#include "gneiss/fields-2d/RowsView.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/utils/output.h"
#include "gneiss/utils/ranges.h"

#include <iostream>

/**
 * @addtogroup fields-2d
 *
 * @{
 */

template <gneiss::fields_2d::field_2d LeftField2d_, gneiss::fields_2d::field_2d RightField2d_>
constexpr bool operator==(const LeftField2d_& leftField, const RightField2d_& rightField) {
  // static_assert(std::is_same_v<std::remove_cvref_t<typename LeftField2d_::scalar_type>,
  //                              std::remove_cvref_t<typename RightField2d_::scalar_type>>,
  //               "Fields must have the same scalar type");
  static_assert(LeftField2d_::width == RightField2d_::width, "Fields must have the width");
  static_assert(LeftField2d_::height == RightField2d_::height, "Fields must have the height");
  bool result = true;

  // Sadly the following code is not yet (C++20) constexpr
  // for (const auto [leftRow, rightRow] : gneiss::ranges::views::zip(leftField | gneiss::fields_2d::view::Rows,
  //                                                                    rightField | gneiss::fields_2d::view::Rows)) {
  //   result &= gneiss::ranges::equal(leftRow, rightRow);
  // }

  // ugly but constexpr
  // const auto leftRows = leftField | gneiss::fields_2d::view::Rows;
  // const auto rightRows = rightField | gneiss::fields_2d::view::Rows;
  // auto leftRowsIter = leftRows.cbegin();
  // auto rightRowsIter = rightRows.cbegin();
  // for (; leftRowsIter != leftRows.cend() && rightRowsIter != rightRows.cend(); ++leftRowsIter, ++rightRowsIter) {
  //   auto leftRow = *leftRowsIter;
  //   auto rightRow = *rightRowsIter;
  //   auto leftRowIter = leftRow.cbegin();
  //   auto rightRowIter = rightRow.cbegin();
  //   for (; leftRowIter != leftRow.cend() && rightRowIter != rightRow.cend(); ++leftRowIter, ++rightRowIter) {
  //     result &= *leftRowIter == *rightRowIter;
  //   }
  // }
  for (std::size_t i = 0; i < LeftField2d_::height; ++i) {
    for (std::size_t j = 0; j < LeftField2d_::width; ++j) {
      result &= leftField(j, i) == rightField(j, i);
    }
  }

  return result;
}

template <gneiss::fields_2d::field_2d Field2d_>
std::ostream& operator<<(std::ostream& outStream, const Field2d_& field) {
  for (const auto row : field | gneiss::fields_2d::view::Rows) {
    for (const auto& cell : row) {
      outStream << cell << " ";
    }
    outStream << '\n';
  }

  return outStream;
}

/** @} */  // end of fields-2d

#endif  // GNEISS_OPERATORS_H
