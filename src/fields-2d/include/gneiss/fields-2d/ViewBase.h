#ifndef GNEISS_VIEWBASE_H
#define GNEISS_VIEWBASE_H
#pragma once
/** @file */

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

struct ViewBase {};

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d

#endif  // GNEISS_VIEWBASE_H
