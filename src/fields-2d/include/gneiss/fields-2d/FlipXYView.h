#ifndef GNEISS_FLIPXYVIEW_H
#define GNEISS_FLIPXYVIEW_H
#pragma once
/** @file */

#include "gneiss/fields-2d/FieldCoordinate.h"
#include "gneiss/fields-2d/ViewBase.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/fields-2d/makeView.h"
#include "gneiss/fields-2d/makeViewClosure.h"

#include <utility>

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

template <field_2d InField2d_>
struct FlipXYView : public ViewBase {
  using scalar_type = typename InField2d_::scalar_type;
  constexpr static std::size_t width = InField2d_::height;
  constexpr static std::size_t height = InField2d_::width;

  FlipXYView() = default;
  constexpr FlipXYView(FlipXYView const& rhs) noexcept = default;
  constexpr FlipXYView(FlipXYView&& rhs) noexcept = default;
  constexpr FlipXYView& operator=(FlipXYView const& rhs) noexcept = default;
  constexpr FlipXYView& operator=(FlipXYView&& rhs) noexcept = default;
  ~FlipXYView() = default;

  explicit constexpr FlipXYView(InField2d_ inField2d) : m_inField2d(std::move(inField2d)) {}

  [[nodiscard]] inline constexpr scalar_type operator()(const FieldCoordinate<std::size_t>& coordinate) const {
    // NOLINTNEXTLINE(readability-suspicious-call-argument)
    return m_inField2d({coordinate.y, coordinate.x});
  }
  [[nodiscard]] inline constexpr scalar_type operator()(const std::size_t xIdx, const std::size_t yIdx) const {
    return operator()(FieldCoordinate<std::size_t>{xIdx, yIdx});
  }

 private:
  InField2d_ m_inField2d;
};

struct FlipXYFunctionBase {
  template <typename InField2d_>
    requires field_2d<std::remove_reference_t<InField2d_>>
  constexpr auto operator()(InField2d_&& inField2d) const
      -> FlipXYView<std::remove_cvref_t<view::makeView_t<InField2d_>>> {
    return FlipXYView{view::makeView(static_cast<InField2d_&&>(inField2d))};
  }
};

namespace view {
inline constexpr auto FlipXY = makeViewClosure(FlipXYFunctionBase{});
}  // namespace view

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d

#endif  // GNEISS_FLIPXYVIEW_H
