#ifndef GNEISS_ZIPVIEW_H
#define GNEISS_ZIPVIEW_H
#pragma once
/** @file */

#include "gneiss/fields-2d/FieldCoordinate.h"
#include "gneiss/fields-2d/ViewBase.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/fields-2d/makeView.h"
#include "gneiss/fields-2d/makeViewClosure.h"

#include <algorithm>
#include <tuple>
#include <utility>

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

template <field_2d... InField2d_>
struct ZipView : public ViewBase {
  using scalar_type = std::tuple<typename InField2d_::scalar_type...>;
  constexpr static std::size_t width = std::min<std::size_t>({InField2d_::width...});
  constexpr static std::size_t height = std::min<std::size_t>({InField2d_::height...});

  ZipView() = default;
  constexpr ZipView(ZipView const& rhs) noexcept = default;
  constexpr ZipView(ZipView&& rhs) noexcept = default;
  constexpr ZipView& operator=(ZipView const& rhs) noexcept = default;
  constexpr ZipView& operator=(ZipView&& rhs) noexcept = default;
  ~ZipView() = default;

  explicit constexpr ZipView(InField2d_... inField2d) : m_inField2ds(std::make_tuple(inField2d...)) {}

  [[nodiscard]] inline constexpr scalar_type operator()(const FieldCoordinate<std::size_t>& coordinate) const {
    const auto evaluate_all_fields = [coordinate](InField2d_... fields) constexpr -> scalar_type {
      return scalar_type(fields(coordinate)...);
    };
    return std::apply(evaluate_all_fields, m_inField2ds);
  }
  [[nodiscard]] inline constexpr scalar_type operator()(const std::size_t xIdx, const std::size_t yIdx) const {
    return operator()(FieldCoordinate<std::size_t>{xIdx, yIdx});
  }

 private:
  std::tuple<InField2d_...> m_inField2ds;
};

struct ZipFunctionBase {
  template <typename... InField2d_>
    requires(field_2d<std::remove_reference_t<InField2d_>> && ...)
  constexpr auto operator()(InField2d_&&... inField2d) const
      -> ZipView<std::remove_cvref_t<view::makeView_t<InField2d_>>...> {
    return ZipView{view::makeView(static_cast<InField2d_&&>(inField2d))...};
  }
};

namespace view {
inline constexpr auto Zip = makeViewClosure(ZipFunctionBase{});
}  // namespace view

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d

#endif  // GNEISS_ZIPVIEW_H
