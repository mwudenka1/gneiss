#ifndef GNEISS_MAKEVIEWCLOSURE_H
#define GNEISS_MAKEVIEWCLOSURE_H
#pragma once
/** @file */

#include "gneiss/fields-2d/concepts.h"

namespace gneiss::fields_2d {

/**
 * @addtogroup fields-2d
 *
 * @{
 */

template <typename ViewFunction_>
struct ViewClosure;

struct ViewClosureBase {
  template <typename InField2d_, typename ViewFunction_>
    requires field_2d<std::remove_reference_t<InField2d_>>
  friend constexpr auto operator|(InField2d_ &&inField2d, ViewClosure<ViewFunction_> viewFunction) {
    return static_cast<ViewFunction_ &&>(viewFunction)(static_cast<InField2d_ &&>(inField2d));
  }
};

template <typename ViewFunction_>
struct ViewClosure : ViewClosureBase, ViewFunction_ {
  ViewClosure() = default;

  constexpr explicit ViewClosure(ViewFunction_ viewFunction)
      : ViewFunction_(static_cast<ViewFunction_ &&>(viewFunction)) {}
};

struct MakeViewClosureFunction {
  template <typename ViewFunction_>
  constexpr ViewClosure<ViewFunction_> operator()(ViewFunction_ function) const {
    return ViewClosure<ViewFunction_>{static_cast<ViewFunction_ &&>(function)};
  }
};

inline constexpr MakeViewClosureFunction makeViewClosure{};

/** @} */  // end of fields-2d

}  // namespace gneiss::fields_2d

#endif  // GNEISS_MAKEVIEWCLOSURE_H
