#include "gneiss/io/VkittiOpticalFlow.h"

#include <filesystem>
#include <utility>

gneiss::io::VkittiOpticalFlow::VkittiOpticalFlow(std::filesystem::path imagePath) : m_imagePath(std::move(imagePath)) {}

gneiss::io::VkittiOpticalFlow::VkittiOpticalFlow(const gneiss::io::VkittiOpticalFlow& other)
    : m_imagePath(other.m_imagePath) {}

// NOLINTNEXTLINE(bugprone-exception-escape)
gneiss::io::VkittiOpticalFlow::VkittiOpticalFlow(gneiss::io::VkittiOpticalFlow&& other) noexcept
    : m_imagePath(std::move(other.m_imagePath)) {}

gneiss::io::VkittiOpticalFlow& gneiss::io::VkittiOpticalFlow::operator=(const gneiss::io::VkittiOpticalFlow& other) {
  if (this != &other) {
    m_imagePath = other.m_imagePath;
  }
  return *this;
}

// NOLINTNEXTLINE(bugprone-exception-escape)
gneiss::io::VkittiOpticalFlow& gneiss::io::VkittiOpticalFlow::operator=(
    gneiss::io::VkittiOpticalFlow&& other) noexcept {
  m_imagePath = std::move(other.m_imagePath);
  return *this;
}