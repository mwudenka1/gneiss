#include "gneiss/io/VkittiLoader.h"

#include "gneiss/io/CameraMeasurement.h"
#include "gneiss/io/Dataset.h"
#include "gneiss/io/OpticalFlow.h"
#include "gneiss/io/VkittiOpticalFlow.h"
#include "gneiss/utils/Timestamp.h"
#include "gneiss/utils/ranges.h"

#include <chrono>
#include <cstddef>
#include <cstdint>
#include <filesystem>
#include <iostream>
#include <iterator>
#include <memory>
#include <optional>
#include <ratio>
#include <set>
#include <string_view>
#include <unordered_map>
#include <vector>

namespace {

std::optional<std::filesystem::path> get_scenario_folder(const std::filesystem::path& path,
                                                         // NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
                                                         const std::string_view scenarioName,
                                                         const std::string_view dataType) {
  auto baseDirIterator = std::filesystem::directory_iterator(path);
  const auto foundDataDirIterator = std::find_if(
      // NOLINTNEXTLINE(misc-include-cleaner) false positive on std::filesystem::begin
      std::filesystem::begin(baseDirIterator), std::filesystem::end(baseDirIterator), [&dataType](const auto& entry) {
        return entry.is_directory() && entry.path().filename().string().ends_with(dataType);
      });

  // NOLINTNEXTLINE(misc-include-cleaner) false positive on std::filesystem::end
  if (foundDataDirIterator == std::filesystem::end(baseDirIterator)) {
    return {};
  }

  const auto dataDir = foundDataDirIterator->path();

  if (std::count_if(scenarioName.begin(), scenarioName.end(), [](char iterChar) { return iterChar == '/'; }) < 1) {
    std::cout << "The scenario name of vkitti sequences should have the form 'scenario number/modifier', e.g. 0006/fog!"
              << '\n';
    return {};
  }

  auto scenarioNameSplitted = gneiss::ranges::views::split(scenarioName, "/");

  auto scenarioDir = dataDir / std::string_view(*scenarioNameSplitted.begin()) /
                     std::string_view(*std::next(scenarioNameSplitted.begin()));

  if (!std::filesystem::is_directory(scenarioDir)) {
    std::cout << "Could not find directory " << scenarioDir << '\n';
    return {};
  }

  return scenarioDir;
}

std::vector<std::unique_ptr<gneiss::io::CameraMeasurement>> load_rgb(const std::filesystem::path& path,
                                                                     const std::string_view scenarioName) {
  std::vector<std::unique_ptr<gneiss::io::CameraMeasurement>> result;

  const auto optionalScenarioDir = get_scenario_folder(path, scenarioName, "rgb");

  if (!optionalScenarioDir) {
    return {};
  }

  for (const auto& entry : std::filesystem::directory_iterator(optionalScenarioDir.value())) {
    if (entry.is_regular_file()) {
      result.push_back(std::make_unique<gneiss::io::CameraMeasurement>(entry.path()));
    }
  }

  // NOLINTNEXTLINE(misc-include-cleaner)
  gneiss::ranges::sort(result, [](const auto& leftMeasurement, const auto& rightMeasurement) noexcept {
    return leftMeasurement->getImagePath() < rightMeasurement->getImagePath();
  });

  return result;
}

std::vector<std::unique_ptr<gneiss::io::OpticalFlow>> load_optical_flow(const std::filesystem::path& path,
                                                                        const std::string_view scenarioName) {
  std::vector<std::unique_ptr<gneiss::io::OpticalFlow>> result;

  const auto optionalScenarioDir = get_scenario_folder(path, scenarioName, "flowgt");

  if (!optionalScenarioDir) {
    return {};
  }

  std::set<std::filesystem::path> sortedImagePaths;
  for (const auto& entry : std::filesystem::directory_iterator(optionalScenarioDir.value())) {
    if (entry.is_regular_file()) {
      sortedImagePaths.insert(entry.path());
    }
  }

  result.reserve(sortedImagePaths.size());
  for (const auto& imagePath : sortedImagePaths) {
    result.push_back(std::make_unique<gneiss::io::VkittiOpticalFlow>(imagePath));
  }

  return result;
}

}  // namespace

std::shared_ptr<gneiss::io::Dataset> gneiss::io::load_vkitti(const std::filesystem::path& path,
                                                             const std::string_view scenarioName) {
  auto result = std::make_shared<Dataset>();

  result->cameraMeasurements.emplace_back(load_rgb(path, scenarioName));
  result->opticalFlow.emplace_back(load_optical_flow(path, scenarioName));

  // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
  result->cameraPeriodLength = std::chrono::duration<int64_t, std::milli>(100);

  result->cameraTimestamps =
      gneiss::ranges::views::iota(std::size_t{0}, result->cameraMeasurements.back().size()) |
      gneiss::ranges::views::transform([&](auto idx) { return Timestamp(idx * result->cameraPeriodLength); }) |
      gneiss::ranges::to<std::vector<Timestamp>>();

  result->cameraIdxs =
      gneiss::ranges::views::zip(result->cameraTimestamps, gneiss::ranges::views::iota(std::size_t{0})) |
      gneiss::ranges::to<std::unordered_map<Timestamp, std::size_t>>();

  return result;
}