#include "gneiss/io/CameraMeasurement.h"

#include <filesystem>
#include <utility>

gneiss::io::CameraMeasurement::CameraMeasurement(std::filesystem::path imagePath) : m_imagePath(std::move(imagePath)) {}

// NOLINTNEXTLINE(bugprone-exception-escape)
gneiss::io::CameraMeasurement::CameraMeasurement(gneiss::io::CameraMeasurement&& other) noexcept
    : m_imagePath(std::move(other.m_imagePath)) {}

gneiss::io::CameraMeasurement& gneiss::io::CameraMeasurement::operator=(const gneiss::io::CameraMeasurement& other) {
  if (this != &other) {
    m_imagePath = other.m_imagePath;
  }
  return *this;
}

// NOLINTNEXTLINE(bugprone-exception-escape)
gneiss::io::CameraMeasurement& gneiss::io::CameraMeasurement::operator=(
    gneiss::io::CameraMeasurement&& other) noexcept {
  m_imagePath = std::move(other.m_imagePath);
  return *this;
}
