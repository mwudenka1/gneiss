#ifndef GNEISS_VKITTIOPTICALFLOW_H
#define GNEISS_VKITTIOPTICALFLOW_H
#pragma once
/** @file */

#include "gneiss/io/OpticalFlow.h"

#include <Eigen/Dense>

#include <optional>

namespace gneiss::io {

/**
 * @addtogroup io
 *
 * @{
 */

class VkittiOpticalFlow : public io::OpticalFlow {
  constexpr static std::size_t numChannels = 3;

 public:
  // NOLINTNEXTLINE(bugprone-exception-escape)
  explicit VkittiOpticalFlow(std::filesystem::path imagePath);
  VkittiOpticalFlow(const VkittiOpticalFlow&);
  // NOLINTNEXTLINE(bugprone-exception-escape)
  VkittiOpticalFlow(VkittiOpticalFlow&& other) noexcept;
  VkittiOpticalFlow& operator=(const VkittiOpticalFlow& other);
  // NOLINTNEXTLINE(bugprone-exception-escape)
  VkittiOpticalFlow& operator=(VkittiOpticalFlow&& other) noexcept;

  [[nodiscard]] std::optional<Eigen::Vector2d> get_optical_flow(
      fields_2d::FieldCoordinate<double> coordinate) const override {
    load_image_cache();

    if (!m_imageCache || m_imageCache.value() == nullptr) {
      return {};
    }

    if (coordinate.x < 0 || coordinate.x > m_actualWidth_pixel - 1 || coordinate.y < 0 ||
        coordinate.y > m_actualWidth_pixel - 1 || m_actualChannelNumber != 3) {
      return {};
    }

    const fields_2d::FieldCoordinate<std::size_t> topLeft(static_cast<std::size_t>(std::floor(coordinate.x)),
                                                          static_cast<std::size_t>(std::floor(coordinate.y)));
    const auto topRight = topLeft + fields_2d::FieldCoordinate<std::size_t>{1, 0};
    const auto bottomLeft = topLeft + fields_2d::FieldCoordinate<std::size_t>{0, 1};
    const auto bottomRight = topLeft + fields_2d::FieldCoordinate<std::size_t>{1, 1};

    const double toLeft = coordinate.x - static_cast<double>(topLeft.x);
    const double toTop = coordinate.y - static_cast<double>(topLeft.y);
    const double toRight = double{1.0} - toLeft;
    const double toBottom = double{1.0} - toTop;

    const double weightLeft = toRight;
    const double weightTop = toBottom;
    const double weightRight = toLeft;
    const double weightBottom = toTop;

    auto optionalFlowTopLeft = evaluate_at(topLeft);
    auto optionalFlowTopRight = evaluate_at(topRight);
    auto optionalFlowBottomLeft = evaluate_at(bottomLeft);
    auto optionalFlowBottomRight = evaluate_at(bottomRight);

    if (optionalFlowTopLeft && optionalFlowTopRight && optionalFlowBottomLeft && optionalFlowBottomRight) {
      return weightLeft * weightTop * optionalFlowTopLeft.value() +
             weightLeft * weightBottom * optionalFlowBottomLeft.value() +
             weightRight * weightTop * optionalFlowTopRight.value() +
             weightRight * weightBottom * optionalFlowBottomRight.value();
    }

    if (optionalFlowTopLeft && optionalFlowTopRight) {
      return weightLeft * optionalFlowTopLeft.value() + weightRight * optionalFlowTopRight.value();
    }

    if (optionalFlowBottomLeft && optionalFlowBottomRight) {
      return weightLeft * optionalFlowBottomLeft.value() + weightRight * optionalFlowBottomRight.value();
    }

    if (optionalFlowTopLeft && optionalFlowBottomLeft) {
      return weightTop * optionalFlowTopLeft.value() + weightBottom * optionalFlowBottomLeft.value();
    }

    if (optionalFlowTopRight && optionalFlowBottomRight) {
      return weightTop * optionalFlowTopRight.value() + weightBottom * optionalFlowBottomRight.value();
    }

    if (optionalFlowTopLeft) {
      return optionalFlowTopLeft;
    }

    if (optionalFlowTopRight) {
      return optionalFlowTopRight;
    }

    if (optionalFlowBottomLeft) {
      return optionalFlowBottomLeft;
    }

    return optionalFlowBottomRight;
  }

  [[nodiscard]] const auto& getImagePath() const { return m_imagePath; }

 private:
  void load_image_cache() const {
    if (!m_imageCache) {
      m_imageCache = executable_utils::ImageData<uint16_t>(m_imagePath.c_str(), &m_actualWidth_pixel,
                                                           &m_actualHeight_pixel, &m_actualChannelNumber, STBI_rgb);
    }
  }

  std::optional<Eigen::Vector2d> evaluate_at(const fields_2d::FieldCoordinate<std::size_t>& coordinate) const {
    const std::size_t rIdx =
        coordinate.y * static_cast<std::size_t>(m_actualWidth_pixel) * numChannels + coordinate.x * numChannels;

    // NOLINTBEGIN(bugprone-unchecked-optional-access)
    const auto rValue = m_imageCache.value()[rIdx];
    const auto gValue = m_imageCache.value()[rIdx + 1];
    const auto bValue = m_imageCache.value()[rIdx + 2];
    // NOLINTEND(bugprone-unchecked-optional-access)

    if (bValue == 0) {
      return {};
    }

    const double scaling = 2. / (static_cast<double>(1ul << 16ul) - 1.);

    return Eigen::Vector2d((scaling * static_cast<double>(rValue) - 1) * (m_actualWidth_pixel - 1),
                           (scaling * static_cast<double>(gValue) - 1) * (m_actualHeight_pixel - 1));
  }

  std::filesystem::path m_imagePath;
  mutable std::optional<executable_utils::ImageData<uint16_t>> m_imageCache;
  mutable int m_actualWidth_pixel = -1;
  mutable int m_actualHeight_pixel = -1;
  mutable int m_actualChannelNumber = -1;
};

/** @} */  // end of io

}  // namespace gneiss::io

#endif  // GNEISS_VKITTIOPTICALFLOW_H
