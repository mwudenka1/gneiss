#ifndef GNEISS_IO_OPTICALFLOW_H
#define GNEISS_IO_OPTICALFLOW_H
#pragma once
/** @file */

#include "gneiss/executable-utils/ImageData.h"
#include "gneiss/image/Image.h"
#include "gneiss/utils/Timestamp.h"

#include <Eigen/Dense>

#include <filesystem>
#include <optional>

namespace gneiss::io {

/**
 * @addtogroup io
 *
 * @{
 */

class OpticalFlow {
 public:
  virtual ~OpticalFlow() = default;

  [[nodiscard]] virtual std::optional<Eigen::Vector2d> get_optical_flow(
      fields_2d::FieldCoordinate<double> coordinate) const = 0;
};

/** @} */  // end of io

}  // namespace gneiss::io
#endif  // GNEISS_IO_OPTICALFLOW_H
