#ifndef GNEISS_DATASET_H
#define GNEISS_DATASET_H
#pragma once
/** @file */

#include "gneiss/executable-utils/ImageData.h"
#include "gneiss/image/Image.h"
#include "gneiss/io/CameraMeasurement.h"
#include "gneiss/io/OpticalFlow.h"
#include "gneiss/utils/Timestamp.h"

#include <vector>

namespace gneiss::io {

/**
 * @addtogroup io
 *
 * @{
 */

struct Dataset {
  Dataset() noexcept = default;
  // NOLINTBEGIN(misc-non-private-member-variables-in-classes)

  // std::vector<Timestamp> imuTimestamps;
  // std::vector<ImuMeasurements> imuMeasurements;

  Duration cameraPeriodLength{0};
  std::vector<Timestamp> cameraTimestamps;                  // frame idx -> timestamp
  std::unordered_map<Timestamp, std::size_t> cameraIdxs{};  // timestamp -> frame idx
  std::vector<std::vector<std::unique_ptr<CameraMeasurement>>>
      cameraMeasurements;  // camera idx -> frame idx -> measurement
  std::vector<std::vector<std::unique_ptr<gneiss::io::OpticalFlow>>>
      opticalFlow;  // camera idx -> start frame idx -> optical flow
  // NOLINTEND(misc-non-private-member-variables-in-classes)

  [[nodiscard]] auto get_number_of_frames() const { return cameraMeasurements.at(0).size(); }
};

/** @} */  // end of io

}  // namespace gneiss::io
#endif  // GNEISS_DATASET_H
