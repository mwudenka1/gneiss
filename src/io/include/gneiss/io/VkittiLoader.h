#ifndef GNEISS_VKITTILOADER_H
#define GNEISS_VKITTILOADER_H
#pragma once
/** @file */

#include "gneiss/io/Dataset.h"

#include <filesystem>
#include <memory>
#include <string_view>

namespace gneiss::io {
/**
 * @addtogroup io
 *
 * @{
 */

std::shared_ptr<Dataset> load_vkitti(const std::filesystem::path& path, std::string_view scenarioName);

/** @} */  // end of io

}  // namespace gneiss::io
#endif  // GNEISS_VKITTILOADER_H
