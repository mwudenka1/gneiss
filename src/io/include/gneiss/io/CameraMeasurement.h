#ifndef GNEISS_CAMERAMEASUREMENT_H
#define GNEISS_CAMERAMEASUREMENT_H
#pragma once
/** @file */

#include "gneiss/executable-utils/ImageData.h"
#include "gneiss/image/Image.h"
#include "gneiss/utils/Timestamp.h"

#include <filesystem>

namespace gneiss::io {

/**
 * @addtogroup io
 *
 * @{
 */

class CameraMeasurement {
 public:
  // NOLINTNEXTLINE(bugprone-exception-escape)
  explicit CameraMeasurement(std::filesystem::path imagePath);
  CameraMeasurement(const CameraMeasurement&) = default;
  // NOLINTNEXTLINE(bugprone-exception-escape)
  CameraMeasurement(CameraMeasurement&& other) noexcept;
  CameraMeasurement& operator=(const CameraMeasurement& other);
  // NOLINTNEXTLINE(bugprone-exception-escape)
  CameraMeasurement& operator=(CameraMeasurement&& other) noexcept;

  template <typename PixelType_, std::size_t width_, std::size_t height_, bool pitched_ = false>
  [[nodiscard]] Image<PixelType_, width_, height_, pitched_> getImage() const {
    int actualWidth_pixel = -1;
    int actualHeight_pixel = -1;
    int actualChannelNumber = -1;
    const auto data = executable_utils::ImageData<PixelType_>(m_imagePath.c_str(), &actualWidth_pixel,
                                                              &actualHeight_pixel, &actualChannelNumber, STBI_grey);

    if (data == nullptr) {
      return Image<PixelType_, width_, height_, pitched_>();
    }

    Image<PixelType_, width_, height_, pitched_> image;
    for (const auto xIdx : gneiss::ranges::views::iota(
             std::size_t{0}, std::min<std::size_t>(width_, static_cast<std::size_t>(actualWidth_pixel)))) {
      for (const auto yIdx : gneiss::ranges::views::iota(
               std::size_t{0}, std::min<std::size_t>(height_, static_cast<std::size_t>(actualHeight_pixel)))) {
        image(xIdx, yIdx) = data[static_cast<std::size_t>(actualWidth_pixel) * yIdx + xIdx];
      }
    }

    return image;
  }

  [[nodiscard]] const auto& getImagePath() const { return m_imagePath; }

 private:
  std::filesystem::path m_imagePath;
};

/** @} */  // end of io

}  // namespace gneiss::io
#endif  // GNEISS_CAMERAMEASUREMENT_H
