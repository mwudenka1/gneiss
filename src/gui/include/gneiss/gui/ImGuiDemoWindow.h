#ifndef GNEISS_IMGUIDEMOWINDOW_H
#define GNEISS_IMGUIDEMOWINDOW_H
#pragma once

#include <imgui.h>
#include <vsg/nodes/Node.h>

namespace gneiss::gui {

/**
 * @addtogroup gui
 *
 * @{
 */

class ImGuiDemoWindow : public vsg::Inherit<vsg::Node, ImGuiDemoWindow> {
 public:
  void accept(vsg::RecordTraversal&) const override { ImGui::ShowDemoWindow(); }
};

/** @} */  // end of gui

}  // namespace gneiss::gui
#endif  // GNEISS_IMGUIDEMOWINDOW_H
