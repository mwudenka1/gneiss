#ifndef GNEISS_DATASETCONTROLWINDOW_H
#define GNEISS_DATASETCONTROLWINDOW_H
#pragma once

#include "gneiss/gui/DatasetState.h"

#include <functional>
#include <optional>

namespace gneiss::gui {

/**
 * @addtogroup gui
 *
 * @{
 */

void dataset_control_window(const DatasetState& datasetState, std::size_t numberFrames,
                            std::optional<std::function<void(std::size_t)>> setCurrentFrame = {},
                            std::optional<std::function<void(bool)>> setFollowExecution = {});

/** @} */  // end of gui

}  // namespace gneiss::gui

#endif  // GNEISS_DATASETCONTROLWINDOW_H
