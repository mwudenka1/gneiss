#ifndef GNEISS_IMIMAGE_H
#define GNEISS_IMIMAGE_H
#pragma once
/** @file
 * An imgui plugin that provides an advanced image widget.
 * With BeginImage and EndImage an image environment can be created that allows for zoom, drag and overlays.
 */

#include <imgui.h>

#include <span>

namespace ImImage {

/**
 * @addtogroup gui
 *
 * @{
 */

struct ImImageContext;

void CreateContext();

void DestroyContext();

void SetCurrentContext(ImImageContext* ctx);

void BeginImage(const char* title_id, void* user_texture_id, const ImVec2& size,
                const ImVec4& tint_col = ImVec4(1, 1, 1, 1), const ImVec4& border_col = ImVec4(0, 0, 0, 0));

void EndImage();

// Overlays
void Line(const ImVec2& start, const ImVec2& end, ImU32 col, float thickness);
void Cross(const ImVec2& center, ImU32 col, float thickness, float scale = 1);
void Polyline(std::span<const ImVec2> points, ImU32 col, ImDrawFlags flags, float thickness);
void Circle(const ImVec2& center, float radius, ImU32 col, int num_segments = 0, float thickness = 1.0f);
void Text(const ImFont* font, float font_size, const ImVec2& pos, ImU32 col, const char* text_begin,
          const char* text_end = nullptr);

/** @} */  // end of gui

}  // namespace ImImage

#endif  // GNEISS_IMIMAGE_H
