#ifndef GNEISS_INTERSECTIONHANDLER_H
#define GNEISS_INTERSECTIONHANDLER_H
#pragma once

#include <vsg/core/Inherit.h>

namespace gneiss::gui {

/**
 * @addtogroup gui
 *
 * @{
 */

class IntersectionHandler : public vsg::Inherit<vsg::Visitor, IntersectionHandler> {
 public:
  IntersectionHandler(const vsg::ref_ptr<vsg::Camera>& in_camera, const vsg::ref_ptr<vsg::Group>& in_scenegraph);

  void apply(vsg::ButtonPressEvent& buttonPressEvent) override;

 private:
  void intersection(vsg::PointerEvent& pointerEvent) const;

  vsg::ref_ptr<vsg::Camera> m_camera;
  vsg::ref_ptr<vsg::Group> m_scenegraph;
  bool m_verbose = true;
};

/** @} */  // end of gui

}  // namespace gneiss::gui

#endif  // GNEISS_INTERSECTIONHANDLER_H
