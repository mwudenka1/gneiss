#ifndef GNEISS_IMAGESKEYPOINTCOLLECTIONSTATE_H
#define GNEISS_IMAGESKEYPOINTCOLLECTIONSTATE_H
#pragma once

#include "gneiss/gui/Redux.h"
#include "gneiss/image/KeypointDescriptor.h"

#include <memory>
#include <vector>

namespace gneiss::gui {

/**
 * @addtogroup gui
 *
 * @{
 */
using KeypointDescriptorCollection = std::vector<KeypointDescriptor>;
using ImagesKeypointCollectionState = std::vector<std::shared_ptr<KeypointDescriptorCollection>>;

class AppendImagesKeypointCollection : public Action {
 public:
  explicit AppendImagesKeypointCollection(const std::shared_ptr<KeypointDescriptorCollection>& inPayload)
      : payload(inPayload) {}

  [[nodiscard]] std::string to_string() const override {
    return "AppendImagesCoordinateCollection (size: " + std::to_string(payload->size()) + ")";
  }

  // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
  std::shared_ptr<KeypointDescriptorCollection> payload;
};

struct ImagesKeypointCollectionStateUpdater {
  template <typename State_>
  static void update(State_&, const gneiss::gui::Action&) {
    // do nothing
  }

  static void update(ImagesKeypointCollectionState& state, const gneiss::gui::Action& action) {
    try {
      const auto& appendAction = dynamic_cast<const AppendImagesKeypointCollection&>(action);
      state.emplace_back(appendAction.payload);
    } catch (const std::bad_cast&) {
      // do nothing
    }
  }
};

/** @} */  // end of gui

}  // namespace gneiss::gui

#endif  // GNEISS_IMAGESKEYPOINTCOLLECTIONSTATE_H
