#ifndef GNEISS_IMPLOTWINDOW_H
#define GNEISS_IMPLOTWINDOW_H
#pragma once

#include <fmt/core.h>
#include <imgui.h>
#include <implot.h>
#include <vsg/nodes/Node.h>

#include <list>

namespace gneiss::gui {

/**
 * @addtogroup gui
 *
 * @{
 */

class ImPlotWindow : public vsg::Inherit<vsg::Node, ImPlotWindow> {
 public:
  using Graph = std::function<void()>;

  explicit ImPlotWindow(std::string_view windowName)
      : m_windowName(windowName), m_plotName(fmt::format("##{}_plot", m_windowName)) {}

  void add_graph(Graph graph) { m_graphs.push_back(std::move(graph)); }

  void accept(vsg::RecordTraversal&) const override {
    ImGui::Begin(m_windowName.c_str());
    if (ImPlot::BeginPlot(m_plotName.c_str(), ImVec2(-1, -1))) {
      ImPlot::SetupAxis(ImAxis_X1, "time [s]", ImPlotAxisFlags_AutoFit);
      for (const auto& graph : m_graphs) {
        graph();
      }
      ImPlot::EndPlot();
    }
    ImGui::End();
  }

 private:
  std::string m_windowName;
  std::string m_plotName;
  std::list<Graph> m_graphs;
};

/** @} */  // end of gui

}  // namespace gneiss::gui
#endif  // GNEISS_IMPLOTWINDOW_H
