#ifndef GNEISS_IMIMAGE_INTERNAL_H
#define GNEISS_IMIMAGE_INTERNAL_H
#pragma once

#include <imgui_internal.h>

namespace ImImage {

/**
 * @addtogroup gui
 *
 * @{
 */

// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
extern ImImageContext* GImImage;  // Current implicit context pointer

struct ImImageImage {
  ImGuiID ID = 0;
  ImVec2 scaleFactor = {1.f, 1.f};
  float zoom = 0;
  float zoomFactor = 1.0;
  float totalSizeFactor = 1.0;
  ImVec2 viewCenter{0.5, 0.5};
  ImVec2 normalizedDraggedDistance = {0, 0};
  ImVec2 normalizedShiftFromCenter = {0, 0};
  ImVec2 normalizedCutoutSize;
  ImVec2 topLeftScreenCoordinate = {0, 0};
  ImVec2 size_pixel;
};

struct ImImageContext {
  // Image States
  ImPool<ImImageImage> Images;
  ImImageImage* CurrentImage = nullptr;
};

/** @} */  // end of gui

}  // namespace ImImage
#endif  // GNEISS_IMIMAGE_INTERNAL_H
