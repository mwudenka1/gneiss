#ifndef GNEISS_OPTICALFLOWEVALUATIONSTATE_H
#define GNEISS_OPTICALFLOWEVALUATIONSTATE_H
#pragma once

#include "gneiss/evaluation/calculate_optical_flow_se.h"
#include "gneiss/evaluation/print_optical_flow_evaluation.h"
#include "gneiss/gui/KeypointsEstimateState.h"
#include "gneiss/gui/Redux.h"
#include "gneiss/image/KeypointsEstimate.h"
#include "gneiss/io/OpticalFlow.h"

#include <memory>
#include <vector>

namespace gneiss::gui {

/**
 * @addtogroup gui
 *
 * @{
 */

template <typename Scalar_>
std::unordered_map<KeypointId, Eigen::Matrix<Scalar_, 2, 1>> keypoint_positions_from_vectorfield(
    const auto& previousOpticalFlow, const auto& keypointIds, const gneiss::io::OpticalFlow& groundTruthOpticalFlow) {
  using Vector2 = Eigen::Matrix<Scalar_, 2, 1>;

  std::unordered_map<KeypointId, Eigen::Matrix<Scalar_, 2, 1>> result;
  for (const auto& keypointId : keypointIds) {
    const auto previousKeypointDescriptorAtLevelIterator = std::find_if(
        previousOpticalFlow.begin(), previousOpticalFlow.end(),
        [&keypointId](const auto& prevOpticalFlowDes) { return prevOpticalFlowDes.keypoint.id == keypointId; });

    if (previousKeypointDescriptorAtLevelIterator == previousOpticalFlow.end()) {
      continue;
    }

    const auto& previousKeypointDescriptorAtLevel = *previousKeypointDescriptorAtLevelIterator;

    const auto optionalGroundTruthOpticalFlowVector =
        groundTruthOpticalFlow.get_optical_flow(fields_2d::FieldCoordinate<double>(
            previousKeypointDescriptorAtLevel.keypoint.transform.translation().template cast<double>()));

    if (!optionalGroundTruthOpticalFlowVector) {
      continue;
    }

    const Vector2 groundTruthOpticalFlowVector = optionalGroundTruthOpticalFlowVector.value().template cast<Scalar_>();

    result[keypointId] =
        previousKeypointDescriptorAtLevel.keypoint.transform.translation() + groundTruthOpticalFlowVector;
  }

  return result;
}

struct OpticalFlowEvaluationState {
  constexpr static auto& cameras = parameters::parameters["cameras"sv].array_data();
  using OpticalFlowCollection =
      std::shared_ptr<const std::vector<std::vector<std::unique_ptr<gneiss::io::OpticalFlow>>>>;
  constexpr static std::string_view optical_flow_scalar =
      parameters::parameters["optical_flow_scalar"sv].template get<std::string_view>();
  using Scalar = string_to_type_t<FixedString<optical_flow_scalar.size()>(optical_flow_scalar.data())>;

  float validThreshold_pixel2;
  OpticalFlowCollection opticalFlowGroundTruth;
  std::shared_ptr<const std::unordered_map<Timestamp, std::size_t>> cameraIdxs;
  std::shared_ptr<const KeypointsEstimate> previousEstimate;
  std::array<std::vector<std::shared_ptr<std::unordered_map<KeypointId, Eigen::Matrix<Scalar, 2, 1>>>>, cameras.size()>
      cameraIdxFrameIdxKeypointIdTransformMapping;

  // optical flow happens between two frames
  // here frame idx is the index of the first frame
  std::array<std::vector<float>, cameras.size()> cameraIdxFrameIdxRmseMapping;
  std::array<std::vector<float>, cameras.size()> cameraIdxFrameIdxNumValidMapping;
};

class OpticalFlowEvaluationStateUpdater {
 public:
  constexpr static auto& cameras = parameters::parameters["cameras"sv].array_data();
  constexpr static std::string_view optical_flow_scalar =
      parameters::parameters["optical_flow_scalar"sv].template get<std::string_view>();
  using Scalar = string_to_type_t<FixedString<optical_flow_scalar.size()>(optical_flow_scalar.data())>;

  template <typename State_>
  static void update(State_&, const gneiss::gui::Action&) {
    // do nothing
  }

  static void update(OpticalFlowEvaluationState& state, const gneiss::gui::Action& action) {
    if (const auto* const appendAction = dynamic_cast<const AppendKeypointEstimate*>(&action)) {
      std::array<std::vector<float>, cameras.size()> cameraIdxKeypointIdxErrors_pixel2;
      constexpr_for<0, cameras.size(), 1>([&](auto cameraIdx) {
        auto& frameIdxKeypointIdTransformMapping = state.cameraIdxFrameIdxKeypointIdTransformMapping.at(cameraIdx);
        frameIdxKeypointIdTransformMapping.push_back(
            std::make_shared<std::unordered_map<KeypointId, Eigen::Matrix<Scalar, 2, 1>>>());
      });

      if (state.previousEstimate) {
        constexpr_for<0, cameras.size(), 1>([&](auto cameraIdx) {
          const auto& opticalFlowGroundTruthForCamera = state.opticalFlowGroundTruth->at(cameraIdx);

          const auto previousFrameIdx = state.cameraIdxs->at(state.previousEstimate->timestamp());
          if (opticalFlowGroundTruthForCamera.size() > previousFrameIdx) {
            const auto& currentOpticalFlow = appendAction->payload->template get<cameraIdx>();
            const auto& previousOpticalFlow = state.previousEstimate->get<cameraIdx>();
            const auto& opticalFlowGroundTruth = *opticalFlowGroundTruthForCamera.at(previousFrameIdx);

            cameraIdxKeypointIdxErrors_pixel2.at(cameraIdx) = calculate_optical_flow_se_pixel2<Scalar>(
                previousOpticalFlow, currentOpticalFlow, opticalFlowGroundTruth);
          }
        });

        constexpr_for<0, cameras.size(), 1>([&](auto cameraIdx) {
          const auto& opticalFlowGroundTruthForCamera = state.opticalFlowGroundTruth->at(cameraIdx);
          auto& frameIdxKeypointIdTransformMapping = state.cameraIdxFrameIdxKeypointIdTransformMapping.at(cameraIdx);
          auto& keypointIdTransformMapping = *frameIdxKeypointIdTransformMapping.back();

          const auto previousFrameIdx = state.cameraIdxs->at(state.previousEstimate->timestamp());
          if (opticalFlowGroundTruthForCamera.size() > previousFrameIdx) {
            const auto& currentOpticalFlow = appendAction->payload->template get<cameraIdx>();
            const auto& previousOpticalFlow = state.previousEstimate->get<cameraIdx>();
            const auto& opticalFlowGroundTruth = *opticalFlowGroundTruthForCamera.at(previousFrameIdx);

            keypointIdTransformMapping = keypoint_positions_from_vectorfield<Scalar>(
                previousOpticalFlow,
                currentOpticalFlow | gneiss::ranges::views::transform([](const auto& opticalFlowDescriptor) noexcept {
                  return opticalFlowDescriptor.keypoint.id;
                }),
                opticalFlowGroundTruth);
          }
        });

        constexpr_for<0, cameras.size(), 1>([&](auto cameraIdx) {
          const auto keypointIdxErrors_pixel2 = cameraIdxKeypointIdxErrors_pixel2.at(cameraIdx);
          auto& frameIdxRmseMapping = state.cameraIdxFrameIdxRmseMapping.at(cameraIdx);

          if (keypointIdxErrors_pixel2.empty()) {
            frameIdxRmseMapping.push_back(0);
          } else {
            const float sum = std::reduce(keypointIdxErrors_pixel2.cbegin(), keypointIdxErrors_pixel2.cend());
            const float mse = 1.f / static_cast<float>(keypointIdxErrors_pixel2.size()) * sum;
            frameIdxRmseMapping.push_back(std::sqrt(mse));
          }
        });

        constexpr_for<0, cameras.size(), 1>([&](auto cameraIdx) {
          const auto keypointIdxErrors_pixel2 = cameraIdxKeypointIdxErrors_pixel2.at(cameraIdx);
          const auto numValid =
              static_cast<float>(std::count_if(keypointIdxErrors_pixel2.begin(), keypointIdxErrors_pixel2.end(),
                                               [validThreshold_pixel2 = state.validThreshold_pixel2](const auto error) {
                                                 return error < validThreshold_pixel2;
                                               }));
          state.cameraIdxFrameIdxNumValidMapping.at(cameraIdx).push_back(numValid);
        });
      }

      state.previousEstimate = appendAction->payload;
    }
  }
};

/** @} */  // end of gui

}  // namespace gneiss::gui

#endif  // GNEISS_OPTICALFLOWEVALUATIONSTATE_H
