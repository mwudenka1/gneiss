#ifndef GNEISS_IMAGEVISUALIZATIONSTATE_H
#define GNEISS_IMAGEVISUALIZATIONSTATE_H
#pragma once

#include "gneiss/gui/Redux.h"

#include <fmt/core.h>

namespace gneiss::gui {

/**
 * @addtogroup gui
 *
 * @{
 */

struct ImageVisualizationState {
  bool keypoints = true;
  bool ids = true;
  bool opticalFlow = false;
  bool groundTruthOpticalFlow = false;
  bool patches = false;
};

class SetKeypointsVisibility : public Action {
 public:
  explicit SetKeypointsVisibility(bool inPayload) : payload(inPayload) {}

  [[nodiscard]] std::string to_string() const override { return fmt::format("SetKeypointsVisibility {}", payload); }

  // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
  bool payload;
};

class SetIdsVisibility : public Action {
 public:
  explicit SetIdsVisibility(bool inPayload) : payload(inPayload) {}

  [[nodiscard]] std::string to_string() const override { return fmt::format("SetIdsVisibility {}", payload); }

  // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
  bool payload;
};

class SetOpticalFlowVisibility : public Action {
 public:
  explicit SetOpticalFlowVisibility(bool inPayload) : payload(inPayload) {}

  [[nodiscard]] std::string to_string() const override { return fmt::format("SetOpticalFlowVisibility {}", payload); }

  // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
  bool payload;
};

class SetGroundTruthOpticalFlowVisibility : public Action {
 public:
  explicit SetGroundTruthOpticalFlowVisibility(bool inPayload) : payload(inPayload) {}

  [[nodiscard]] std::string to_string() const override {
    return fmt::format("SetGroundTruthOpticalFlowVisibility {}", payload);
  }

  // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
  bool payload;
};

class SetPatchesVisibility : public Action {
 public:
  explicit SetPatchesVisibility(bool inPayload) : payload(inPayload) {}

  [[nodiscard]] std::string to_string() const override { return fmt::format("SetPatchesVisibility {}", payload); }

  // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
  bool payload;
};

struct ImageVisualizationStateUpdater {
  template <typename State_>
  static void update(State_&, const gneiss::gui::Action&) {
    // do nothing
  }

  static void update(ImageVisualizationState& state, const gneiss::gui::Action& action) {
    if (const auto* const keypointsAction = dynamic_cast<const SetKeypointsVisibility*>(&action)) {
      state.keypoints = keypointsAction->payload;
    } else if (const auto* const idsAction = dynamic_cast<const SetIdsVisibility*>(&action)) {
      state.ids = idsAction->payload;
    } else if (const auto* const opticalFlowAction = dynamic_cast<const SetOpticalFlowVisibility*>(&action)) {
      state.opticalFlow = opticalFlowAction->payload;
    } else if (const auto* const groundTruthOpticalFlowAction =
                   dynamic_cast<const SetGroundTruthOpticalFlowVisibility*>(&action)) {
      state.groundTruthOpticalFlow = groundTruthOpticalFlowAction->payload;
    } else if (const auto* const patchesAction = dynamic_cast<const SetPatchesVisibility*>(&action)) {
      state.patches = patchesAction->payload;
    }
  }
};

/** @} */  // end of gui

}  // namespace gneiss::gui
#endif  // GNEISS_IMAGEVISUALIZATIONSTATE_H
