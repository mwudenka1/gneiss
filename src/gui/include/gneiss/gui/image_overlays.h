#ifndef GNEISS_IMAGE_OVERLAYS_H
#define GNEISS_IMAGE_OVERLAYS_H
#pragma once

#include "gneiss/gui/KeypointsEstimateState.h"
#include "gneiss/gui/imimage.h"
#include "gneiss/gui/tinycolormap.h"
#include "gneiss/image/KeypointDescriptorAtLevel.h"
#include "gneiss/optical-flow/ParameterizedPatchPattern.h"
#include "gneiss/utils/ranges.h"

#include <imgui.h>

namespace gneiss::gui {

/**
 * @addtogroup gui
 *
 * @{
 */

void draw_optical_flow(const auto& keypoints, const auto& idCameraPatchHistoryMapping, const std::size_t cameraIdx,
                       const Timestamp& currentTimestamp) {
  static constexpr float levelToThickness = 1.f;

  for (const auto& keypointDescriptor : keypoints) {
    const auto& keypointId = keypointDescriptor.keypoint.id;

    const float level = static_cast<float>(keypointDescriptor.level) + 1.f;
    const gui::PatchHistory& patchHistory = idCameraPatchHistoryMapping.at(keypointId).at(cameraIdx);

    auto pointsRange = patchHistory | ranges::views::reverse |
                       ranges::views::drop_while([currentTimestamp](const auto& historyItem) noexcept {
                         return historyItem.timestamp > currentTimestamp;
                       }) |
                       ranges::views::transform([](const auto& historyItem) noexcept {
                         return ImVec2(static_cast<float>(historyItem.transform.translation().x()),
                                       static_cast<float>(historyItem.transform.translation().y()));
                       }) |
                       ranges::views::common;
    // ranges::to<std::vector> does not work in clang 17
    const auto points = std::vector<ImVec2>(ranges::begin(pointsRange), ranges::end(pointsRange));

    constexpr static float alpha = 0.8f;
    // views::adjacent and is not supported by clang 17 and std::views:drop is not compatible with range-v3
    const auto pointsm1 = std::vector<ImVec2>(std::next(ranges::begin(pointsRange)), ranges::end(pointsRange));
    for (const auto& [idx, coordinates] : ranges::views::zip(points, pointsm1) | ranges::views::enumerate) {
      const auto& [coordinate1, coordinate2] = coordinates;
      ImImage::Line(coordinate1, coordinate2,
                    tinycolormap::GetViridisColor(static_cast<double>(idx) / static_cast<double>(points.size()))
                        .as_imgui_color(alpha),
                    level * levelToThickness);
    }
  }
}

template <typename Scalar_>
void draw_ground_truth_optical_flow(
    const auto& prevKeypoints,
    const std::unordered_map<KeypointId, Eigen::Matrix<Scalar_, 2, 1>>& groundTruthKeypointIdxTransformMapping) {
  const ImU32 opaqueBlue = ImColor(ImVec4(0.4f, 0.4f, 1.0f, 0.8f));

  for (const auto& keypointDescriptor : prevKeypoints) {
    if (!groundTruthKeypointIdxTransformMapping.contains(keypointDescriptor.keypoint.id)) {
      continue;
    }

    const Eigen::Vector2f prevKeypointCoordinate =
        keypointDescriptor.keypoint.transform.translation().template cast<float>();
    const Eigen::Vector2f groundTruthKeypointCoordinate =
        groundTruthKeypointIdxTransformMapping.at(keypointDescriptor.keypoint.id).template cast<float>();
    const float level = static_cast<float>(keypointDescriptor.level) + 1.f;

    ImImage::Line(ImVec2(prevKeypointCoordinate.x(), prevKeypointCoordinate.y()),
                  ImVec2(groundTruthKeypointCoordinate.x(), groundTruthKeypointCoordinate.y()), opaqueBlue, level);
  }
}

void draw_keypoints(const auto& keypoints) {
  static constexpr float levelToLength = 2.2f;
  static constexpr float levelToThickness = 1.f;
  const ImU32 opaqueYellow = ImColor(ImVec4(1.0f, 1.0f, 0.4f, 0.8f));

  for (const auto& keypointDescriptor : keypoints) {
    const auto& transform = keypointDescriptor.keypoint.transform;
    const float level = static_cast<float>(keypointDescriptor.level) + 1.f;
    const auto coordinate =
        ImVec2(static_cast<float>(transform.translation().x()), static_cast<float>(transform.translation().y()));
    ImImage::Cross(coordinate, opaqueYellow, level * levelToThickness, level * levelToLength);
  }
}

template <std::size_t cameraIdx>
void draw_patches(const auto& keypoints) {
  const ImU32 opaqueBlue = ImColor(ImVec4(0.4f, 0.4f, 1.0f, 0.8f));

  for (const auto& keypointDescriptor : keypoints) {
    const auto& transform = keypointDescriptor.keypoint.transform.template cast<float>();
    const auto scale = static_cast<float>(1ul << keypointDescriptor.level);
    const auto patternCoordinates = ParameterizedPatchPattern<cameraIdx>::template type<float>::pattern2.colwise();

    for (const auto patternCoordinate :
         patternCoordinates | ranges::views::transform([&transform, &scale](const auto& coordinate) {
           const auto transformedCoordinate = transform * (scale * coordinate);
           return ImVec2(transformedCoordinate.x(), transformedCoordinate.y());
         })) {
      static constexpr float scaleToRadius = 0.5f;
      static constexpr float scaleToThickness = 0.1f;
      ImImage::Circle(patternCoordinate, scale * scaleToRadius, opaqueBlue, 0, scale * scaleToThickness);
    }
  }
}

void draw_ids(const auto& keypoints, const ImFont* font, const float fontSize) {
  const ImU32 yellow = ImColor(ImVec4(1.0f, 1.0f, 0.4f, 1.f));
  static constexpr float levelToFontSize = 1.5f;

  for (const auto& keypointDescriptor : keypoints) {
    const auto& transform = keypointDescriptor.keypoint.transform;
    const float level = static_cast<float>(keypointDescriptor.level) + 1.f;
    const auto coordinate =
        ImVec2(static_cast<float>(transform.translation().x()), static_cast<float>(transform.translation().y()));
    ImImage::Text(font, fontSize + level * levelToFontSize, coordinate, yellow,
                  fmt::format("{}", keypointDescriptor.keypoint.id).c_str());
  }
}

/** @} */  // end of gui

}  // namespace gneiss::gui

#endif  // GNEISS_IMAGE_OVERLAYS_H
