#ifndef GNEISS_PROFILINGSTATSSTATE_H
#define GNEISS_PROFILINGSTATSSTATE_H
#pragma once

#include "gneiss/gui/Redux.h"
#include "gneiss/profiling/ProfilingStats.h"
#include "gneiss/profiling/ProfilingTarget.h"

#include <fmt/core.h>

namespace gneiss::gui {

/**
 * @addtogroup gui
 *
 * @{
 */

struct ProfilingStatsState {
  ProfilingTarget target;
  std::vector<float> executionTimes_ms;
};

class AppendProfilingStats : public Action {
 public:
  explicit AppendProfilingStats(ProfilingStats inPayload) : payload(inPayload) {}

  [[nodiscard]] std::string to_string() const override {
    return fmt::format("AppendProfilingStats {{target: {}, exec time: {}ms}}", payload.target,
                       payload.executionTime.count());
  }

  // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
  ProfilingStats payload;
};

struct ProfilingStatsStateUpdater {
  template <typename State_>
  static void update(State_&, const gneiss::gui::Action&) {
    // do nothing
  }

  static void update(ProfilingStatsState& state, const gneiss::gui::Action& action) {
    if (const auto* const appendAction = dynamic_cast<const AppendProfilingStats*>(&action)) {
      if (state.target == appendAction->payload.target) {
        state.executionTimes_ms.push_back(static_cast<float>(appendAction->payload.executionTime.count()));
      }
    }
  }
};

/** @} */  // end of gui

}  // namespace gneiss::gui
#endif  // GNEISS_PROFILINGSTATSSTATE_H
