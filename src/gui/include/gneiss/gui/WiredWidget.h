#ifndef GNEISS_WIREDWIDGET_H
#define GNEISS_WIREDWIDGET_H
#pragma once

#include <vsg/nodes/Node.h>

#include <concepts>
#include <functional>

namespace gneiss::gui {

/**
 * @addtogroup gui
 *
 * @{
 */

/**
 * @brief Combines a rendering function with a provideArgs function
 *
 * If given a tuple-returning provideArgs Function it applies the values of the tuple to the rendering function.
 * That way the provideArgs can be an adapter to a gui state or default
 *
 * @tparam Properties_
 */
template <typename... Properties_>
class WiredWidget : public vsg::Inherit<vsg::Node, WiredWidget<Properties_...>> {
 public:
  WiredWidget(std::function<void(Properties_...)> widget, std::function<std::tuple<Properties_...>()> provideArgs)
      : m_widget(std::move(widget)), m_provideArgs(std::move(provideArgs)) {}

  void accept(vsg::RecordTraversal&) const override { std::apply(m_widget, m_provideArgs()); }

 private:
  std::function<void(Properties_...)> m_widget;
  std::function<std::tuple<Properties_...>()> m_provideArgs;
};

// template<typename >

/** @} */  // end of gui

}  // namespace gneiss::gui
#endif  // GNEISS_WIREDWIDGET_H
