#ifndef GNEISS_IMAGECOMMAND_H
#define GNEISS_IMAGECOMMAND_H
#pragma once

#include "gneiss/image/Image.h"

#include <imgui_internal.h>
#include <vsg/app/Viewer.h>
#include <vsg/app/Window.h>
#include <vsg/core/Inherit.h>
#include <vsg/io/Options.h>
#include <vsgImGui/Texture.h>
#include <vulkan/vulkan.h>

#include <list>

namespace gneiss::gui {

/**
 * @addtogroup gui
 *
 * @{
 */

class ImageCommand : public vsg::Inherit<vsg::Command, ImageCommand> {
 public:
  using Overlay = std::function<void()>;

  ImageCommand(std::string_view imageName, const vsg::ref_ptr<vsg::Viewer>& viewer,
               const vsg::ref_ptr<vsg::Options>& options);

  void showImage(const vsg::Path& filename);
  template <typename PixelType_>
  void showImage(const ImageReference<PixelType_>& imageReference);

  void compile(vsg::Context& context) override;
  void record(vsg::CommandBuffer& commandBuffer) const override;

  [[nodiscard]] uint32_t getHeight() const noexcept { return m_height; }

  [[nodiscard]] uint32_t getWidth() const noexcept { return m_width; }

  [[nodiscard]] ImVec2 getSize() const noexcept { return {static_cast<float>(m_width), static_cast<float>(m_height)}; }

  void addOverlay(Overlay overlay) { m_overlays.push_back(std::move(overlay)); }

 private:
  std::string m_imageName;
  vsg::ref_ptr<vsg::Viewer> m_viewer;
  vsg::ref_ptr<vsgImGui::Texture> m_texture;
  vsg::ref_ptr<vsg::Options> m_options;
  uint32_t m_height = 0;
  uint32_t m_width = 0;
  std::list<Overlay> m_overlays;
};

/** @} */  // end of gui

}  // namespace gneiss::gui
#endif  // GNEISS_IMAGECOMMAND_H
