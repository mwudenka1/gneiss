#ifndef GNEISS_CHECKBOXCONTROLWINDOW_H
#define GNEISS_CHECKBOXCONTROLWINDOW_H
#pragma once

#include <functional>
#include <optional>
#include <string>
#include <unordered_map>

namespace gneiss::gui {

/**
 * @addtogroup gui
 *
 * @{
 */

void checkbox_control(const char* label, bool value, std::optional<std::function<void(bool)>> setValue = {});

void checkbox_control_window(const char* windowName, const std::unordered_map<std::string, bool>& controls,
                             std::optional<std::function<void(const char*, bool)>> setValue);

/** @} */  // end of gui

}  // namespace gneiss::gui

#endif  // GNEISS_CHECKBOXCONTROLWINDOW_H
