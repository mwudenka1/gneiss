#ifndef GNEISS_DATASETSTATE_H
#define GNEISS_DATASETSTATE_H
#pragma once

#include "gneiss/gui/Redux.h"

#include <fmt/core.h>

namespace gneiss::gui {

/**
 * @addtogroup gui
 *
 * @{
 */

struct DatasetState {
  std::size_t currentFrame = 0;
  bool followExecution = true;
};

class SetCurrentFrame : public Action {
 public:
  explicit SetCurrentFrame(std::size_t inPayload) : payload(inPayload) {}

  [[nodiscard]] std::string to_string() const override {
    return fmt::format("SetCurentFrameState: {{currentFrame: {})}}", payload);
  }

  // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
  std::size_t payload;
};

class SetFollowExecution : public Action {
 public:
  explicit SetFollowExecution(bool inPayload) : payload(inPayload) {}

  [[nodiscard]] std::string to_string() const override { return fmt::format("SetFollowExecution: {}", payload); }

  // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
  bool payload;
};

struct DatasetStateUpdater {
  template <typename State_>
  static void update(State_&, const gneiss::gui::Action&) {
    // do nothing
  }

  static void update(DatasetState& state, const gneiss::gui::Action& action) {
    if (const auto* const currentFrameAction = dynamic_cast<const SetCurrentFrame*>(&action)) {
      state.currentFrame = currentFrameAction->payload;
    } else if (const auto* const followExecutionAction = dynamic_cast<const SetFollowExecution*>(&action)) {
      state.followExecution = followExecutionAction->payload;
    }
  }
};

/** @} */  // end of gui

}  // namespace gneiss::gui

#endif  // GNEISS_DATASETSTATE_H
