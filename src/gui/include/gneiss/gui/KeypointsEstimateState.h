#ifndef GNEISS_KEYPOINTSESTIMATESTATE_H
#define GNEISS_KEYPOINTSESTIMATESTATE_H
#pragma once

#include "gneiss/gui/Redux.h"
#include "gneiss/image/KeypointsEstimate.h"

#include <memory>
#include <vector>

namespace gneiss::gui {

/**
 * @addtogroup gui
 *
 * @{
 */

struct PatchHistoryItem {
  constexpr static std::string_view optical_flow_scalar =
      parameters::parameters["optical_flow_scalar"sv].template get<std::string_view>();
  using Scalar = string_to_type_t<FixedString<optical_flow_scalar.size()>(optical_flow_scalar.data())>;
  Eigen::Transform<Scalar, 2, Eigen::AffineCompact> transform;
  Timestamp timestamp;
};

using PatchHistory = std::vector<PatchHistoryItem>;

struct KeypointsEstimateState {
  constexpr static auto& cameras = parameters::parameters["cameras"sv].array_data();

  std::vector<std::shared_ptr<const KeypointsEstimate>> estimates;
  std::unordered_map<KeypointId, std::array<PatchHistory, cameras.size()>> idCameraPatchHistoryMapping;
};

class AppendKeypointEstimate : public Action {
 public:
  explicit AppendKeypointEstimate(const std::shared_ptr<const KeypointsEstimate>& inPayload) : payload(inPayload) {}

  [[nodiscard]] std::string to_string() const override { return "AppendKeypointEstimate"; }

  // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
  std::shared_ptr<const KeypointsEstimate> payload;
};

struct KeypointsEstimateStateUpdater {
  constexpr static auto& cameras = parameters::parameters["cameras"sv].array_data();

  template <typename State_>
  static void update(State_&, const gneiss::gui::Action&) {
    // do nothing
  }

  static void update(KeypointsEstimateState& state, const gneiss::gui::Action& action) {
    if (const auto* const appendAction = dynamic_cast<const AppendKeypointEstimate*>(&action)) {
      state.estimates.push_back(appendAction->payload);

      constexpr_for<0, cameras.size(), 1>([&](auto cameraIdx) {
        const auto& opticalFlow = appendAction->payload->get<cameraIdx>();
        for (const auto& keypointAtLevelDescriptor :
             opticalFlow | gneiss::ranges::views::filter([](const auto& descriptor) noexcept {
               return descriptor.keypoint.id != gneiss::unidentifiedKeypoint;
             })) {
          state.idCameraPatchHistoryMapping[keypointAtLevelDescriptor.keypoint.id][cameraIdx].push_back(
              PatchHistoryItem{.transform = keypointAtLevelDescriptor.keypoint.transform,
                               .timestamp = appendAction->payload->timestamp()});
        }
      });
    }
  }
};

/** @} */  // end of gui

}  // namespace gneiss::gui

#endif  // GNEISS_KEYPOINTSESTIMATESTATE_H
