#ifndef GNEISS_REDUX_H
#define GNEISS_REDUX_H
#pragma once
/** @file
 * Redux implementation with RCU
 */

#include "gneiss/utils/NamedTuple.h"
#include "gneiss/utils/Specializes.h"
#include "gneiss/utils/constant.h"

#include <crill/reclaim_object.h>
#include <oneapi/tbb/concurrent_queue.h>

#include <iostream>
#include <list>
#include <sstream>

namespace gneiss::gui {
/**
 * @addtogroup gui
 *
 * @{
 */

template <typename StateUpdater_, typename State_>
class StorePart {
 public:
  using StateType = std::remove_const_t<State_>;
  using StateUpdater = StateUpdater_;

  explicit StorePart(State_&& initialState) noexcept : m_initialState(std::forward<State_>(initialState)) {}

  [[nodiscard]] const State_& getInitialState() const { return m_initialState; }

 private:
  State_ m_initialState;
};

class Action {
 public:
  virtual ~Action() = default;
  [[nodiscard]] virtual std::string to_string() const = 0;
};

template <typename... StateUpdater_>
struct CombinedStateUpdater {
  template <typename State_>
  static void update(State_& state, const Action& action) {
    std::apply([&action](auto&... stateParts) { ((StateUpdater_::update(stateParts.value, action)), ...); },
               state.get_tuple());
  }
};

template <SpecializesWithType<Named>... NamedStoreParts_>
  requires((SpecializesWithType<typename NamedStoreParts_::WrappedType, StorePart>) && ...)
auto combine_store_parts(NamedStoreParts_... namedStoreParts) {
  using CombinedStateUpdaterType = CombinedStateUpdater<typename decltype(namedStoreParts.value)::StateUpdater...>;
  auto combinedInitialState =
      gneiss::make_named_tuple(gneiss::make_named<namedStoreParts.name>(namedStoreParts.value.getInitialState())...);

  return StorePart<CombinedStateUpdaterType, decltype(combinedInitialState)>{std::move(combinedInitialState)};
}

template <typename State_>
using ActionProcessor = std::function<void(const Action&)>;

template <typename State_>
using Middleware = std::function<ActionProcessor<State_>(const ActionProcessor<State_>&,
                                                         typename crill::reclaim_object<State_>::reader&)>;

template <typename StateUpdater_, typename State_>
class Store {
 public:
  using StateUpdaterType = StateUpdater_;
  using StateType = State_;
  using ReaderType = typename crill::reclaim_object<State_>::reader;
  using PointerType = typename crill::reclaim_object<State_>::read_ptr;

  explicit Store(const StorePart<StateUpdater_, State_>& rootStoreNode)
      : m_state(rootStoreNode.getInitialState()), m_internalReader(m_state.get_reader()) {
    m_actionProcessors.emplace_back([this](const Action& action) {
      auto stateWritePtr = this->m_state.write_lock();
      StateUpdater_::update(*stateWritePtr, action);
    });

    m_updateThread = std::make_unique<std::thread>([this]() {
      while (!shouldStop) {
        process_actions();
        static constexpr double updateFrequency_Hz = 100.;
        std::this_thread::sleep_for(
            std::chrono::duration<double, std::chrono::seconds::period>(1. / updateFrequency_Hz));
      }
    });

    m_garbageCollectionThread = std::make_unique<std::thread>([this]() {
      while (!shouldStop) {
        m_state.reclaim();
        static constexpr double garbageCollectionFrequency_Hz = 100.;
        std::this_thread::sleep_for(
            std::chrono::duration<double, std::chrono::seconds::period>(1. / garbageCollectionFrequency_Hz));
      }
    });
  }

  ~Store() {
    shouldStop = true;
    m_updateThread->join();
    m_garbageCollectionThread->join();
  }

  void dispatch(std::unique_ptr<Action> action) { m_actionQueue.push(std::move(action)); }

  ReaderType get_reader() { return m_state.get_reader(); }

  void register_middleware(const Middleware<State_>& middleware) {
    const auto actionProcessor = middleware(m_actionProcessors.back(), m_internalReader);
    m_actionProcessors.push_back(actionProcessor);
  }

  void process_actions() {
    std::unique_ptr<const Action> action;
    while (m_actionQueue.try_pop(action)) {
      m_actionProcessors.back()(*action);
    }
  }

 private:
  std::list<ActionProcessor<State_>> m_actionProcessors;
  tbb::concurrent_queue<std::unique_ptr<const Action>> m_actionQueue;
  crill::reclaim_object<State_> m_state;
  ReaderType m_internalReader;
  std::unique_ptr<std::thread> m_updateThread;
  std::unique_ptr<std::thread> m_garbageCollectionThread;
  std::atomic<bool> shouldStop = false;
};

template <typename State_>
auto logger_middleware(std::ostream& outStream) {
  return [&outStream](const ActionProcessor<State_>& next,
                      typename crill::reclaim_object<State_>::reader& stateReader) -> ActionProcessor<State_> {
    return [&outStream, &next, &stateReader](const Action& action) {
      std::stringstream actionOutStream;
      actionOutStream << "---------------------------" << '\n';
      actionOutStream << "State before update:\n" << *(stateReader.read_lock()) << '\n';
      actionOutStream << "Applying action " << action.to_string() << '\n';

      next(action);

      actionOutStream << "State after update:\n" << *(stateReader.read_lock()) << '\n';
      actionOutStream << "---------------------------" << '\n';

      outStream << actionOutStream.str();
    };
  };
}

/** @} */  // end of gui

}  // namespace gneiss::gui

#endif  // GNEISS_REDUX_H
