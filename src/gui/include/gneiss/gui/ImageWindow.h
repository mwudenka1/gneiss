#ifndef GNEISS_IMAGEWINDOW2_H
#define GNEISS_IMAGEWINDOW2_H
#pragma once

#include "gneiss/gui/DatasetState.h"
#include "gneiss/gui/ImageCommand.h"
#include "gneiss/io/Dataset.h"

#include <functional>
#include <list>
#include <optional>

namespace gneiss::gui {

namespace internal {
struct ImageWindowState : public vsg::Inherit<vsg::Object, ImageWindowState> {
  std::size_t currentFrame = 0;
};
}  // namespace internal

/**
 * @addtogroup gui
 *
 * @{
 */

/**
 * @brief An ImGui window that renders an image with overlays. Supports zoom and shift.
 *
 */
class ImageWindow : public vsg::Inherit<vsg::Command, ImageWindow> {
 public:
  ImageWindow(const vsg::ref_ptr<vsg::Viewer>& viewer, const vsg::ref_ptr<vsg::Options>& options,
              const std::shared_ptr<std::vector<std::unique_ptr<io::CameraMeasurement>>>& cameraMeasurements,
              const std::optional<std::function<DatasetState()>>& getDatasetState = {},
              const std::string& windowName = "Image Window");

  void showImage(const vsg::Path& filename) const;
  template <typename PixelType_>
  void showImage(const ImageReference<PixelType_>& imageReference) const;
  void compile(vsg::Context& context) override;
  void record(vsg::CommandBuffer& commandBuffer) const override;

  void addOverlay(ImageCommand::Overlay overlay) { m_image->addOverlay(std::move(overlay)); }

 private:
  vsg::ref_ptr<ImageCommand> m_image;
  std::string m_windowName;
  vsg::ref_ptr<internal::ImageWindowState> m_state;
  std::shared_ptr<std::vector<std::unique_ptr<io::CameraMeasurement>>> m_cameraMeasurements;
  std::optional<std::function<DatasetState()>> m_getDatasetState;
};

/** @} */  // end of gui

}  // namespace gneiss::gui

#endif  // GNEISS_IMAGEWINDOW2_H
