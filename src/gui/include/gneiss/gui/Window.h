#ifndef GNEISS_WINDOW_H
#define GNEISS_WINDOW_H
#pragma once

// TODO(martinwudenka) create PR to vsg as Path is not self contained
#include <vsg/core/Object.h>
#include <vector>

#include <imgui.h>
#include <vsg/app/Viewer.h>
#include <vsg/app/WindowTraits.h>
#include <vsg/core/ref_ptr.h>
#include <vsg/io/Options.h>
#include <vsg/io/Path.h>
#include <vsg/nodes/Node.h>
#include <vsg/nodes/StateGroup.h>
#include <vsgImGui/RenderImGui.h>

#include <filesystem>
#include <functional>
#include <string_view>

namespace gneiss::gui {

namespace internal {
class ImGuiDockspace : public vsg::Inherit<vsg::Node, ImGuiDockspace> {
 public:
  void accept(vsg::RecordTraversal&) const override {
    ImGui::DockSpaceOverViewport(0, ImGui::GetMainViewport(), ImGuiDockNodeFlags_PassthruCentralNode);
  }
};
}  // namespace internal

/**
 * @addtogroup gui
 *
 * @{
 */

/**
 * Builds up a VulkanSceneGraphWindow and initializes an ImGui dock space
 */
class Window {
 public:
  Window(int argc, char** argv, const vsg::ref_ptr<vsg::Options>& options, std::string_view windowTitle = "gneiss");

  void add_imgui_component(const vsg::ref_ptr<vsg::Node>& component) { m_imguiRenderer->addChild(component); }
  void add_scene_component(const vsg::ref_ptr<vsg::Node>& component) { m_scene->addChild(component); }

  vsg::ref_ptr<vsg::Viewer> get_viewer() { return m_viewer; }
  vsg::ref_ptr<vsg::Window> get_window() { return m_window; }

  void show();

  ~Window() noexcept;

  ImFont* get_font() { return m_font; }

  [[nodiscard]] float get_font_size() const { return m_fontSize; }

  void add_before_frame_hook(std::function<void()> hook) { m_beforeFrameHooks.emplace_back(std::move(hook)); }

  void add_after_frame_hook(std::function<void()> hook) { m_afterFrameHooks.emplace_back(std::move(hook)); }

 private:
  void load_font();
  void load_ini();

  vsg::ref_ptr<vsg::Options> m_options;
  vsg::ref_ptr<vsg::WindowTraits> m_windowTraits;
  vsg::ref_ptr<vsg::StateGroup> m_scene;
  vsg::ref_ptr<vsg::Camera> m_camera;
  vsg::ref_ptr<vsg::Window> m_window;
  vsg::ref_ptr<vsgImGui::RenderImGui> m_imguiRenderer;
  vsg::ref_ptr<vsg::Viewer> m_viewer;
  vsg::Path m_fontFile;
  std::filesystem::path m_iniFile;
  ImFont* m_font = nullptr;
  float m_fontSize;
  std::vector<std::function<void()>> m_beforeFrameHooks;
  std::vector<std::function<void()>> m_afterFrameHooks;
};

/** @} */  // end of gui

}  // namespace gneiss::gui
#endif  // GNEISS_WINDOW_H
