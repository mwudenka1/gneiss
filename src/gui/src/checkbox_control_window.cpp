#include "gneiss/gui/checkbox_control_window.h"

#include <imgui.h>

#include <functional>
#include <optional>
#include <string>
#include <unordered_map>

void gneiss::gui::checkbox_control(const char* label, bool value, std::optional<std::function<void(bool)>> setValue) {
  auto new_value = value;
  ImGui::Checkbox(label, &new_value);
  if (value != new_value && setValue.has_value()) {
    setValue.value()(new_value);
  }
}

void gneiss::gui::checkbox_control_window(const char* windowName, const std::unordered_map<std::string, bool>& controls,
                                          std::optional<std::function<void(const char*, bool)>> setValue) {
  ImGui::Begin(windowName);
  for (const auto& [controlName, controlValue] : controls) {
    if (setValue) {
      checkbox_control(controlName.c_str(), controlValue, std::bind_front(setValue.value(), controlName.c_str()));
    } else {
      checkbox_control(controlName.c_str(), controlValue);
    }
  }
  ImGui::End();
}
