#include "gneiss/gui/IntersectionHandler.h"

#include <vsg/app/Camera.h>
#include <vsg/core/ref_ptr.h>
#include <vsg/nodes/Group.h>
#include <vsg/ui/PointerEvent.h>
#include <vsg/utils/LineSegmentIntersector.h>

#include <algorithm>
#include <iostream>
#include <string>

gneiss::gui::IntersectionHandler::IntersectionHandler(const vsg::ref_ptr<vsg::Camera>& camera,
                                                      const vsg::ref_ptr<vsg::Group>& scenegraph)
    : m_camera(camera), m_scenegraph(scenegraph) {}

void gneiss::gui::IntersectionHandler::apply(vsg::ButtonPressEvent& buttonPressEvent) {
  if (buttonPressEvent.button == 1) {
    intersection(buttonPressEvent);
  }
}

void gneiss::gui::IntersectionHandler::intersection(vsg::PointerEvent& pointerEvent) const {
  auto intersector = vsg::LineSegmentIntersector::create(*m_camera, pointerEvent.x, pointerEvent.y);
  m_scenegraph->accept(*intersector);

  if (m_verbose) {
    std::cout << "intersection(" << pointerEvent.x << ", " << pointerEvent.y << ") "
              << intersector->intersections.size() << ")" << '\n';
  }

  if (intersector->intersections.empty()) {
    return;
  }

  // sort the intersectors front to back
  std::sort(intersector->intersections.begin(), intersector->intersections.end(),
            [](auto& lhs, auto& rhs) { return lhs->ratio < rhs->ratio; });

  for (auto& intersection : intersector->intersections) {
    if (m_verbose) {
      std::cout << "intersection = world(" << intersection->worldIntersection << "), instanceIndex "
                << intersection->instanceIndex;
    }

    if (m_verbose) {
      std::string name;
      for (auto& node : intersection->nodePath) {
        std::cout << ", " << node->className();
        if (node->getValue("name", name)) {
          std::cout << ":name=" << name;
        }
      }

      std::cout << ", Arrays[ ";
      for (auto& array : intersection->arrays) {
        std::cout << array << " ";
      }
      std::cout << "] [";
      for (auto& indexRatio : intersection->indexRatios) {
        std::cout << "{" << indexRatio.index << ", " << indexRatio.ratio << "} ";
      }
      std::cout << "]";

      std::cout << '\n';
    }
  }
}