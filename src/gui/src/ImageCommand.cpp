#include "gneiss/gui/ImageCommand.h"

#include "gneiss/gui/imimage.h"
#include "gneiss/image/Image.h"
#include "gneiss/utils/ranges.h"

#include <vsg/app/Viewer.h>
#include <vsg/core/Array2D.h>
#include <vsg/core/Data.h>
#include <vsg/core/ref_ptr.h>
#include <vsg/io/Options.h>
#include <vsg/io/Path.h>
#include <vsg/io/read.h>
#include <vsg/state/Sampler.h>
#include <vsg/vk/CommandBuffer.h>
#include <vsg/vk/Context.h>
#include <vsgImGui/Texture.h>
#include <vulkan/vulkan_core.h>

#include <cstddef>
#include <cstdint>
#include <iostream>
#include <limits>
#include <string_view>

namespace {

auto getDefaultSampler() {
  auto sampler = vsg::Sampler::create();
  sampler->maxLod = 9.0;  // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
  sampler->addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
  sampler->addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
  sampler->addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
  sampler->magFilter = VK_FILTER_NEAREST;
  sampler->minFilter = VK_FILTER_NEAREST;
  return sampler;
}

}  // namespace

gneiss::gui::ImageCommand::ImageCommand(const std::string_view imageName, const vsg::ref_ptr<vsg::Viewer>& viewer,
                                        const vsg::ref_ptr<vsg::Options>& options)
    : m_imageName(imageName), m_viewer(viewer), m_options(options) {}

void gneiss::gui::ImageCommand::showImage(const vsg::Path& filename) {
  auto texData = vsg::read_cast<vsg::Data>(filename, m_options);
  m_texture = vsgImGui::Texture::create_if(texData != nullptr, texData, getDefaultSampler());
  if (!m_texture) {
    return;
  }
  m_height = m_texture->height;
  m_width = m_texture->width;

  if (!m_viewer->compileManager) {
    std::cout << "WARN! Call viewer::compile first!" << '\n';
    return;
  }
  m_viewer->compileManager->compile(vsg::ref_ptr(this));
}

template <typename PixelType_>
void gneiss::gui::ImageCommand::showImage(const ImageReference<PixelType_>& imageReference) {
  auto texData = vsg::ubvec4Array2D::create(imageReference.getWidth(), imageReference.getHeight(),
                                            vsg::Data::Properties{VK_FORMAT_R8G8B8A8_UNORM});

  for (const auto xIdx : gneiss::ranges::views::iota(std::size_t{0}, imageReference.getWidth())) {
    for (const auto yIdx : gneiss::ranges::views::iota(std::size_t{0}, imageReference.getHeight())) {
      const auto brightness = static_cast<uint8_t>(imageReference(xIdx, yIdx));
      auto& rgbaAtIdx = (*texData)(static_cast<uint32_t>(xIdx), static_cast<uint32_t>(yIdx));
      // NOLINTBEGIN(cppcoreguidelines-pro-type-union-access)
      rgbaAtIdx.r = brightness;
      rgbaAtIdx.g = brightness;
      rgbaAtIdx.b = brightness;
      rgbaAtIdx.a = std::numeric_limits<uint8_t>::max();
      // NOLINTEND(cppcoreguidelines-pro-type-union-access)
    }
  }

  m_texture = vsgImGui::Texture::create(texData, getDefaultSampler());

  m_height = m_texture->height;
  m_width = m_texture->width;

  if (!m_viewer->compileManager) {
    std::cout << "WARN! Call viewer::compile first!" << '\n';
    return;
  }
  m_viewer->compileManager->compile(vsg::ref_ptr(this));
}

template void gneiss::gui::ImageCommand::showImage<uint8_t>(const ImageReference<uint8_t>&);
template void gneiss::gui::ImageCommand::showImage<uint16_t>(const ImageReference<uint16_t>&);

void gneiss::gui::ImageCommand::compile(vsg::Context& context) {
  if (m_texture) {
    m_texture->compile(context);
  }
}

void gneiss::gui::ImageCommand::record(vsg::CommandBuffer& commandBuffer) const {
  if (!m_texture) {
    return;
  }

  ImImage::BeginImage(m_imageName.c_str(), m_texture->id(commandBuffer.deviceID), getSize());
  for (const auto& overlay : m_overlays) {
    overlay();
  }
  ImImage::EndImage();
}
