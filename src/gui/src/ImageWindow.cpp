#include "gneiss/gui/ImageWindow.h"

#include "gneiss/gui/DatasetState.h"
#include "gneiss/gui/ImageCommand.h"
#include "gneiss/image/Image.h"
#include "gneiss/io/CameraMeasurement.h"

#include <imgui.h>
#include <vsg/app/Viewer.h>
#include <vsg/core/ref_ptr.h>
#include <vsg/io/Options.h>
#include <vsg/io/Path.h>
#include <vsg/vk/CommandBuffer.h>
#include <vsg/vk/Context.h>

#include <functional>
#include <memory>
#include <optional>
#include <string>
#include <vector>

gneiss::gui::ImageWindow::ImageWindow(
    const vsg::ref_ptr<vsg::Viewer>& viewer, const vsg::ref_ptr<vsg::Options>& options,
    const std::shared_ptr<std::vector<std::unique_ptr<gneiss::io::CameraMeasurement>>>& cameraMeasurements,
    const std::optional<std::function<gneiss::gui::DatasetState()>>& getDatasetState, const std::string& windowName)
    : m_image(ImageCommand::create(windowName + "_image", viewer, options)),
      m_windowName(windowName),
      m_state(gneiss::gui::internal::ImageWindowState::create()),
      m_cameraMeasurements(cameraMeasurements),
      m_getDatasetState(getDatasetState) {
  showImage(m_cameraMeasurements->at(m_state->currentFrame)->getImagePath().generic_string());
}

void gneiss::gui::ImageWindow::showImage(const vsg::Path& filename) const { m_image->showImage(filename); }

template <typename PixelType_>
void gneiss::gui::ImageWindow::showImage(const ImageReference<PixelType_>& imageReference) const {
  m_image->showImage(imageReference);
}

template void gneiss::gui::ImageWindow::showImage<uint8_t>(const ImageReference<uint8_t>&) const;
template void gneiss::gui::ImageWindow::showImage<uint16_t>(const ImageReference<uint16_t>&) const;

void gneiss::gui::ImageWindow::compile(vsg::Context& context) { m_image->compile(context); }

void gneiss::gui::ImageWindow::record(vsg::CommandBuffer& commandBuffer) const {
  if (m_getDatasetState && m_getDatasetState.value()().currentFrame != m_state->currentFrame) {
    m_state->currentFrame = m_getDatasetState.value()().currentFrame;
    showImage(m_cameraMeasurements->at(m_state->currentFrame)->getImagePath().generic_string());
  }

  ImGui::GetIO().ConfigWindowsMoveFromTitleBarOnly = true;
  ImGui::Begin(m_windowName.c_str(), nullptr, ImGuiWindowFlags_NoScrollbar);
  // ImGui::SetItemKeyOwner(ImGuiKey_MouseWheelY);

  m_image->record(commandBuffer);

  ImGui::End();
}
