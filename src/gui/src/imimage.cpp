#include "gneiss/gui/imimage.h"
#include "gneiss/gui/imimage_internal.h"

#include <imgui.h>
#include <imgui_internal.h>

#include <algorithm>
#include <cmath>
#include <span>
#include <vector>

namespace ImImage {

// Global image context
#ifndef GImImage
// NOLINTNEXTLINE(cppcoreguidelines-avoid-non-const-global-variables)
ImImageContext* GImImage = nullptr;
#endif

void CreateContext() {
  auto* ctx = IM_NEW(ImImageContext)();
  if (GImImage == nullptr) {
    SetCurrentContext(ctx);
  }
}

void DestroyContext() {
  ImImageContext* ctx = GImImage;
  SetCurrentContext(nullptr);
  IM_DELETE(ctx);
}

void SetCurrentContext(ImImageContext* ctx) { GImImage = ctx; }

void BeginImage(const char* title_id, void* user_texture_id, const ImVec2& size, const ImVec4& tint_col,
                const ImVec4& border_col) {
  IM_ASSERT_USER_ERROR(GImImage != nullptr, "No current context. Did you call ImImage::CreateContext()?");
  ImImageContext& imImageContext = *GImImage;
  IM_ASSERT_USER_ERROR(imImageContext.CurrentImage == nullptr, "Mismatched BeginImage()/EndImage()!");

  // get globals
  const ImGuiContext& imguiContext = *GImGui;
  ImGuiWindow* Window = imguiContext.CurrentWindow;

  const ImGuiID imageID = Window->GetID(title_id);
  imImageContext.CurrentImage = imImageContext.Images.GetOrAddByKey(imageID);

  ImImageImage& image = *imImageContext.CurrentImage;
  image.ID = imageID;

  ImGui::SetItemKeyOwner(ImGuiKey_MouseWheelY);

  image.size_pixel = size;
  const auto availableSpace = ImGui::GetWindowContentRegionMax() - ImGui::GetWindowContentRegionMin();
  const auto scaleFactorWidth = availableSpace.x / size.x;
  const auto scaleFactorHeight = availableSpace.y / size.y;
  const auto scaleFactor = std::min(scaleFactorWidth, scaleFactorHeight);

  const auto scaledSize = size * scaleFactor * image.zoomFactor;

  const auto screenSize =
      ImVec2(std::min<float>(scaledSize.x, availableSpace.x), std::min<float>(scaledSize.y, availableSpace.y));

  image.scaleFactor = screenSize / size;

  image.normalizedCutoutSize = screenSize / scaledSize;
  const auto maxAllowedShift = ImVec2(0.5, 0.5) - image.normalizedCutoutSize / 2.f;

  image.viewCenter = ImVec2(std::clamp(image.viewCenter.x, 0.5f - maxAllowedShift.x, 0.5f + maxAllowedShift.x),
                            std::clamp(image.viewCenter.y, 0.5f - maxAllowedShift.y, 0.5f + maxAllowedShift.y));

  const auto normalizedRequestedShift = image.viewCenter - image.normalizedDraggedDistance - ImVec2(0.5f, 0.5f);
  image.normalizedShiftFromCenter =
      ImVec2(std::clamp(normalizedRequestedShift.x, -maxAllowedShift.x, maxAllowedShift.x),
             std::clamp(normalizedRequestedShift.y, -maxAllowedShift.y, maxAllowedShift.y));

  const auto uv1Centered = (ImVec2(1, 1) + image.normalizedCutoutSize) * 0.5f;
  const auto uv0Centered = ImVec2(1, 1) - uv1Centered;
  const auto uv0 = uv0Centered + image.normalizedShiftFromCenter;
  const auto uv1 = uv1Centered + image.normalizedShiftFromCenter;

  ImGui::SetCursorPosX(ImGui::GetCursorPosX() +
                       (ImGui::GetContentRegionAvail().x - screenSize.x) * 0.5f);  // center image
  image.topLeftScreenCoordinate = ImGui::GetCursorScreenPos();
  ImGui::Image(user_texture_id, screenSize, uv0, uv1, tint_col, border_col);

  if (ImGui::IsItemHovered()) {
    // handle zooming
    if (ImGui::GetIO().MouseWheel != 0) {
      image.zoom += ImGui::GetIO().MouseWheel;
      if (image.zoom <= 0) {
        image.zoom = 0;
        image.zoomFactor = 1.f;
      } else {
        // cursor should point to same pixel as before zooming -> feels natural to user
        // strategy:
        // 1. calculate vector from center of visible part to pixel hovered by mouse cursor
        // 2. update zoom level
        // 3. calculate vector from center of visible part to previously hovered pixel after new zoom applied
        // 4. correct viewCenter by difference of those two
        auto cursorScreenPosition = (ImGui::GetMousePos() - image.topLeftScreenCoordinate);
        const auto centerToHovered =
            (cursorScreenPosition / screenSize - ImVec2(0.5f, 0.5f)) * image.normalizedCutoutSize;

        static constexpr float zoomDamping = 0.1f;
        image.zoomFactor = std::exp(zoomDamping * image.zoom);

        const auto nextScaledSize = size * image.scaleFactor * image.zoomFactor;
        const auto nextScreenSize = ImVec2(std::min<float>(nextScaledSize.x, availableSpace.x),
                                           std::min<float>(nextScaledSize.x, availableSpace.y));
        const auto nextNormalizedCutoutSize = nextScreenSize / nextScaledSize;

        const auto centerToHoveredNext =
            (cursorScreenPosition / nextScreenSize - ImVec2(0.5f, 0.5f)) * nextNormalizedCutoutSize;

        image.viewCenter -= centerToHoveredNext - centerToHovered;
      }
    }

    // handle drag
    if (ImGui::IsMouseDragging(ImGuiMouseButton_Left)) {
      image.normalizedDraggedDistance = ImGui::GetMouseDragDelta() / scaledSize;
    } else if (image.normalizedDraggedDistance.x != 0.f || image.normalizedDraggedDistance.y != 0.f) {
      image.viewCenter -= image.normalizedDraggedDistance;
      image.normalizedDraggedDistance = ImVec2(0.f, 0.f);
    }
  }

  image.totalSizeFactor = image.zoomFactor * std::min(image.scaleFactor.x, image.scaleFactor.y);
}

void EndImage() {
  IM_ASSERT_USER_ERROR(GImImage != nullptr, "No current context. Did you call ImImage::CreateContext()?");
  ImImageContext& imImageContext = *GImImage;
  IM_ASSERT_USER_ERROR(imImageContext.CurrentImage != nullptr, "Mismatched BeginImage()/EndImage()!");

  imImageContext.CurrentImage = nullptr;
}

// Overlays

namespace {
ImVec2 toScreenCoordinates(const ImVec2& imageCoordinates) {
  const ImImageContext& imImageContext = *GImImage;
  IM_ASSERT_USER_ERROR(imImageContext.CurrentImage != nullptr,
                       "Overlays can only be drawn between BeginImage and EndImage!");

  const ImImageImage& image = *imImageContext.CurrentImage;

  static constexpr auto pixelCenterPoint = ImVec2(0.5f, 0.5f);  // top left of pixel is (0,0) bottom right is (1,1)

  const auto relativeDistanceToCenter =
      (imageCoordinates + pixelCenterPoint - (image.size_pixel * 0.5f)) / image.size_pixel;

  return image.topLeftScreenCoordinate +
         image.size_pixel * image.scaleFactor *
             (ImVec2(0.5f, 0.5f) +
              (relativeDistanceToCenter - image.normalizedShiftFromCenter) / image.normalizedCutoutSize);
}
}  // namespace

void Line(const ImVec2& start, const ImVec2& end, ImU32 col, float thickness) {
  const ImImageContext& imImageContext = *GImImage;
  IM_ASSERT_USER_ERROR(imImageContext.CurrentImage != nullptr,
                       "Overlays can only be drawn between BeginImage and EndImage!");

  const ImImageImage& image = *imImageContext.CurrentImage;

  ImDrawList* draw_list = ImGui::GetWindowDrawList();

  draw_list->AddLine(toScreenCoordinates(start), toScreenCoordinates(end), col, thickness * image.totalSizeFactor);
}

// NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
void Cross(const ImVec2& center, const ImU32 col, const float thickness, const float scale) {
  const auto line1Start = center + ImVec2(-0.5f, -0.5f) * scale;
  const auto line1End = center + ImVec2(+0.5f, +0.5f) * scale;
  const auto line2Start = center + ImVec2(+0.5f, -0.5f) * scale;
  const auto line2End = center + ImVec2(-0.5f, +0.5f) * scale;

  Line(line1Start, line1End, col, thickness);
  Line(line2Start, line2End, col, thickness);
}

void Polyline(const std::span<const ImVec2> points, const ImU32 col, const ImDrawFlags flags, const float thickness) {
  const ImImageContext& imImageContext = *GImImage;
  IM_ASSERT_USER_ERROR(imImageContext.CurrentImage != nullptr,
                       "Overlays can only be drawn between BeginImage and EndImage!");

  const ImImageImage& image = *imImageContext.CurrentImage;

  ImDrawList* draw_list = ImGui::GetWindowDrawList();

  std::vector<ImVec2> transformedPoints;
  transformedPoints.reserve(points.size());
  for (const auto& point : points) {
    transformedPoints.push_back(toScreenCoordinates(point));
  }

  draw_list->AddPolyline(transformedPoints.data(), static_cast<int>(transformedPoints.size()), col, flags,
                         thickness * image.totalSizeFactor);
}

void Circle(const ImVec2& center, const float radius, const ImU32 col, const int num_segments, const float thickness) {
  const ImImageContext& imImageContext = *GImImage;
  IM_ASSERT_USER_ERROR(imImageContext.CurrentImage != nullptr,
                       "Overlays can only be drawn between BeginImage and EndImage!");

  const ImImageImage& image = *imImageContext.CurrentImage;

  ImDrawList* draw_list = ImGui::GetWindowDrawList();

  draw_list->AddCircle(toScreenCoordinates(center), radius * image.totalSizeFactor, col, num_segments,
                       thickness * image.totalSizeFactor);
}

void Text(const ImFont* font, float font_size, const ImVec2& pos, ImU32 col, const char* text_begin,
          const char* text_end) {
  const ImImageContext& imImageContext = *GImImage;
  IM_ASSERT_USER_ERROR(imImageContext.CurrentImage != nullptr,
                       "Overlays can only be drawn between BeginImage and EndImage!");

  const ImImageImage& image = *imImageContext.CurrentImage;

  ImDrawList* draw_list = ImGui::GetWindowDrawList();

  if (font == nullptr) {
    font = ImGui::GetFont();
  }

  draw_list->AddText(font, font_size * image.totalSizeFactor, toScreenCoordinates(pos), col, text_begin, text_end);
}

}  // namespace ImImage
