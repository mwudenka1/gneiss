#include "gneiss/gui/dataset_control_window.h"

#include "gneiss/gui/DatasetState.h"

#include <imgui.h>

#include <algorithm>
#include <cstddef>
#include <functional>
#include <optional>

void gneiss::gui::dataset_control_window(const DatasetState& datasetState, std::size_t numberFrames,
                                         std::optional<std::function<void(std::size_t)>> setCurrentFrame,
                                         std::optional<std::function<void(bool)>> setFollowExecution) {
  ImGui::Begin("Dataset Control");
  ImGui::SetNextFrameWantCaptureKeyboard(true);
  // NOLINTNEXTLINE(cppcoreguidelines-pro-type-vararg,hicpp-vararg)
  ImGui::Text("Frame:");
  ImGui::PushButtonRepeat(true);
  if (ImGui::ArrowButton("##left", ImGuiDir_Left) || ImGui::IsKeyPressed(ImGuiKey_LeftArrow)) {
    if (setCurrentFrame) {
      setCurrentFrame.value()(std::max<std::size_t>(1, datasetState.currentFrame) - 1);
    }
  }
  const auto arrow_button_width = ImGui::GetWindowWidth() - ImGui::GetContentRegionAvail().x;
  ImGui::SameLine();
  auto newCurrentFrame = static_cast<int>(datasetState.currentFrame);
  ImGui::PushItemWidth(ImGui::GetContentRegionAvail().x - 2 * arrow_button_width);
  ImGui::SliderInt("##frame slider", &newCurrentFrame, 0, static_cast<int>(numberFrames) - 1);
  if (static_cast<int>(datasetState.currentFrame) != newCurrentFrame) {
    if (setCurrentFrame) {
      setCurrentFrame.value()(static_cast<std::size_t>(newCurrentFrame));
    }
  }
  ImGui::SameLine();
  if (ImGui::ArrowButton("##right", ImGuiDir_Right) || ImGui::IsKeyPressed(ImGuiKey_RightArrow)) {
    if (setCurrentFrame) {
      setCurrentFrame.value()(std::min<std::size_t>(numberFrames - 1, datasetState.currentFrame + 1));
    }
  }
  ImGui::PopButtonRepeat();

  auto newFollowExecution = datasetState.followExecution;
  ImGui::Checkbox("Follow Execution", &newFollowExecution);
  if (datasetState.followExecution != newFollowExecution && setFollowExecution.has_value()) {
    setFollowExecution.value()(newFollowExecution);
  }

  ImGui::End();
}