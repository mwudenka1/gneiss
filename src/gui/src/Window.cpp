#include "gneiss/gui/Window.h"

#include "gneiss/gui/IntersectionHandler.h"
#include "gneiss/gui/imimage.h"

#include <imgui.h>
#include <vsg/app/Camera.h>
#include <vsg/app/CloseHandler.h>
#include <vsg/app/CommandGraph.h>
#include <vsg/app/ProjectionMatrix.h>
#include <vsg/app/RenderGraph.h>
#include <vsg/app/Trackball.h>
#include <vsg/app/View.h>
#include <vsg/app/ViewMatrix.h>
#include <vsg/app/Viewer.h>
#include <vsg/app/WindowTraits.h>
#include <vsg/core/Exception.h>
#include <vsg/core/ref_ptr.h>
#include <vsg/io/FileSystem.h>
#include <vsg/io/Path.h>
#include <vsg/maths/vec3.h>
#include <vsg/nodes/StateGroup.h>
#include <vsg/state/ViewportState.h>
#include <vsg/ui/UIEvent.h>
#include <vsg/utils/CommandLine.h>
#include <vsg/utils/ComputeBounds.h>
#include <vsg/utils/SharedObjects.h>
#include <vsgImGui/RenderImGui.h>
#include <vsgImGui/SendEventsToImGui.h>

#include <chrono>
#include <filesystem>
#include <iostream>
#include <string>
#include <string_view>
#include <thread>

gneiss::gui::Window::Window(int argc, char** argv, const vsg::ref_ptr<vsg::Options>& options,
                            std::string_view windowTitle)
    : m_options(options),
      m_windowTraits(vsg::WindowTraits::create()),
      m_scene(vsg::StateGroup::create()),
      m_camera(vsg::Camera::create()) {
  // set up defaults and read command line arguments to override them
  m_options->sharedObjects = vsg::SharedObjects::create();
  m_options->fileCache = vsg::getEnv("VSG_FILE_CACHE");
  m_options->paths = vsg::getEnvPaths("VSG_FILE_PATH");

  m_windowTraits->windowTitle = windowTitle;

  // set up defaults and read command line arguments to override them
  vsg::CommandLine arguments(&argc, argv);
  arguments.read(m_options);

  auto event_read_filename = arguments.value(std::string(""), "-i");
  auto event_output_filename = arguments.value(std::string(""), "-o");

  m_windowTraits->debugLayer = arguments.read({"--debug", "-d"});
  m_windowTraits->apiDumpLayer = arguments.read({"--api", "-a"});
  arguments.read("--screen", m_windowTraits->screenNum);
  arguments.read("--display", m_windowTraits->display);
  m_fontFile = arguments.value<vsg::Path>("assets/fonts/ubuntu/Ubuntu-R.ttf", "--font");
  static constexpr float defaultFontSize = 16.f;
  m_fontSize = arguments.value<float>(defaultFontSize, "--font-size");

  if (arguments.errors()) {
    arguments.writeErrorMessages(std::cerr);
    return;
  }

  try {
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    m_windowTraits->width = 1280;
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
    m_windowTraits->height = 720;
    m_window = vsg::Window::create(m_windowTraits);
    if (!m_window) {
      std::cout << "Could not create windows." << '\n';
      return;
    }

    // initialize ImGui
    ImGui::CreateContext();
    ImGuiIO& guiIo = ImGui::GetIO();
    guiIo.ConfigInputTrickleEventQueue = false;
    load_font();
    load_ini();

    // create the normal 3D view of the scene
    auto view = vsg::View::create(m_camera);
    // view->addChild(vsg::createHeadlight());
    view->addChild(m_scene);

    m_imguiRenderer = vsgImGui::RenderImGui::create(m_window);
    ImImage::CreateContext();
    m_imguiRenderer->addChild(internal::ImGuiDockspace::create());
    // NOLINTNEXTLINE(hicpp-signed-bitwise)
    ImGui::GetIO().ConfigFlags |= ImGuiConfigFlags_DockingEnable;

    // The commandGraph will contain a 2 stage renderGraph 1) 3D scene 2) ImGui (by default also includes clear
    // depth buffers)
    auto renderGraph = vsg::RenderGraph::create(m_window);
    renderGraph->addChild(view);
    renderGraph->addChild(m_imguiRenderer);

    auto commandGraph = vsg::CommandGraph::create(m_window);
    commandGraph->addChild(renderGraph);

    // create the viewer and assign window(s) to it
    m_viewer = vsg::Viewer::create();
    m_viewer->addWindow(m_window);
    // Add the ImGui event handler first to handle events early
    m_viewer->addEventHandler(vsgImGui::SendEventsToImGui::create());

    // add close handler to respond the close window button and pressing escape
    m_viewer->addEventHandler(vsg::CloseHandler::create(m_viewer));

    m_viewer->addEventHandler(gneiss::gui::IntersectionHandler::create(m_camera, m_scene));

    m_viewer->assignRecordAndSubmitTaskAndPresentation({commandGraph});

    m_viewer->compile();
  } catch (const vsg::Exception& ve) {
    std::cerr << "[Exception] - " << ve.message << '\n';
  }
}

void gneiss::gui::Window::show() {
  try {
    m_viewer->compile();

    // compute the bounds of the scene graph to help position camera
    vsg::ComputeBounds computeBounds;
    m_scene->accept(computeBounds);
    const vsg::dvec3 centre = (computeBounds.bounds.min + computeBounds.bounds.max) * 0.5;
    const double radius = vsg::length(computeBounds.bounds.max - computeBounds.bounds.min) * 0.6;

    // These are set statically because the geometry in the class is expanded in the shader
    const double nearFarRatio = 0.01;

    // set up the camera
    const auto cameraDistanceFromScene = radius * 3.5;
    auto lookAt =
        vsg::LookAt::create(centre + vsg::dvec3(0.0, -cameraDistanceFromScene, 0.0), centre, vsg::dvec3(0.0, 0.0, 1.0));

    vsg::ref_ptr<vsg::ProjectionMatrix> perspective;
    static constexpr double fieldOfView_deg = 30.;
    const double aspectRatio =
        static_cast<double>(m_window->extent2D().width) / static_cast<double>(m_window->extent2D().height);
    const double nearClipping = nearFarRatio * radius;
    const double farClipping = radius * 400.5;

    perspective = vsg::Perspective::create(fieldOfView_deg, aspectRatio, nearClipping, farClipping);

    m_camera->projectionMatrix = perspective;
    m_camera->viewMatrix = lookAt;
    m_camera->viewportState = vsg::ViewportState::create(m_window->extent2D());

    m_viewer->addEventHandler(vsg::Trackball::create(m_camera));

    // rendering main loop
    while (m_viewer->advanceToNextFrame()) {
      auto startTime = vsg::clock::now();
      {
        for (const auto& hook : m_beforeFrameHooks) {
          hook();
        }

        m_viewer->handleEvents();

        m_viewer->update();

        m_viewer->recordAndSubmit();

        m_viewer->present();
        for (const auto& hook : m_afterFrameHooks) {
          hook();
        }
      }
      static constexpr double fps = 124.;
      auto duration = std::chrono::duration<double, std::chrono::seconds::period>(vsg::clock::now() - startTime);
      std::this_thread::sleep_for(std::chrono::duration<double, std::chrono::seconds::period>(1. / fps) - duration);
    }

  } catch (const vsg::Exception& ve) {
    std::cerr << "[Exception] - " << ve.message << '\n';
  }
}

gneiss::gui::Window::~Window() noexcept { ImImage::DestroyContext(); }

void gneiss::gui::Window::load_font() {
  if (m_fontFile) {
    auto foundFontFile = vsg::findFile(m_fontFile, m_options);
    if (foundFontFile) {
      // convert native filename to UTF8 string that is compatible with ImuGUi.
      const std::string c_fontFile = foundFontFile.string();

      // read the font via ImGui, which will then be current when vsgImGui::RenderImGui initializes the rest of
      // ImGui/Vulkan below
      ImGuiIO& guiIo = ImGui::GetIO();
      m_font = guiIo.Fonts->AddFontFromFileTTF(c_fontFile.c_str(), m_fontSize);
      if (m_font == nullptr) {
        std::cout << "Failed to load font: " << c_fontFile << '\n';
      }
    }
  }
}

void gneiss::gui::Window::load_ini() {
  // TODO(martinwudenka) only works on linux
  // see https://stackoverflow.com/a/1528493
  m_iniFile = std::filesystem::canonical("/proc/self/exe").parent_path() / "imgui.ini";

  if (std::filesystem::exists(m_iniFile)) {
    ImGuiIO& guiIo = ImGui::GetIO();
    guiIo.IniFilename = m_iniFile.c_str();
  }
}