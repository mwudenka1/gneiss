#ifndef GNEISS_PARAMETERS_H
#define GNEISS_PARAMETERS_H
#pragma once

#include "gneiss/parameters/generated_impl.hpp"

namespace gneiss::parameters {

/**
 * @addtogroup parameters
 *
 * @{
 */

using namespace std::literals;

constexpr const static auto &parameters = compiled_json::parameters::impl::document;

/** @} */  // end of parameters

}  // namespace gneiss::parameters

#endif  // GNEISS_PARAMETERS_H
