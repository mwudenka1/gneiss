#ifndef GNEISS_STORAGEIMPLEMENTATION_H
#define GNEISS_STORAGEIMPLEMENTATION_H
#pragma once
/** @file */

#include "gneiss/Storage/Storage.h"
#include "gneiss/parameters/parameters.h"

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

class StorageImplementation : public Storage<StorageImplementation> {
 public:
  void shutdownImpl() {}

  KeypointsEstimatePtr storeKeypointsEstimateImpl(const KeypointsEstimate& estimate) {
    return keypointsArena.emplace(estimate);
  }

  ImageMeasurementPtr storeImageMeasurementImpl(const ImageMeasurement& measurement) {
    return imageArena.emplace(measurement);
  }

 private:
  ConcurrentArena<ImageMeasurement, parameters::parameters.at("image_measurement_arena_capacity"sv).get<std::size_t>()>
      imageArena;
  ConcurrentArena<KeypointsEstimate, parameters::parameters.at("keypoints_arena_capacity"sv).get<std::size_t>()>
      keypointsArena;
};

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_STORAGEIMPLEMENTATION_H
