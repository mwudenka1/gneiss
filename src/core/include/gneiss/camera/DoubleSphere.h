#ifndef GNEISS_DOUBLESPHERE_H
#define GNEISS_DOUBLESPHERE_H
#pragma once
/** @file */

#include "gneiss/utils/ConditionalTuple.h"

#include <Eigen/Dense>
#include <sophus/common.hpp>

#include <concepts>
#include <cstddef>
#include <optional>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

/**
 * @brief Double Sphere camera model
 *
 * See https://cvg.cit.tum.de/_media/spezial/bib/usenko18double-sphere.pdf
 *
 * This model has N=6 parameters \f$ \mathbf{i} = \left[f_x, f_y, c_x, c_y, \xi, \alpha \right]^T \f$ with \f$ \xi \in
 * [-1,1], \alpha \in [0,1] \f$.
 */
template <std::floating_point Scalar_>
class DoubleSphere {
 public:
  constexpr static std::size_t numberOfParameter = 6;

  using Vector2 = Eigen::Matrix<Scalar_, 2, 1>;
  using Vector3 = Eigen::Matrix<Scalar_, 3, 1>;
  using VectorNParams = Eigen::Matrix<Scalar_, numberOfParameter, 1>;
  using Matrix2x3 = Eigen::Matrix<Scalar_, 2, 3>;
  using Matrix3x2 = Eigen::Matrix<Scalar_, 3, 2>;
  using Matrix2xNParams = Eigen::Matrix<Scalar_, 2, numberOfParameter>;
  using Matrix3xNParams = Eigen::Matrix<Scalar_, 3, numberOfParameter>;

  explicit DoubleSphere(const VectorNParams& params) : m_params(params) {}

  /**
   * @brief Project the point and (optionally) compute Jacobians with respect to point and/ or params.
   *
   * Projection function is defined as follows:
   * \f{align}{
   *    \pi(\mathbf{x}, \mathbf{i}) &=
   *    \begin{bmatrix}
   *    f_x{\frac{x}{\alpha d_2 + (1-\alpha) (\xi d_1 + z)}}
   *    \\ f_y{\frac{y}{\alpha d_2 + (1-\alpha) (\xi d_1 + z)}}
   *    \\ \end{bmatrix}
   *    +
   *    \begin{bmatrix}
   *    c_x
   *    \\ c_y
   *    \\ \end{bmatrix},
   *    \\ d_1 &= \sqrt{x^2 + y^2 + z^2},
   *    \\ d_2 &= \sqrt{x^2 + y^2 + (\xi  d_1 + z)^2}.
   * \f}
   *
   * A set of 3D points that results in valid projection is expressed as
   * follows: \f{align}{
   *    \Omega &= \{\mathbf{x} \in \mathbb{R}^3 ~|~ z > -w_2 d_1 \}
   *    \\ w_2 &= \frac{w_1+\xi}{\sqrt{2w_1\xi + \xi^2 + 1}}
   *    \\ w_1 &= \begin{cases} \frac{\alpha}{1-\alpha}, & \mbox{if } \alpha
   *    \le 0.5 \\ \frac{1-\alpha}{\alpha} & \mbox{if } \alpha > 0.5
   *    \end{cases}
   * \f}
   *
   * Code taken from
   * https://gitlab.com/VladyslavUsenko/basalt-headers/-/blob/master/include/basalt/camera/double_sphere_camera.hpp?ref_type=heads
   *
   * @tparam diffByPoint_ Compute the derivative with respect to the input point
   * @tparam diffByParams_ Compute the derivative with respect to the params
   * @tparam DerivedPoint3D_ Eigen type of input. Will be deduced.
   * @param pointToProject Point which will be projected
   * @return Empty std::optional if point is invalid
   *         else projection or tuple with projection and respective Jacobians
   */
  template <bool diffByPoint_ = false, bool diffByParams_ = false, typename DerivedPoint3D_>
  [[nodiscard]] std::optional<typename ConditionalTuple<Typelist<Vector2, Matrix2x3, Matrix2xNParams>,
                                                        Boollist<true, diffByPoint_, diffByParams_>>::type>
  project(const Eigen::MatrixBase<DerivedPoint3D_>& pointToProject) const {
    using std::sqrt;

    const auto& x = pointToProject[0];  // NOLINT(readability-identifier-length)
    const auto& y = pointToProject[1];  // NOLINT(readability-identifier-length)
    const auto& z = pointToProject[2];  // NOLINT(readability-identifier-length)

    const auto& fx = m_params[0];  // NOLINT(readability-identifier-length)
    const auto& fy = m_params[1];  // NOLINT(readability-identifier-length)
    const auto& cx = m_params[2];  // NOLINT(readability-identifier-length)
    const auto& cy = m_params[3];  // NOLINT(readability-identifier-length)
    const auto& xi = m_params[4];  // NOLINT(readability-identifier-length)
    const auto& alpha = m_params[5];

    const Scalar_ xx = x * x;  // NOLINT(readability-identifier-length)
    const Scalar_ yy = y * y;  // NOLINT(readability-identifier-length)
    const Scalar_ zz = z * z;  // NOLINT(readability-identifier-length)

    const Scalar_ r2 = xx + yy;

    const Scalar_ d1_2 = r2 + zz;
    const Scalar_ d1 = sqrt(d1_2);

    const Scalar_ w1 = alpha > Scalar_{0.5} ? (Scalar_{1} - alpha) / alpha : alpha / (Scalar_{1} - alpha);
    const Scalar_ w2 = (w1 + xi) / sqrt(Scalar_{2} * w1 * xi + xi * xi + Scalar_{1});

    if (z <= -w2 * d1) {
      return {};
    }

    const Scalar_ k = xi * d1 + z;
    const Scalar_ kk = k * k;

    const Scalar_ d2_2 = r2 + kk;
    const Scalar_ d2 = sqrt(d2_2);

    const Scalar_ norm = alpha * d2 + (Scalar_{1} - alpha) * k;

    const Scalar_ mx = x / norm;
    const Scalar_ my = y / norm;

    const auto projection = Vector2(fx * mx + cx, fy * my + cy);

    if constexpr (!(diffByPoint_ or diffByParams_)) {
      return projection;  // NOLINT(performance-no-automatic-move)
    } else {
      typename ConditionalTuple<Typelist<Vector2, Matrix2x3, Matrix2xNParams>,
                                Boollist<true, diffByPoint_, diffByParams_>>::type result;
      std::get<Vector2>(result) = projection;

      if constexpr (diffByPoint_) {
        const Scalar_ norm2 = norm * norm;
        const Scalar_ xy = x * y;
        const Scalar_ tt2 = xi * z / d1 + Scalar_{1};

        const Scalar_ d_norm_d_r2 = (xi * (Scalar_{1} - alpha) / d1 + alpha * (xi * k / d1 + Scalar_{1}) / d2) / norm2;

        const Scalar_ tmp2 = ((Scalar_{1} - alpha) * tt2 + alpha * k * tt2 / d2) / norm2;

        std::get<Matrix2x3>(result) = Matrix2x3({
            // clang-format off
            {fx * (Scalar_{1} / norm - xx * d_norm_d_r2),  -fx * xy * d_norm_d_r2                     , -fx * x * tmp2},
            {-fy * xy * d_norm_d_r2,                       fy * (Scalar_{1} / norm - yy * d_norm_d_r2), -fy * y * tmp2}
            // clang-format on
        });
      }

      if constexpr (diffByParams_) {
        const Scalar_ norm2 = norm * norm;
        const Scalar_ tmp4 = (alpha - Scalar_{1} - alpha * k / d2) * d1 / norm2;
        const Scalar_ tmp5 = (k - d2) / norm2;

        std::get<Matrix2xNParams>(result) = Matrix2xNParams({
            // clang-format off
            {mx, 0,  1, 0, fx * x * tmp4, fx * x * tmp5},
            {0,  my, 0, 1, fy * y * tmp4, fy * y * tmp5}
            // clang-format on
        });
      }
      return result;
    }
  }

  /**
   * @brief Unroject the point and (optionally) compute Jacobians with respect to point and/ or params.
   *
   * The unprojection function is computed as follows: \f{align}{
   *    \pi^{-1}(\mathbf{u}, \mathbf{i}) &=
   *    \frac{m_z \xi + \sqrt{m_z^2 + (1 - \xi^2) r^2}}{m_z^2 + r^2}
   *    \begin{bmatrix}
   *    m_x \\ m_y \\m_z
   *    \\ \end{bmatrix}-\begin{bmatrix}
   *    0 \\ 0 \\ \xi
   *    \\ \end{bmatrix},
   *    \\ m_x &= \frac{u - c_x}{f_x},
   *    \\ m_y &= \frac{v - c_y}{f_y},
   *    \\ r^2 &= m_x^2 + m_y^2,
   *    \\ m_z &= \frac{1 - \alpha^2  r^2}{\alpha  \sqrt{1 - (2 \alpha - 1)
   *    r^2}
   *    + 1 - \alpha},
   * \f}
   *
   * The valid range of unprojections is \f{align}{
   *    \Theta &= \begin{cases}
   *    \mathbb{R}^2 & \mbox{if } \alpha \le 0.5
   *    \\ \{ \mathbf{u} \in \mathbb{R}^2 ~|~ r^2 \le \frac{1}{2\alpha-1} \}  &
   *    \mbox{if} \alpha > 0.5 \end{cases}
   * \f}
   *
   * Code taken from
   * https://gitlab.com/VladyslavUsenko/basalt-headers/-/blob/master/include/basalt/camera/double_sphere_camera.hpp?ref_type=heads
   *
   * @tparam diffByPoint_ Compute the derivative with respect to the input point
   * @tparam diffByParams_ Compute the derivative with respect to the params
   * @tparam DerivedPoint3D_ Eigen type of input. Will be deduced.
   * @param pointToUnproject Point which will be projected
   * @return Empty std::optional if point is invalid - never the case for the Pinhole camera
   *         else unprojection (bearing vector) or tuple with unprojection and respective Jacobians
   */
  template <bool diffByPoint_ = false, bool diffByParams_ = false, typename DerivedPoint2D_>
  [[nodiscard]] std::optional<typename ConditionalTuple<Typelist<Vector3, Matrix3x2, Matrix3xNParams>,
                                                        Boollist<true, diffByPoint_, diffByParams_>>::type>
  unproject(const Eigen::MatrixBase<DerivedPoint2D_>& pointToUnproject) const {
    const auto& fx = m_params[0];  // NOLINT(readability-identifier-length)
    const auto& fy = m_params[1];  // NOLINT(readability-identifier-length)
    const auto& cx = m_params[2];  // NOLINT(readability-identifier-length)
    const auto& cy = m_params[3];  // NOLINT(readability-identifier-length)
    const auto& xi = m_params[4];  // NOLINT(readability-identifier-length)
    const auto& alpha = m_params[5];

    const auto& u = pointToUnproject[0];  // NOLINT(readability-identifier-length)
    const auto& v = pointToUnproject[1];  // NOLINT(readability-identifier-length)

    const Scalar_ mx = (u - cx) / fx;
    const Scalar_ my = (v - cy) / fy;

    const Scalar_ r2 = mx * mx + my * my;

    if (alpha > Scalar_{0.5} && r2 > Scalar_{1} / (Scalar_{2} * alpha - Scalar_{1})) {
      return {};
    }

    const Scalar_ xi2_2 = alpha * alpha;
    const Scalar_ xi1_2 = xi * xi;

    const Scalar_ sqrt2 = sqrt(Scalar_{1} - (Scalar_{2} * alpha - Scalar_{1}) * r2);

    const Scalar_ norm2 = alpha * sqrt2 + Scalar_{1} - alpha;

    const Scalar_ mz = (Scalar_{1} - xi2_2 * r2) / norm2;
    const Scalar_ mz2 = mz * mz;

    const Scalar_ norm1 = mz2 + r2;
    const Scalar_ sqrt1 = sqrt(mz2 + (Scalar_{1} - xi1_2) * r2);
    const Scalar_ k = (mz * xi + sqrt1) / norm1;

    const auto unprojection = Vector3(k * mx, k * my, k * mz - xi);

    if constexpr (!(diffByPoint_ or diffByParams_)) {
      return unprojection;  // NOLINT(performance-no-automatic-move)
    } else {
      typename ConditionalTuple<Typelist<Vector3, Matrix3x2, Matrix3xNParams>,
                                Boollist<true, diffByPoint_, diffByParams_>>::type result;
      std::get<Vector3>(result) = unprojection;

      const Scalar_ norm2_2 = norm2 * norm2;
      const Scalar_ norm1_2 = norm1 * norm1;

      const Scalar_ d_mz_d_r2 =
          (Scalar_{0.5} * alpha - xi2_2) * (r2 * xi2_2 - Scalar_{1}) / (sqrt2 * norm2_2) - xi2_2 / norm2;

      const Scalar_ d_mz_d_mx = 2 * mx * d_mz_d_r2;
      const Scalar_ d_mz_d_my = 2 * my * d_mz_d_r2;

      const Scalar_ d_k_d_mz = (norm1 * (xi * sqrt1 + mz) - 2 * mz * (mz * xi + sqrt1) * sqrt1) / (norm1_2 * sqrt1);

      const Scalar_ d_k_d_r2 =
          (xi * d_mz_d_r2 + Scalar_{0.5} / sqrt1 * (Scalar_{2} * mz * d_mz_d_r2 + Scalar_{1} - xi1_2)) / norm1 -
          (mz * xi + sqrt1) * (Scalar_{2} * mz * d_mz_d_r2 + Scalar_{1}) / norm1_2;

      const Scalar_ d_k_d_mx = d_k_d_r2 * 2 * mx;
      const Scalar_ d_k_d_my = d_k_d_r2 * 2 * my;

      const Vector3 c0 = Vector3(mx * d_k_d_mx + k, my * d_k_d_mx, mz * d_k_d_mx + k * d_mz_d_mx) / fx;

      const auto c1 = Vector3(mx * d_k_d_my, my * d_k_d_my + k, mz * d_k_d_my + k * d_mz_d_my) / fy;

      if constexpr (diffByPoint_) {
        std::get<Matrix3x2>(result).col(0) = c0;
        std::get<Matrix3x2>(result).col(1) = c1;
      }

      if constexpr (diffByParams_) {
        const Scalar_ d_k_d_xi1 = (mz * sqrt1 - xi * r2) / (sqrt1 * norm1);

        const Scalar_ d_mz_d_xi2 =
            ((Scalar_{1} - r2 * xi2_2) * (r2 * alpha / sqrt2 - sqrt2 + Scalar_{1}) / norm2 - Scalar_{2} * r2 * alpha) /
            norm2;

        const Scalar_ d_k_d_xi2 = d_k_d_mz * d_mz_d_xi2;

        const Vector3 nc0_mx = -c0 * mx;
        const Vector3 nc1_my = -c1 * my;

        std::get<Matrix3xNParams>(result) = Matrix3xNParams({
            // clang-format off
            {nc0_mx(0), nc1_my(0), -c0(0), -c1(0), mx * d_k_d_xi1,     mx * d_k_d_xi2},
            {nc0_mx(1), nc1_my(1), -c0(1), -c1(1), my * d_k_d_xi1,     my * d_k_d_xi2},
            {nc0_mx(2), nc1_my(2), -c0(2), -c1(2), mz * d_k_d_xi1 - 1, mz * d_k_d_xi2 + k * d_mz_d_xi2},
            // clang-format on
        });
      }

      return result;
    }
  }

 private:
  VectorNParams m_params;
};

/** @} */  // end of core

}  // namespace gneiss
#endif  // GNEISS_DOUBLESPHERE_H
