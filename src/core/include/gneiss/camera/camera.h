#ifndef GNEISS_CAMERA_H
#define GNEISS_CAMERA_H
#pragma once
/** @file */

#include <Eigen/Core>

#include <concepts>
#include <cstddef>
#include <optional>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

/**
 * @brief Requirements for a type to be a camera: Have a project and unproject member function.
 */
template <typename Type_>
concept camera = requires(Type_ camera, Eigen::Vector3d p3d, Eigen::Vector3d p2d) {
  { Type_::numberOfParameter } -> std::convertible_to<std::size_t>;
  { camera.project(p3d) } -> std::convertible_to<std::optional<Eigen::Vector2d>>;
  { camera.unproject(p2d) } -> std::convertible_to<std::optional<Eigen::Vector3d>>;
};

/** @} */  // end of core

}  // namespace gneiss
#endif  // GNEISS_CAMERA_H
