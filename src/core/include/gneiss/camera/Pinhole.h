#ifndef GNEISS_PINHOLE_H
#define GNEISS_PINHOLE_H
#pragma once
/** @file */

#include "gneiss/utils/ConditionalTuple.h"

#include <Eigen/Dense>
#include <sophus/common.hpp>

#include <concepts>
#include <cstddef>
#include <optional>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

/**
 * @brief Pinhole camera model
 *
 * See https://en.wikipedia.org/wiki/Pinhole_camera_model
 *
 * This model has 4 parameter: \f$ \mathbf{i} = \left[f_x, f_y, c_x, c_y \right]^T \f$.
 */
template <std::floating_point Scalar_>
class Pinhole {
 public:
  constexpr static std::size_t numberOfParameter = 4;

  using Vector2 = Eigen::Matrix<Scalar_, 2, 1>;
  using Vector3 = Eigen::Matrix<Scalar_, 3, 1>;
  using VectorNParams = Eigen::Matrix<Scalar_, numberOfParameter, 1>;
  using Matrix2x3 = Eigen::Matrix<Scalar_, 2, 3>;
  using Matrix3x2 = Eigen::Matrix<Scalar_, 3, 2>;
  using Matrix2xNParams = Eigen::Matrix<Scalar_, 2, numberOfParameter>;
  using Matrix3xNParams = Eigen::Matrix<Scalar_, 3, numberOfParameter>;

  explicit Pinhole(const VectorNParams& params) : m_params(params) {}

  /**
   * @brief Project the point and (optionally) compute Jacobians with respect to point and/ or params.
   *
   * Projection function is defined as follows:
   * \f{align}{
   *    \pi(\mathbf{x}, \mathbf{i}) &=
   *    \begin{bmatrix}
   *    f_x{\frac{x}{z}}
   *    \\ f_y{\frac{y}{z}}
   *    \\ \end{bmatrix}
   *    +
   *    \begin{bmatrix}
   *    c_x
   *    \\ c_y
   *    \\ \end{bmatrix}.
   * \f}
   * A set of 3D points that results in valid projection is expressed as
   * follows: \f{align}{
   *    \Omega &= \{\mathbf{x} \in \mathbb{R}^3 ~|~ z > 0 \}
   * \f}
   *
   * Code taken from
   * https://gitlab.com/VladyslavUsenko/basalt-headers/-/blob/master/include/basalt/camera/pinhole_camera.hpp?ref_type=heads
   *
   * @tparam diffByPoint_ Compute the derivative with respect to the input point
   * @tparam diffByParams_ Compute the derivative with respect to the params
   * @tparam DerivedPoint3D_ Eigen type of input. Will be deduced.
   * @param pointToProject Point which will be projected
   * @return Empty std::optional if point is invalid
   *         else projection or tuple with projection and respective Jacobians
   */
  template <bool diffByPoint_ = false, bool diffByParams_ = false, typename DerivedPoint3D_>
  [[nodiscard]] std::optional<typename ConditionalTuple<Typelist<Vector2, Matrix2x3, Matrix2xNParams>,
                                                        Boollist<true, diffByPoint_, diffByParams_>>::type>
  project(const Eigen::MatrixBase<DerivedPoint3D_>& pointToProject) const {
    const auto& z = pointToProject[2];  // NOLINT(readability-identifier-length)

    if (z < Sophus::Constants<Scalar_>::epsilonSqrt()) {
      return {};
    }

    const auto& x = pointToProject[0];  // NOLINT(readability-identifier-length)
    const auto& y = pointToProject[1];  // NOLINT(readability-identifier-length)

    const auto& fx = m_params[0];  // NOLINT(readability-identifier-length)
    const auto& fy = m_params[1];  // NOLINT(readability-identifier-length)
    const auto& cx = m_params[2];  // NOLINT(readability-identifier-length)
    const auto& cy = m_params[3];  // NOLINT(readability-identifier-length)

    const auto projection = Vector2(fx * x / z + cx, fy * y / z + cy);

    if constexpr (!(diffByPoint_ or diffByParams_)) {
      return projection;  // NOLINT(performance-no-automatic-move)
    } else {
      typename ConditionalTuple<Typelist<Vector2, Matrix2x3, Matrix2xNParams>,
                                Boollist<true, diffByPoint_, diffByParams_>>::type result;
      std::get<Vector2>(result) = projection;

      if constexpr (diffByPoint_) {
        const auto z2 = z * z;  // NOLINT(readability-identifier-length)
        std::get<Matrix2x3>(result) = Matrix2x3({
            // clang-format off
            {fx / z,  0,       -fx * x / z2},
            {0,       fy / z,  -fy * y / z2}
            // clang-format on
        });
      }

      if constexpr (diffByParams_) {
        std::get<Matrix2xNParams>(result) = Matrix2xNParams({
            // clang-format off
            {x / z,  0,      1,  0},
            {0,      y / z,  0,  1}  // clang-format on
        });
      }
      return result;
    }
  }

  /**
   * @brief Unroject the point and (optionally) compute Jacobians with respect to point and/ or params.
   *
   * The unprojection function is computed as follows: \f{align}{
   *    \pi^{-1}(\mathbf{u}, \mathbf{i}) &=
   *    \frac{1}{\sqrt{m_x^2 + m_y^2 + 1}}
   *    \begin{bmatrix}
   *    m_x \\ m_y \\ 1
   *    \\ \end{bmatrix}
   *    \\ m_x &= \frac{u - c_x}{f_x},
   *    \\ m_y &= \frac{v - c_y}{f_y}.
   * \f}
   *
   * Code taken from
   * https://gitlab.com/VladyslavUsenko/basalt-headers/-/blob/master/include/basalt/camera/pinhole_camera.hpp?ref_type=heads
   *
   * @tparam diffByPoint_ Compute the derivative with respect to the input point
   * @tparam diffByParams_ Compute the derivative with respect to the params
   * @tparam DerivedPoint3D_ Eigen type of input. Will be deduced.
   * @param pointToUnproject Point which will be projected
   * @return Empty std::optional if point is invalid - never the case for the Pinhole camera
   *         else unprojection (bearing vector) or tuple with unprojection and respective Jacobians
   */
  template <bool diffByPoint_ = false, bool diffByParams_ = false, typename DerivedPoint2D_>
  [[nodiscard]] std::optional<typename ConditionalTuple<Typelist<Vector3, Matrix3x2, Matrix3xNParams>,
                                                        Boollist<true, diffByPoint_, diffByParams_>>::type>
  unproject(const Eigen::MatrixBase<DerivedPoint2D_>& pointToUnproject) const {
    const auto& fx = m_params[0];  // NOLINT(readability-identifier-length)
    const auto& fy = m_params[1];  // NOLINT(readability-identifier-length)
    const auto& cx = m_params[2];  // NOLINT(readability-identifier-length)
    const auto& cy = m_params[3];  // NOLINT(readability-identifier-length)

    const auto& u = pointToUnproject[0];  // NOLINT(readability-identifier-length)
    const auto& v = pointToUnproject[1];  // NOLINT(readability-identifier-length)

    const Scalar_ mx = (u - cx) / fx;  // NOLINT(readability-identifier-length)
    const Scalar_ my = (v - cy) / fy;  // NOLINT(readability-identifier-length)

    const Scalar_ r2 = mx * mx + my * my;

    const Scalar_ norm = sqrt(Scalar_{1} + r2);
    const Scalar_ normInv = Scalar_{1} / norm;

    const auto unprojection = Vector3(mx * normInv, my * normInv, normInv);

    if constexpr (!(diffByPoint_ or diffByParams_)) {
      return unprojection;  // NOLINT(performance-no-automatic-move)
    } else {
      typename ConditionalTuple<Typelist<Vector3, Matrix3x2, Matrix3xNParams>,
                                Boollist<true, diffByPoint_, diffByParams_>>::type result;
      std::get<Vector3>(result) = unprojection;

      const Scalar_ d_normInv_d_r2 = -Scalar_{0.5} * normInv * normInv * normInv;
      const auto c0 = Vector3((normInv + Scalar_{2} * mx * mx * d_normInv_d_r2) / fx,
                              (Scalar_{2} * my * mx * d_normInv_d_r2) / fx, Scalar_{2} * mx * d_normInv_d_r2 / fx);

      const auto c1 =
          Vector3((Scalar_{2} * my * mx * d_normInv_d_r2) / fy, (normInv + Scalar_{2} * my * my * d_normInv_d_r2) / fy,
                  Scalar_{2} * my * d_normInv_d_r2 / fy);

      if constexpr (diffByPoint_) {
        std::get<Matrix3x2>(result).col(0) = c0;
        std::get<Matrix3x2>(result).col(1) = c1;
      }

      if constexpr (diffByParams_) {
        std::get<Matrix3xNParams>(result).col(0) = -c0 * mx;
        std::get<Matrix3xNParams>(result).col(1) = -c1 * my;
        std::get<Matrix3xNParams>(result).col(2) = -c0;
        std::get<Matrix3xNParams>(result).col(3) = -c1;
      }

      return result;
    }
  }

 private:
  VectorNParams m_params;
};

/** @} */  // end of core

}  // namespace gneiss
#endif  // GNEISS_PINHOLE_H
