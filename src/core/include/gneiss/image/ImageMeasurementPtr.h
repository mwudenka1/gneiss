#ifndef GNEISS_IMAGEMEASUREMENTPTR_H
#define GNEISS_IMAGEMEASUREMENTPTR_H
#pragma once
/** @file */

#include "gneiss/image/ImageMeasurement.h"
#include "gneiss/utils/ConcurrentArena.h"

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

using ImageMeasurementPtr = std::unique_ptr<ImageMeasurement, ArenaPtrDeleter<ImageMeasurement>>;

static_assert(std::is_move_constructible_v<ImageMeasurementPtr>);
static_assert(std::is_move_assignable_v<ImageMeasurementPtr>);

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_IMAGEMEASUREMENTPTR_H
