#ifndef GNEISS_KEYPOINTS_H
#define GNEISS_KEYPOINTS_H
#pragma once
/** @file */

#include "gneiss/image/KeypointDescriptorAtLevel.h"
#include "gneiss/image/detect_keypoints.h"
#include "gneiss/utils/StaticVector.h"

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

template <std::floating_point Scalar_, GridPyramidParameters parameters_, std::size_t upToLevel_>
using Keypoints = StaticVector<KeypointDescriptorAtLevel<Scalar_>, keypoint_number<parameters_, upToLevel_>>;

/** @} */  // end of core

}  // namespace gneiss
#endif  // GNEISS_KEYPOINTS_H
