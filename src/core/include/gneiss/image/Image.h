#ifndef GNEISS_IMAGE_H
#define GNEISS_IMAGE_H
#pragma once
/** @file */

#include "gneiss/fields-2d/FieldCoordinate.h"
#include "gneiss/fields-2d/RowsView.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/fields-2d/copy.h"
#include "gneiss/fields-2d/in_bounds.h"
#include "gneiss/utils/assert.h"
#include "gneiss/utils/next_pow2.h"
#include "gneiss/utils/parallelize.h"
#include "gneiss/utils/ranges.h"
#include "gneiss/utils/unused.h"

#include <Eigen/Dense>

#include <algorithm>
#include <array>
#include <cmath>
#include <concepts>
#include <cstddef>
#include <cstdint>
#include <memory>

// TODO(mwudenka) use concepts for PixelType_

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

struct NoInterpolation {};
struct LinearInterpolation {};
static constexpr LinearInterpolation interpolate_linear{};

/**
 * @brief A pixel image representation on the stack
 *
 * Image is a class template that represents a monochrome pixel image. It
 * doesn't use any heap allocation. The data is not compressed.
 *
 * The right hand coordinate system origin is located at the top left corner:
 *
 * @code
 *     x
 *   +----> ... width
 * y |
 *   |
 *   |
 *   v
 *
 *   ... height
 * @endcode
 *
 * @tparam PixelType_ the data type of one pixel
 * @tparam width_ the width in pixel of the image
 * @tparam height_ the height in pixel of the image
 * @tparam pitched_ if true the data will be padded so that every row starts at
 * a power of 2 address
 */
template <typename PixelType_, std::size_t width_, std::size_t height_, bool pitched_ = false>
class Image {
 public:
  using PixelType = PixelType_;
  constexpr static std::size_t width = width_;
  constexpr static std::size_t height = height_;
  constexpr static bool pitched = pitched_;

  /**
   * @brief number of values in a row including padding
   */
  static constexpr std::size_t pitch = pitched_ ? next_pow2(width_) : width_;

 private:
  std::array<PixelType_, pitch * height_> m_data{};

 public:
  using scalar_type = PixelType_&;
  using value_type = typename decltype(m_data)::value_type;
  using difference_type = typename decltype(m_data)::difference_type;
  using size_type = typename decltype(m_data)::size_type;
  using pointer = typename decltype(m_data)::pointer;
  using reference = typename decltype(m_data)::reference;
  using const_pointer = typename decltype(m_data)::const_pointer;
  using const_reference = typename decltype(m_data)::const_reference;
  using iterator = typename decltype(m_data)::iterator;
  using reverse_iterator = typename decltype(m_data)::reverse_iterator;
  using const_iterator = typename decltype(m_data)::const_iterator;
  using const_reverse_iterator = typename decltype(m_data)::const_reverse_iterator;

  Image() = default;
  explicit constexpr Image(const std::array<PixelType_, pitch * height_>& data) : m_data(data) {}
  template <gneiss::ranges::sized_range Range_>
    requires std::convertible_to<gneiss::ranges::range_value_t<Range_>, PixelType_>
  explicit constexpr Image(const Range_& data) {
    GNEISS_ASSERT_MSG(gneiss::ranges::size(data) == gneiss::ranges::size(m_data), "Input data has wrong size.");
    gneiss::ranges::copy(data, m_data.begin());
  }
  template <fields_2d::field_2d Field_, typename Parallelize_ = do_not_parallelize>
    requires std::convertible_to<typename Field_::scalar_type, PixelType_>
  explicit constexpr Image(const Field_& field,
                           Parallelize_ parallelize = Parallelize_()) noexcept(noexcept(field(0, 0))) {
    gneiss::fields_2d::copy(field, *this, parallelize);
  }

 private:
  template <typename Self>
  constexpr static decltype(auto) operator_call_common(Self* self, const std::size_t index) {
    GNEISS_ASSERT_MSG(index < width_ * height_, "Argument index out of range.");
    return self->m_data.at(index);
  }

  template <typename Self>
  constexpr static decltype(auto) operator_call_common(Self* self,
                                                       const fields_2d::FieldCoordinate<std::size_t>& coordinate) {
    GNEISS_ASSERT_MSG(coordinate.x < width_, "Argument x out of range.");
    GNEISS_ASSERT_MSG(coordinate.y < height_, "Argument y out of range.");

    return self->m_data.at(coordinate.y * width_ + coordinate.x);
  }

  template <typename Self>
  constexpr static decltype(auto) at_reflected_common(Self* self, const int xIdx, const int yIdx) {
    const auto reflect = [](const int value, const int border) {
      return static_cast<std::size_t>(value <= 0 ? std::abs(value) : border - 1 - std::abs(border - 1 - value));
    };

    const std::size_t xIdx_reflected = reflect(xIdx, width_);
    const std::size_t yIdx_reflected = reflect(yIdx, height_);

    return (*self)(xIdx_reflected, yIdx_reflected);
  }

 public:
  /**
   * Intensity access by continuous index
   *
   * @param index
   * @return
   */
  [[nodiscard]] constexpr const PixelType_& operator()(const std::size_t index) const {
    return operator_call_common(this, index);
  }

  /**
   * Intensity access by continuous index
   *
   * @param index
   * @return
   */
  [[nodiscard]] constexpr PixelType_& operator()(const std::size_t index) { return operator_call_common(this, index); }

  /**
   * Intensity access by coordinate
   *
   * @param coordinate
   * @param interpolation overload selector (do not pass, use default)
   * @return
   */
  [[nodiscard]] constexpr const PixelType_& operator()(const fields_2d::FieldCoordinate<std::size_t>& coordinate,
                                                       NoInterpolation interpolation = NoInterpolation{}) const {
    GNEISS_UNUSED(interpolation);
    return operator_call_common(this, coordinate);
  }

  /**
   * Intensity access by coordinate
   *
   * @param coordinate
   * @return
   */
  [[nodiscard]] constexpr PixelType_& operator()(const fields_2d::FieldCoordinate<std::size_t>& coordinate) {
    return operator_call_common(this, coordinate);
  }

  /**
   * Intensity access by coordinate
   * @param xIdx
   * @param yIdx
   * @return
   */
  [[nodiscard]] constexpr const PixelType_& operator()(const std::size_t xIdx, const std::size_t yIdx) const {
    return operator_call_common(this, {xIdx, yIdx});
  }

  /**
   * Intensity access by coordinate
   *
   * @param xIdx
   * @param yIdx
   * @return
   */
  [[nodiscard]] constexpr PixelType_& operator()(const std::size_t xIdx, const std::size_t yIdx) {
    return operator_call_common(this, {xIdx, yIdx});
  }

  /**
   * Intensity access by coordinate with reflection
   *
   * @param xIdx
   * @param yIdx
   * @return
   */
  [[nodiscard]] constexpr const PixelType_& at_reflected(const int xIdx, const int yIdx) const {
    return at_reflected_common(this, xIdx, yIdx);
  }

  /**
   * Intensity access by coordinate with reflection
   *
   * @param xIdx
   * @param yIdx
   * @return
   */
  [[nodiscard]] constexpr PixelType_& at_reflected(const int xIdx, const int yIdx) {
    return at_reflected_common(this, xIdx, yIdx);
  }

  /**
   * Bilinear interpolation of images + gradient
   *
   * @image html image-linear-interpolation.svg
   *
   * @tparam Scalar_
   */
  template <std::floating_point Scalar_>
  class LinearInterpolator {
   public:
    LinearInterpolator(const Image* image, const fields_2d::FieldCoordinate<Scalar_>& coordinate) : m_image(image) {
      m_topLeft = fields_2d::FieldCoordinate<std::size_t>{static_cast<std::size_t>(std::floor(coordinate.x)),
                                                          static_cast<std::size_t>(std::floor(coordinate.y))};

      const Scalar_ toLeft = coordinate.x - static_cast<Scalar_>(m_topLeft.x);
      const Scalar_ toTop = coordinate.y - static_cast<Scalar_>(m_topLeft.y);
      const Scalar_ toRight = Scalar_{1.0} - toLeft;
      const Scalar_ toBottom = Scalar_{1.0} - toTop;

      m_weightLeft = toRight;
      m_weightTop = toBottom;
      m_weightRight = toLeft;
      m_weightBottom = toTop;

      if (coordinate.x >= Scalar_{0} && coordinate.y >= Scalar_{0} &&
          fields_2d::in_bounds<Image>(fields_2d::FieldCoordinate<std::size_t>{
              static_cast<std::size_t>(std::ceil(coordinate.x)), static_cast<std::size_t>(std::ceil(coordinate.y))})) {
        m_intensity = interpolate(m_topLeft);
      }
    }

    /**
     * Interpolated value
     *
     * @return @f$ \boldsymbol{I}(x,y) @f$
     */
    [[nodiscard]] std::optional<Scalar_> value() const { return m_intensity; }

    /**
     * Mathematically not the correct gradient but smoother -> works better in practice
     *
     * @image html image-linear-smoothed-gradient.svg
     *
     * @return @return @f$ \approx \begin{bmatrix} \frac{\delta \boldsymbol{I}}{\delta x} \\ \frac{\delta
     * \boldsymbol{I}}{\delta y} \end{bmatrix} @f$
     */
    [[nodiscard]] std::optional<Eigen::Matrix<Scalar_, 2, 1>> smoothed_gradient() const {
      if (!fields_2d::in_bounds<Image>(
              m_topLeft +
              fields_2d::FieldCoordinate<std::size_t>{1 + static_cast<std::size_t>(std::ceil(m_weightRight)),
                                                      1 + static_cast<std::size_t>(std::ceil(m_weightBottom))}) ||
          m_topLeft.x == 0 || m_topLeft.y == 0) {
        return {};
      }

      const auto leftOfTopLeft = m_topLeft - fields_2d::FieldCoordinate<std::size_t>{1, 0};
      const Scalar_ intensityToTheLeft = interpolate(leftOfTopLeft);

      const auto topRight = m_topLeft + fields_2d::FieldCoordinate<std::size_t>{1, 0};
      const Scalar_ intensityToTheRight = interpolate(topRight);

      const auto topOfTopLeft = m_topLeft - fields_2d::FieldCoordinate<std::size_t>{0, 1};
      const Scalar_ intensityAbove = interpolate(topOfTopLeft);

      const auto bottomLeft = m_topLeft + fields_2d::FieldCoordinate<std::size_t>{0, 1};
      const Scalar_ intensityBelow = interpolate(bottomLeft);

      return Eigen::Matrix<Scalar_, 2, 1>(Scalar_{0.5} * (intensityToTheRight - intensityToTheLeft),
                                          Scalar_{0.5} * (intensityBelow - intensityAbove));
    }

    /**
     * linear interpolated gradient
     *
     * @image html image-linear-gradient.svg
     *
     * @return @f$ \begin{bmatrix} \frac{\delta \boldsymbol{I}}{\delta x} \\ \frac{\delta \boldsymbol{I}}{\delta y}
     * \end{bmatrix} @f$
     */
    [[nodiscard]] std::optional<Eigen::Matrix<Scalar_, 2, 1>> gradient() const {
      const auto topRight = m_topLeft + fields_2d::FieldCoordinate<std::size_t>{1, 0};
      const auto bottomLeft = m_topLeft + fields_2d::FieldCoordinate<std::size_t>{0, 1};
      const auto bottomRight = m_topLeft + fields_2d::FieldCoordinate<std::size_t>{1, 1};

      if (!m_intensity || !fields_2d::in_bounds<Image>(bottomRight)) {
        return {};
      }

      const Scalar_ valueLeft = m_weightTop * (*m_image)(m_topLeft) + m_weightBottom * (*m_image)(bottomLeft);
      const Scalar_ valueRight = m_weightTop * (*m_image)(topRight) + m_weightBottom * (*m_image)(bottomRight);

      const Scalar_ valueTop = m_weightLeft * (*m_image)(m_topLeft) + m_weightRight * (*m_image)(topRight);
      const Scalar_ valueBottom = m_weightLeft * (*m_image)(bottomLeft) + m_weightRight * (*m_image)(bottomRight);

      return Eigen::Matrix<Scalar_, 2, 1>(valueRight - valueLeft, valueBottom - valueTop);
    }

   private:
    [[nodiscard]] Scalar_ interpolate(const fields_2d::FieldCoordinate<std::size_t> topLeft) const {
      const auto topRight = topLeft + fields_2d::FieldCoordinate<std::size_t>{1, 0};
      const auto bottomLeft = topLeft + fields_2d::FieldCoordinate<std::size_t>{0, 1};
      const auto bottomRight = topLeft + fields_2d::FieldCoordinate<std::size_t>{1, 1};

      const Scalar_ valueTopLeft = m_weightTop * m_weightLeft * (*m_image)(topLeft);
      const Scalar_ valueTopRight =
          m_weightRight > Scalar_{0.0} ? m_weightTop * m_weightRight * (*m_image)(topRight) : Scalar_{0.0};
      const Scalar_ valueBottomLeft =
          m_weightBottom > Scalar_{0.0} ? m_weightBottom * m_weightLeft * (*m_image)(bottomLeft) : Scalar_{0.0};
      const Scalar_ valueBottomRight = m_weightBottom > Scalar_{0.0} && m_weightRight > Scalar_{0.0}
                                           ? m_weightBottom * m_weightRight * (*m_image)(bottomRight)
                                           : Scalar_{0.0};

      return valueTopLeft + valueTopRight + valueBottomLeft + valueBottomRight;
    }

    const Image* m_image;
    fields_2d::FieldCoordinate<std::size_t> m_topLeft;
    Scalar_ m_weightLeft, m_weightTop, m_weightRight, m_weightBottom;

    std::optional<Scalar_> m_intensity{};
  };

  /**
   * Intensity access with linear differentiable interpolator
   *
   * @tparam Scalar_
   * @param coordinate
   * @param interpolation overload selector
   * @return Interpolator object (call .value() and/ or .gradient() on it)
   */
  template <std::floating_point Scalar_>
  [[nodiscard]] constexpr LinearInterpolator<Scalar_> operator()(const fields_2d::FieldCoordinate<Scalar_>& coordinate,
                                                                 LinearInterpolation interpolation) const {
    GNEISS_UNUSED(interpolation);
    return {this, coordinate};
  }

  [[nodiscard]] constexpr auto begin() noexcept { return m_data.begin(); }
  [[nodiscard]] constexpr auto end() noexcept { return m_data.end(); }
  [[nodiscard]] constexpr auto begin() const noexcept { return m_data.cbegin(); }
  [[nodiscard]] constexpr auto end() const noexcept { return m_data.cend(); }
  [[nodiscard]] constexpr auto cbegin() const noexcept { return m_data.cbegin(); }
  [[nodiscard]] constexpr auto cend() const noexcept { return m_data.cend(); }
  [[nodiscard]] constexpr auto rbegin() noexcept { return m_data.rbegin(); }
  [[nodiscard]] constexpr auto rend() noexcept { return m_data.rend(); }
  [[nodiscard]] constexpr auto rbegin() const noexcept { return m_data.crbegin(); }
  [[nodiscard]] constexpr auto rend() const noexcept { return m_data.crend(); }
  [[nodiscard]] constexpr auto crbegin() const noexcept { return m_data.crbegin(); }
  [[nodiscard]] constexpr auto crend() const noexcept { return m_data.crend(); }
};

/**
 * Reference wrapper with type erasure around \ref Image
 *
 * @tparam PixelType_
 */
template <typename PixelType_>
class ImageReference {
 public:
  template <std::size_t width_, std::size_t height_, bool pitched_>
  explicit ImageReference(const Image<PixelType_, width_, height_, pitched_>& image)
      : m_imageConcept(std::make_shared<ImageModel<width_, height_, pitched_>>(image)) {}

  [[nodiscard]] std::size_t getWidth() const { return m_imageConcept->getWidth(); }
  [[nodiscard]] std::size_t getHeight() const { return m_imageConcept->getHeight(); }
  [[nodiscard]] const PixelType_& operator()(const std::size_t xIdx, const std::size_t yIdx) const {
    return (*m_imageConcept)(xIdx, yIdx);
  }

  class ImageConcept {
   public:
    virtual ~ImageConcept() = default;
    [[nodiscard]] virtual std::size_t getWidth() const = 0;
    [[nodiscard]] virtual std::size_t getHeight() const = 0;
    [[nodiscard]] virtual const PixelType_& operator()(std::size_t xIdx, std::size_t yIdx) const = 0;
  };

  template <std::size_t width_, std::size_t height_, bool pitched_>
  class ImageModel : public ImageConcept {
   public:
    using ImageType_ = Image<PixelType_, width_, height_, pitched_>;
    explicit ImageModel(const ImageType_& image) noexcept : m_image(std::addressof(image)) {}
    [[nodiscard]] std::size_t getWidth() const override { return ImageType_::width; }
    [[nodiscard]] std::size_t getHeight() const override { return ImageType_::height; }
    [[nodiscard]] const PixelType_& operator()(const std::size_t xIdx, const std::size_t yIdx) const override {
      return (*m_image)(xIdx, yIdx);
    }

   private:
    const ImageType_* m_image;
  };

 private:
  std::shared_ptr<const ImageConcept> m_imageConcept;
};

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_IMAGE_H
