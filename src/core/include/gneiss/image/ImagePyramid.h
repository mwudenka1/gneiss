#ifndef GNEISS_IMAGEPYRAMID_H
#define GNEISS_IMAGEPYRAMID_H
#pragma once
/** @file */

#include "gneiss/image/Image.h"
#include "gneiss/image/halve_image.h"
#include "gneiss/utils/ipow.h"

#include <cstdint>
#include <utility>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

namespace internal {

template <typename PixelType_, std::size_t width_0_, std::size_t height_0_, bool pitched_, std::size_t levelIndex_>
struct ImagePyramidLevel {
  static_assert(width_0_ >= ipow(2, levelIndex_), "Too many pyramid levels");
  static_assert(height_0_ >= ipow(2, levelIndex_), "Too many pyramid levels");

  using Image_ = Image<PixelType_, width_0_ / ipow(2, levelIndex_), height_0_ / ipow(2, levelIndex_), pitched_>;

  Image_ image;
};

template <typename PixelType_, std::size_t width_0_, std::size_t height_0_, bool pitched_,
          typename ImagePyramidIndexes_>
struct ImagePyramidImpl;

template <typename PixelType_, std::size_t width_0_, std::size_t height_0_, bool pitched_,
          std::size_t... level_indexes_>
struct ImagePyramidImpl<PixelType_, width_0_, height_0_, pitched_, std::index_sequence<level_indexes_...>>
    : public ImagePyramidLevel<PixelType_, width_0_, height_0_, pitched_, level_indexes_>... {
  explicit constexpr ImagePyramidImpl()
      : ImagePyramidLevel<PixelType_, width_0_, height_0_, pitched_, level_indexes_>()... {}

  template <typename Parallelize_>
  explicit constexpr ImagePyramidImpl(const Image<PixelType_, width_0_, height_0_, pitched_>& baseImage,
                                      Parallelize_ parallelize) {
    ImagePyramidLevel<PixelType_, width_0_, height_0_, pitched_, 0>::image = baseImage;

    (
        [&]() {
          if constexpr (level_indexes_ > 0) {
            ImagePyramidLevel<PixelType_, width_0_, height_0_, pitched_, level_indexes_>::image = halve_image(
                ImagePyramidLevel<PixelType_, width_0_, height_0_, pitched_, level_indexes_ - 1>::image, parallelize);
          }
        }(),
        ...);
  }

 public:
  template <std::size_t level_index_>
  [[nodiscard]] constexpr inline auto& get_level() {
    return ImagePyramidLevel<PixelType_, width_0_, height_0_, pitched_, level_index_>::image;
  }

  template <std::size_t level_index_>
  [[nodiscard]] constexpr inline const auto& get_level() const {
    return ImagePyramidLevel<PixelType_, width_0_, height_0_, pitched_, level_index_>::image;
  }
};

}  // namespace internal

template <std::size_t num_level_, typename PixelType_, std::size_t width_0_, std::size_t height_0_,
          bool pitched_ = false>
class ImagePyramid {
 public:
  using PixelType = PixelType_;
  constexpr static std::size_t num_level = num_level_;
  constexpr static std::size_t width_0 = width_0_;
  constexpr static std::size_t height_0 = height_0_;
  constexpr static bool pitched = pitched_;

  ImagePyramid() = default;

  template <typename Parallelize_ = do_not_parallelize>
  explicit constexpr ImagePyramid(const Image<PixelType_, width_0, height_0, pitched>& baseImage,
                                  Parallelize_ parallelize = Parallelize_())
      : m_impl(baseImage, parallelize) {}

 private:
  template <std::size_t level_index_, typename Self_>
  [[nodiscard]] constexpr static inline decltype(auto) get_level_common(Self_* self) {
    GNEISS_ASSERT_MSG(level_index_ < num_level_, "Template parameter level_index_ out of range");
    return self->m_impl.template get_level<level_index_>();
  }

 public:
  template <std::size_t level_index_>
  [[nodiscard]] constexpr inline auto& get_level() {
    return get_level_common<level_index_>(this);
  }

  template <std::size_t level_index_>
  [[nodiscard]] constexpr inline const auto& get_level() const {
    return get_level_common<level_index_>(this);
  }

 private:
  internal::ImagePyramidImpl<PixelType_, width_0_, height_0_, pitched_, std::make_index_sequence<num_level_>> m_impl;
};

template <std::size_t num_level_, typename Parallelize_>
struct ImagePyramidFactory {
  template <typename PixelType_, std::size_t width_0_, std::size_t height_0_, bool pitched_ = false>
  [[nodiscard]] constexpr auto operator()(const Image<PixelType_, width_0_, height_0_, pitched_>& baseImage) const {
    return ImagePyramid<num_level_, PixelType_, width_0_, height_0_, pitched_>(baseImage, Parallelize_());
  }
};

template <std::size_t num_level_, typename Parallelize_ = do_not_parallelize>
static constexpr auto make_image_pyramid = ImagePyramidFactory<num_level_, Parallelize_>{};

[[nodiscard]] constexpr fields_2d::FieldCoordinate<std::size_t> coordinate_in_base_image(
    fields_2d::FieldCoordinate<std::size_t> coordinateInLevel, std::size_t level) {
  return coordinateInLevel * (std::size_t{1} << level);
}

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_IMAGEPYRAMID_H
