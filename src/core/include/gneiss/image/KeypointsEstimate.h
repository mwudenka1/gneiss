#ifndef GNEISS_KEYPOINTSESTIMATE_H
#define GNEISS_KEYPOINTSESTIMATE_H
#pragma once
/** @file */

#include "gneiss/image/ImagePyramid.h"
#include "gneiss/image/Keypoints.h"
#include "gneiss/parameters/parameters.h"
#include "gneiss/profiling/ProfilingStats.h"
#include "gneiss/utils/Timestamp.h"
#include "gneiss/utils/string_to_type.h"

#include <cstdint>
#include <string_view>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */
using namespace std::literals;

namespace internal {

template <std::size_t cameraIdx_>
struct ParameterizedKeypoints {
  constexpr static auto& cameras = parameters::parameters["cameras"sv].array_data();
  constexpr static std::size_t gridCellSize =
      std::next(cameras.begin(), cameraIdx_)->at("optical_flow_detection_grid_size"sv).template get<std::size_t>();
  constexpr static std::size_t gridCapacity =
      std::next(cameras.begin(), cameraIdx_)->at("optical_flow_grid_cell_capacity"sv).template get<std::size_t>();
  constexpr static std::size_t pyramidLevel =
      std::next(cameras.begin(), cameraIdx_)->at("optical_flow_level"sv).template get<std::size_t>();
  constexpr static std::size_t keypointDetectionLevel =
      std::next(cameras.begin(), cameraIdx_)->at("keypoint_detection_level"sv).template get<std::size_t>();
  constexpr static std::size_t width =
      std::next(cameras.begin(), cameraIdx_)->at("res_x_px"sv).template get<std::size_t>();
  constexpr static std::size_t height =
      std::next(cameras.begin(), cameraIdx_)->at("res_y_px"sv).template get<std::size_t>();
  constexpr static std::string_view optical_flow_scalar =
      parameters::parameters["optical_flow_scalar"sv].template get<std::string_view>();
  using Scalar = string_to_type_t<FixedString<optical_flow_scalar.size()>(optical_flow_scalar.data())>;
  using Keypoints_ = Keypoints<Scalar,
                               GridPyramidParameters{.baseImageWidth = width,
                                                     .baseImageHeight = height,
                                                     .gridSize = gridCellSize,
                                                     .padding = 1,
                                                     .gridCapacity = gridCapacity},
                               keypointDetectionLevel>;

  Keypoints_ keypoints;
};

template <typename CameraIndexes_>
struct KeypointsEstimateImpl;

template <std::size_t... cameraIndexes_>
struct KeypointsEstimateImpl<std::index_sequence<cameraIndexes_...>>
    : public ParameterizedKeypoints<cameraIndexes_>... {
  using Scalar = ParameterizedKeypoints<0>::Scalar;

  template <std::size_t cameraIdx_>
  [[nodiscard]] constexpr inline auto& get() {
    return ParameterizedKeypoints<cameraIdx_>::keypoints;
  }

  template <std::size_t cameraIdx_>
  [[nodiscard]] constexpr inline const auto& get() const {
    return ParameterizedKeypoints<cameraIdx_>::keypoints;
  }
};

}  // namespace internal

/**
 * @brief Container for all optical flow estimations of the parameterized cameras for one time step
 *
 */
class KeypointsEstimate {
  constexpr static auto& cameras = parameters::parameters["cameras"sv].array_data();

 public:
  constexpr static std::size_t numCameras = cameras.size();
  using KeypointsEstimateImpl = internal::KeypointsEstimateImpl<std::make_index_sequence<numCameras>>;

  using Scalar = KeypointsEstimateImpl::Scalar;

 private:
  template <std::size_t cameraIdx_, typename Self_>
  [[nodiscard]] constexpr static inline decltype(auto) get_common(Self_* self) {
    static_assert(cameraIdx_ < numCameras, "Template parameter cameraIdx_ out of range");
    return self->m_impl.template get<cameraIdx_>();
  }

 public:
  template <std::size_t cameraIdx_>
  [[nodiscard]] constexpr inline auto& get() {
    return get_common<cameraIdx_>(this);
  }

  template <std::size_t cameraIdx_>
  [[nodiscard]] constexpr inline const auto& get() const {
    return get_common<cameraIdx_>(this);
  }

  [[nodiscard]] const Timestamp& timestamp() const { return m_timestamp; }

  [[nodiscard]] Timestamp& timestamp() { return m_timestamp; }

 private:
  KeypointsEstimateImpl m_impl;
  Timestamp m_timestamp;
};

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_KEYPOINTSESTIMATE_H
