#ifndef GNEISS_HALVEIMAGE_H
#define GNEISS_HALVEIMAGE_H
#pragma once
/** @file */

#include "gneiss/fields-2d/FlipXYView.h"
#include "gneiss/fields-2d/HorizontalConvolutionView.h"
#include "gneiss/fields-2d/HorizontalReflectPaddingView.h"
#include "gneiss/fields-2d/TakeOddRowsView.h"
#include "gneiss/fields-2d/TransformView.h"
#include "gneiss/fields-2d/copy.h"
#include "gneiss/image/Image.h"
#include "gneiss/utils/division_power_2.h"
#include "gneiss/utils/ranges.h"
#include "gneiss/utils/unused.h"

#include <array>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

namespace internal {

template <typename PixelType_, std::size_t width_, std::size_t height_, bool pitched_,
          typename Parallelize_ = do_not_parallelize>
constexpr Image<PixelType_, width_ / 2, height_ / 2, pitched_> halve_image_new(
    const Image<PixelType_, width_, height_, pitched_>& inImage, Parallelize_ parallelize = Parallelize_()) {
  constexpr const /* static */ std::array<int, 5> gaussKernel{1, 4, 6, 4, 1};
  [[maybe_unused]] const uint32_t kernelSumPower2 = 8;  // sum of the quadratic kernel is 2^8 = 256

  const auto firstResultView = inImage | fields_2d::view::HorizontalReflectPadding<2> |
                               fields_2d::view::HorizontalConvolution<2, gaussKernel> | fields_2d::view::FlipXY |
                               fields_2d::view::TakeOddRows;

  const Image<int, firstResultView.width, firstResultView.height, pitched_> firstResult(firstResultView, parallelize);

  const auto secondResultView = firstResult | fields_2d::view::HorizontalReflectPadding<2> |
                                fields_2d::view::HorizontalConvolution<2, gaussKernel> | fields_2d::view::FlipXY |
                                fields_2d::view::TakeOddRows |
                                fields_2d::view::Transform([](const auto pixelInt) -> uint8_t {
                                  return static_cast<uint8_t>(division_power_2(pixelInt, kernelSumPower2));
                                });

  const Image<PixelType_, secondResultView.width, secondResultView.height, pitched_> secondResult(secondResultView,
                                                                                                  parallelize);
  return secondResult;
}

template <typename PixelType_, std::size_t width_, std::size_t height_, bool pitched_ = false>
constexpr Image<PixelType_, width_ / 2, height_ / 2, pitched_> halve_image_old(
    const Image<PixelType_, width_, height_, pitched_>& inImage) {
  constexpr std::size_t halveWidth = width_ / 2;
  constexpr std::size_t halveHeight = height_ / 2;

  Image<int, halveHeight, width_, pitched_> firstConvResult{};  // flip of height and width is on purpose
  Image<PixelType_, halveWidth, halveHeight, pitched_> result{};

  constexpr std::array<int, 5> kernel = {1, 4, 6, 4, 1};

  // vertical convolution
  for (std::size_t yIdx = 0; yIdx < halveHeight; yIdx++) {
    const int centerY = static_cast<int>(2 * yIdx + 1);
    for (std::size_t xIdx = 0; xIdx < static_cast<int>(width_); xIdx++) {
      const auto xIdxSigned = static_cast<int>(xIdx);
      const int pixelValueMinus2 = inImage.at_reflected(xIdxSigned, centerY - 2);
      const int pixelValueMinus1 = inImage.at_reflected(xIdxSigned, centerY - 1);
      const int pixelValueCenter = inImage.at_reflected(xIdxSigned, centerY);
      const int pixelValuePlus1 = inImage.at_reflected(xIdxSigned, centerY + 1);
      const int pixelValuePlus2 = inImage.at_reflected(xIdxSigned, centerY + 2);

      // NOLINTNEXTLINE(readability-suspicious-call-argument)
      firstConvResult(yIdx, xIdx) = kernel.at(0) * pixelValueMinus2 + kernel.at(1) * pixelValueMinus1 +
                                    kernel.at(2) * pixelValueCenter + kernel.at(3) * pixelValuePlus1 +
                                    kernel.at(4) * pixelValuePlus2;
    }
  }

  // horizontal convolution
  for (std::size_t xIdx = 0; xIdx < halveWidth; xIdx++) {
    const int centerX = static_cast<int>(2 * xIdx + 1);

    for (std::size_t yIdx = 0; yIdx < halveHeight; yIdx++) {
      const auto yIdxSigned = static_cast<int>(yIdx);
      // NOLINTNEXTLINE(readability-suspicious-call-argument)
      const int pixelValueMinus2 = firstConvResult.at_reflected(yIdxSigned, centerX - 2);
      // NOLINTNEXTLINE(readability-suspicious-call-argument)
      const int pixelValueMinus1 = firstConvResult.at_reflected(yIdxSigned, centerX - 1);
      // NOLINTNEXTLINE(readability-suspicious-call-argument)
      const int pixelValueCenter = firstConvResult.at_reflected(yIdxSigned, centerX);
      // NOLINTNEXTLINE(readability-suspicious-call-argument)
      const int pixelValuePlus1 = firstConvResult.at_reflected(yIdxSigned, centerX + 1);
      // NOLINTNEXTLINE(readability-suspicious-call-argument)
      const int pixelValuePlus2 = firstConvResult.at_reflected(yIdxSigned, centerX + 2);

      const int valInt = kernel.at(0) * pixelValueMinus2 + kernel.at(1) * pixelValueMinus1 +
                         kernel.at(2) * pixelValueCenter + kernel.at(3) * pixelValuePlus1 +
                         kernel.at(4) * pixelValuePlus2;
      const auto val = static_cast<PixelType_>(division_power_2(valInt, 8));
      result(xIdx, yIdx) = val;
    }
  }

  return result;
}

}  // namespace internal

/**
 * @brief Subsample image by using a gaussian convolution and skipping every second line.
 *
 * Convolution is done with Gaussian Kernel
 * \f[
 * \frac{1}{256}
 * \begin{bmatrix}
 *   1 & 4 & 6 & 4 & 1
 * \\4 & 16 & 24 & 16 & 4
 * \\6 & 24 & 36 & 24 & 6
 * \\4 & 16 & 24 & 16 & 4
 * \\1 & 4 & 6 & 4 & 1
 * \\ \end{bmatrix}
 * \f]
 *
 * @tparam PixelType_ same as for @ref Image
 * @tparam width_ same as for @ref Image
 * @tparam height_ same as for @ref Image
 * @tparam pitched_ same as for @ref Image
 * @param inImage image to be processed
 * @return subsampled and smoothed inImage
 */
template <typename PixelType_, std::size_t width_, std::size_t height_, bool pitched_,
          typename Parallelize_ = do_not_parallelize>
constexpr Image<PixelType_, width_ / 2, height_ / 2, pitched_> halve_image(
    const Image<PixelType_, width_, height_, pitched_>& inImage, Parallelize_ parallelize = Parallelize_()) {
  return internal::halve_image_new(inImage, parallelize);
  // return internal::halve_image_old(inImage);
}

/** @} */  // end of core

}  // namespace gneiss
#endif  // GNEISS_HALVEIMAGE_H
