#ifndef GNEISS_IMAGEMEASUREMENT_H
#define GNEISS_IMAGEMEASUREMENT_H
#pragma once
/** @file */

#include "gneiss/image/Image.h"
#include "gneiss/parameters/parameters.h"
#include "gneiss/utils/Timestamp.h"
#include "gneiss/utils/least_uint.h"

#include <cstdint>
#include <string_view>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */
using namespace std::literals;

namespace internal {

template <std::size_t cameraIdx_>
struct ParameterizedImage {
  constexpr static auto& cameras = gneiss::parameters::parameters["cameras"sv].array_data();
  using Image_ =
      Image<least_uint<std::next(cameras.begin(), cameraIdx_)->at("bits_per_pixel"sv).template get<uint64_t>()>,
            std::next(cameras.begin(), cameraIdx_)->at("res_x_px"sv).template get<uint64_t>(),
            std::next(cameras.begin(), cameraIdx_)->at("res_y_px"sv).template get<uint64_t>()>;

  Image_ image;
};

template <typename CameraIndexes_>
struct ImageMeasurementImpl;

template <std::size_t... cameraIndexes_>
struct ImageMeasurementImpl<std::index_sequence<cameraIndexes_...>> : public ParameterizedImage<cameraIndexes_>... {
  template <std::size_t cameraIdx_>
  [[nodiscard]] constexpr inline auto& get() {
    return ParameterizedImage<cameraIdx_>::image;
  }

  template <std::size_t cameraIdx_>
  [[nodiscard]] constexpr inline const auto& get() const {
    return ParameterizedImage<cameraIdx_>::image;
  }
};

}  // namespace internal

/**
 * @brief Container for all images of the parameterized cameras for one time step
 *
 */
class ImageMeasurement {
  constexpr static auto& cameras = gneiss::parameters::parameters["cameras"sv].array_data();

 public:
  constexpr static std::size_t numCameras = cameras.size();

  ImageMeasurement() noexcept = default;

 private:
  template <std::size_t cameraIdx_, typename Self_>
  [[nodiscard]] constexpr static inline decltype(auto) get_common(Self_* self) {
    GNEISS_ASSERT_MSG(cameraIdx_ < numCameras, "Template parameter cameraIdx_ out of range");
    return self->m_impl.template get<cameraIdx_>();
  }

 public:
  template <std::size_t cameraIdx_>
  [[nodiscard]] constexpr inline auto& get() {
    return get_common<cameraIdx_>(this);
  }

  template <std::size_t cameraIdx_>
  [[nodiscard]] constexpr inline const auto& get() const {
    return get_common<cameraIdx_>(this);
  }

  [[nodiscard]] const Timestamp& timestamp() const { return m_timestamp; }

  [[nodiscard]] Timestamp& timestamp() { return m_timestamp; }

 private:
  internal::ImageMeasurementImpl<std::make_index_sequence<numCameras>> m_impl;
  Timestamp m_timestamp;
};

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_IMAGEMEASUREMENT_H
