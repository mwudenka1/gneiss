#ifndef GNEISS_KEYPOINTDESCRIPTORATLEVEL_H
#define GNEISS_KEYPOINTDESCRIPTORATLEVEL_H
#pragma once
/** @file */

#include "gneiss/image/KeypointDescriptor.h"

#include <cstdint>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

template <std::floating_point Scalar_>
struct KeypointDescriptorAtLevel {
  using Scalar = Scalar_;
  using Keypoint = KeypointDescriptor<Scalar>;

  // NOLINTBEGIN(misc-non-private-member-variables-in-classes)
  Keypoint keypoint{};
  std::size_t level = 0;
  // NOLINTEND(misc-non-private-member-variables-in-classes)

  friend std::ostream& operator<<(std::ostream& outStream, const KeypointDescriptorAtLevel& self) {
    outStream << "[keypoint: " << self.keypoint << "; level: " << self.level << ']';
    return outStream;
  }
};

template <std::floating_point Scalar_>
bool valid_descriptor(const KeypointDescriptorAtLevel<Scalar_>& descriptor) {
  return descriptor.keypoint.id != unidentifiedKeypoint;
}

/** @} */  // end of core

}  // namespace gneiss
#endif  // GNEISS_KEYPOINTDESCRIPTORATLEVEL_H
