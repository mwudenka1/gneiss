#ifndef GNEISS_KEYPOINTSESTIMATECONSTPTR_H
#define GNEISS_KEYPOINTSESTIMATECONSTPTR_H
#pragma once
/** @file */

#include "gneiss/image/KeypointsEstimate.h"
#include "gneiss/utils/ConcurrentArena.h"

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

using KeypointsEstimateConstPtr = std::unique_ptr<const KeypointsEstimate, ArenaPtrDeleter<KeypointsEstimate>>;

static_assert(std::is_move_constructible_v<KeypointsEstimateConstPtr>);
static_assert(std::is_move_assignable_v<KeypointsEstimateConstPtr>);

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_KEYPOINTSESTIMATECONSTPTR_H
