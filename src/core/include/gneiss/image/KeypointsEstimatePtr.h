#ifndef GNEISS_KEYPOINTSESTIMATEPTR_H
#define GNEISS_KEYPOINTSESTIMATEPTR_H
#pragma once
/** @file */

#include "gneiss/image/KeypointsEstimate.h"
#include "gneiss/utils/ConcurrentArena.h"

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

using KeypointsEstimatePtr = std::unique_ptr<KeypointsEstimate, ArenaPtrDeleter<KeypointsEstimate>>;

static_assert(std::is_move_constructible_v<KeypointsEstimate>);
static_assert(std::is_move_assignable_v<KeypointsEstimate>);

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_KEYPOINTSESTIMATEPTR_H
