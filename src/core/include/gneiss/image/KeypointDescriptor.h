#ifndef GNEISS_KEYPOINTDESCRIPTOR_H
#define GNEISS_KEYPOINTDESCRIPTOR_H
#pragma once
/** @file */

#include "gneiss/fields-2d/FieldCoordinate.h"

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

using KeypointId = std::size_t;

static constexpr KeypointId unidentifiedKeypoint = 0;

template <typename Scalar_>
struct KeypointDescriptor {
  using Scalar = Scalar_;
  using AffineTransform = Eigen::Transform<Scalar_, 2, Eigen::AffineCompact>;
  // NOLINTBEGIN(misc-non-private-member-variables-in-classes)
  AffineTransform transform;
  KeypointId id = unidentifiedKeypoint;
  // NOLINTEND(misc-non-private-member-variables-in-classes)

  friend std::ostream& operator<<(std::ostream& outStream, const KeypointDescriptor& self) {
    outStream << "[translation: " << self.transform.translation() << "; id: " << self.id << ']';
    return outStream;
  }
};

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_KEYPOINTDESCRIPTOR_H
