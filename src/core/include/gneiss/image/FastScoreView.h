#ifndef GNEISS_FASTSCOREVIEW_H
#define GNEISS_FASTSCOREVIEW_H
#pragma once
/** @file */

#include "gneiss/fields-2d/FieldCoordinate.h"
#include "gneiss/fields-2d/concepts.h"
#include "gneiss/fields-2d/makeView.h"
#include "gneiss/fields-2d/makeViewClosure.h"
#include "gneiss/utils/abs.h"
#include "gneiss/utils/ranges.h"
#include "gneiss/utils/transform_to_array.h"

#include <array>
#include <cstdint>
#include <type_traits>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */
namespace internal {

struct ApplyOffset {
  constexpr inline fields_2d::FieldCoordinate<std::size_t> operator()(const auto& offset) const {
    return static_cast<fields_2d::FieldCoordinate<std::size_t>>(coordinate + offset);
  }

  fields_2d::FieldCoordinate<int64_t> coordinate{};
};

template <fields_2d::field_2d InField2d_, typename scalar_tpe>
struct Difference {
  constexpr inline int32_t operator()(const auto& coordinate) const {
    return static_cast<int32_t>((*inField2d)(coordinate)) - static_cast<int32_t>(centerBrightness);
  }

  const InField2d_* inField2d;
  scalar_tpe centerBrightness;
};

}  // namespace internal

/**
 * @brief Radius of the bresenham circle used for the FAST corner score calculation
 */
constexpr static std::size_t fastRadius = 3;

template <fields_2d::field_2d InField2d_>
  requires(InField2d_::height > fastRadius * 2 && InField2d_::width > fastRadius * 2)
struct FastScoreView : fields_2d::ViewBase {
  using scalar_type = std::remove_const_t<std::remove_reference_t<typename InField2d_::scalar_type>>;
  constexpr static std::size_t radius = fastRadius;
  constexpr static std::size_t width = InField2d_::width;
  constexpr static std::size_t height = InField2d_::height;
  constexpr static std::size_t circumference = 16;
  constexpr static std::size_t cornerThreshold = circumference / 2 + 1;
  constexpr static std::array<fields_2d::FieldCoordinate<int64_t>, circumference> bresenhamCircleOffsets = {{{0, 3},
                                                                                                             {1, 3},
                                                                                                             {2, 2},
                                                                                                             {3, 1},
                                                                                                             {3, 0},
                                                                                                             {3, -1},
                                                                                                             {2, -2},
                                                                                                             {1, -3},
                                                                                                             {0, -3},
                                                                                                             {-1, -3},
                                                                                                             {-2, -2},
                                                                                                             {-3, -1},
                                                                                                             {-3, 0},
                                                                                                             {-3, 1},
                                                                                                             {-2, 2},
                                                                                                             {-1, 3}}};
  constexpr static auto bresenhamCircleOffsetsOvershot = ([]() consteval {
    std::array<fields_2d::FieldCoordinate<int64_t>, circumference + cornerThreshold> result;
    // sadly not (yet (C++20)) constexpr :(
    // gneiss::ranges::copy(
    //     gneiss::ranges23::view::concat(bresenhamCircleOffsets,
    //                                    bresenhamCircleOffsets | gneiss::ranges::view::take(cornerThreshold)),
    //     result.begin());

    gneiss::ranges::copy(bresenhamCircleOffsets, result.begin());
    gneiss::ranges::copy(bresenhamCircleOffsets.begin(), std::next(bresenhamCircleOffsets.begin(), cornerThreshold),
                         std::next(result.begin(), circumference));

    return result;
  })();

  FastScoreView() = default;
  constexpr FastScoreView(FastScoreView const& rhs) noexcept = default;
  constexpr FastScoreView(FastScoreView&& rhs) noexcept = default;
  constexpr FastScoreView& operator=(FastScoreView const& rhs) noexcept = default;
  constexpr FastScoreView& operator=(FastScoreView&& rhs) noexcept = default;
  ~FastScoreView() = default;

  explicit constexpr FastScoreView(InField2d_ inField2d) : m_inField2d(std::move(inField2d)) {}

  [[nodiscard]] constexpr scalar_type operator()(const fields_2d::FieldCoordinate<std::size_t>& coordinate) const {
    if (coordinate.x < radius || coordinate.x >= width - radius || coordinate.y < radius ||
        coordinate.y >= height - radius) {
      return scalar_type{0};
    }

    const auto centerBrightness = m_inField2d(coordinate);
    const auto coordinateSigned = static_cast<decltype(bresenhamCircleOffsets)::value_type>(coordinate);
    const auto bresenhamCircleCoordinatesOvershot =
        transform_to_array(bresenhamCircleOffsetsOvershot, internal::ApplyOffset{coordinateSigned});

    const auto circleBrightnessDifferences =
        transform_to_array(bresenhamCircleCoordinatesOvershot,
                           internal::Difference<decltype(m_inField2d), scalar_type>{&m_inField2d, centerBrightness});

    int32_t bestScore = std::numeric_limits<int32_t>::max();
    for (std::size_t idx = 0; idx < circumference; ++idx) {
      const int32_t minDifferenceConsecutiveWindow =
          std::max({circleBrightnessDifferences.at(idx), circleBrightnessDifferences.at(idx + 1),
                    circleBrightnessDifferences.at(idx + 2), circleBrightnessDifferences.at(idx + 3),
                    circleBrightnessDifferences.at(idx + 4), circleBrightnessDifferences.at(idx + 5),
                    circleBrightnessDifferences.at(idx + 6), circleBrightnessDifferences.at(idx + 7),
                    circleBrightnessDifferences.at(idx + 8), circleBrightnessDifferences.at(idx + 9)});
      bestScore = std::min(bestScore, minDifferenceConsecutiveWindow);
    }
    bestScore = -bestScore;
    for (std::size_t idx = 0; idx < circumference; ++idx) {
      const int32_t minDifferenceConsecutiveWindow =
          std::min({circleBrightnessDifferences.at(idx), circleBrightnessDifferences.at(idx + 1),
                    circleBrightnessDifferences.at(idx + 2), circleBrightnessDifferences.at(idx + 3),
                    circleBrightnessDifferences.at(idx + 4), circleBrightnessDifferences.at(idx + 5),
                    circleBrightnessDifferences.at(idx + 6), circleBrightnessDifferences.at(idx + 7),
                    circleBrightnessDifferences.at(idx + 8), circleBrightnessDifferences.at(idx + 9)});
      bestScore = std::max(bestScore, minDifferenceConsecutiveWindow);
    }

    return static_cast<scalar_type>(abs(bestScore));
  }

  [[nodiscard]] constexpr inline scalar_type operator()(const std::size_t xIdx, const std::size_t yIdx) const {
    return operator()({xIdx, yIdx});
  }

 private:
  InField2d_ m_inField2d;
};

struct FastScoreFunctionBase {
  template <typename InField2d_>
    requires fields_2d::field_2d<std::remove_reference_t<InField2d_>>
  constexpr auto operator()(InField2d_&& inField2d) const {
    return FastScoreView<std::remove_reference_t<fields_2d::view::makeView_t<InField2d_>>>(
        fields_2d::view::makeView(static_cast<InField2d_&&>(inField2d)));
  }
};

namespace view {
inline constexpr auto FastScore = fields_2d::makeViewClosure(FastScoreFunctionBase{});
}  // namespace view

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_FASTSCOREVIEW_H
