#ifndef GNEISS_IMAGEMEASUREMENTCONSTPTR_H
#define GNEISS_IMAGEMEASUREMENTCONSTPTR_H
#pragma once
/** @file */

#include "gneiss/image/ImageMeasurement.h"
#include "gneiss/utils/ConcurrentArena.h"

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

using ImageMeasurementConstPtr = std::unique_ptr<const ImageMeasurement, ArenaPtrDeleter<ImageMeasurement>>;

static_assert(std::is_move_constructible_v<ImageMeasurementConstPtr>);
static_assert(std::is_move_assignable_v<ImageMeasurementConstPtr>);

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_IMAGEMEASUREMENTCONSTPTR_H
