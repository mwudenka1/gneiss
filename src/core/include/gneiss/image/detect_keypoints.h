#ifndef GNEISS_DETECTKEYPOINTS_H
#define GNEISS_DETECTKEYPOINTS_H
#pragma once
/** @file */

#include "gneiss/fields-2d/BlocksView.h"
#include "gneiss/fields-2d/EnumerateView.h"
#include "gneiss/image/FastScoreView.h"
#include "gneiss/image/ImagePyramid.h"
#include "gneiss/image/KeypointDescriptor.h"
#include "gneiss/utils/StaticVector.h"
#include "gneiss/utils/constexpr_for.h"
#include "gneiss/utils/parallelize.h"

#if GNEISS_PARALLELIZATION == GNEISS_PARALLELIZATION_TBB
#include "oneapi/tbb.h"
#endif

#include <array>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

constexpr std::size_t correct_grid_size(const std::size_t gridSize, const std::size_t padding,
                                        const std::size_t imageHeight, const std::size_t imageWidth) {
  return std::min({gridSize, imageHeight - 2 * padding, imageWidth - 2 * padding});
}

template <typename Scalar_, std::size_t imageWidth_, std::size_t imageHeight_, std::size_t gridSize_,
          std::size_t padding_, std::size_t capacity_>
class KeypointGrid {
 private:
  constexpr static std::size_t actualGridSize = correct_grid_size(gridSize_, padding_, imageHeight_, imageWidth_);
  static constexpr std::size_t stride = actualGridSize;
  using Blocks = fields_2d::BlocksView<Image<uint8_t, imageWidth_, imageHeight_>, actualGridSize, actualGridSize,
                                       padding_, padding_, stride, stride>;

 public:
  using value_type = StaticVector<KeypointDescriptor<Scalar_>, capacity_>;
  using scalar_type = value_type&;
  static constexpr std::size_t width = Blocks::width;
  static constexpr std::size_t height = Blocks::height;

  [[nodiscard]] static constexpr fields_2d::FieldCoordinate<std::size_t> image_coordinate_to_grid(
      const fields_2d::FieldCoordinate<Scalar_> imageCoordinate) {
    const auto result = fields_2d::FieldCoordinate<std::size_t>{
        static_cast<std::size_t>((imageCoordinate.x - padding_) / ((width - 1) * stride + actualGridSize) * width),
        static_cast<std::size_t>((imageCoordinate.y - padding_) / ((height - 1) * stride + actualGridSize) * height)};

    return result;
  }

 private:
  template <typename Self>
  constexpr static decltype(auto) operator_call_common(Self* self,
                                                       const fields_2d::FieldCoordinate<std::size_t>& coordinate) {
    GNEISS_ASSERT_MSG(coordinate.x < width, "Argument x out of range.");
    GNEISS_ASSERT_MSG(coordinate.y < height, "Argument y out of range.");

    return self->m_data.at(coordinate.y * width + coordinate.x);
  }

 public:
  [[nodiscard]] value_type& operator()(fields_2d::FieldCoordinate<std::size_t> coordinate) {
    return operator_call_common(this, coordinate);
  }

  [[nodiscard]] value_type& operator()(const std::size_t xIdx, const std::size_t yIdx) {
    return operator_call_common(this, {xIdx, yIdx});
  }

  [[nodiscard]] const value_type& operator()(fields_2d::FieldCoordinate<std::size_t> coordinate) const {
    return operator_call_common(this, coordinate);
  }

  [[nodiscard]] const value_type& operator()(const std::size_t xIdx, const std::size_t yIdx) const {
    return operator_call_common(this, {xIdx, yIdx});
  }

 private:
  std::array<value_type, Blocks::width * Blocks::height> m_data;
};

/**
 * Fill keypoint grid with best FAST keypoint if grid cell is empty.
 *
 * @tparam Scalar_
 * @tparam gridSize_
 * @tparam padding_
 * @tparam gridCapacity_
 * @tparam Image_
 * @tparam Parallelize_
 * @param image
 * @param grid KeypointGrid to be completed (populates empty grid cells)
 * @param scale
 * @param parallelize
 * @return
 */
template <typename Scalar_, std::size_t gridSize_, std::size_t padding_, std::size_t gridCapacity_, typename Image_,
          typename Parallelize_ = do_not_parallelize>
constexpr void detect_keypoints_image(
    const Image_& image, KeypointGrid<Scalar_, Image_::width, Image_::height, gridSize_, padding_, gridCapacity_>& grid,
    const Scalar_ scale, Parallelize_ parallelize = Parallelize_()) {
  static_assert(Image_::height > 2 * padding_, "Image(level) is too small to detect keypoints.");
  static_assert(Image_::width > 2 * padding_, "Image(level) is too small to detect keypoints.");
  const std::size_t actualGridSize = correct_grid_size(gridSize_, padding_, Image_::height, Image_::width);
  static_assert(actualGridSize > 2 * fastRadius, "Image(level) is too small to detect keypoints.");
  const auto fastKeypoints =
      image | gneiss::view::FastScore | gneiss::fields_2d::view::Enumerate |
      gneiss::fields_2d::view::Blocks<actualGridSize, actualGridSize, padding_, padding_, actualGridSize,
                                      actualGridSize> |
      gneiss::fields_2d::view::Transform([](const auto& block) {
        const auto& [bestKeypoint, bestScore] = gneiss::ranges::fold_left(
            block | gneiss::fields_2d::view::Range, block(0, 0), [](const auto& best, const auto& current) noexcept {
              return std::get<1>(best) >= std::get<1>(current) ? best : current;
            });

        return bestKeypoint;
      });

  const auto enumeratedGridRange = gneiss::fields_2d::view::Enumerate(grid) | gneiss::fields_2d::view::Range;

  if constexpr (parallelize) {
#if GNEISS_PARALLELIZATION == GNEISS_PARALLELIZATION_TBB
    using KeypointGrid = KeypointGrid<Scalar_, Image_::width, Image_::height, gridSize_, padding_, gridCapacity_>;

    tbb::parallel_for(
        tbb::blocked_range<std::ranges::range_difference_t<decltype(enumeratedGridRange)>>(
            0, KeypointGrid::height * KeypointGrid::width),
        [&enumeratedGridRange, &fastKeypoints, &scale](const auto& idxs) {
          for (auto idx = idxs.begin(); idx != idxs.end(); ++idx) {
            auto [blockCoordinate, keypointCollection] = enumeratedGridRange.begin()[idx];
            using AffineTransform = std::remove_reference_t<decltype(keypointCollection)>::value_type::AffineTransform;

            if (keypointCollection.size() == 0) {
              keypointCollection.push_back({.transform = AffineTransform(typename AffineTransform::TranslationType(
                                                fastKeypoints(blockCoordinate).template as_vector<Scalar_>() * scale)),
                                            .id = unidentifiedKeypoint});
            }
          }
        });
#else
    static_assert(!parallelize, "parallelization type not supported");
#endif
  } else {
    for (auto [blockCoordinate, keypointCollection] : enumeratedGridRange) {
      using AffineTransform = std::remove_reference_t<decltype(keypointCollection)>::value_type::AffineTransform;

      if (keypointCollection.size() == 0) {
        keypointCollection.push_back({.transform = AffineTransform(typename AffineTransform::TranslationType(
                                          fastKeypoints(blockCoordinate).template as_vector<Scalar_>() * scale)),
                                      .id = unidentifiedKeypoint});
      }
    }
  }
}

struct GridPyramidParameters {
  std::size_t baseImageWidth;
  std::size_t baseImageHeight;
  std::size_t gridSize;
  std::size_t padding;
  std::size_t gridCapacity;
};

namespace internal {

template <typename Scalar_, GridPyramidParameters parameters, std::size_t levelIndex_>
struct KeypointPyramidLevel {
  using Grid = KeypointGrid<Scalar_, parameters.baseImageWidth / ipow(2, levelIndex_),
                            parameters.baseImageHeight / ipow(2, levelIndex_), parameters.gridSize, parameters.padding,
                            parameters.gridCapacity>;
  Grid grid;
};

template <typename Scalar_, GridPyramidParameters parameters_, typename ImagePyramidIndexes_>
struct KeypointPyramidImpl;

template <typename Scalar_, GridPyramidParameters parameters_, std::size_t... levelIndexes_>
struct KeypointPyramidImpl<Scalar_, parameters_, std::index_sequence<levelIndexes_...>>
    : public KeypointPyramidLevel<Scalar_, parameters_, levelIndexes_>... {
  explicit constexpr KeypointPyramidImpl() : KeypointPyramidLevel<Scalar_, parameters_, levelIndexes_>()... {}

 public:
  template <std::size_t levelIndex_>
  [[nodiscard]] constexpr inline auto& get_level() {
    return KeypointPyramidLevel<Scalar_, parameters_, levelIndex_>::grid;
  }

  template <std::size_t levelIndex_>
  [[nodiscard]] constexpr inline const auto& get_level() const {
    return KeypointPyramidLevel<Scalar_, parameters_, levelIndex_>::grid;
  }
};

template <GridPyramidParameters parameters, std::size_t... pyramidLevels_>
constexpr std::size_t calc_keypoint_numbers(std::index_sequence<pyramidLevels_...>) {
  return ((internal::KeypointPyramidLevel<float, parameters, pyramidLevels_>::Grid::width *
           internal::KeypointPyramidLevel<float, parameters, pyramidLevels_>::Grid::height * parameters.gridCapacity) +
          ... + 0);
}

}  // namespace internal

template <GridPyramidParameters parameters_, std::size_t upToLevel_>
static constexpr std::size_t keypoint_number =
    internal::calc_keypoint_numbers<parameters_>(std::make_index_sequence<upToLevel_>());

template <typename Scalar_, GridPyramidParameters parameters_, std::size_t numLevel_>
class KeypointPyramid {
 public:
  constexpr static std::size_t numLevel = numLevel_;
  constexpr static std::size_t baseImageWidth = parameters_.baseImageWidth;
  constexpr static std::size_t baseImageHeight = parameters_.baseImageHeight;

  KeypointPyramid() = default;

 private:
  template <std::size_t levelIndex_, typename Self_>
  [[nodiscard]] constexpr static inline decltype(auto) get_level_common(Self_* self) {
    static_assert(levelIndex_ < numLevel, "Template parameter levelIndex_ out of range");
    return self->m_impl.template get_level<levelIndex_>();
  }

 public:
  template <std::size_t levelIndex_>
  [[nodiscard]] constexpr inline auto& get_level() {
    return get_level_common<levelIndex_>(this);
  }

  template <std::size_t levelIndex_>
  [[nodiscard]] constexpr inline const auto& get_level() const {
    return get_level_common<levelIndex_>(this);
  }

 private:
  internal::KeypointPyramidImpl<Scalar_, parameters_, std::make_index_sequence<numLevel_>> m_impl;
};

template <typename Scalar_, std::size_t gridSize_, std::size_t padding_, std::size_t gridCapacity_,
          typename ImagePyramid_, std::size_t upToLevel_, typename Parallelize_ = do_not_parallelize>
  requires(ImagePyramid_::num_level >= upToLevel_)
constexpr void detect_keypoints(const ImagePyramid_& imagePyramid,
                                KeypointPyramid<Scalar_,
                                                GridPyramidParameters{.baseImageWidth = ImagePyramid_::width_0,
                                                                      .baseImageHeight = ImagePyramid_::height_0,
                                                                      .gridSize = gridSize_,
                                                                      .padding = padding_,
                                                                      .gridCapacity = gridCapacity_},
                                                upToLevel_>& keypointPyramid,
                                Parallelize_ parallelize = Parallelize_()) {
  constexpr_for<std::size_t{0}, upToLevel_, std::size_t{1}>(
      [&imagePyramid, &keypointPyramid, &parallelize](auto level) {
        const auto& image = imagePyramid.template get_level<level>();
        detect_keypoints_image<Scalar_, gridSize_, padding_, gridCapacity_, std::remove_reference_t<decltype(image)>,
                               Parallelize_>(image, keypointPyramid.template get_level<level>(), (1ul << level),
                                             parallelize);
      });
}

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_DETECTKEYPOINTS_H
