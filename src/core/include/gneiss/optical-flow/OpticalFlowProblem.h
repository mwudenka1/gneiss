#ifndef GNEISS_OPTICALFLOWPROBLEM_H
#define GNEISS_OPTICALFLOWPROBLEM_H
#pragma once
/** @file */

#include <Eigen/Dense>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

template <std::floating_point Scalar_, typename ImagePyramid_>
struct OpticalFlowProblem {
  using Vector2 = Eigen::Matrix<Scalar_, 2, 1>;

  const ImagePyramid_& fromPyramid;
  const ImagePyramid_& toPyramid;
  const Vector2 fromPosition;
};

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_OPTICALFLOWPROBLEM_H
