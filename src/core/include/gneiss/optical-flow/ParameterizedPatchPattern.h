#ifndef GNEISS_DEV_PARAMETERIZEDPATCHPATTERN_H
#define GNEISS_DEV_PARAMETERIZEDPATCHPATTERN_H
#pragma once
/** @file */

#include "gneiss/optical-flow/PatchPatterns.h"
#include "gneiss/parameters/parameters.h"

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

template <auto cameraIdx>
struct ParameterizedPatchPattern {
  // constexpr static std::string_view optical_flow_scalar =
  //     parameters::parameters["optical_flow_scalar"sv].template get<std::string_view>();
  // using Scalar = string_to_type_t<FixedString<optical_flow_scalar.size()>(optical_flow_scalar.data())>;
  constexpr static auto& cameras = gneiss::parameters::parameters["cameras"sv].array_data();
  constexpr static std::string_view optical_flow_patch_pattern =
      std::next(cameras.begin(), cameraIdx)->at("optical_flow_patch_pattern"sv).template get<std::string_view>();

  template <typename Scalar_>
  using type = string_to_pattern<FixedString<optical_flow_patch_pattern.size()>(
      optical_flow_patch_pattern.data())>::template type<Scalar_>;
};

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_DEV_PARAMETERIZEDPATCHPATTERN_H
