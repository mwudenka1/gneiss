#ifndef GNEISS_PATCHPATTERNS_H
#define GNEISS_PATCHPATTERNS_H
#pragma once
/** @file */

#include "gneiss/utils/FixedString.h"

#include <Eigen/Dense>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

template <class Scalar_>
struct Pattern24 {
  //              00    01
  //
  //        02    03    04    05
  //
  //  06    07    08    09    10    11
  //
  //  12    13    14    15    16    17
  //
  //        18    19    20    21
  //
  //              22    23
  //
  // -----> x
  // |
  // |
  // y

  static constexpr std::size_t size = 24;

  using Matrix2P = Eigen::Matrix<Scalar_, 2, size>;

  static const Matrix2P pattern2;
};

template <class Scalar_>
const Pattern24<Scalar_>::Matrix2P Pattern24<Scalar_>::pattern2 =
    Eigen::Matrix<Scalar_, Pattern24<Scalar_>::size, 2>({
                                                            // clang-format off
                             {-1, -5},    {1, -5},


                {-3, -3},    {-1, -3},    {1, -3},     {3, -3},


   {-5, -1},    {-3, -1},    {-1, -1},    {1, -1},     {3, -1},     {5, -1},


   {-5,  1},    {-3,  1},    {-1,  1},    {1,  1},     {3,  1},     {5,  1},


                {-3,  3},    {-1,  3},    {1,  3},     {3,  3},


                             {-1,  5},    {1,  5}
                                                            // clang-format on
                                                        })
        .transpose();

template <class Scalar_>
struct Pattern37 {
  //              00  01  02
  //
  //        03    04  05  06    07
  //
  //  08    09    10  11  12    13    14
  //  15    16    17  18  19    20    21
  //  22    23    24  25  26    27    28
  //
  //        29    30  31  32    33
  //
  //              34  35  36
  //
  // -----> x
  // |
  // |
  // y

  static constexpr std::size_t size = 37;

  using Matrix2P = Eigen::Matrix<Scalar_, 2, size>;

  static const Matrix2P pattern2;
};

template <class Scalar_>
const Pattern37<Scalar_>::Matrix2P Pattern37<Scalar_>::pattern2 =
    Eigen::Matrix<Scalar_, Pattern37<Scalar_>::size, 2>(
        {
            // clang-format off
                             {-1, -5},  {0, -5},  {1, -5},


                {-3, -3},    {-1, -3},  {0, -3},  {1, -3},     {3, -3},


   {-5, -1},    {-3, -1},    {-1, -1},  {0, -1},  {1, -1},     {3, -1},     {5, -1},

   {-5,  0},    {-3,  0},    {-1,  0},  {0,  0},  {1,  0},     {3,  0},     {5,  0},

   {-5,  1},    {-3,  1},    {-1,  1},  {0,  1},  {1,  1},     {3,  1},     {5,  1},


                {-3,  3},    {-1,  3},  {0,  3},  {1,  3},     {3,  3},


                             {-1,  5},  {0,  5},  {1,  5}
            // clang-format on
        })
        .transpose();

template <class Scalar_>
struct Pattern52 {
  //                00    01    02    03
  //
  //          04    05    06    07    08    09
  //
  //    10    11    12    13    14    15    16    17
  //
  //    18    19    20    21    22    23    24    25
  //
  //    26    27    28    29    30    31    32    33
  //
  //    34    35    36    37    38    39    40    41
  //
  //          42    43    44    45    46    47
  //
  //                48    49    50    51
  //
  // -----> x
  // |
  // |
  // y

  static constexpr std::size_t size = 52;

  using Matrix2P = Eigen::Matrix<Scalar_, 2, size>;

  static const Matrix2P pattern2;
};

template <class Scalar_>
const Pattern52<Scalar_>::Matrix2P Pattern52<Scalar_>::pattern2 =
    Eigen::Matrix<Scalar_, Pattern52<Scalar_>::size, 2>(
        {
            // clang-format off
                             {-3,  7},    {-1,  7},     {1,  7},     {3,  7},


                {-5,  5},    {-3,  5},    {-1,  5},     {1,  5},     {3,  5},     {5,  5},


   {-7,  3},    {-5,  3},    {-3,  3},    {-1,  3},     {1,  3},     {3,  3},     {5,  3},     {7,  3},


   {-7,  1},    {-5,  1},    {-3,  1},    {-1,  1},     {1,  1},     {3,  1},     {5,  1},     {7,  1},


   {-7, -1},    {-5, -1},    {-3, -1},    {-1, -1},     {1, -1},     {3, -1},     {5, -1},     {7, -1},


   {-7, -3},    {-5, -3},    {-3, -3},     {-1, -3},    {1, -3},     {3, -3},     {5, -3},     {7, -3},


                {-5, -5},    {-3, -5},     {-1, -5},    {1, -5},     {3, -5},     {5, -5},


                             {-3, -7},     {-1, -7},    {1, -7},     {3, -7}
            // clang-format on
        })
        .transpose();

template <class Scalar_>
struct Pattern52x05 {
  //          00  01  02  03
  //      04  05  06  07  08  09
  //  10  11  12  13  14  15  16  17
  //  18  19  20  21  22  23  24  25
  //  26  27  28  29  30  31  32  33
  //  34  35  36  37  38  39  40  41
  //      42  43  44  45  46  47
  //          48  49  50  51
  //
  // -----> x
  // |
  // |
  // y

  static constexpr std::size_t size = 52;

  using Matrix2P = Eigen::Matrix<Scalar_, 2, size>;

  static const Matrix2P pattern2;
};

template <class Scalar_>
const Pattern52x05<Scalar_>::Matrix2P Pattern52x05<Scalar_>::pattern2 = Pattern52<Scalar_>::pattern2 * Scalar_{0.5};

template <FixedString specifier_>
struct string_to_pattern;

template <>
struct string_to_pattern<"pattern24"> {
  template <typename Scalar_>
  using type = Pattern24<Scalar_>;
};

template <>
struct string_to_pattern<"pattern37"> {
  template <typename Scalar_>
  using type = Pattern37<Scalar_>;
};

template <>
struct string_to_pattern<"pattern52"> {
  template <typename Scalar_>
  using type = Pattern52<Scalar_>;
};

template <>
struct string_to_pattern<"pattern52x05"> {
  template <typename Scalar_>
  using type = Pattern52x05<Scalar_>;
};

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_PATCHPATTERNS_H
