#ifndef GNEISS_SOLVE_OPTICAL_FLOW_H
#define GNEISS_SOLVE_OPTICAL_FLOW_H
#pragma once
/** @file */

#include "gneiss/image/Image.h"
#include "gneiss/image/KeypointDescriptor.h"
#include "gneiss/optical-flow/OpticalFlowProblem.h"
#include "gneiss/utils/constexpr_for.h"
#include "gneiss/utils/ranges.h"

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <sophus/se2.hpp>

#include <optional>

#if GNEISS_PARALLELIZATION == GNEISS_PARALLELIZATION_TBB
#include "oneapi/tbb.h"
#endif

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

/**
 * Use the static instance below!
 *
 * @tparam Scalar_
 * @tparam ImagePyramid_
 * @tparam PatchPattern_
 * @tparam downToLevel_ down to which pyramid level should be tracked
 */
template <std::floating_point Scalar_, typename ImagePyramid_, typename PatchPattern_, std::size_t downToLevel_ = 0>
class OpticalFlowKLTSolver {
 private:
  using PatchPattern = PatchPattern_;
  using Vector2 = Eigen::Matrix<Scalar_, 2, 1>;
  using Vector3 = Eigen::Matrix<Scalar_, 3, 1>;
  using Matrix3 = Eigen::Matrix<Scalar_, 3, 3>;
  using MatrixP3 = Eigen::Matrix<Scalar_, PatchPattern::size, 3>;
  using Matrix3P = Eigen::Matrix<Scalar_, 3, PatchPattern::size>;
  using VectorP = Eigen::Matrix<Scalar_, PatchPattern::size, 1>;
  using VectorPI = Eigen::Matrix<int, PatchPattern::size, 1>;
  using Transform = Eigen::Transform<Scalar_, 2, Eigen::AffineCompact>;

 public:
  [[nodiscard]] std::optional<Transform> operator()(const OpticalFlowProblem<Scalar_, ImagePyramid_>& problem,
                                                    std::size_t maxIterations) const {
    Transform toTransform(typename Transform::TranslationType(problem.fromPosition));

    bool isPatchValid = true;

    gneiss::constexpr_for<downToLevel_, ImagePyramid_::num_level, 1ul>([&](auto inverseLevel) {
      const auto level = ImagePyramid_::num_level - inverseLevel - 1;
      const Scalar_ scale = 1ul << level;
      toTransform.translation() /= scale;

      const auto& fromImage = problem.fromPyramid.template get_level<level>();
      const auto& toImage = problem.toPyramid.template get_level<level>();

      const auto optionalFromPatchEvaluation = evaluate_from_patch(fromImage, problem.fromPosition / scale);
      if (!optionalFromPatchEvaluation) {
        isPatchValid = false;
        return;
      }

      const auto [fromIntensitiesNormalized, H_inc_inv_J_inc_T, fromPatchValidityMask] =
          optionalFromPatchEvaluation.value();

      for (std::size_t i = 0; i < maxIterations; ++i) {
        const auto optionalResiduals =
            calculate_residuals(toImage, toTransform, fromIntensitiesNormalized, fromPatchValidityMask);

        if (!optionalResiduals) {
          isPatchValid = false;
          break;
        }

        const Vector3 inc = -H_inc_inv_J_inc_T * optionalResiduals.value();

        static constexpr Scalar_ bigIncrementThreshold = 1e6;
        if (!inc.allFinite() || inc.template lpNorm<Eigen::Infinity>() > bigIncrementThreshold) {
          isPatchValid = false;
          break;
        }

        toTransform *= Sophus::SE2<Scalar_>::exp(inc).matrix();
      }

      toTransform.translation() *= scale;
    });

    if (!isPatchValid) {
      return {};
    }

    return toTransform;
  }

 private:
  template <typename Image_>
  [[nodiscard]] std::optional<std::tuple<VectorP, Matrix3P, VectorPI>> evaluate_from_patch(
      const Image_& fromImage, const Vector2 fromPosition) const {
    VectorP fromIntensities;
    VectorPI fromPatchValidityMask;

    MatrixP3 d_fromImage_d_inc;

    const auto patternCoordinates = PatchPattern::pattern2.colwise();
    auto d_fromImage_d_inc_rows = d_fromImage_d_inc.rowwise();
    for (auto zipped : gneiss::ranges::views::zip(patternCoordinates, fromIntensities, d_fromImage_d_inc_rows,
                                                  fromPatchValidityMask)) {
      auto [patternCoordinate, intensity, d_fromImage_d_inc_row, valid] = zipped;
      const fields_2d::FieldCoordinate<Scalar_> patternCoordinateInImage =
          Vector2(fromPosition + Vector2(patternCoordinate));
      const auto intensityInterpolation = fromImage(patternCoordinateInImage, gneiss::interpolate_linear);
      const auto optionalIntensity = intensityInterpolation.value();
      const auto optionalGradient = intensityInterpolation.smoothed_gradient();
      // NOLINTNEXTLINE(bugprone-unchecked-optional-access)
      intensity = optionalGradient ? optionalIntensity.value() : Scalar_{0};
      d_fromImage_d_inc_row =
          optionalGradient
              ? Vector3(optionalGradient->x(), optionalGradient->y(),
                        optionalGradient->transpose() * Vector2(-patternCoordinate.y(), patternCoordinate.x()))
              : Vector3::Zero();
      valid = optionalGradient.has_value();
    }

    const Scalar_ fromSum = fromIntensities.sum();
    const auto fromNumValid = static_cast<Scalar_>(fromPatchValidityMask.sum());

    const Scalar_ fromMeanInverse = fromNumValid / fromSum;
    const Vector3 d_fromImage_d_inc_sum = d_fromImage_d_inc.colwise().sum();
    const MatrixP3 d_fromImageNormalized_d_inc =
        fromMeanInverse * (d_fromImage_d_inc - fromIntensities * (d_fromImage_d_inc_sum / fromSum).transpose());

    const Matrix3 H_inc = d_fromImageNormalized_d_inc.transpose() * d_fromImageNormalized_d_inc;
    Matrix3 H_inc_inv = Matrix3::Identity();
    H_inc.ldlt().solveInPlace(H_inc_inv);

    const Matrix3P H_se2_inv_J_se2_T = H_inc_inv * d_fromImageNormalized_d_inc.transpose();

    const VectorP fromIntensitiesNormalized = fromIntensities * fromMeanInverse;

    if (static_cast<std::size_t>(fromNumValid) < PatchPattern::size / 2 ||
        fromSum <= std::numeric_limits<Scalar_>::epsilon() || !H_se2_inv_J_se2_T.array().isFinite().all()) {
      return {};
    }

    return std::make_tuple(fromIntensitiesNormalized, H_se2_inv_J_se2_T, fromPatchValidityMask);
  }

  template <typename Image_>
  [[nodiscard]] std::optional<VectorP> calculate_residuals(const Image_& toImage, const Transform& toTransform,
                                                           const VectorP& fromIntensitiesNormalized,
                                                           const VectorPI& fromPatchValidityMask) const {
    VectorPI toPatchValidityMask;
    VectorP toIntensities;
    const auto patternCoordinates = PatchPattern::pattern2.colwise();
    for (auto idxPatternPositionXIntensity :
         gneiss::ranges::views::zip(patternCoordinates, toIntensities, toPatchValidityMask)) {
      auto [patternCoordinate, intensity, valid] = idxPatternPositionXIntensity;
      const fields_2d::FieldCoordinate<Scalar_> transformedPosition = toTransform * Vector2(patternCoordinate);
      const auto intensityInterpolation = toImage(transformedPosition, gneiss::interpolate_linear);
      const auto optionalIntensity = intensityInterpolation.value();
      intensity = optionalIntensity ? optionalIntensity.value() : Scalar_{0};
      valid = optionalIntensity.has_value();
    }

    const Scalar_ toSum = toIntensities.sum();
    const auto toNumValid = static_cast<Scalar_>(toPatchValidityMask.sum());

    const Scalar_ toMeanInverse = toNumValid / toSum;

    const VectorP toIntensitiesNormalized = toIntensities * toMeanInverse;
    VectorP residuals = ((toPatchValidityMask.array() != 0) && (fromPatchValidityMask.array() != 0))
                            .select(toIntensitiesNormalized - fromIntensitiesNormalized, 0);

    if (toSum < std::numeric_limits<Scalar_>::epsilon() || !residuals.allFinite()) {
      return {};
    }

    return residuals;
  }
};

/**
 * @brief Solves the optical flow problem for one patch using the Kanade-Lucas-Tomasi (KLT) algorithm.
 *
 * The mathematics are explained in \ref architecture-optical-flow.
 *
 * @tparam Scalar_
 * @tparam ImagePyramid_
 * @tparam PatchPattern_
 * @tparam downToLevel_ down to which pyramid level should be tracked
 * @param problem The optical flow problem to be solved, consisting of from an to image and start position.
 * @param maxIterations
 * @return The computed optical flow.
 */
template <std::floating_point Scalar_, typename ImagePyramid_, template <std::floating_point> typename PatchPattern_,
          std::size_t downToLevel_ = 0>
constexpr static auto solve_optical_flow_klt =
    OpticalFlowKLTSolver<Scalar_, ImagePyramid_, PatchPattern_<Scalar_>, downToLevel_>{};

/**
 *  @brief Solves the optical flow problem for one patch using the Kanade-Lucas-Tomasi (KLT) algorithm in both
 *  directions and checks that the patch returns to the same start position.
 *
 * @tparam Scalar_
 * @tparam ImagePyramid_
 * @tparam PatchPattern_
 * @tparam downToLevel_ down to which pyramid level should be tracked
 * @param problem
 * @param maxIterations
 * @param maxRecoveredDistance
 * @return The computed optical flow.
 */
template <std::floating_point Scalar_, typename ImagePyramid_, template <std::floating_point> typename PatchPattern_,
          std::size_t downToLevel_ = 0>
[[nodiscard]] std::optional<Eigen::Transform<Scalar_, 2, Eigen::AffineCompact>> solve_optical_flow_klt_robust(
    const OpticalFlowProblem<Scalar_, ImagePyramid_>& problem, std::size_t maxIterations,
    Scalar_ maxRecoveredDistanceSquared) {
  auto optionalTransform =
      solve_optical_flow_klt<Scalar_, ImagePyramid_, PatchPattern_, downToLevel_>(problem, maxIterations);

  if (!optionalTransform) {
    return {};
  }

  const OpticalFlowProblem<Scalar_, ImagePyramid_> inverseProblem{.fromPyramid = problem.toPyramid,
                                                                  .toPyramid = problem.fromPyramid,
                                                                  .fromPosition = optionalTransform->translation()};

  const auto optionalFromTransformRecovered =
      solve_optical_flow_klt<Scalar_, ImagePyramid_, PatchPattern_, downToLevel_>(inverseProblem, maxIterations);

  if (!optionalFromTransformRecovered) {
    return {};
  }

  constexpr static Scalar_ lowestScale = 1ul << downToLevel_;

  if ((problem.fromPosition - optionalFromTransformRecovered->translation()).squaredNorm() >
      maxRecoveredDistanceSquared * lowestScale) {
    return {};
  }

  return optionalTransform;
}

template <std::floating_point Scalar_, typename ImagePyramid_, typename KeypointPyramid_,
          std::size_t maxKeypointDetectionLevel_, template <std::floating_point> typename PatchPattern_,
          typename Parallelize_ = do_not_parallelize>
void track_points(const ImagePyramid_& fromImagePyramid, const ImagePyramid_& toImagePyramid,
                  const KeypointPyramid_& fromKeypointPyramid, KeypointPyramid_& toKeypointPyramid,
                  Parallelize_ parallelize = Parallelize_()) {
  auto workingKeypointPyramid = fromKeypointPyramid;

  constexpr_for<std::size_t{0}, maxKeypointDetectionLevel_, std::size_t{1}>([&](auto detectionLevel) {
    auto& workingKeypointGrid = workingKeypointPyramid.template get_level<detectionLevel>();

    const auto trackPoints = [&fromImagePyramid, &toImagePyramid, detectionLevel](auto& workingKeypointCollection) {
      auto keypointIterator = workingKeypointCollection.begin();
      while (keypointIterator != workingKeypointCollection.end()) {
        const auto problem =
            OpticalFlowProblem<Scalar_, ImagePyramid_>{.fromPyramid = fromImagePyramid,
                                                       .toPyramid = toImagePyramid,
                                                       .fromPosition = keypointIterator->transform.translation()};
        const auto optionalTransform =
            gneiss::solve_optical_flow_klt_robust<Scalar_, ImagePyramid_, PatchPattern_, detectionLevel>(problem, 7,
                                                                                                         0.4f);

        if (!optionalTransform) {
          keypointIterator = workingKeypointCollection.erase(keypointIterator);
        } else {
          keypointIterator->transform = optionalTransform.value();
          keypointIterator++;
        }
      }
    };
    const auto workingKeypointGridRange = workingKeypointGrid | fields_2d::view::Range;

    if constexpr (parallelize) {
#if GNEISS_PARALLELIZATION == GNEISS_PARALLELIZATION_TBB
      tbb::parallel_for(tbb::blocked_range<std::ranges::range_difference_t<decltype(workingKeypointGridRange)>>(
                            0, workingKeypointGridRange.size()),
                        [&workingKeypointGridRange, &trackPoints](const auto& idxs) {
                          for (auto idx = idxs.begin(); idx != idxs.end(); ++idx) {
                            auto& workingKeypointCollection = workingKeypointGridRange[idx];
                            trackPoints(workingKeypointCollection);
                          }
                        });
#else
      static_assert(!parallelize, "parallelization type not supported");
#endif
    } else {
      gneiss::ranges::for_each(workingKeypointGridRange, trackPoints);
    }

    auto& toKeypointGrid = toKeypointPyramid.template get_level<detectionLevel>();
    for (auto& workingKeypointCollection : workingKeypointGrid | fields_2d::view::Range) {
      for (const auto& keypointDescriptor : workingKeypointCollection) {
        sort_keypoint_into_grid<Scalar_>(keypointDescriptor, toKeypointGrid, detectionLevel);
      }
    }
  });
}

template <std::floating_point Scalar_>
void sort_keypoint_into_grid(const KeypointDescriptor<Scalar_>& keypointDescriptor, auto& keypointGrid,
                             std::size_t detectionLevel) {
  using Vector2 = Eigen::Matrix<Scalar_, 2, 1>;

  const auto scale = static_cast<Scalar_>(1ul << detectionLevel);
  const auto gridCoordinate =
      keypointGrid.image_coordinate_to_grid(Vector2(keypointDescriptor.transform.translation() / scale));
  if (!fields_2d::in_bounds<std::remove_reference_t<decltype(keypointGrid)>>(gridCoordinate)) {
    return;
  }

  auto& keypointCollection = keypointGrid(gridCoordinate);
  if (keypointCollection.is_full()) {
    return;
  }

  bool tooClose = false;
  for (const auto& otherKeypointDescriptor : keypointCollection) {
    tooClose = tooClose || keypointDescriptor.transform.translation().isApprox(
                               otherKeypointDescriptor.transform.translation(), Scalar_{0.5} * scale);
  }

  if (tooClose) {
    return;
  }

  keypointCollection.push_back(keypointDescriptor);
}

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_SOLVE_OPTICAL_FLOW_H
