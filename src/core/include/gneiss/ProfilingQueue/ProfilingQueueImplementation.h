#ifndef GNEISS_PROFILINGQUEUEIMPLEMENTATION_H
#define GNEISS_PROFILINGQUEUEIMPLEMENTATION_H
#pragma once
/** @file */

#include "gneiss/ProfilingQueue/ConcurrentProfilingQueue.h"
#include "gneiss/ProfilingQueue/NoOpProfilingQueue.h"

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

using ProfilingQueueImplementation =
    std::conditional_t<gneiss::parameters::parameters.at("simple_profiling"sv).get<bool>(), ConcurrentProfilingQueue,
                       NoOpProfilingQueue>;

/** @} */  // end of core

}  // namespace gneiss
#endif  // GNEISS_PROFILINGQUEUEIMPLEMENTATION_H
