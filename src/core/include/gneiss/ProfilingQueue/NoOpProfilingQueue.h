#ifndef GNEISS_NOOPPROFILINGQUEUE_H
#define GNEISS_NOOPPROFILINGQUEUE_H
#pragma once
/** @file */

#include "gneiss/ProfilingQueue/ProfilingQueue.h"

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

class NoOpProfilingQueue : public ProfilingQueue<NoOpProfilingQueue> {
 public:
  using ProfilingQueue::ProfilingQueue;

  void pushImpl(ProfilingStats) {}

  // NOLINTNEXTLINE(readability-convert-member-functions-to-static)
  std::optional<ProfilingStats> popImpl() { return {}; }

  void shutdownImpl() {}

  void stopImpl() {}
};

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_NOOPPROFILINGQUEUE_H
