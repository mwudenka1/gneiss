#ifndef GNEISS_CONCURRENTPROFILINGQUEUE_H
#define GNEISS_CONCURRENTPROFILINGQUEUE_H
#pragma once
/** @file */

#include "gneiss/ProfilingQueue/ProfilingQueue.h"
#include "gneiss/parameters/parameters.h"

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

class ConcurrentProfilingQueue : public ProfilingQueue<ConcurrentProfilingQueue> {
 public:
  using ProfilingQueue::ProfilingQueue;

  void pushImpl(ProfilingStats profilingStats) { m_queue.push(profilingStats); }

  std::optional<ProfilingStats> popImpl() { return m_queue.pop(); }

  void shutdownImpl() { m_queue.shutdown(); }

  void stopImpl() { m_queue.stop(); }

 private:
  ConcurrentQueue<ProfilingStats, gneiss::parameters::parameters.at("profiling_queue_capacity"sv).get<std::size_t>(),
                  true>
      m_queue;
};

/** @} */  // end of core

}  // namespace gneiss
#endif  // GNEISS_CONCURRENTPROFILINGQUEUE_H
