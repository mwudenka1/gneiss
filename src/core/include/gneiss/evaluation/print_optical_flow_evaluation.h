#ifndef GNEISS_PRINT_OPTICAL_FLOW_EVALUATION_H
#define GNEISS_PRINT_OPTICAL_FLOW_EVALUATION_H
#pragma once
/** @file */

#include "gneiss/utils/ranges.h"

#include <ostream>
#include <vector>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

void inline print_optical_flow_evaluation(std::ostream& out,
                                          // NOLINTNEXTLINE(bugprone-easily-swappable-parameters)
                                          std::span<const std::vector<float>> cameraIdxFrameIdxRmseMapping,
                                          std::span<const std::vector<float>> cameraIdxFrameIdxNumValidMapping) {
  const auto averageRmseForCamera_pixel =
      cameraIdxFrameIdxRmseMapping | gneiss::ranges::views::transform([](const auto& frameIdxRmseMapping) -> float {
        return frameIdxRmseMapping.size() > 0 ? std::reduce(frameIdxRmseMapping.cbegin(), frameIdxRmseMapping.cend()) /
                                                    static_cast<float>(frameIdxRmseMapping.size())
                                              : 0;
      });

  const auto totalAverageRmse_pixel = gneiss::ranges::fold_left(averageRmseForCamera_pixel, 0, std::plus<float>{}) /
                                      static_cast<float>(averageRmseForCamera_pixel.size());

  const auto averageNumValidForCamera =
      cameraIdxFrameIdxNumValidMapping | gneiss::ranges::views::transform([](const auto& frameIdxNumValidMapping) {
        return frameIdxNumValidMapping.size() > 0
                   ? std::reduce(frameIdxNumValidMapping.cbegin(), frameIdxNumValidMapping.cend()) /
                         static_cast<float>(frameIdxNumValidMapping.size())
                   : 0;
      });
  const auto totalAverageNumValid = gneiss::ranges::fold_left(averageNumValidForCamera, 0, std::plus<float>{}) /
                                    static_cast<float>(averageNumValidForCamera.size());

  out << "optical_flow_rmse_pixel:\n";
  out << "  cameras:\n";
  for (const auto& averageRmse_pixel : averageRmseForCamera_pixel) {
    out << "    - " << averageRmse_pixel << '\n';
  }
  out << "  total: " << totalAverageRmse_pixel << '\n';
  out << "optical_flow_num_valid:\n";
  out << "  cameras:\n";
  for (const auto& averageNumValid : averageNumValidForCamera) {
    out << "    - " << averageNumValid << '\n';
  }
  out << "  total: " << totalAverageNumValid << '\n';
}

/** @} */  // end of core

}  // namespace gneiss
#endif  // GNEISS_PRINT_OPTICAL_FLOW_EVALUATION_H
