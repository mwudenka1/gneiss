#ifndef GNEISS_CALCULATE_OPTICAL_FLOW_SE_H
#define GNEISS_CALCULATE_OPTICAL_FLOW_SE_H
#pragma once
/** @file */

#include "gneiss/io/OpticalFlow.h"
#include "gneiss/utils/ranges.h"

#include <vector>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

template <typename Scalar_>
std::vector<float> calculate_optical_flow_se_pixel2(const auto& previousOpticalFlow, const auto& currentOpticalFlow,
                                                    const gneiss::io::OpticalFlow& groundTruthOpticalFlow) {
  using Vector2 = Eigen::Matrix<Scalar_, 2, 1>;

  std::vector<float> errors_pixel2;

  for (const auto& currentKeypointDescriptorAtLevel : currentOpticalFlow) {
    const auto previousKeypointDescriptorAtLevelIterator =
        std::find_if(previousOpticalFlow.begin(), previousOpticalFlow.end(),
                     [&currentKeypointDescriptorAtLevel](const auto& prevOpticalFlowDes) {
                       return prevOpticalFlowDes.keypoint.id == currentKeypointDescriptorAtLevel.keypoint.id;
                     });

    if (previousKeypointDescriptorAtLevelIterator == previousOpticalFlow.end()) {
      continue;
    }

    const auto& previousKeypointDescriptorAtLevel = *previousKeypointDescriptorAtLevelIterator;

    const auto optionalGroundTruthOpticalFlowVector =
        groundTruthOpticalFlow.get_optical_flow(fields_2d::FieldCoordinate<double>(
            previousKeypointDescriptorAtLevel.keypoint.transform.translation().template cast<double>()));

    if (!optionalGroundTruthOpticalFlowVector) {
      continue;
    }

    const Vector2 groundTruthOpticalFlowVector = optionalGroundTruthOpticalFlowVector.value().template cast<Scalar_>();

    const Vector2 opticalFlowVector = currentKeypointDescriptorAtLevel.keypoint.transform.translation() -
                                      previousKeypointDescriptorAtLevel.keypoint.transform.translation();

    errors_pixel2.push_back(static_cast<float>((opticalFlowVector - groundTruthOpticalFlowVector).squaredNorm()));
  }
  return errors_pixel2;
}

/** @} */  // end of core

}  // namespace gneiss
#endif  // GNEISS_CALCULATE_OPTICAL_FLOW_SE_H
