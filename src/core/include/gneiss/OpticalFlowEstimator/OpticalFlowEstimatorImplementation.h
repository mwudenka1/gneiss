#ifndef GNEISS_OPTICALFLOWESTIMATORIMPLEMENTATION_H
#define GNEISS_OPTICALFLOWESTIMATORIMPLEMENTATION_H
#pragma once
/** @file */

#include "gneiss/ImageQueue/ImageQueueImplementation.h"
#include "gneiss/KeypointsQueue/KeypointsQueueImplementation.h"
#include "gneiss/OpticalFlowEstimator/OpticalFlowEstimator.h"
#include "gneiss/ProfilingQueue/ProfilingQueueImplementation.h"
#include "gneiss/Storage/StorageImplementation.h"
#include "gneiss/image/ImagePyramid.h"
#include "gneiss/image/KeypointsEstimate.h"
#include "gneiss/image/detect_keypoints.h"
#include "gneiss/optical-flow/OpticalFlowProblem.h"
#include "gneiss/optical-flow/ParameterizedPatchPattern.h"
#include "gneiss/optical-flow/solve_optical_flow.h"
#include "gneiss/parameters/parameters.h"
#include "gneiss/profiling/Profiler.h"
#include "gneiss/utils/constexpr_for.h"

#include <thread>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

class OpticalFlowEstimatorImplementation
    : public OpticalFlowEstimator<OpticalFlowEstimatorImplementation, ImageQueueImplementation,
                                  KeypointsQueueImplementation, ProfilingQueueImplementation, StorageImplementation> {
 public:
  constexpr static std::string_view optical_flow_scalar =
      parameters::parameters["optical_flow_scalar"sv].template get<std::string_view>();
  using Scalar = string_to_type_t<FixedString<optical_flow_scalar.size()>(optical_flow_scalar.data())>;

  OpticalFlowEstimatorImplementation(ImageQueue<ImageQueueImplementation>& imageQueue,
                                     KeypointsQueue<KeypointsQueueImplementation>& keypointsQueue,
                                     ProfilingQueue<ProfilingQueueImplementation>& profilingQueue,
                                     Storage<StorageImplementation>& storage)
      : OpticalFlowEstimator(imageQueue, keypointsQueue, profilingQueue, storage),
        m_worker(std::thread(&OpticalFlowEstimatorImplementation::worker_thread, this)) {}

  ~OpticalFlowEstimatorImplementation() {
    if (m_worker.joinable()) {
      m_worker.join();
    }
  }

  void shutdownImpl() {}

 private:
  constexpr static auto& cameras = gneiss::parameters::parameters["cameras"sv].array_data();
  static constexpr std::size_t padding = 1;

  template <std::size_t cameraIdx_>
  struct ParameterizedImagePyramid {
    constexpr static std::size_t pyramidLevel =
        std::next(cameras.begin(), cameraIdx_)->at("optical_flow_level"sv).template get<std::size_t>();
    constexpr static std::size_t width =
        std::next(cameras.begin(), cameraIdx_)->at("res_x_px"sv).template get<std::size_t>();
    constexpr static std::size_t height =
        std::next(cameras.begin(), cameraIdx_)->at("res_y_px"sv).template get<std::size_t>();
    constexpr static std::size_t bitsPerPixel =
        std::next(cameras.begin(), cameraIdx_)->at("bits_per_pixel"sv).template get<uint64_t>();
    using ImagePyramid_ = ImagePyramid<pyramidLevel, least_uint<bitsPerPixel>, width, height>;

    std::optional<ImagePyramid_> imagePyramid;
  };

  template <typename CameraIndexes_>
  struct ImagePyramides;

  template <std::size_t... cameraIndexes_>
  struct ImagePyramides<std::index_sequence<cameraIndexes_...>> : public ParameterizedImagePyramid<cameraIndexes_>... {
    template <std::size_t cameraIdx_>
    [[nodiscard]] constexpr inline auto& get() {
      return ParameterizedImagePyramid<cameraIdx_>::imagePyramid;
    }

    template <std::size_t cameraIdx_>
    [[nodiscard]] constexpr inline const auto& get() const {
      return ParameterizedImagePyramid<cameraIdx_>::imagePyramid;
    }
  };

  using ParameterizedImagePyramides = ImagePyramides<std::make_index_sequence<cameras.size()>>;

  template <std::size_t cameraIdx_>
  struct ParameterizedKeypointPyramid {
    constexpr static std::size_t blockSize =
        std::next(cameras.begin(), cameraIdx_)->at("optical_flow_detection_grid_size"sv).template get<std::size_t>();
    constexpr static std::size_t gridCapacity =
        std::next(cameras.begin(), cameraIdx_)->at("optical_flow_grid_cell_capacity"sv).template get<std::size_t>();
    constexpr static std::size_t keypointDetectionLevel =
        std::next(cameras.begin(), cameraIdx_)->at("keypoint_detection_level"sv).template get<std::size_t>();
    constexpr static std::size_t width =
        std::next(cameras.begin(), cameraIdx_)->at("res_x_px"sv).template get<std::size_t>();
    constexpr static std::size_t height =
        std::next(cameras.begin(), cameraIdx_)->at("res_y_px"sv).template get<std::size_t>();
    constexpr static std::string_view optical_flow_scalar =
        parameters::parameters["optical_flow_scalar"sv].template get<std::string_view>();
    using Scalar = string_to_type_t<FixedString<optical_flow_scalar.size()>(optical_flow_scalar.data())>;
    using KeypointPyramid_ = KeypointPyramid<Scalar,
                                             GridPyramidParameters{.baseImageWidth = width,
                                                                   .baseImageHeight = height,
                                                                   .gridSize = blockSize,
                                                                   .padding = padding,
                                                                   .gridCapacity = gridCapacity},
                                             keypointDetectionLevel>;

    std::optional<KeypointPyramid_> keypointPyramid;
  };

  template <typename CameraIndexes_>
  struct KeypointPyramides;

  template <std::size_t... cameraIndexes_>
  struct KeypointPyramides<std::index_sequence<cameraIndexes_...>>
      : public ParameterizedKeypointPyramid<cameraIndexes_>... {
    template <std::size_t cameraIdx_>
    [[nodiscard]] constexpr inline auto& get() {
      return ParameterizedKeypointPyramid<cameraIdx_>::keypointPyramid;
    }

    template <std::size_t cameraIdx_>
    [[nodiscard]] constexpr inline const auto& get() const {
      return ParameterizedKeypointPyramid<cameraIdx_>::keypointPyramid;
    }
  };

  using ParameterizedKeypointPyramides = KeypointPyramides<std::make_index_sequence<cameras.size()>>;

  void worker_thread() {
    while (true) {
      const auto optionalImageMeasurementPtr = m_imageQueue.pop();

      if (!optionalImageMeasurementPtr || !optionalImageMeasurementPtr.value()) {
        m_keypointsQueue.stop();
        m_profilingQueue.stop();
        break;
      }
      auto estimatePtr = cycle(*optionalImageMeasurementPtr.value());
      m_keypointsQueue.push(std::move(estimatePtr));
    }
  }

  KeypointsEstimateConstPtr cycle(const auto& imageMeasurement) {
    [[maybe_unused]] const auto profiler = Profiler(ProfilingTarget::OPTICAL_FLOW_ESTIMATOR, m_profilingQueue);

    const auto currentImagePyramides = create_image_pyramides(imageMeasurement);

    auto currentKeypointPyramides = track_points_in_time(currentImagePyramides);

    add_points(currentImagePyramides, currentKeypointPyramides);

    previousImagePyramides = currentImagePyramides;
    previousKeypointPyramides = currentKeypointPyramides;

    auto estimatePtr = m_storage.storeKeypointsEstimate(
        keypoint_pyramides_to_keypoints_estimate(currentKeypointPyramides, imageMeasurement.timestamp()));

    return estimatePtr;
  }

  ParameterizedImagePyramides create_image_pyramides(const auto& imageMeasurement) {
    [[maybe_unused]] const auto pyramidProfiler =
        Profiler(ProfilingTarget::OPTICAL_FLOW_PYRAMID_CREATION, m_profilingQueue);

    ParameterizedImagePyramides pyramides;

    constexpr_for<0, cameras.size(), 1>([&](auto cameraIdx) {
      const std::size_t pyramidLevel =
          std::next(cameras.begin(), cameraIdx)->at("optical_flow_level"sv).template get<uint64_t>();

      const auto currentImagePyramid = make_image_pyramid<
          pyramidLevel,
          std::conditional_t<
              gneiss::parameters::parameters.at("optical_flow_parallelize_pyramid_creation"sv).get<bool>(),
              do_parallelize, do_not_parallelize>>(imageMeasurement.template get<cameraIdx>());

      pyramides.template get<cameraIdx>() = currentImagePyramid;
    });

    return pyramides;
  }

  ParameterizedKeypointPyramides track_points_in_time(const ParameterizedImagePyramides& currentImagePyramides) {
    [[maybe_unused]] const auto trackingProfiler =
        Profiler(ProfilingTarget::OPTICAL_FLOW_POINTS_TRACKING, m_profilingQueue);

    ParameterizedKeypointPyramides keypointPyramides;

    constexpr static bool parallelize =
        gneiss::parameters::parameters.at("optical_flow_parallelize_points_tracking"sv).get<bool>();

    constexpr_for<0, cameras.size(), 1>([&](auto cameraIdx) {
      constexpr static std::size_t maxKeypointDetectionLevel =
          std::next(cameras.begin(), cameraIdx)->at("keypoint_detection_level"sv).template get<uint64_t>();

      const auto& currentImagePyramid = currentImagePyramides.template get<cameraIdx>().value();
      const auto& previousImagePyramid = previousImagePyramides.get<cameraIdx>();
      const auto& previousKeypointPyramid = previousKeypointPyramides.get<cameraIdx>();

      auto currentKeypointPyramid = typename ParameterizedKeypointPyramid<cameraIdx>::KeypointPyramid_{};

      if (previousImagePyramid && previousKeypointPyramid) {
        track_points<Scalar, typename ParameterizedImagePyramid<cameraIdx>::ImagePyramid_,
                     typename ParameterizedKeypointPyramid<cameraIdx>::KeypointPyramid_, maxKeypointDetectionLevel,
                     ParameterizedPatchPattern<cameraIdx>::template type,
                     std::conditional_t<parallelize, do_parallelize, do_not_parallelize>>(
            previousImagePyramid.value(), currentImagePyramid, previousKeypointPyramid.value(), currentKeypointPyramid);
      }
      keypointPyramides.template get<cameraIdx>() = currentKeypointPyramid;
    });

    return keypointPyramides;
  }

  void add_points(const ParameterizedImagePyramides& currentImagePyramides,
                  ParameterizedKeypointPyramides& currentKeypointPyramides) {
    [[maybe_unused]] const auto pointDetectionProfiler =
        Profiler(ProfilingTarget::OPTICAL_FLOW_KEYPOINT_DETECTION, m_profilingQueue);
    constexpr_for<0, cameras.size(), 1>([&](auto cameraIdx) {
      constexpr static std::size_t blockSize =
          std::next(cameras.begin(), cameraIdx)->at("optical_flow_detection_grid_size"sv).template get<uint64_t>();
      constexpr static std::size_t gridCapacity =
          std::next(cameras.begin(), cameraIdx)->at("optical_flow_grid_cell_capacity"sv).template get<std::size_t>();
      constexpr static std::size_t keypointDetectionLevel =
          std::next(cameras.begin(), cameraIdx)->at("keypoint_detection_level"sv).template get<uint64_t>();

      const auto& currentImagePyramid = currentImagePyramides.template get<cameraIdx>().value();
      auto& currentKeypointPyramid = currentKeypointPyramides.template get<cameraIdx>().value();

      detect_keypoints<
          Scalar, blockSize, padding, gridCapacity, std::remove_reference_t<decltype(currentImagePyramid)>,
          keypointDetectionLevel,
          std::conditional_t<
              gneiss::parameters::parameters.at("optical_flow_parallelize_keypoint_detection"sv).get<bool>(),
              do_parallelize, do_not_parallelize>>(currentImagePyramid, currentKeypointPyramid);

      constexpr_for<std::size_t{0}, keypointDetectionLevel, std::size_t{1}>([&](auto level) {
        auto& grid = currentKeypointPyramid.template get_level<level>();

        for (auto& keypointCollection : grid | fields_2d::view::Range) {
          for (auto& keypointDescriptor : keypointCollection) {
            if (keypointDescriptor.id == unidentifiedKeypoint) {
              keypointDescriptor.id = nextKeypointId;
              nextKeypointId++;
            }
          }
        }
      });
    });
  }

  static KeypointsEstimate keypoint_pyramides_to_keypoints_estimate(
      const ParameterizedKeypointPyramides& currentKeypointPyramides, const Timestamp& timestamp) {
    KeypointsEstimate estimate;
    estimate.timestamp() = timestamp;

    constexpr_for<0, cameras.size(), 1>([&](auto cameraIdx) {
      const std::size_t keypointDetectionLevel =
          std::next(cameras.begin(), cameraIdx)->at("keypoint_detection_level"sv).template get<uint64_t>();

      const auto& currentKeypointPyramid = currentKeypointPyramides.template get<cameraIdx>().value();

      const auto keypointDescriptors = ([cameraIdx, &currentKeypointPyramid]() {
        using Keypoints_ = std::remove_reference_t<decltype(estimate.get<cameraIdx>())>;

        Keypoints_ descriptors;
        constexpr_for<std::size_t{0}, keypointDetectionLevel, std::size_t{1}>([&](auto level) {
          const auto& grid = currentKeypointPyramid.template get_level<level>();

          for (auto& keypointCollection : grid | fields_2d::view::Range) {
            for (const auto& keypointDescriptor : keypointCollection) {
              descriptors.push_back({.keypoint = keypointDescriptor, .level = level});
            }
          }
        });
        return descriptors;
      })();

      estimate.get<cameraIdx>() = keypointDescriptors;
    });

    return estimate;
  }

  std::thread m_worker;
  std::size_t nextKeypointId = 1;
  ParameterizedImagePyramides previousImagePyramides;
  KeypointPyramides<std::make_index_sequence<cameras.size()>> previousKeypointPyramides;
};

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_OPTICALFLOWESTIMATORIMPLEMENTATION_H
