#ifndef GNEISS_IMAGEQUEUEIMPLEMENTATION_H
#define GNEISS_IMAGEQUEUEIMPLEMENTATION_H
#pragma once
/** @file */

#include "gneiss/ImageQueue/ImageQueue.h"
#include "gneiss/parameters/parameters.h"
#include "gneiss/utils/ConcurrentQueue.h"
#include "gneiss/utils/unused.h"

#include <iostream>
#include <string_view>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

class ImageQueueImplementation : public ImageQueue<ImageQueueImplementation> {
 public:
  using ImageQueue::ImageQueue;

  void pushImpl(ImageMeasurementConstPtr imageMeasurement) { m_queue.push(std::move(imageMeasurement)); }

  std::optional<ImageMeasurementConstPtr> popImpl() { return m_queue.pop(); }

  void shutdownImpl() { m_queue.shutdown(); }

  void stopImpl() { m_queue.stop(); }

 private:
  ConcurrentQueue<ImageMeasurementConstPtr,
                  gneiss::parameters::parameters.at("image_queue_capacity"sv).get<std::size_t>()>
      m_queue;
};

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_IMAGEQUEUEIMPLEMENTATION_H
