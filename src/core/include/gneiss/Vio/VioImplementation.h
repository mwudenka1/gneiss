#ifndef GNEISS_VIOIMPLEMENTATION_H
#define GNEISS_VIOIMPLEMENTATION_H
#pragma once
/** @file */

#include "gneiss/ImuQueue/ImuQueueImplementation.h"
#include "gneiss/KeypointsQueue/KeypointsQueueImplementation.h"
#include "gneiss/ProfilingQueue/ProfilingQueueImplementation.h"
#include "gneiss/Storage/StorageImplementation.h"
#include "gneiss/Vio/Vio.h"
#include "gneiss/VioQueue/VioQueueImplementation.h"

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

class VioImplementation : public Vio<VioImplementation, ImuQueueImplementation, KeypointsQueueImplementation,
                                     ProfilingQueueImplementation, StorageImplementation, VioQueueImplementation> {
 public:
  using Vio::Vio;

  void shutdownImpl() {}
};

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_VIOIMPLEMENTATION_H