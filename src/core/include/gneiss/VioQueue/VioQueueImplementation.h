#ifndef GNEISS_VIOQUEUEIMPLEMENTATION_H
#define GNEISS_VIOQUEUEIMPLEMENTATION_H
#pragma once
/** @file */

#include "gneiss/VioQueue/VioQueue.h"

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

class VioQueueImplementation : public VioQueue<VioQueueImplementation> {
 public:
  using VioQueue::VioQueue;

  void shutdownImpl() {}

  void stopImpl() {}
};

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_VIOQUEUEIMPLEMENTATION_H
