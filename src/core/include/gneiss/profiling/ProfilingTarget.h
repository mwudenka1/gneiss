#ifndef GNEISS_PROFILINGTARGET_H
#define GNEISS_PROFILINGTARGET_H
#pragma once
/** @file */

#include <fmt/format.h>

#include <cstdint>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

enum ProfilingTarget : uint8_t {
  OPTICAL_FLOW_ESTIMATOR = 0,
  OPTICAL_FLOW_PYRAMID_CREATION,
  OPTICAL_FLOW_POINTS_TRACKING,
  OPTICAL_FLOW_KEYPOINT_DETECTION
};

inline auto format_as(ProfilingTarget profilingTarget) { return fmt::underlying(profilingTarget); }

/** @} */  // end of core

}  // namespace gneiss
#endif  // GNEISS_PROFILINGTARGET_H
