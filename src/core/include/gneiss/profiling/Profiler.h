#ifndef GNEISS_PROFILER_H
#define GNEISS_PROFILER_H
#pragma once
/** @file */

#include "gneiss/parameters/parameters.h"
#include "gneiss/profiling/NoOpProfiler.h"
#include "gneiss/profiling/ScopedProfiler.h"

#include <string_view>
#include <type_traits>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

using Profiler = std::conditional_t<gneiss::parameters::parameters.at("simple_profiling"sv).get<bool>(),
                                    ScopedProfiler<ProfilingQueue<ProfilingQueueImplementation>>,
                                    NoOpProfiler<ProfilingQueue<ProfilingQueueImplementation>>>;

/** @} */  // end of core

}  // namespace gneiss
#endif  // GNEISS_PROFILER_H
