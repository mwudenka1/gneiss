#ifndef GNEISS_NOOPPROFILER_H
#define GNEISS_NOOPPROFILER_H
#pragma once
/** @file */

#include <chrono>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

template <typename ProfilingQueue_>
class NoOpProfiler {
 public:
  NoOpProfiler(ProfilingTarget, ProfilingQueue_&) {}
};

/** @} */  // end of core

}  // namespace gneiss
#endif  // GNEISS_NOOPPROFILER_H
