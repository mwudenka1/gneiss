#ifndef GNEISS_PROFILINGSTATS_H
#define GNEISS_PROFILINGSTATS_H
#pragma once
/** @file */

#include "gneiss/profiling/ProfilingTarget.h"

#include <chrono>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

struct ProfilingStats {
  ProfilingTarget target;
  std::chrono::duration<int64_t, std::milli> executionTime;
};

/** @} */  // end of core

}  // namespace gneiss
#endif  // GNEISS_PROFILINGSTATS_H
