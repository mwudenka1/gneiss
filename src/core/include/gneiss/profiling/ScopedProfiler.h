#ifndef GNEISS_SCOPEDPROFILER_H
#define GNEISS_SCOPEDPROFILER_H
#pragma once
/** @file */

#include "gneiss/profiling/ProfilingStats.h"
#include "gneiss/profiling/ProfilingTarget.h"

#include <chrono>

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

template <typename ProfilingQueue_>
class ScopedProfiler {
 public:
  ScopedProfiler(ProfilingTarget target, ProfilingQueue_& profilingQueue)
      : m_target(target), m_profilingQueue(profilingQueue), m_start(std::chrono::high_resolution_clock::now()) {}

  ~ScopedProfiler() {
    const auto end = std::chrono::high_resolution_clock::now();
    m_profilingQueue.push(ProfilingStats{
        .target = m_target,
        .executionTime = std::chrono::duration_cast<std::chrono::duration<int64_t, std::milli>>(end - m_start)});
  }

 private:
  ProfilingTarget m_target;
  ProfilingQueue_& m_profilingQueue;
  std::chrono::time_point<std::chrono::high_resolution_clock> m_start;
};

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_SCOPEDPROFILER_H
