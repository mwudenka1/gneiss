#ifndef GNEISS_KEYPOINTSQUEUEIMPLEMENTATION_H
#define GNEISS_KEYPOINTSQUEUEIMPLEMENTATION_H
#pragma once
/** @file */

#include "gneiss/KeypointsQueue/KeypointsQueue.h"

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

class KeypointsQueueImplementation : public KeypointsQueue<KeypointsQueueImplementation> {
 public:
  using KeypointsQueue::KeypointsQueue;

  void pushImpl(KeypointsEstimateConstPtr keypointsEstimate) { m_queue.push(std::move(keypointsEstimate)); }

  std::optional<KeypointsEstimateConstPtr> popImpl() { return m_queue.pop(); }

  void shutdownImpl() { m_queue.shutdown(); }

  void stopImpl() { m_queue.stop(); }

 private:
  ConcurrentQueue<KeypointsEstimateConstPtr,
                  gneiss::parameters::parameters.at("keypoints_queue_capacity"sv).get<std::size_t>()>
      m_queue;
};

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_KEYPOINTSQUEUEIMPLEMENTATION_H
