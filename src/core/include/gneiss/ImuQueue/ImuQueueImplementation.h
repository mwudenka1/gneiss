#ifndef GNEISS_IMUQUEUEIMPLEMENTATION_H
#define GNEISS_IMUQUEUEIMPLEMENTATION_H
#pragma once
/** @file */

#include "gneiss/ImuQueue/ImuQueue.h"

namespace gneiss {

/**
 * @addtogroup core
 *
 * @{
 */

class ImuQueueImplementation : public ImuQueue<ImuQueueImplementation> {
 public:
  using ImuQueue::ImuQueue;

  void shutdownImpl() {}

  void stopImpl() {}
};

/** @} */  // end of core

}  // namespace gneiss

#endif  // GNEISS_IMUQUEUEIMPLEMENTATION_H
