#ifndef GNEISS_IGNORE_H
#define GNEISS_IGNORE_H
#pragma once
/** @file */

namespace gneiss {

/**
 * @addtogroup utils
 *
 * @{
 */

/**
 * @brief Complements std::true_type and std::false_type.
 *
 * taken from https://github.com/ericniebler/range-v3/blob/master/include/range/v3/range_fwd.hpp
 */
struct ignore_type {
  ignore_type() = default;
  template <typename Type_>
  // NOLINTNEXTLINE(bugprone-forwarding-reference-overload,google-explicit-constructor,hicpp-explicit-conversions)
  constexpr ignore_type(Type_ &&) noexcept {}

  template <typename Type_>
  // NOLINTNEXTLINE(cppcoreguidelines-c-copy-assignment-signature,misc-unconventional-assign-operator)
  constexpr ignore_type const &operator=(Type_ &&) const noexcept {
    return *this;
  }
};

/** @} */  // end of utils

}  // namespace gneiss

#endif  // GNEISS_IGNORE_H