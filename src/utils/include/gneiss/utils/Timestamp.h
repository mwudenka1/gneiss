#ifndef GNEISS_TIMESTAMP_H
#define GNEISS_TIMESTAMP_H
#pragma once
/** @file */

#include "gneiss/utils/Duration.h"

#include <chrono>
#include <functional>

namespace gneiss {

/**
 * @addtogroup utils
 *
 * @{
 */

using Timestamp = std::chrono::time_point<std::chrono::steady_clock, Duration>;

/** @} */  // end of utils

}  // namespace gneiss

template <>
struct std::hash<gneiss::Timestamp> {
  std::size_t operator()(const gneiss::Timestamp& timestamp) const {
    return std::hash<gneiss::Duration::rep>()(timestamp.time_since_epoch().count());
  }
};

#endif  // GNEISS_TIMESTAMP_H
