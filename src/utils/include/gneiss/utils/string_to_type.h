#ifndef GNEISS_STRING_TO_TYPE_H
#define GNEISS_STRING_TO_TYPE_H
#pragma once
/** @file */

#include "gneiss/utils/FixedString.h"

namespace gneiss {

/**
 * @addtogroup utils
 *
 * @{
 */

template <FixedString specifier_>
struct string_to_type;

template <>
struct string_to_type<"float"> {
  using type = float;
};

template <>
struct string_to_type<"double"> {
  using type = double;
};

template <FixedString specifier_>
using string_to_type_t = string_to_type<specifier_>::type;

/** @} */  // end of utils

}  // namespace gneiss
#endif  // GNEISS_STRING_TO_TYPE_H
