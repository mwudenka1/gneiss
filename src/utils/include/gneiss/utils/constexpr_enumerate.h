#ifndef GNEISS_CONSTEXPR_FOR_H
#define GNEISS_CONSTEXPR_FOR_H
#pragma once
/** @file */

#include "gneiss/utils/constant.h"
#include "gneiss/utils/ranges.h"

#include <concepts>
#include <iterator>
#include <type_traits>

namespace gneiss {

/**
 * @addtogroup utils
 *
 * @{
 */

/**
 * @brief Ensures a type has an at(std::size_t) operator
 *
 * @tparam Type_
 */
template <typename Type_>
concept has_at = requires(Type_ instance, const std::size_t index) { instance.at(index); };

/**
 * @brief Ensures the size of a type can be retrieved with std::tuple_size
 *
 * @tparam Type_
 */
template <typename Type_>
concept has_size = requires(Type_ instance) { std::tuple_size<Type_>(); };

/**
 * @brief Conjunction of \ref gneiss::has_at and \ref gneiss::has_size
 *
 * @tparam Type_
 */
template <typename Type_>
concept has_at_and_size = has_at<Type_> && has_size<Type_>;

/**
 * @brief constexpr enumerate approximation. Use the overload with 2 template parameters as entry point.
 *
 * @tparam Start_ Current iteration index
 * @tparam End_ Last iteration index (will not be reached)
 * @tparam Inc_ Iteration step
 * @tparam container container to iterate over
 * @tparam Body_ type of lambda (will be deduced)
 * @param body lambda
 */
template <std::size_t Start_, std::size_t End_, std::size_t Inc_, auto container, typename Body_>
  requires has_at<decltype(container)>
constexpr void constexpr_enumerate(Body_&& body) {
  if constexpr (Start_ < End_) {
    constexpr const auto value = container.at(Start_);
    std::invoke(body, std::integral_constant<decltype(Start_), Start_>(), constant<decltype(value), value>());
    constexpr_enumerate<Start_ + Inc_, End_, Inc_, container>(std::forward<Body_>(body));
  }
}

/**
 * @brief constexpr enumerate approximation
 *
 * Usage:
 * \code{.cpp}
 * std::array<int, 5> data = {0, 1, 2, 3, 4};
 * constexpr_enumerate<data>([&](auto idx, auto datumConstant) {do something with idx});
 * \endcode
 *
 * see https://artificial-mind.net/blog/2020/10/31/constexpr-for
 *
 * @tparam container container to iterate over
 * @tparam Body_ type of lambda (will be deduced)
 * @param body lambda
 */
template <auto container, typename Body_>
  requires has_at_and_size<decltype(container)>
constexpr void constexpr_enumerate(Body_&& body) {
  constexpr_enumerate<static_cast<std::size_t>(0), static_cast<std::size_t>(std::tuple_size<decltype(container)>()),
                      static_cast<std::size_t>(1), container>(std::forward<Body_>(body));
}

/** @} */  // end of utils

}  // namespace gneiss

#endif  // GNEISS_CONSTEXPR_FOR_H
