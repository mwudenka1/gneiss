#ifndef GNEISS_FIXEDSTRING_H
#define GNEISS_FIXEDSTRING_H
#pragma once
/** @file */

namespace gneiss {

/**
 * @addtogroup utils
 *
 * @{
 */

/**
 * @brief fixed length char container
 *
 * Useful as template parameter that can directly deduced from const char*
 *
 * See https://stackoverflow.com/a/58841797
 *
 * Usage:
 * \code{.cpp}
 * template<FixedString name_>
 * struct NamedThing{};
 *
 * NamedThing<"Bob"> bob{};
 * \endcode
 *
 * @tparam n_ string length without trailing 0
 */
template <unsigned n_>
struct FixedString {
  // NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays,hicpp-avoid-c-arrays,modernize-avoid-c-arrays,misc-non-private-member-variables-in-classes)
  char buf[n_ + 1]{};
  // NOLINTNEXTLINE(google-explicit-constructor,hicpp-explicit-conversions)
  constexpr FixedString(char const* charArray) {
    for (unsigned i = 0; i != n_; ++i) {
      // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic,cppcoreguidelines-pro-bounds-constant-array-index)
      buf[i] = charArray[i];
    }
  }

  // NOLINTNEXTLINE(google-explicit-constructor,hicpp-explicit-conversions)
  constexpr operator char const*() const { return buf; }
};
template <unsigned n_>
// NOLINTNEXTLINE(cppcoreguidelines-avoid-c-arrays,hicpp-avoid-c-arrays,modernize-avoid-c-arrays)
FixedString(char const (&)[n_]) -> FixedString<n_ - 1>;

/** @} */  // end of utils

}  // namespace gneiss

#endif  // GNEISS_FIXEDSTRING_H
