#ifndef GNEISS_ASSERT_H
#define GNEISS_ASSERT_H
#pragma once
/** @file */

#include <cstdint>
#include <cstdlib>
#include <iostream>

namespace gneiss {

/**
 * @addtogroup utils
 *
 * @{
 */

inline void assertion_failed(std::string_view expr, std::string_view function, std::string_view file, int64_t line) {
  std::cerr << "***** Assertion (" << expr << ") failed in " << function << ":\n" << file << ':' << line << ":" << '\n';
  std::abort();
}

inline void assertion_failed_msg(std::string_view expr, std::string_view msg, std::string_view function,
                                 std::string_view file, int64_t line) {
  std::cerr << "***** Assertion (" << expr << ") failed in " << function << ":\n"
            << file << ':' << line << ": " << msg << '\n';
  std::abort();
}

/** @} */  // end of utils

}  // namespace gneiss

/**
 * @addtogroup utils
 *
 * @{
 */

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define GNEISS_LIKELY(boolean_expr) __builtin_expect(!!(boolean_expr), 1)
// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define GNEISS_UNLIKELY(boolean_expr) __builtin_expect(!!(boolean_expr), 0)

#if defined(GNEISS_DISABLE_ASSERTS)

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define GNEISS_ASSERT(expr) ((void)0)

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define GNEISS_ASSERT_MSG(expr, msg) ((void)0)

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define GNEISS_ASSERT_STREAM(expr, msg) ((void)0)

#else

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define GNEISS_ASSERT(expr) \
  (GNEISS_LIKELY(!!(expr)) ? ((void)0) : ::gneiss::assertion_failed(#expr, __PRETTY_FUNCTION__, __FILE__, __LINE__))

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define GNEISS_ASSERT_MSG(expr, msg)   \
  (GNEISS_LIKELY(!!(expr)) ? ((void)0) \
                           : ::gneiss::assertion_failed_msg(#expr, msg, __PRETTY_FUNCTION__, __FILE__, __LINE__))

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define GNEISS_ASSERT_STREAM(expr, msg)                        \
  (GNEISS_LIKELY(!!(expr)) ? ((void)0)                         \
                           : (std::cerr << (msg) << std::endl, \
                              ::gneiss::assertion_failed(#expr, __PRETTY_FUNCTION__, __FILE__, __LINE__)))

#endif

/** @} */  // end of utils

#endif  // GNEISS_ASSERT_H
