#ifndef GNEISS_LEAST_UINT_H
#define GNEISS_LEAST_UINT_H
#pragma once
/** @file */

#include <cstdint>

namespace gneiss {

namespace internal {
// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
template <int bytes>
struct least_uint_special;
template <>
struct least_uint_special<1> {
  using type = std::uint8_t;
};
template <>
struct least_uint_special<2> {
  using type = std::uint16_t;
};
template <>
struct least_uint_special<3> {
  using type = std::uint32_t;
};
template <>
struct least_uint_special<4> {
  using type = std::uint32_t;
};
template <>
struct least_uint_special<5> {
  using type = std::uint64_t;
};
template <>
struct least_uint_special<6> {
  using type = std::uint64_t;
};
template <>
struct least_uint_special<7> {
  using type = std::uint64_t;
};
template <>
struct least_uint_special<8> {
  using type = std::uint64_t;
};
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
}  // namespace internal

/**
 * @addtogroup utils
 *
 * @{
 */

/**
 * Gives the smallest unsigned integer type with at least 'bits' bits
 *
 * @tparam bits least bits the type has to hold
 */
template <int bits>
// NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
using least_uint = internal::least_uint_special<(bits + 7) / 8>::type;

/** @} */  // end of utils

}  // namespace gneiss
#endif  // GNEISS_LEAST_UINT_H
