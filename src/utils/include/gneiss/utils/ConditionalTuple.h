#ifndef GNEISS_CONDITIONALTUPLE_H
#define GNEISS_CONDITIONALTUPLE_H
#pragma once

#include <tuple>

/** @file */
namespace gneiss {
/**
 * @addtogroup utils
 *
 * @{
 */

template <typename...>
struct Typelist {};

template <bool...>
struct Boollist {};

namespace internal {
template <typename, typename>
struct PrependTuple;

template <typename T, typename... Args>
struct PrependTuple<T, std::tuple<Args...>> {
  using type = std::tuple<T, Args...>;
};

template <typename Typelist_, typename Boollist_>
struct ConditionalTupleImpl;

template <>
struct ConditionalTupleImpl<Typelist<>, Boollist<>> {
  using type = std::tuple<>;
};

template <typename TypesHead_, typename... TypesTail_, bool CondHead_, bool... CondTail_>
struct ConditionalTupleImpl<Typelist<TypesHead_, TypesTail_...>, Boollist<CondHead_, CondTail_...>> {
  using type = std::conditional_t<
      CondHead_,
      typename PrependTuple<TypesHead_,
                            typename ConditionalTupleImpl<Typelist<TypesTail_...>, Boollist<CondTail_...>>::type>::type,
      typename ConditionalTupleImpl<Typelist<TypesTail_...>, Boollist<CondTail_...>>::type>;
};

template <typename Tuple_>
struct RewriteTuple;

template <>
struct RewriteTuple<std::tuple<>> {
  using type = void;
};

template <typename Type_>
struct RewriteTuple<std::tuple<Type_>> {
  using type = Type_;
};

template <typename... Types_>
struct RewriteTuple<std::tuple<Types_...>> {
  using type = std::tuple<Types_...>;
};
}  // namespace internal

template <typename Typelist_, typename Boollist_>
struct ConditionalTuple;

/**
 * @brief Constructs a tuple from specified types and conditions.
 *
 * Filters the provided type list based on corresponding boolean conditions
 * and generates a tuple containing only the types with `true` conditions.
 * If the conditions filter out all types void will be returned.
 * If only one type remains, that type is returned.
 *
 * @tparam Types_ List of types to be conditionally included in the tuple.
 * @tparam Cond_ Boolean conditions to filter the types.
 */
template <typename... Types_, bool... Cond_>
  requires(sizeof...(Types_) == sizeof...(Cond_))
struct ConditionalTuple<Typelist<Types_...>, Boollist<Cond_...>> {
  using type = typename internal::RewriteTuple<
      typename internal::ConditionalTupleImpl<Typelist<Types_...>, Boollist<Cond_...>>::type>::type;
};

/** @} */  // end of utils
}  // namespace gneiss
#endif  // GNEISS_CONDITIONALTUPLE_H
