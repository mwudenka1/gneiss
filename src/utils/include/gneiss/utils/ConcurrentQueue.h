#ifndef GNEISS_CONCURRENTQUEUE_H
#define GNEISS_CONCURRENTQUEUE_H
#pragma once
/** @file */

#include "gneiss/utils/assert.h"
#include "gneiss/utils/inplace_function.h"

#include <array>
#include <atomic>
#include <concepts>
#include <condition_variable>
#include <cstdint>
#include <mutex>
#include <optional>
#include <type_traits>
#include <variant>

namespace gneiss {

namespace internal {
struct StopSignal {};
}  // namespace internal

/**
 * @addtogroup utils
 *
 * @{
 */

/**
 * @brief thread safe bounded queue without heap
 *
 *
 * Heavily inspired by
 * https://github.com/gnzlbg/static_vector/blob/28606f87553425ab0ca14ceaf927eaf8ce414bce/include/experimental/FixedCapacityVector
 * and https://www.youtube.com/watch?v=I8QJLGI0GOE
 *
 *
 * @tparam ItemsType_
 * @tparam capacity_
 */
template <typename ItemsType_, std::size_t capacity_, bool override_ = false>
class ConcurrentQueue {
  using StoredType = std::variant<ItemsType_, internal::StopSignal>;

 public:
  ConcurrentQueue() noexcept = default;
  ConcurrentQueue(ConcurrentQueue const &) noexcept = delete;
  ConcurrentQueue &operator=(ConcurrentQueue const &) noexcept = delete;
  ConcurrentQueue(ConcurrentQueue &&) noexcept = delete;
  ConcurrentQueue &operator=(ConcurrentQueue &&) noexcept = delete;

  // NOLINTNEXTLINE(cppcoreguidelines-noexcept-destructor,performance-noexcept-destructor)
  ~ConcurrentQueue() noexcept(std::is_nothrow_destructible_v<ItemsType_>) {
    for (std::size_t idx = m_begin; idx < std::min(capacity_, m_size); ++idx) {
      // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
      std::destroy_at(data() + idx);
    }
    if (m_size > capacity_ - m_begin) {
      for (std::size_t idx = 0; idx < m_size - (capacity_ - m_begin); ++idx) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        std::destroy_at(data() + idx);
      }
    }
  }

  [[nodiscard]] constexpr auto data() const noexcept -> StoredType const * {
    if constexpr (is_sufficiently_trivial) {
      return static_cast<const StoredType *>(m_data.data());
    } else {
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
      return reinterpret_cast<StoredType const *>(std::addressof(m_data));
    }
  }

  [[nodiscard]] constexpr auto data() noexcept -> StoredType * {
    if constexpr (is_sufficiently_trivial) {
      return static_cast<StoredType *>(m_data.data());
    } else {
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
      return reinterpret_cast<StoredType *>(std::addressof(m_data));
    }
  }

  [[nodiscard]] bool is_empty() {
    std::scoped_lock<std::mutex> lock(m_mutex);
    return unsafe_is_empty();
  }

  void stop() { push_impl(internal::StopSignal{}); }

  template <typename... Args_>
    requires std::constructible_from<ItemsType_, Args_...>
  void push(Args_ &&...args) noexcept(noexcept(std::construct_at<StoredType, Args_...>(data(),
                                                                                       std::forward<Args_>(args)...))) {
    push_impl(std::forward<Args_>(args)...);
  }

  void shutdown() noexcept {
    m_shutdown = true;
    m_popSignal.notify_all();
    m_pushSignal.notify_all();
  }

  [[maybe_unused]] std::optional<ItemsType_> pop() noexcept(std::is_nothrow_copy_constructible_v<ItemsType_>) {
    std::unique_lock<std::mutex> lock(m_mutex);
    m_pushSignal.wait(lock, [this]() { return !unsafe_is_empty() || m_shutdown; });

    if (!m_shutdown) {
      // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
      const auto address = data() + m_begin;
      auto tmpItem = std::move(*address);
      std::destroy_at(address);
      ++m_begin;
      if (m_begin >= capacity_) {
        m_begin = 0;
      }
      m_size--;

      m_popSignal.notify_one();
      if (std::holds_alternative<internal::StopSignal>(tmpItem)) {
        shutdown();
        return {};
      }
      return std::optional<ItemsType_>(std::move(std::get<ItemsType_>(tmpItem)));
    }
    return {};
  }

 private:
  [[nodiscard]] bool unsafe_is_full() const { return m_size == capacity_; }
  [[nodiscard]] bool unsafe_is_empty() const { return m_size == 0; }

  template <typename... Args_>
  void push_impl(Args_ &&...args) noexcept(
      noexcept(std::construct_at<StoredType, Args_...>(data(), std::forward<Args_>(args)...))) {
    if constexpr (override_) {
      const std::unique_lock<std::mutex> lock(m_mutex);

      // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
      auto address = data() + ((m_begin + m_size) % capacity_);
      std::construct_at<StoredType, Args_...>(address, std::forward<Args_>(args)...);
      if (unsafe_is_full()) {
        ++m_begin;
        if (m_begin >= capacity_) {
          m_begin = 0;
        }
      } else {
        m_size++;
      }
      m_pushSignal.notify_one();
    } else {
      std::unique_lock<std::mutex> lock(m_mutex);
      m_popSignal.wait(lock, [this]() { return !unsafe_is_full() || m_shutdown; });

      if (!m_shutdown) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        auto address = data() + ((m_begin + m_size) % capacity_);
        std::construct_at<StoredType, Args_...>(address, std::forward<Args_>(args)...);

        m_size++;

        m_pushSignal.notify_one();
      }
    }
  }

  std::size_t m_begin = 0;
  std::size_t m_size = 0;
  static constexpr auto is_sufficiently_trivial =
      std::is_trivially_default_constructible_v<ItemsType_> && std::is_trivially_destructible_v<ItemsType_>;
  using storage_type = std::conditional_t<is_sufficiently_trivial, std::array<StoredType, capacity_>,
                                          std::aligned_storage_t<sizeof(StoredType) * capacity_>>;
  [[no_unique_address]] storage_type m_data{};
  std::atomic_bool m_shutdown = false;
  std::condition_variable m_popSignal;
  std::condition_variable m_pushSignal;
  std::mutex m_mutex;
};

/** @} */  // end of utils

}  // namespace gneiss
#endif  // GNEISS_CONCURRENTQUEUE_H
