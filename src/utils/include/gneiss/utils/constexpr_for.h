#ifndef GNEISS_CONSTEXPR_FOR_H
#define GNEISS_CONSTEXPR_FOR_H
#pragma once
/** @file */

#include <type_traits>

namespace gneiss {

/**
 * @addtogroup utils
 *
 * @{
 */

/**
 * @brief constexpr for approximation
 *
 * see https://artificial-mind.net/blog/2020/10/31/constexpr-for
 *
 * @tparam Start
 * @tparam End
 * @tparam Inc
 * @tparam Body_ Type of lambda (will be deuced)
 * @param body lambda
 * @return
 */
template <auto Start, auto End, auto Inc, typename Body_>
constexpr void constexpr_for(Body_&& body) {
  if constexpr (Start < End) {
    body(std::integral_constant<decltype(Start), Start>());
    constexpr_for<Start + Inc, End, Inc>(body);
  }
}

/** @} */  // end of utils

}  // namespace gneiss

#endif  // GNEISS_CONSTEXPR_FOR_H
