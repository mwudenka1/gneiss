#ifndef GNEISS_NEXT_POW2_H
#define GNEISS_NEXT_POW2_H
#pragma once
/** @file */

#include <cstdint>

namespace gneiss {

/**
 * @addtogroup utils
 *
 * @{
 */

namespace internal {
constexpr uint32_t next_pow2m1(uint32_t value) {
  // #if __GNUC__
  //   return x == 1u ? 1u : 1u<<(32-__builtin_clzl(x-1));
  // #else
  value |= value >> 1u;
  value |= value >> 2u;
  value |= value >> 4u;
  value |= value >> 8u;   // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
  value |= value >> 16u;  // NOLINT(cppcoreguidelines-avoid-magic-numbers,readability-magic-numbers)
  return value;
  // #endif
}
}  // namespace internal

/**
 * @brief Rounds a number to the next greater power of 2
 *
 * see https://jameshfisher.com/2018/03/30/round-up-power-2/
 *
 * @param value
 * @return Next greater power of 2
 */
constexpr uint32_t next_pow2(uint32_t value) { return internal::next_pow2m1(value - 1) + 1; }

/** @} */  // end of utils

}  // namespace gneiss

#endif  // GNEISS_NEXT_POW2_H
