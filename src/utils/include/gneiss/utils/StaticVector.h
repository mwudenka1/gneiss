#ifndef GNEISS_STATICVECTOR_H
#define GNEISS_STATICVECTOR_H
#pragma once
/** @file */

#include "gneiss/utils/assert.h"

#include <array>
#include <iterator>
#include <type_traits>

namespace gneiss {

namespace internal {

template <std::input_iterator InputIterator_, typename OutputIterator_, typename Item_>
  requires std::output_iterator<OutputIterator_, Item_>
constexpr OutputIterator_ move(InputIterator_ begin, InputIterator_ end, OutputIterator_ target) {
  for (; begin != end; ++begin, (void)++target) {
    *target = ::std::move(*begin);
  }
  return target;
}

}  // namespace internal

/**
 * @addtogroup utils
 *
 * @{
 */

template <typename ItemsType_, std::size_t capacity_>
class StaticVector {
 public:
  using value_type = ItemsType_;
  constexpr static std::size_t capacity = capacity_;

  // NOLINTNEXTLINE(cppcoreguidelines-noexcept-destructor, performance-noexcept-destructor)
  ~StaticVector() noexcept(std::is_nothrow_destructible_v<ItemsType_>) {
    for (std::size_t idx = 0; idx < m_size; ++idx) {
      // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
      std::destroy_at(data() + idx);
    }
  }

  [[nodiscard]] constexpr ItemsType_ *data() noexcept {
    if constexpr (is_sufficiently_trivial) {
      return static_cast<ItemsType_ *>(m_data.data());
    } else {
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
      return reinterpret_cast<ItemsType_ *>(std::addressof(m_data));
    }
  }

  [[nodiscard]] constexpr ItemsType_ const *data() const noexcept {
    if constexpr (is_sufficiently_trivial) {
      return static_cast<const ItemsType_ *>(m_data.data());
    } else {
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
      return reinterpret_cast<ItemsType_ const *>(std::addressof(m_data));
    }
  }

  [[nodiscard]] constexpr ItemsType_ &at(const std::size_t idx) & noexcept {
    GNEISS_ASSERT_MSG(idx < m_size, "Index is out of range");
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    return data()[idx];
  }

  [[nodiscard]] constexpr ItemsType_ &&at(const std::size_t idx) && noexcept {
    GNEISS_ASSERT_MSG(idx < m_size, "Index is out of range");
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    return std::move(data()[idx]);
  }

  [[nodiscard]] constexpr const ItemsType_ &at(const std::size_t idx) const & noexcept {
    GNEISS_ASSERT_MSG(idx < m_size, "Index is out of range");
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    return data()[idx];
  }

  [[nodiscard]] constexpr const ItemsType_ &&at(const std::size_t idx) const && noexcept {
    GNEISS_ASSERT_MSG(idx < m_size, "Index is out of range");
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    return std::move(data()[idx]);
  }

  [[nodiscard]] constexpr bool is_empty() const noexcept { return m_size == 0; }

  [[nodiscard]] constexpr bool is_full() const noexcept { return m_size >= capacity_; }

  [[nodiscard]] constexpr std::size_t size() const noexcept { return m_size; }

  [[nodiscard]] constexpr ItemsType_ &back() & noexcept {
    GNEISS_ASSERT_MSG(!is_empty(), "Index is out of range");
    return at(m_size - 1);
  }

  [[nodiscard]] constexpr ItemsType_ &&back() && noexcept {
    GNEISS_ASSERT_MSG(!is_empty(), "Index is out of range");
    return at(m_size - 1);
  }

  [[nodiscard]] constexpr const ItemsType_ &back() const & noexcept {
    GNEISS_ASSERT_MSG(!is_empty(), "Index is out of range");
    return at(m_size - 1);
  }

  [[nodiscard]] constexpr const ItemsType_ &&back() const && noexcept {
    GNEISS_ASSERT_MSG(!is_empty(), "Index is out of range");
    return at(m_size - 1);
  }

  void push_back(const ItemsType_ &value) noexcept {
    GNEISS_ASSERT_MSG(m_size < capacity_, "No more capacity");
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    auto address = data() + m_size;
    std::construct_at(address, value);
    ++m_size;
  }

  constexpr void pop_back() noexcept {
    std::destroy_at(std::addressof(back()));
    --m_size;
  }

  using iterator = ItemsType_ *;
  using const_iterator = const ItemsType_ *;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  [[nodiscard]] constexpr iterator begin() noexcept { return data(); }
  [[nodiscard]] constexpr const_iterator begin() const noexcept { return data(); }
  [[nodiscard]] constexpr const_iterator cbegin() const noexcept { return data(); }

  [[nodiscard]] constexpr iterator end() noexcept {  // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    return data() + m_size;
  }
  [[nodiscard]] constexpr const_iterator end() const noexcept {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    return data() + m_size;
  }
  [[nodiscard]] constexpr const_iterator cend() const noexcept {
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    return data() + m_size;
  }

  [[nodiscard]] constexpr reverse_iterator rbegin() noexcept { return reverse_iterator(end()); }
  [[nodiscard]] constexpr const_reverse_iterator rbegin() const noexcept { return const_reverse_iterator(end()); }
  [[nodiscard]] constexpr const_reverse_iterator crbegin() const noexcept { return const_reverse_iterator(end()); }

  [[nodiscard]] constexpr reverse_iterator rend() noexcept { return reverse_iterator(begin()); }
  [[nodiscard]] constexpr const_reverse_iterator rend() const noexcept { return const_reverse_iterator(begin()); }
  [[nodiscard]] constexpr const_reverse_iterator crend() const noexcept { return const_reverse_iterator(begin()); }

  [[maybe_unused]] constexpr iterator erase(const_iterator first) { return erase(first, std::next(first)); }

  [[maybe_unused]] constexpr iterator erase(const_iterator first, const_iterator last) {
    iterator target = begin() + (first - begin());
    if (first != last) {
      unsafe_destroy(internal::move<const_iterator, iterator, ItemsType_>(target + (last - first), end(), target),
                     end());
      m_size -= static_cast<std::size_t>(last - first);
    }

    return target;
  }

 private:
  template <std::input_iterator InputIt>
  void unsafe_destroy(InputIt first, InputIt last) noexcept(std::is_nothrow_destructible_v<ItemsType_>) {
    GNEISS_ASSERT_MSG(first >= data() && first <= end(), "first is out-of-bounds");
    GNEISS_ASSERT_MSG(last >= first && last <= end(), "last is out-of-bounds");
    for (; first != last; ++first) {
      std::destroy_at(first);
    }
  }

  static constexpr auto is_sufficiently_trivial =
      std::is_trivially_default_constructible_v<ItemsType_> && std::is_trivially_destructible_v<ItemsType_>;
  using storage_type = std::conditional_t<is_sufficiently_trivial, std::array<ItemsType_, capacity_>,
                                          std::aligned_storage_t<sizeof(ItemsType_) * capacity_>>;
  [[no_unique_address]] storage_type m_data{};
  std::size_t m_size = 0;
};

/** @} */  // end of utils

}  // namespace gneiss

#endif  // GNEISS_STATICVECTOR_H