#ifndef GNEISS_ABS_H
#define GNEISS_ABS_H
#pragma once
/** @file */

namespace gneiss {

/**
 * @addtogroup utils
 *
 * @{
 */

#if __cpp_lib_constexpr_cmath >= 202202L
#include <cmath>
using std::abs;
#else

template <typename Type_>
constexpr Type_ abs(const Type_ value) {
  if (value < 0) {
    return -value;
  }

  return value;
}

#endif

/** @} */  // end of utils

}  // namespace gneiss

#endif  // GNEISS_ABS_H
