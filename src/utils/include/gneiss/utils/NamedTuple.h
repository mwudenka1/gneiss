#ifndef GNEISS_NAMEDTUPLE_H
#define GNEISS_NAMEDTUPLE_H
#pragma once
/** @file */

#include "gneiss/utils/Named.h"

#include <ostream>
#include <tuple>

namespace gneiss {

/**
 * @addtogroup utils
 *
 * @{
 */

/**
 * @brief Wrapper around std::tuple to index elements by name and not numeric index
 *
 * Use the factory function @ref make_named_tuple
 *
 * @tparam Named_
 */
template <typename... Named_>
class NamedTuple {
 public:
  explicit constexpr NamedTuple(Named_... named) : m_named(std::make_tuple(named...)) {}

  template <std::size_t idx>
  constexpr auto& get() {
    return std::get<idx>(m_named).value;
  }

  template <std::size_t idx>
  [[nodiscard]] constexpr const auto& get() const {
    return std::get<idx>(m_named).value;
  }

 private:
  template <typename Parent_, typename Name_>
  struct Finder {
    template <std::size_t idx>
    static constexpr decltype(auto) find(Parent_* parent) {
      if constexpr (std::is_same_v<Name_, typename std::tuple_element_t<idx, decltype(parent->m_named)>::NameType>) {
        return (std::get<idx>(parent->m_named).value);
      } else {
        return (find<idx + 1>(parent));
      }
    }
  };

 public:
  template <FixedString name_>
  [[nodiscard]] constexpr decltype(auto) get() {
    return (Finder<std::remove_pointer_t<decltype(this)>, gneiss::constant<decltype(name_), name_>>::template find<0>(
        this));
  }

  template <FixedString name_>
  [[nodiscard]] constexpr decltype(auto) get() const {
    return (Finder<std::remove_pointer_t<decltype(this)>, gneiss::constant<decltype(name_), name_>>::template find<0>(
        this));
  }

  [[nodiscard]] const std::tuple<Named_...>& get_tuple() const { return m_named; }

  [[nodiscard]] std::tuple<Named_...>& get_tuple() { return m_named; }

  friend std::ostream& operator<<(std::ostream& outStream, const NamedTuple& self) {
    std::apply(
        [&outStream](const Named_&... named) {
          outStream << '(';
          std::size_t elementIdx{0};
          ((outStream << named.name << ": " << named.value << (++elementIdx != sizeof...(Named_) ? ", " : "")), ...);
          outStream << ')';
        },
        self.m_named);

    return outStream;
  }

 private:
  std::tuple<Named_...> m_named;
};

/**
 * @brief Factory function for @ref NamedTuple
 *
 * * Usage:
 * \code{.cpp}
 * auto myNamedTuple = gneiss::make_named_tuple(gneiss::make_named<"first">(5.f), gneiss::make_named<"second">(6));
 * auto firstVal = myNamedTuple.get<"first">();
 * \endcode *
 *
 * @tparam Named_ Multiple @ref Named types
 * @param named  Variadic parameter pack of @ref Named
 * @return the new NamedTuple instance
 */
template <typename... Named_>
constexpr auto make_named_tuple(Named_... named) {
  return NamedTuple<Named_...>(named...);
}

/** @} */  // end of utils

}  // namespace gneiss

#endif  // GNEISS_NAMEDTUPLE_H
