#ifndef GNEISS_UNUSED_H
#define GNEISS_UNUSED_H
#pragma once
/** @file */

/**
 * @addtogroup utils
 *
 * @{
 */

// NOLINTNEXTLINE(cppcoreguidelines-macro-usage)
#define GNEISS_UNUSED(x) (void)(x)

/** @} */  // end of utils

#endif  // GNEISS_UNUSED_H
