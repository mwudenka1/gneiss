#ifndef GNEISS_NAMED_H
#define GNEISS_NAMED_H
#pragma once
/** @file */

#include "gneiss/utils/FixedString.h"
#include "gneiss/utils/Specializes.h"
#include "gneiss/utils/constant.h"

namespace gneiss {

/**
 * @addtogroup utils
 *
 * @{
 */

/**
 * @brief A wrapper to bake a name into a type
 *
 * Consider using @ref make_named as factory
 *
 * Baking a name into a type means that it will be eliminated at compile time (no runtime overhead).
 * So you can write expressive yet efficient code.
 *
 * @tparam Name_ the name type
 * @tparam Wrapped_ the type of the wrapped element
 */
template <typename Name_, typename Wrapped_>
  requires SpecializesWithValue<typename Name_::value_type, FixedString>
struct Named {
  using NameType = Name_;
  using WrappedType = Wrapped_;
  constexpr static FixedString name = Name_::value;
  Wrapped_ value;
};

template <FixedString name_>
struct NamedFactory {
  template <typename Wrapped_>
  constexpr auto operator()(Wrapped_ value) const {
    return Named<constant<decltype(name_), name_>, Wrapped_>{value};
  }
};

/**
 * @brief Factory to name things with types
 *
 * @ref Named
 *
 * Usage:
 * \code{.cpp}
 * auto namedPerson = make_named<"Bob">(Person());
 * \endcode
 *
 * @tparam name_
 */
template <FixedString name_>
constexpr static NamedFactory<name_> make_named = {};

/** @} */  // end of utils

}  // namespace gneiss

#endif  // GNEISS_NAMED_H
