#ifndef GNEISS_IPOW_H
#define GNEISS_IPOW_H
#pragma once
/** @file */

#include <cstdint>

namespace gneiss {

/**
 * @addtogroup utils
 *
 * @{
 */

/**
 * @brief constexpr integer power
 *
 * Calculates \f$ \mathrm{base}^{\mathrm{exp}} \f$
 *
 * see
 * https://stackoverflow.com/questions/17719674/c11-fast-constexpr-integer-powers
 *
 * @param base
 * @param exp
 * @param result default: 1. Do not set! Used for recursion
 * @return base ^ exp
 */
// NOLINTNEXTLINE(misc-no-recursion, bugprone-easily-swappable-parameters)
[[nodiscard]] constexpr int64_t ipow(int64_t base, uint32_t exp, int64_t result = 1) {
  return exp < 1 ? result : ipow(base * base, exp / 2, (exp % 2) == 1 ? result * base : result);
}

/** @} */  // end of utils

}  // namespace gneiss

#endif  // GNEISS_IPOW_H
