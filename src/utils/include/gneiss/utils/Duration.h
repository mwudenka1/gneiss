#ifndef GNEISS_DURATION_H
#define GNEISS_DURATION_H
#pragma once
/** @file */

#include <chrono>
#include <cstdint>

namespace gneiss {

/**
 * @addtogroup utils
 *
 * @{
 */

using Duration = std::chrono::duration<int64_t, std::micro>;

/** @} */  // end of utils

}  // namespace gneiss

#endif  // GNEISS_DURATION_H
