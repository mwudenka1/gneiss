#ifndef GNEISS_CONCURRENTARENA_H
#define GNEISS_CONCURRENTARENA_H
#pragma once
/** @file */

#include "gneiss/utils/assert.h"
#include "gneiss/utils/inplace_function.h"

#include <array>
#include <atomic>
#include <bitset>
#include <concepts>
#include <condition_variable>
#include <cstdint>
#include <mutex>
#include <type_traits>

namespace gneiss {

/**
 * @addtogroup utils
 *
 * @{
 */

constexpr static std::size_t DESTRUCTOR_SIZE = 8;

/**
 * @brief custom deleter for std:unique_ptr
 *
 * @tparam ItemsType_
 */
template <typename ItemsType_>
struct ArenaPtrDeleter {
  using DestructorType = stdext::inplace_function<void(std::add_const_t<ItemsType_> *), DESTRUCTOR_SIZE>;
  constexpr ArenaPtrDeleter() noexcept = default;
  explicit constexpr ArenaPtrDeleter(DestructorType destructor) noexcept : m_destructor(destructor){};
  ArenaPtrDeleter(const ArenaPtrDeleter &) noexcept = default;
  ArenaPtrDeleter(ArenaPtrDeleter &&) noexcept = default;
  ArenaPtrDeleter &operator=(ArenaPtrDeleter const &) noexcept = default;
  ArenaPtrDeleter &operator=(ArenaPtrDeleter &&) noexcept = default;

  /**
   * Allow conversion to const
   *
   * @tparam OtherItemsType_
   * @param other
   */
  template <typename OtherItemsType_>
    requires std::same_as<OtherItemsType_, std::add_const_t<ItemsType_>>
  // NOLINTNEXTLINE(google-explicit-constructor,hicpp-explicit-conversions)
  ArenaPtrDeleter(const ArenaPtrDeleter<OtherItemsType_> &other) : m_destructor(other.m_destructor) {}

  void operator()(std::add_const_t<ItemsType_> *ptr) const { m_destructor(ptr); }

 private:
  DestructorType m_destructor;
};

static_assert(std::is_copy_constructible_v<ArenaPtrDeleter<float>>);

/**
 * @brief thread safe arena memory manager without heap allocation
 *
 * Memory can be requested through the emplace method.
 * Once the returned UniqueArenaPtr goes out of scope the memory is freed.
 * If threads are waiting for element insertion, they will be unblocked by the shutdown method.
 *
 * Heavily inspired by
 * https://github.com/gnzlbg/static_vector/blob/28606f87553425ab0ca14ceaf927eaf8ce414bce/include/experimental/FixedCapacityVector
 * and https://www.youtube.com/watch?v=I8QJLGI0GOE
 *
 *
 * @tparam ItemsType_
 * @tparam capacity_
 */
template <typename ItemsType_, std::size_t capacity_>
class ConcurrentArena {
 public:
  ConcurrentArena() noexcept = default;
  ConcurrentArena(ConcurrentArena const &) noexcept = delete;
  ConcurrentArena &operator=(ConcurrentArena const &) noexcept = delete;
  ConcurrentArena(ConcurrentArena &&) noexcept = delete;
  ConcurrentArena &operator=(ConcurrentArena &&) noexcept = delete;

  ~ConcurrentArena() noexcept {
    GNEISS_ASSERT_MSG(m_occupancyMap.none(), "Wrong destruction order. Destroy the pointers first.");
  }

  [[nodiscard]] constexpr auto data() const noexcept -> ItemsType_ const * {
    if constexpr (is_sufficiently_trivial) {
      return static_cast<const ItemsType_ *>(m_data.data());
    } else {
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
      return reinterpret_cast<ItemsType_ const *>(std::addressof(m_data));
    }
  }

  [[nodiscard]] constexpr auto data() noexcept -> ItemsType_ * {
    if constexpr (is_sufficiently_trivial) {
      return static_cast<ItemsType_ *>(m_data.data());
    } else {
      // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
      return reinterpret_cast<ItemsType_ *>(std::addressof(m_data));
    }
  }

  /**
   * @brief Create new object at first available space or wait.
   *
   * Is blocking!
   *
   * Returns nullptr after shutdown.
   *
   * @tparam Args_
   * @param args
   * @return
   */
  template <typename... Args_>
    requires std::constructible_from<ItemsType_, Args_...>
  [[nodiscard]] auto emplace(Args_ &&...args) noexcept(
      noexcept(std::construct_at<ItemsType_, Args_...>(data(), std::forward<Args_>(args)...)))
      -> std::unique_ptr<ItemsType_, ArenaPtrDeleter<ItemsType_>> {
    std::unique_lock<std::mutex> lock(m_mutex);
    m_freeSpaceSignal.wait(lock, [this]() { return !m_occupancyMap.all() || m_shutdown; });

    if (!m_shutdown) {
      for (std::size_t idx = 0; idx < capacity_; ++idx) {
        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        auto address = data() + idx;
        if (!m_occupancyMap[static_cast<std::size_t>(idx)]) {
          std::construct_at<ItemsType_, Args_...>(address, std::forward<Args_>(args)...);
          m_occupancyMap[static_cast<std::size_t>(idx)] = 1;

          return std::unique_ptr<ItemsType_, ArenaPtrDeleter<ItemsType_>>(
              address, ArenaPtrDeleter<ItemsType_>{
                           [this](std::add_const_t<ItemsType_> *itemAddress) { this->destroy_at(itemAddress); }});
        }
      }
    }

    return std::unique_ptr<ItemsType_, ArenaPtrDeleter<ItemsType_>>();
  }

  /**
   * @brief Gracefully shutdown all waiting threads
   */
  void shutdown() noexcept {
    m_shutdown = true;
    m_freeSpaceSignal.notify_all();
  }

 private:
  /**
   * @brief Destroy element and free space
   *
   * To be called by destructor or destructor of std::unique_ptr
   *
   * Is blocking!
   *
   * @param address
   */
  void destroy_at(std::add_const_t<ItemsType_> *address) noexcept(
      noexcept(std::is_nothrow_destructible_v<ItemsType_>)) {
    const std::unique_lock<std::mutex> lock(m_mutex);
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
    const auto idx = address - data();
    GNEISS_ASSERT_MSG(idx < static_cast<decltype(idx)>(capacity_), "Index out of bounds");
    GNEISS_ASSERT_MSG(m_occupancyMap[static_cast<std::size_t>(idx)], "Cannot destroy uninitialized element.");
    std::destroy_at(address);
    m_occupancyMap[static_cast<std::size_t>(idx)] = 0;
    m_freeSpaceSignal.notify_one();
  }

  std::bitset<capacity_> m_occupancyMap;
  static constexpr auto is_sufficiently_trivial =
      std::is_trivially_default_constructible_v<ItemsType_> && std::is_trivially_destructible_v<ItemsType_>;
  using storage_type = std::conditional_t<is_sufficiently_trivial, std::array<ItemsType_, capacity_>,
                                          std::aligned_storage_t<sizeof(ItemsType_) * capacity_>>;
  [[no_unique_address]] storage_type m_data{};
  std::atomic_bool m_shutdown = false;
  std::condition_variable m_freeSpaceSignal;
  std::mutex m_mutex;
};

/** @} */  // end of utils

}  // namespace gneiss

#endif  // GNEISS_CONCURRENTARENA_H
