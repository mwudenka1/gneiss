#ifndef GNEISS_DIVISION_POWER_2_H
#define GNEISS_DIVISION_POWER_2_H
#pragma once
/** @file */

#include "gneiss/utils/assert.h"

#include <concepts>
#include <cstdint>

namespace gneiss {

/**
 * @addtogroup utils
 *
 * @{
 */

/**
 * @brief Fast calculation of integer division by power of 2 with correct rounding.
 *
 * Calculates \f$ \lfloor \frac{\mathrm{inValue}}{ 2^{\mathrm{power}} } \rceil \f$
 *
 * @tparam Type_ some integer type
 * @param inValue dividend
 * @param power exponent
 * @return round(inValue / (2 ^ power))
 */
template <typename Type_>
  requires std::integral<Type_>
constexpr inline Type_ division_power_2(const Type_ inValue, const uint32_t power) {
  GNEISS_ASSERT_MSG(power != 0, "Division by 0!");
  // NOLINTNEXTLINE(hicpp-signed-bitwise)
  return (inValue + (1 << (power - 1u))) >> power;
}

/** @} */  // end of utils

}  // namespace gneiss

#endif  // GNEISS_DIVISION_POWER_2_H
