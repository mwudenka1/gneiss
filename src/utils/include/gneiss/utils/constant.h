#ifndef GNEISS_CONSTANT_H
#define GNEISS_CONSTANT_H
#pragma once
/** @file */

#include <type_traits>

namespace gneiss {

/**
 * @addtogroup utils
 *
 * @{
 */

/**
 * @brief Wraps a value in a type.
 *
 * Works similar to std::integral_constant but not just for integrals. This is useful for templates.
 *
 * Usage:
 * \code{.cpp}
 * const auto val = 6.f;
 * auto constVal = constant<decltype(val), val>();
 * \endcode
 *
 * @tparam Type_
 * @tparam value_
 */
template <typename Type_, auto value_>
struct constant {
  static constexpr Type_ value = value_;
  using value_type = typename std::remove_cv_t<Type_>;
  using type = constant;
  // NOLINTNEXTLINE(google-explicit-constructor,hicpp-explicit-conversions)
  constexpr operator value_type() const noexcept { return value; }
  constexpr value_type operator()() const noexcept { return value; }
};

/** @} */  // end of utils

}  // namespace gneiss

#endif  // GNEISS_CONSTANT_H
