#ifndef GNEISS_RANGES_H
#define GNEISS_RANGES_H
#pragma once
/** @file
 * Helper to include either ranges-v3 or std::ranges depending on compiler support
 */

#include <version>

/**
 * @addtogroup utils
 *
 * @{
 */

// resolve gneiss::ranges to either ranges-v3 or std::ranges
// clang <= 15 doesn't support C++20 std::ranges
// gcc >= 10 does
#if __cpp_lib_ranges >= 201911L
#include <algorithm>                    // IWYU pragma: export
#include <range/v3/view/interface.hpp>  // IWYU pragma: export
#include <ranges>                       // IWYU pragma: export

#if __cpp_lib_ranges_enumerate < 202302L
#include <range/v3/view/enumerate.hpp>  // IWYU pragma: export
namespace gneiss::ranges::views {
RANGES_INLINE_VARIABLE(::ranges::views::view_closure<::ranges::views::enumerate_fn>, enumerate)
}  // namespace gneiss::ranges::views
#endif

#if __cpp_lib_ranges_zip < 202110L
#include <range/v3/view/zip.hpp>  // IWYU pragma: export
namespace gneiss::ranges::views {
RANGES_INLINE_VARIABLE(::ranges::views::zip_fn, zip)
}  // namespace gneiss::ranges::views
#endif

#if __cpp_lib_ranges_to_container < 202202L
#include <range/v3/range/conversion.hpp>  // IWYU pragma: export
namespace gneiss::ranges {
using namespace ::ranges::_to_;  // NOLINT(google-build-using-namespace)
}  // namespace gneiss::ranges
#endif

#if __cpp_lib_containers_ranges < 202202L && defined(__GNUC__) && !defined(__clang__)
#include <range/v3/range/conversion.hpp>  // IWYU pragma: export
namespace gneiss::ranges {
using namespace ::ranges::_to_;  // NOLINT(google-build-using-namespace)
}  // namespace gneiss::ranges
#endif

#if __cpp_lib_ranges_fold < 202207L
#include <range/v3/algorithm/fold_left.hpp>   // IWYU pragma: export
#include <range/v3/algorithm/fold_right.hpp>  // IWYU pragma: export
namespace gneiss::ranges {
RANGES_INLINE_VARIABLE(::ranges::fold_right_fn, fold_right)
RANGES_INLINE_VARIABLE(::ranges::fold_left_fn, fold_left)
}  // namespace gneiss::ranges
#endif

namespace gneiss {
namespace ranges {
using namespace std::ranges;  // NOLINT(google-build-using-namespace)
namespace views {

using namespace std::ranges::views;  // NOLINT(google-build-using-namespace)
}  // namespace views
}  // namespace ranges
constexpr static bool std_ranges_used = true;
}  // namespace gneiss

template <>

inline constexpr bool ranges::enable_view<std::ranges::iota_view<std::size_t>> = true;
#else
#include <range/v3/all.hpp>  // IWYU pragma: export
namespace gneiss {
namespace ranges = ::ranges;
constexpr static bool std_ranges_used = false;
}  // namespace gneiss
#endif

/** @} */  // end of utils

#endif  // GNEISS_RANGES_H
