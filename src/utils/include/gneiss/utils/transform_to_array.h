#ifndef GNEISS_TRANSFORM_TO_ARRAY_H
#define GNEISS_TRANSFORM_TO_ARRAY_H
#pragma once
/** @file */

#include <cstdint>
#include <tuple>
#include <type_traits>

namespace gneiss {

/**
 * @addtogroup utils
 *
 * @{
 */

/**
 * @brief Applies a predicate to a source array and returns an array of the results.
 *
 * Sadly the following more elegant code is not (yet - C++20) constexpr when the lambdas capture non-literal types.
 *
 * \code{.cpp}
 * const auto resultArray = ([]{
 *      std::array<...> result;
 *      std::ranges::copy(source | std::ranges::views::transform([...](const& auto element){return some_op(element)}),
 *                        result.begin());
 *      return result;
 * })();
 * \endcode
 *
 * So this function is a constexpr enabled workaround. Better ideas are welcome.
 *
 * @tparam Source_ The type of the data source array.
 * @tparam Callable_ The tpe of the predicate. Needs to be callable.
 * @param source A data source
 * @param callable A callable predicate
 * @return
 */
template <typename Source_, typename Callable_>
constexpr inline auto transform_to_array(const Source_& source, Callable_&& callable) {
  constexpr const std::size_t arraySize = std::tuple_size_v<std::remove_reference_t<Source_>>;
  std::array<std::invoke_result_t<Callable_, typename std::remove_reference_t<Source_>::value_type>, arraySize>
      result{};
  for (std::size_t idx = 0; idx < arraySize; ++idx) {
    result.at(idx) = callable(source.at(idx));
  }
  return result;
}

/** @} */  // end of utils

}  // namespace gneiss
#endif  // GNEISS_TRANSFORM_TO_ARRAY_H
