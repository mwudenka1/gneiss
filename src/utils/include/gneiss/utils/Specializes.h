#ifndef GNEISS_SPECIALIZES_H
#define GNEISS_SPECIALIZES_H
#pragma once
/** @file */

#include <type_traits>

namespace gneiss {

/**
 * @addtogroup utils
 *
 * @{
 */

template <typename T, template <typename...> class Z>
struct is_type_specialization_of : std::false_type {};

template <typename... Args, template <typename...> class Z>
struct is_type_specialization_of<Z<Args...>, Z> : std::true_type {};

template <typename T, template <typename...> class Z>
inline constexpr bool is_type_specialization_of_v = is_type_specialization_of<T, Z>::value;

/**
 * @brief Checks that a type is a specialization of certain type template
 *
 * @tparam T
 * @tparam Z
 */
template <typename T, template <typename...> class Z>
concept SpecializesWithType = is_type_specialization_of_v<T, Z>;

template <typename T, template <auto...> class Z>
struct is_value_specialization_of : std::false_type {};

template <auto... Args, template <auto...> class Z>
struct is_value_specialization_of<Z<Args...>, Z> : std::true_type {};

template <typename T, template <auto...> class Z>
inline constexpr bool is_value_specialization_of_v = is_value_specialization_of<T, Z>::value;

/**
 * @brief Checks that a type is a specialization of certain value template
 *
 * @tparam T
 * @tparam Z
 */
template <typename T, template <auto...> class Z>
concept SpecializesWithValue = is_value_specialization_of_v<T, Z>;

/** @} */  // end of utils

}  // namespace gneiss
#endif  // GNEISS_SPECIALIZES_H
