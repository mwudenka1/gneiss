#ifndef GNEISS_OUTPUT_H
#define GNEISS_OUTPUT_H
#pragma once
/** @file */

#include <ostream>
#include <utility>

/**
 * @addtogroup utils
 *
 * @{
 */

template <typename TypeA_, typename TypeB_>
std::ostream& operator<<(std::ostream& outStream, const std::pair<TypeA_, TypeB_>& pair) {
  outStream << "(" << pair.first << ", " << pair.second << ")";

  return outStream;
}

namespace gneiss::internal {
template <typename... Ts, std::size_t... Is>
std::ostream& println_tuple_impl(std::ostream& outStream, std::tuple<Ts...> tuple, std::index_sequence<Is...>) {
  static_assert(sizeof...(Is) == sizeof...(Ts), "Indices must have same number of elements as tuple types!");
  static_assert(sizeof...(Ts) > 0, "Cannot insert empty tuple into stream.");
  auto last = sizeof...(Ts) - 1;  // assuming index sequence 0,...,N-1
  outStream << "[";
  return ((outStream << std::get<Is>(tuple) << (Is != last ? ", " : "]")), ...);
}
}  // namespace gneiss::internal

/**
 * @brief Print a tuple to a stream
 *
 * taken from https://geo-ant.github.io/blog/2020/stream-insertion-for-tuples/
 *
 * @tparam Ts
 * @param os
 * @param tuple
 * @return std::ostream&
 */
template <typename... Ts>
std::ostream& operator<<(std::ostream& outStream, const std::tuple<Ts...>& tuple) {
  return gneiss::internal::println_tuple_impl(outStream, tuple, std::index_sequence_for<Ts...>{});
}

/** @} */  // end of utils

#endif  // GNEISS_OUTPUT_H
