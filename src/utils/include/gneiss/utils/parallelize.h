#ifndef GNEISS_PARALLELIZE_H
#define GNEISS_PARALLELIZE_H
#pragma once
/** @file */

/**
 * @addtogroup utils
 *
 * @{
 */

namespace gneiss {

struct do_parallelize : std::true_type {};
struct do_not_parallelize : std::false_type {};

}  // namespace gneiss

/** @} */  // end of utils
#endif     // GNEISS_PARALLELIZE_H
