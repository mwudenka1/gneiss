import os
import sys
from itertools import chain
from typing import List, Dict
import warnings

from structurizr import Workspace
from structurizr.model import Container, Component, Relationship
from pathlib import Path
from jinja2 import Environment, FileSystemLoader
from subprocess import call
from argparse import ArgumentParser

from . import parse_cpp_member_function


def find_file_location(name, path):
    for root, dirs, files in os.walk(path):
        if name in files:
            return root


def match_type_2_include(type, source_path):
    if type in ["double",
                "float",
                "int",
                "char",
                "wchar_t",
                "bool",
                "void",
                "long",
                "long long"]:
        return []
    elif type in ["int8_t", "int16_t", "int32_t", "int64_t", "int_fast8_t", "int_fast16_t", "int_fast32_t",
                  "int_fast64_t", "int_least8_t", "int_least16_t", "int_least32_t", "int_least64_t", "intmax_t",
                  "intptr_t", "uint8_t", "uint16_t", "uint32_t", "uint64_t", "uint_fast8_t", "uint_fast16_t",
                  "uint_fast32_t", "uint_fast64_t", "uint_least8_t", "uint_least16_t", "uint_least32_t",
                  "uint_least64_t", "uintmax_t", "uintptr_t"]:
        return ["<cstdint>"]
    elif type == "std::array":
        return ["<array>"]
    elif type == "std::optional":
        return ["<optional>"]
    else:
        file_name = f"{type}.h"
        dir_path = find_file_location(
            file_name, source_path / "include" / "gneiss")
        if dir_path:
            return [f"\"gneiss/{os.path.basename(dir_path)}/{file_name}\""]
        else:
            warnings.warn(f"Type resolver could not resolve {type}.")
            return []


def clang_format(file_path):
    call(["clang-format", "--style=file", "-i", file_path])


class GlueCodeGenerator():
    software_system_name = "RobotSoftwareSystem"
    core_container_name = "gneiss core"
    templates_path = Path(__file__).parent / "templates"
    includes_path = Path("include") / Path("gneiss")
    system_assembler_file_name = "VioSystem.h"

    def __init__(
            self,
            structurizr_json_path: Path,
            output_path: Path,
            source_path: Path):
        self.__structurizr_json_path = structurizr_json_path
        self.__output_path = output_path / self.includes_path
        self.__source_path = source_path
        self.__workspace = self.__retrieve_workspace()
        self.__components = self.__retrieve_components()
        self.__internal_relationships = self.__retrieve_internal_relationships()
        self.__dependencies = self.__retrieve_dependencies()
        self.__member_functions = self.__retrieve_member_functions()
        self.__includes = self.__retrieve_includes()
        self.__resolve_order = self.__solve_dependency_tree()
        self.__transitive_dependencies = self.__retrieve_transitive_dependencies()
        self.__full_dependencies = self.__retrieve_full_dependencies()
        self.__setup_jinja_env()

    def generate_glue_code(self):
        self.render_interfaces()
        self.render_system_assembler()

    def render_interfaces(self):
        interface_template = self.__jinja_env.get_template(
            'ComponentInterface.h.tmpl')

        for com in self.__components:
            component_path = self.__output_path / com.name

            os.makedirs(component_path, exist_ok=True)

            file_name = f"{com.name}.h"
            file_path = component_path / file_name

            file_content = interface_template.render(
                component=com,
                dependencies=self.__dependencies[com.name],
                includes=self.__includes[com.name],
                member_functions=self.__member_functions[com.name],
                full_dependencies=self.__full_dependencies[com.name],
                all_full_dependencies=self.__full_dependencies
            )

            with open(file_path, "w+") as f:
                f.write(file_content)

            clang_format(file_path)

    def render_system_assembler(self):
        file_path = self.__output_path / self.system_assembler_file_name

        system_template = self.__jinja_env.get_template('VioSystem.h.tmpl')

        file_content = system_template.render(
            resolve_order=self.__resolve_order,
            dependencies=self.__dependencies,
            full_dependencies=self.__full_dependencies
        )

        with open(file_path, "w+") as f:
            f.write(file_content)

        clang_format(file_path)

    def print_inputs(self):
        print(str(self.__structurizr_json_path))

    def print_outputs(self):
        com_files = [str(self.__output_path / com.name /
                         f"{com.name}.h") for com in self.__components]
        system_assembler_file = str(
            self.__output_path /
            self.system_assembler_file_name)

        all_files = com_files + [system_assembler_file]

        print(";".join(all_files))

    def __retrieve_workspace(self) -> Workspace:
        return Workspace.load(self.__structurizr_json_path)

    def __retrieve_components(self) -> List[Component]:
        robot_software_system = next(
            ss for ss in self.__workspace.model.software_systems if ss.name == self.software_system_name)
        gneiss_core_container = next(
            con for con in robot_software_system.containers if con.name == self.core_container_name)

        return gneiss_core_container.components

    def __retrieve_internal_relationships(self) -> List[Relationship]:
        component_names = [com.name for com in self.__components]

        return [rel for rel in self.__workspace.model.get_relationships(
        ) if rel.source.name in component_names and rel.destination.name in component_names]

    def __retrieve_dependencies(self) -> Dict[str, List[str]]:
        dependencies = {com.name: set() for com in self.__components}
        for rel in self.__internal_relationships:
            dependencies[rel.source.name].add(rel.destination.name)

        for com in dependencies:
            dependencies[com] = sorted(dependencies[com])

        return dependencies

    def __retrieve_member_functions(self) -> Dict[str, List[Dict]]:
        member_functions = {com.name: list() for com in self.__components}
        for com in self.__components:
            for key, prop_value in com.properties.items():
                if key == "structurizr.dsl.identifier":
                    continue

                parsed_function_decl = parse_cpp_member_function(prop_value)
                member_functions[com.name].append(parsed_function_decl)

        return member_functions

    def __retrieve_includes(self) -> Dict[str, List[str]]:
        includes = {com.name: set() for com in self.__components}

        def match_type_2_include_recursive(
                type_desc: dict, source_path) -> set:
            result = set((match_type_2_include(type_desc['type_name'], source_path))).union(
                list(chain(*list(
                    *[(list(match_type_2_include_recursive(template_type_desc, source_path)) for template_type_desc in
                       type_desc['template_arguments']) if 'template_arguments' in type_desc else []]))))
            return result

        for component_name, member_functions in self.__member_functions.items():
            for function in member_functions:
                includes[component_name] = includes[component_name].union(
                    match_type_2_include_recursive(function["return_type"], self.__source_path))
                for param in function["parameter_list"] if "parameter_list" in function else [
                ]:
                    includes[component_name] = includes[component_name].union(
                        match_type_2_include_recursive(param["type"], self.__source_path))

        return includes

    def __solve_dependency_tree(self) -> List[str]:
        # keep storage at first position to avoid dangling pointers
        # use decorate - sort - undecorate idiom
        components_todo = sorted(
            [(0 if com.name == "Storage" else 1, com.name) for com in self.__components])
        components_todo = [com_tuple[1] for com_tuple in components_todo]

        components_resolved = []

        progress_made = True
        while components_todo and progress_made:
            progress_made = False
            for component_todo in components_todo:
                # TODO use any?
                all_dependencies_resolved = True
                for dep in self.__dependencies[component_todo]:
                    if dep not in components_resolved:
                        all_dependencies_resolved = False

                if all_dependencies_resolved:
                    progress_made = True
                    components_resolved.append(component_todo)
                    components_todo.remove(component_todo)
                    break

        if progress_made:
            return components_resolved

        warnings.warn("Could not build the dependency tree.")

        return []

    def __retrieve_transitive_dependencies(self) -> Dict[str, List[str]]:
        transitive_dependencies = {com.name: set()
                                   for com in self.__components}
        for component in self.__resolve_order:
            for dep in self.__dependencies[component]:
                transitive_dependencies[component] = set(
                    self.__dependencies[dep]).union(
                    transitive_dependencies[dep])

        for com in transitive_dependencies:
            transitive_dependencies[com] = sorted(transitive_dependencies[com])

        return transitive_dependencies

    def __retrieve_full_dependencies(self) -> Dict[str, List[str]]:
        full_dependencies = {com.name: set(self.__transitive_dependencies[com.name]).union(
            self.__dependencies[com.name]) for com in self.__components}

        for com in full_dependencies:
            full_dependencies[com] = sorted(full_dependencies[com])

        return full_dependencies

    def __setup_jinja_env(self):
        self.__jinja_env = Environment(
            loader=FileSystemLoader(
                self.templates_path))
        self.__jinja_env.trim_blocks = True
        self.__jinja_env.lstrip_blocks = True


def main():
    argument_parser = ArgumentParser(
        description="Generates glue code (interfaces + system assembler) from the structurizr workspace")
    argument_parser.add_argument(
        "--workspace_path",
        type=str,
        help="Path to the workspace json file",
        required=True)
    argument_parser.add_argument(
        "--output_path",
        type=str,
        help="Path to the root output folder",
        required=True)
    argument_parser.add_argument(
        "--source_path",
        type=str,
        help="Path to the source folder",
        required=True)
    argument_parser.add_argument('--print_inputs', action='store_true')
    argument_parser.set_defaults(print_inputs=False)
    argument_parser.add_argument('--print_outputs', action='store_true')
    argument_parser.set_defaults(print_outputs=False)

    args = argument_parser.parse_args()

    if not os.path.isfile(args.workspace_path):
        print(f"Error: {args.workspace_path} is not a file.")
        sys.exit(1)
    workspace_path = Path(args.workspace_path)

    if not os.path.isdir(args.output_path):
        print(f"Error: {args.output_path} is not a directory.")
        sys.exit(1)
    output_path = Path(args.output_path)

    if not os.path.isdir(args.source_path):
        print(f"Error: {args.source_path} is not a directory.")
        sys.exit(1)
    source_path = Path(args.source_path)

    glue_code_generator = GlueCodeGenerator(
        workspace_path, output_path, source_path)

    if args.print_inputs:
        glue_code_generator.print_inputs()
        return 0

    if args.print_outputs:
        glue_code_generator.print_outputs()
        return 0

    glue_code_generator.generate_glue_code()

    print("Generated glue code")

    return 0
