
def main():
    try:
        from glue_code_generation import main
        exit_status = main()
    except KeyboardInterrupt:
        exit_status = 130

    return exit_status


if __name__ == '__main__':
    import sys
    sys.exit(main())
