from pyparsing import *


def parse_cpp_member_function(function_declaration_to_parse: str):
    LPAREN, RPAREN, LCHEVRON, RCHEVRON, COMMA = map(Suppress, "()<>,")

    identifier = Word(alphas + "_", alphanums + "_")
    qualified_identifier = delimitedList(identifier, delim="::", combine=True)
    constexpr = Keyword("constexpr")
    inline = Keyword("inline")
    noexcept = Keyword("noexcept")
    maybe_unused = Keyword("[[maybe_unused]]")
    nodiscard = Keyword("[[nodiscard]]")
    return_usage = maybe_unused ^ nodiscard
    const = Keyword("const")
    pointer = Literal("*")
    const_pointer = Group(pointer + const)
    reference = Literal("&")
    const_reference = Group(reference + const)
    indirection = ZeroOrMore(pointer ^ const_pointer ^
                             reference ^ const_reference)
    type_name = Forward()
    type = Group(
        Optional(const)("left_const") +
        type_name +
        Optional(const)("right_const") +
        indirection("indirection"))
    type_name << qualified_identifier("type_name") + Optional(
        LCHEVRON + delimitedList(type, delim=COMMA) + RCHEVRON)("template_arguments")
    parameter = Group(type("type") + qualified_identifier("name"))
    parameter_list = delimitedList(parameter, delim=COMMA)
    function_decl = Optional(return_usage)("return_usage") + (
        Optional(constexpr)("constexpr") & Optional(inline)("inline")) + type(
        "return_type") + qualified_identifier("function_name") + LPAREN + Optional(
        parameter_list)("parameter_list") + RPAREN + (Optional(const)("const_method") & Optional(noexcept)("noexcept"))

    # Todo check for left and right const (maybe via addCondition?)

    parsed_function_declaration = function_decl.parseString(
        function_declaration_to_parse, parseAll=True)

    return parsed_function_declaration.as_dict()
