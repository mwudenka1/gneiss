from glue_code_generation.cpp_function_declaration_parser import parse_cpp_member_function

import pytest
import pyparsing


def test_member_function_parser_valid_samples():
    test_strings = [
        "void name()",
        "inline void push()",
        "constexpr void push()",
        "constexpr inline void push(const Image& image)",
        "constexpr inline void push(const Image& image, double number)",
        "inline constexpr void push(const Image& image, double number) const",
        "constexpr inline void push(const Image&* const image, double number) noexcept",
        "constexpr inline void push(const gneiss::Image& image) const noexcept",
        "[[nodiscard]] const double& push(const Image& image) noexcept const",
        "std::optional<int> pop()",
        "std::unique_ptr<int, deleter> my_func()"]

    results = [
        {'return_type': {'type_name': 'void', 'indirection': []}, 'function_name': 'name'},
        {'inline': 'inline', 'return_type': {'type_name': 'void', 'indirection': []}, 'function_name': 'push'},
        {'constexpr': 'constexpr', 'return_type': {'type_name': 'void', 'indirection': []}, 'function_name': 'push'},
        {'constexpr': 'constexpr', 'inline': 'inline', 'return_type': {'type_name': 'void', 'indirection': []},
         'function_name': 'push',
         'parameter_list': [
             {'type': {'left_const': 'const', 'type_name': 'Image', 'indirection': ['&']}, 'name': 'image'}]},
        {'constexpr': 'constexpr', 'inline': 'inline', 'return_type': {'type_name': 'void', 'indirection': []},
         'function_name': 'push',
         'parameter_list': [
             {'type': {'left_const': 'const', 'type_name': 'Image', 'indirection': ['&']}, 'name': 'image'},
             {'type': {'type_name': 'double', 'indirection': []}, 'name': 'number'}]},
        {'inline': 'inline', 'constexpr': 'constexpr', 'return_type': {'type_name': 'void', 'indirection': []},
         'function_name': 'push',
         'parameter_list': [
             {'type': {'left_const': 'const', 'type_name': 'Image', 'indirection': ['&']}, 'name': 'image'},
             {'type': {'type_name': 'double', 'indirection': []}, 'name': 'number'}],
         'const_method': 'const'},
        {'constexpr': 'constexpr', 'inline': 'inline', 'return_type': {'type_name': 'void', 'indirection': []},
         'function_name': 'push', 'parameter_list': [
            {'type': {'left_const': 'const', 'type_name': 'Image', 'indirection': ['&', ['*', 'const']]},
             'name': 'image'},
            {'type': {'type_name': 'double', 'indirection': []}, 'name': 'number'}], 'noexcept': 'noexcept'},
        {'constexpr': 'constexpr', 'inline': 'inline', 'return_type': {'type_name': 'void', 'indirection': []},
         'function_name': 'push', 'parameter_list': [
            {'type': {'left_const': 'const', 'type_name': 'gneiss::Image', 'indirection': ['&']}, 'name': 'image'}],
         'const_method': 'const', 'noexcept': 'noexcept'},
        {'return_usage': '[[nodiscard]]',
         'return_type': {'left_const': 'const', 'type_name': 'double', 'indirection': ['&']}, 'function_name': 'push',
         'parameter_list': [
             {'type': {'left_const': 'const', 'type_name': 'Image', 'indirection': ['&']}, 'name': 'image'}],
         'noexcept': 'noexcept', 'const_method': 'const'},
        {'return_type': {'indirection': [], 'template_arguments': [{'type_name': 'int', 'indirection': []}],
                         'type_name': 'std::optional'}, 'function_name': 'pop', },
        {'function_name': 'my_func',
         'return_type': {'indirection': [],
                         'template_arguments': [{'type_name': 'int', 'indirection': []},
                                                {'type_name': 'deleter', 'indirection': []}],
                         'type_name': 'std::unique_ptr'}}
    ]

    for test_string, result in zip(test_strings, results):
        # print(parse_cpp_member_function(test_string))
        assert parse_cpp_member_function(test_string) == result


def test_member_function_parser_invalid_samples():
    test_strings = [
        "void push)",
        "inline inline void push()",
        "constexpr push()",
        "constexprinline void push()",
        "constexpr inline void push(const Image& image,)",
        "void push() cnst",
        "Image& const const * push()"
    ]

    for test_string in test_strings:
        with pytest.raises(pyparsing.exceptions.ParseException):
            parse_cpp_member_function(test_string)
