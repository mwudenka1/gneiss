#include "gneiss/VioSystem.h"
#include "gneiss/gui/DatasetState.h"
// #include "gneiss/gui/ImGuiDemoWindow.h"
#include "gneiss/executable-utils/Evaluator.h"
#include "gneiss/gui/ImPlotWindow.h"
#include "gneiss/gui/ImageVisualizationState.h"
#include "gneiss/gui/ImageWindow.h"
#include "gneiss/gui/KeypointsEstimateState.h"
#include "gneiss/gui/OpticalFlowEvaluationState.h"
#include "gneiss/gui/ProfilingStatsState.h"
#include "gneiss/gui/Redux.h"
#include "gneiss/gui/Window.h"
#include "gneiss/gui/WiredWidget.h"
#include "gneiss/gui/checkbox_control_window.h"
#include "gneiss/gui/dataset_control_window.h"
#include "gneiss/gui/image_overlays.h"
#include "gneiss/image/ImageMeasurement.h"
#include "gneiss/image/KeypointsEstimate.h"
#include "gneiss/io/CameraMeasurement.h"
#include "gneiss/io/Dataset.h"
#include "gneiss/io/OpticalFlow.h"
#include "gneiss/io/VkittiLoader.h"
#include "gneiss/parameters/parameters.h"
#include "gneiss/profiling/ProfilingStats.h"
#include "gneiss/profiling/ProfilingTarget.h"
#include "gneiss/utils/Named.h"
#include "gneiss/utils/Timestamp.h"
#include "gneiss/utils/constexpr_for.h"
#include "gneiss/utils/ranges.h"

#include <fmt/core.h>
#include <implot.h>
#include <vsg/core/ref_ptr.h>
#include <vsg/io/Options.h>
#include <vsg/utils/Builder.h>
#include <vsgXchange/all.h>
#include <CLI/CLI.hpp>  // IWYU pragma: keep

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <cstddef>
#include <exception>
#include <filesystem>
#include <functional>
#include <iostream>
#include <iterator>
#include <memory>
#include <mutex>
#include <optional>
#include <string>
#include <string_view>
#include <thread>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

using namespace std::literals;

class OpticalFlowGuiExe {
 public:
  OpticalFlowGuiExe(int argc, char** argv, const std::filesystem::path& datasetPath,
                    const std::string_view scenarioName)
      : m_vsgOptions(vsg::Options::create()),
        m_window(argc, argv, m_vsgOptions),
        m_dataset(gneiss::io::load_vkitti(datasetPath, scenarioName)),
        m_evaluator(m_dataset),
        m_guiStateReader(m_guiStore.get_reader()),
        m_cameraTimes_s(([this]() {
          return m_dataset->cameraTimestamps | gneiss::ranges::views::transform([&](const auto& timestamp) {
                   return static_cast<float>(
                       (std::chrono::duration<float>(timestamp - m_dataset->cameraTimestamps.at(0))).count());
                 }) |
                 gneiss::ranges::to<std::vector>();
        })()),
        m_imageFeeder([this]() { feed_images(); }),
        m_resultFetcher([this]() { fetch_results(); }),
        m_profilingFetcher([this]() {
          if constexpr (gneiss::parameters::parameters.at("simple_profiling"sv).get<bool>()) {
            fetch_profiling();
          }
        }),
        m_evaluationThread([this]() { evaluate_when_finished(); }) {
    m_window.add_before_frame_hook([this]() noexcept { m_guiState = m_guiStateReader.read_lock(); });
    m_window.add_after_frame_hook([this]() noexcept { m_guiState.reset(); });

    // add vsgXchange's support for reading and writing 3rd party file formats
    m_vsgOptions->add(vsgXchange::all::create());

    create_3d_scene();

    create_imgui_windows();

    m_opticalFlowSystem.get_vio().shutdown();
  }

  void show_gui() { m_window.show(); }

  ~OpticalFlowGuiExe() {
    m_opticalFlowSystem.get_keypoints_queue().shutdown();
    m_opticalFlowSystem.get_image_queue().shutdown();
    m_opticalFlowSystem.get_profiling_queue().shutdown();
    m_shutdown = true;

    if (m_imageFeeder.joinable()) {
      m_imageFeeder.join();
    }
    if (m_resultFetcher.joinable()) {
      m_resultFetcher.join();
    }
    if (m_profilingFetcher.joinable()) {
      m_profilingFetcher.join();
    }
  }

 private:
  void create_3d_scene() {
    auto builder = vsg::Builder::create();
    const vsg::GeometryInfo info;  // defaults to base size of 1,1,1 at position 0,0,0
    vsg::StateInfo stateInfo;
    stateInfo.wireframe = true;
    stateInfo.lighting = false;
    auto node = builder->createSphere(info, stateInfo);
    node->setValue("name", "Sphere");
    m_window.add_scene_component(node);
  }

  void create_imgui_windows() {
    create_image_windows();
    create_dataset_control_window();
    create_image_control_window();
    create_profiling_plot_window();
    create_kpis_plot_window();
    // m_window.add_imgui_component(gneiss::gui::ImGuiDemoWindow::create());
  }

  void create_image_windows() {
    gneiss::constexpr_for<0, cameras.size(), 1>([&](auto cameraIdx) {
      auto imageWindow = gneiss::gui::ImageWindow::create(
          m_window.get_viewer(), m_vsgOptions,
          std::shared_ptr<std::vector<std::unique_ptr<gneiss::io::CameraMeasurement>>>(
              m_dataset, &m_dataset->cameraMeasurements.at(cameraIdx)),
          [this]() noexcept { return get_dataset_state(); }, fmt::format("Camera {}", cameraIdx.value));

      imageWindow->addOverlay([this, &cameraIdx]() {
        const auto currentFrame = get_dataset_state().currentFrame;
        const auto currentTimestamp = m_dataset->cameraTimestamps.at(currentFrame);
        const auto& opticalFlowEstimates = get_optical_flow_estimates();
        const auto& opticalFlowEvaluations = get_optical_flow_evaluations();
        const auto& imageVisualizationState = get_image_visualization_state();

        if (currentFrame >= opticalFlowEstimates.estimates.size()) {
          return;
        }

        const auto& opticalFlow = opticalFlowEstimates.estimates.at(currentFrame)->get<cameraIdx>();

        if (imageVisualizationState.opticalFlow) {
          gneiss::gui::draw_optical_flow(opticalFlow, opticalFlowEstimates.idCameraPatchHistoryMapping, cameraIdx,
                                         currentTimestamp);
        }

        const auto& groundTruthFrameIdxKeypointIdxTransformMapping =
            opticalFlowEvaluations.cameraIdxFrameIdxKeypointIdTransformMapping.at(cameraIdx);

        if (imageVisualizationState.groundTruthOpticalFlow && currentFrame > 0 &&
            currentFrame < groundTruthFrameIdxKeypointIdxTransformMapping.size()) {
          const auto& prevOpticalFlow = opticalFlowEstimates.estimates.at(currentFrame - 1)->get<cameraIdx>();
          const auto& groundTruthKeypointIdxTransformMapping =
              groundTruthFrameIdxKeypointIdxTransformMapping.at(currentFrame);

          gneiss::gui::draw_ground_truth_optical_flow(prevOpticalFlow, *groundTruthKeypointIdxTransformMapping);
        }

        if (imageVisualizationState.keypoints) {
          gneiss::gui::draw_keypoints(opticalFlow);
        }

        if (imageVisualizationState.patches) {
          gneiss::gui::draw_patches<cameraIdx>(opticalFlow);
        }

        if (imageVisualizationState.ids) {
          gneiss::gui::draw_ids(opticalFlow, m_window.get_font(), m_window.get_font_size());
        }
      });
      m_window.add_imgui_component(imageWindow);
    });
  }

  void create_dataset_control_window() {
    m_window.add_imgui_component(
        gneiss::gui::WiredWidget<
            gneiss::gui::DatasetState, std::size_t, std::optional<std::function<void(std::size_t)>>,
            std::optional<std::function<void(bool)>>>::create(gneiss::gui::dataset_control_window, [this]() {
          const auto setCurrentFrameAdapter = [this](std::size_t currentFrame) { set_current_frame(currentFrame); };
          const auto setFollowExecutionAdapter = [this](bool followExecution) {
            set_follow_execution(followExecution);
          };
          return std::make_tuple(get_dataset_state(), m_dataset->get_number_of_frames(), setCurrentFrameAdapter,
                                 setFollowExecutionAdapter);
        }));
  }

  void create_image_control_window() {
    m_window.add_imgui_component(
        gneiss::gui::WiredWidget<
            const char*, std::unordered_map<std::string, bool>,
            std::function<void(const char*, bool)>>::create(gneiss::gui::checkbox_control_window, [this]() {
          const auto& imageVisualizationState = get_image_visualization_state();
          const std::unordered_map<std::string, bool> imageControls = {
              {"keypoints", imageVisualizationState.keypoints},
              {"ids", imageVisualizationState.ids},
              {"optical flow", imageVisualizationState.opticalFlow},
              {"ground truth optical flow", imageVisualizationState.groundTruthOpticalFlow},
              {"patches", imageVisualizationState.patches}};
          const auto setControlValueAdapter = [this](const char* controlName, bool value) {
            if (std::string(controlName) == "keypoints") {
              set_keypoints_visibility(value);
            } else if (std::string(controlName) == "ids") {
              set_ids_visibility(value);
            } else if (std::string(controlName) == "optical flow") {
              set_optical_flow_visibility(value);
            } else if (std::string(controlName) == "ground truth optical flow") {
              set_ground_truth_optical_flow_visibility(value);
            } else if (std::string(controlName) == "patches") {
              set_patches_visibility(value);
            }
          };
          return std::make_tuple("Image Visualization Controls", imageControls, setControlValueAdapter);
        }));
  }

  void create_profiling_plot_window() {
    if constexpr (gneiss::parameters::parameters.at("simple_profiling"sv).get<bool>()) {
      auto plotWindow = gneiss::gui::ImPlotWindow::create("Profiling");

      const auto barWidth = std::chrono::duration<double>(m_dataset->cameraPeriodLength).count();
      plotWindow->add_graph([this, barWidth]() {
        ImPlot::SetupAxis(ImAxis_Y1, "time [ms]", ImPlotAxisFlags_AutoFit);
        const auto currentTimestamp_s =
            static_cast<float>(std::chrono::duration<float>(
                                   m_dataset->cameraTimestamps.at(get_dataset_state().currentFrame).time_since_epoch())
                                   .count());
        ImPlot::PlotInfLines("##current", &currentTimestamp_s, 1);
        ImPlot::PlotBars("optical flow", m_cameraTimes_s.data(), get_optical_flow_exec_times_ms().data(),
                         static_cast<int>(get_optical_flow_exec_times_ms().size()), barWidth);
        ImPlot::PlotBars("pyramid creation", m_cameraTimes_s.data(), get_pyramid_creation_exec_times_ms().data(),
                         static_cast<int>(get_pyramid_creation_exec_times_ms().size()), barWidth);
        ImPlot::PlotBars("points tracking", m_cameraTimes_s.data(), get_points_tracking_exec_times_ms().data(),
                         static_cast<int>(get_points_tracking_exec_times_ms().size()), barWidth);
        ImPlot::PlotBars("keypoint detection", m_cameraTimes_s.data(), get_keypoint_detection_exec_times_ms().data(),
                         static_cast<int>(get_keypoint_detection_exec_times_ms().size()), barWidth);
      });
      m_window.add_imgui_component(plotWindow);
    }
  }

  void create_kpis_plot_window() {
    auto plotWindow = gneiss::gui::ImPlotWindow::create("KPIs");

    const auto barWidth = std::chrono::duration<double>(m_dataset->cameraPeriodLength).count();
    plotWindow->add_graph([this, barWidth]() {
      ImPlot::SetupAxis(ImAxis_Y1, "RMSE [pixel]", ImPlotAxisFlags_AutoFit);
      ImPlot::SetupAxis(ImAxis_Y2, "num", ImPlotAxisFlags_AutoFit);

      const auto currentTimestamp_s =
          static_cast<float>(std::chrono::duration<float>(
                                 m_dataset->cameraTimestamps.at(get_dataset_state().currentFrame).time_since_epoch())
                                 .count());
      ImPlot::PlotInfLines("##current", &currentTimestamp_s, 1);

      const auto& opticalFlowEvaluations = get_optical_flow_evaluations();

      ImPlot::SetAxes(ImAxis_X1, ImAxis_Y1);
      for (const auto cameraIdx : gneiss::ranges::views::iota(std::size_t{0}, cameras.size())) {
        const auto& rmses = opticalFlowEvaluations.cameraIdxFrameIdxRmseMapping.at(cameraIdx);

        ImPlot::PlotBars(fmt::format("RMSE camera {}", cameraIdx).c_str(), std::next(m_cameraTimes_s.data()),
                         rmses.data(), static_cast<int>(rmses.size()), barWidth);
      }

      ImPlot::SetAxes(ImAxis_X1, ImAxis_Y2);
      for (const auto cameraIdx : gneiss::ranges::views::iota(std::size_t{0}, cameras.size())) {
        const auto& numsValid = opticalFlowEvaluations.cameraIdxFrameIdxNumValidMapping.at(cameraIdx);

        ImPlot::PlotBars(fmt::format("num valid camera {}", cameraIdx).c_str(), std::next(m_cameraTimes_s.data()),
                         numsValid.data(), static_cast<int>(numsValid.size()), barWidth);
      }
    });
    m_window.add_imgui_component(plotWindow);
  }

  void feed_images() {
    for (std::size_t frameIdx = 0; frameIdx < m_dataset->get_number_of_frames(); ++frameIdx) {
      if (m_shutdown) {
        return;
      }

      gneiss::ImageMeasurement imageMeasurement;
      gneiss::constexpr_for<0, cameras.size(), 1>([&](auto cameraIdx) {
        using ThisImage = typename gneiss::internal::ParameterizedImage<cameraIdx>::Image_;
        imageMeasurement.get<cameraIdx>() = m_dataset->cameraMeasurements.at(cameraIdx)
                                                .at(frameIdx)
                                                ->template getImage<typename ThisImage::PixelType, ThisImage::width,
                                                                    ThisImage::height, ThisImage::pitched>();
      });
      imageMeasurement.timestamp() = m_dataset->cameraTimestamps.at(frameIdx);
      auto imageMeasurementPtr = m_opticalFlowSystem.get_storage().storeImageMeasurement(imageMeasurement);
      m_opticalFlowSystem.get_image_queue().push(std::move(imageMeasurementPtr));
    }

    m_opticalFlowSystem.get_image_queue().stop();
  }

  void fetch_results() {
    {
      const std::unique_lock<std::mutex> lock(m_numberFetcherRunningMutex);
      ++m_numberFetcherRunning;
    }
    m_hasStarted = true;
    auto stateReader = m_guiStore.get_reader();
    while (!m_shutdown) {
      const auto optionalOpticalFlowEstimate = m_opticalFlowSystem.get_keypoints_queue().pop();
      if (!optionalOpticalFlowEstimate) {
        break;
      }
      const auto opticalFlowEstimate = *optionalOpticalFlowEstimate.value();
      m_evaluator.push_back(opticalFlowEstimate);
      append_optical_flow_estimate(std::make_shared<gneiss::KeypointsEstimate>(opticalFlowEstimate));

      const auto state = stateReader.read_lock();

      if (state->template get<"dataset">().followExecution) {
        const std::size_t measurementIdx = ([&]() {
          const auto measurementIterator = std::find(
              m_dataset->cameraTimestamps.begin(), m_dataset->cameraTimestamps.end(), opticalFlowEstimate.timestamp());
          if (measurementIterator != m_dataset->cameraTimestamps.end()) {
            return static_cast<std::size_t>(measurementIterator - m_dataset->cameraTimestamps.begin());
          }

          return std::size_t{0};
        })();
        set_current_frame(measurementIdx);
      }
    }

    {
      const std::unique_lock<std::mutex> lock(m_numberFetcherRunningMutex);
      --m_numberFetcherRunning;
      m_fetcherFinishedSignal.notify_all();
    }
  }

  void fetch_profiling() {
    {
      const std::unique_lock<std::mutex> lock(m_numberFetcherRunningMutex);
      ++m_numberFetcherRunning;
    }
    m_hasStarted = true;

    while (!m_shutdown) {
      const auto optionalProfilingStats = m_opticalFlowSystem.get_profiling_queue().pop();
      if (!optionalProfilingStats) {
        break;
      }
      m_evaluator.push_back(optionalProfilingStats.value());
      append_profiling_stats(optionalProfilingStats.value());
    }
    {
      const std::unique_lock<std::mutex> lock(m_numberFetcherRunningMutex);
      --m_numberFetcherRunning;
      m_fetcherFinishedSignal.notify_all();
    }
  }

  void evaluate_when_finished() {
    // wait for all fetcher to finish
    std::unique_lock<std::mutex> lock(m_numberFetcherRunningMutex);
    m_fetcherFinishedSignal.wait(lock,
                                 [this]() { return (m_numberFetcherRunning == 0 && m_hasStarted) || m_shutdown; });

    if (!m_shutdown) {
      m_evaluator.summarize();
    }
  }

  constexpr static auto& cameras = gneiss::parameters::parameters["cameras"sv].array_data();
  std::atomic_bool m_shutdown = false;

  /*
   * ------------ gui state start --------------
   */

  static constexpr auto storeFactory = [](const std::shared_ptr<const gneiss::io::Dataset>& dataset) {
    auto datasetState = gneiss::gui::StorePart<gneiss::gui::DatasetStateUpdater, gneiss::gui::DatasetState>({});
    auto imageVisualizationState =
        gneiss::gui::StorePart<gneiss::gui::ImageVisualizationStateUpdater, gneiss::gui::ImageVisualizationState>({});
    auto opticalFlowEstimateState =
        gneiss::gui::StorePart<gneiss::gui::KeypointsEstimateStateUpdater, gneiss::gui::KeypointsEstimateState>({});
    auto opticalFlowEvaluationState =
        gneiss::gui::StorePart<gneiss::gui::OpticalFlowEvaluationStateUpdater, gneiss::gui::OpticalFlowEvaluationState>(
            {.validThreshold_pixel2 = 4,
             .opticalFlowGroundTruth =
                 std::shared_ptr<const std::vector<std::vector<std::unique_ptr<gneiss::io::OpticalFlow>>>>(
                     dataset, &dataset->opticalFlow),
             .cameraIdxs = std::shared_ptr<const std::unordered_map<gneiss::Timestamp, std::size_t>>(
                 dataset, &dataset->cameraIdxs),
             .previousEstimate = nullptr,
             .cameraIdxFrameIdxKeypointIdTransformMapping = {},
             .cameraIdxFrameIdxRmseMapping = {},
             .cameraIdxFrameIdxNumValidMapping = {}});
    auto opticalFlowProfiling =
        gneiss::gui::StorePart<gneiss::gui::ProfilingStatsStateUpdater, gneiss::gui::ProfilingStatsState>(
            {.target = gneiss::ProfilingTarget::OPTICAL_FLOW_ESTIMATOR, .executionTimes_ms = {}});
    auto pyramidCreationProfiling =
        gneiss::gui::StorePart<gneiss::gui::ProfilingStatsStateUpdater, gneiss::gui::ProfilingStatsState>(
            {.target = gneiss::ProfilingTarget::OPTICAL_FLOW_PYRAMID_CREATION, .executionTimes_ms = {}});
    auto pointsTrackingProfiling =
        gneiss::gui::StorePart<gneiss::gui::ProfilingStatsStateUpdater, gneiss::gui::ProfilingStatsState>(
            {.target = gneiss::ProfilingTarget::OPTICAL_FLOW_POINTS_TRACKING, .executionTimes_ms = {}});
    auto keypointDetectionProfiling =
        gneiss::gui::StorePart<gneiss::gui::ProfilingStatsStateUpdater, gneiss::gui::ProfilingStatsState>(
            {.target = gneiss::ProfilingTarget::OPTICAL_FLOW_KEYPOINT_DETECTION, .executionTimes_ms = {}});

    auto opticalFlowExeState = gneiss::gui::combine_store_parts(
        gneiss::make_named<"dataset">(datasetState), gneiss::make_named<"image visualization">(imageVisualizationState),
        gneiss::make_named<"optical flow estimates">(opticalFlowEstimateState),
        gneiss::make_named<"optical flow evaluation">(opticalFlowEvaluationState),
        gneiss::make_named<"optical flow profiling">(opticalFlowProfiling),
        gneiss::make_named<"pyramid creation profiling">(pyramidCreationProfiling),
        gneiss::make_named<"points tracking profiling">(pointsTrackingProfiling),
        gneiss::make_named<"keypoint detection profiling">(keypointDetectionProfiling));
    return gneiss::gui::Store(opticalFlowExeState);
  };

  using StoreType = std::invoke_result_t<decltype(storeFactory), std::shared_ptr<const gneiss::io::Dataset>>;

  const gneiss::gui::DatasetState& get_dataset_state() { return m_guiState->template get<"dataset">(); }
  void set_current_frame(const std::size_t currentFrame) {
    m_guiStore.dispatch(std::make_unique<gneiss::gui::SetCurrentFrame>(currentFrame));
  }
  void set_follow_execution(const bool followExecution) {
    m_guiStore.dispatch(std::make_unique<gneiss::gui::SetFollowExecution>(followExecution));
  }

  const gneiss::gui::ImageVisualizationState& get_image_visualization_state() {
    return m_guiState->template get<"image visualization">();
  }
  void set_keypoints_visibility(bool visibility) {
    m_guiStore.dispatch(std::make_unique<gneiss::gui::SetKeypointsVisibility>(visibility));
  }
  void set_ids_visibility(bool visibility) {
    m_guiStore.dispatch(std::make_unique<gneiss::gui::SetIdsVisibility>(visibility));
  }
  void set_optical_flow_visibility(bool visibility) {
    m_guiStore.dispatch(std::make_unique<gneiss::gui::SetOpticalFlowVisibility>(visibility));
  }
  void set_ground_truth_optical_flow_visibility(bool visibility) {
    m_guiStore.dispatch(std::make_unique<gneiss::gui::SetGroundTruthOpticalFlowVisibility>(visibility));
  }
  void set_patches_visibility(bool visibility) {
    m_guiStore.dispatch(std::make_unique<gneiss::gui::SetPatchesVisibility>(visibility));
  }

  const gneiss::gui::KeypointsEstimateState& get_optical_flow_estimates() {
    return m_guiState->template get<"optical flow estimates">();
  }
  void append_optical_flow_estimate(const std::shared_ptr<gneiss::KeypointsEstimate>& opticalFlowEstimate) {
    m_guiStore.dispatch(std::make_unique<gneiss::gui::AppendKeypointEstimate>(opticalFlowEstimate));
  }
  const gneiss::gui::OpticalFlowEvaluationState& get_optical_flow_evaluations() {
    return m_guiState->template get<"optical flow evaluation">();
  }

  const std::vector<float>& get_optical_flow_exec_times_ms() {
    return m_guiState->template get<"optical flow profiling">().executionTimes_ms;
  }
  const std::vector<float>& get_pyramid_creation_exec_times_ms() {
    return m_guiState->template get<"pyramid creation profiling">().executionTimes_ms;
  }
  const std::vector<float>& get_points_tracking_exec_times_ms() {
    return m_guiState->template get<"points tracking profiling">().executionTimes_ms;
  }
  const std::vector<float>& get_keypoint_detection_exec_times_ms() {
    return m_guiState->template get<"keypoint detection profiling">().executionTimes_ms;
  }
  void append_profiling_stats(const gneiss::ProfilingStats& profilingStats) {
    m_guiStore.dispatch(std::make_unique<gneiss::gui::AppendProfilingStats>(profilingStats));
  }

  /*
   * ------------ gui state end --------------
   */

  vsg::ref_ptr<vsg::Options> m_vsgOptions;
  gneiss::gui::Window m_window;
  std::shared_ptr<gneiss::io::Dataset> m_dataset;
  gneiss::executable_utils::Evaluator m_evaluator;
  StoreType m_guiStore = storeFactory(m_dataset);
  typename StoreType::ReaderType m_guiStateReader;
  typename StoreType::PointerType m_guiState;  // rcu
  std::vector<float> m_cameraTimes_s;
  gneiss::VioSystem m_opticalFlowSystem;
  std::atomic<bool> m_hasStarted;
  std::mutex m_numberFetcherRunningMutex;
  int m_numberFetcherRunning = 0;
  std::condition_variable m_fetcherFinishedSignal;
  std::thread m_imageFeeder;
  std::thread m_resultFetcher;
  std::thread m_profilingFetcher;
  std::thread m_evaluationThread;
};

int main(int argc, char** argv) {
  try {
    // NOLINTNEXTLINE(misc-include-cleaner)
    CLI::App app{"App description"};

    std::filesystem::path datasetPath = "undefined";
    std::string scenarioName = "undefined";
    app.add_option("-d,--dataset_path", datasetPath, "Path to the dataset root folder")
        ->required()
        ->check(CLI::ExistingDirectory);  // NOLINT(misc-include-cleaner)
    app.add_option("-s, --scenario", scenarioName, "Name of the scenario.")->required();

    try {
      app.parse((argc), (argv));
    }
    // NOLINTNEXTLINE(misc-include-cleaner)
    catch (const CLI::ParseError& e) {
      return app.exit(e);
    }

    OpticalFlowGuiExe opticalFlowExe(argc, argv, datasetPath, scenarioName);
    opticalFlowExe.show_gui();

  } catch (const std::exception& e) {
    std::cout << e.what();
    return 1;
  }

  return 0;
}
