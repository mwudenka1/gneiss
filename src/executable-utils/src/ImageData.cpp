#define STB_IMAGE_IMPLEMENTATION
#include "gneiss/executable-utils/ImageData.h"

#include <cstdint>

void gneiss::executable_utils::freeCharImage(uint8_t* imageData) { stbi_image_free(imageData); }

void gneiss::executable_utils::freeShortImage(uint16_t* imageData) { stbi_image_free(imageData); }