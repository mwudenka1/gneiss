#include "gneiss/executable-utils/is_directory_or_created.h"

#include <filesystem>

bool gneiss::executable_utils::is_directory_or_created(const std::filesystem::path& directory) {
  if (std::filesystem::is_directory(directory)) {
    return true;
  }

  try {
    return std::filesystem::create_directory(directory);
  } catch (const std::filesystem::filesystem_error& e) {
    return false;
  }
}