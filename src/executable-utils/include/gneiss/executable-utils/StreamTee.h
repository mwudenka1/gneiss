#ifndef GNEISS_STREAMTEE_H
#define GNEISS_STREAMTEE_H
#pragma once
/** @file
 * Taken from https://stackoverflow.com/a/1761027/4906760
 * */

#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <vector>

namespace gneiss::executable_utils {

/**
 * @addtogroup executable-utils
 *
 * @{
 */

/**
 * @brief Ostream that forwards its content to n linked streams.
 *
 * Use typedefs StreamTee or WStreamTee.
 *
 * @tparam Char_
 */
template <typename Char_>
class BasicStreamTee : public std::basic_ostream<Char_> {
  struct TeeBuffer : public std::basic_streambuf<Char_> {
    using Base = std::basic_streambuf<Char_>;

    void addBuffer(std::basic_streambuf<Char_>* buf) { m_buffers.push_back(buf); }

    int overflow(Base::int_type character) override {
      for (auto& buffer : m_buffers) {
        buffer->sputc(Base::traits_type::to_char_type(character));
      }

      return character;
    }

   private:
    std::vector<std::basic_streambuf<Char_>*> m_buffers;
  };

  TeeBuffer m_buffer;

 public:
  BasicStreamTee() : std::basic_ostream<Char_>(nullptr) { std::basic_ostream<Char_>::rdbuf(&m_buffer); }

  template <std::derived_from<std::basic_ostream<Char_>>... Streams_>
  explicit BasicStreamTee(Streams_&... streams) : BasicStreamTee() {
    (link_stream(streams), ...);
  }

  void link_stream(std::basic_ostream<Char_>& ostream) {
    ostream.flush();
    m_buffer.addBuffer(ostream.rdbuf());
  }
};

using StreamTee = BasicStreamTee<char>;
using WStreamTee = BasicStreamTee<wchar_t>;

/** @} */  // end of executable-utils

}  // namespace gneiss::executable_utils

#endif  // GNEISS_STREAMTEE_H
