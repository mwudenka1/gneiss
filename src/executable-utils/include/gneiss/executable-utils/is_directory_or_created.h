#ifndef GNEISS_IS_DIRECTORY_OR_CREATED_H
#define GNEISS_IS_DIRECTORY_OR_CREATED_H
#pragma once
/** @file
 * Taken from https://stackoverflow.com/a/1761027/4906760
 * */

#include <filesystem>

namespace gneiss::executable_utils {

/**
 * @addtogroup executable-utils
 *
 * @{
 */

/**
 * Checks if a directory exists. If not tries to create it.
 *
 * @param directory
 * @return True if the directory (now) exists, false otherwise.
 */
[[nodiscard]] bool is_directory_or_created(const std::filesystem::path& directory);

/** @} */  // end of executable-utils

}  // namespace gneiss::executable_utils
#endif  // GNEISS_IS_DIRECTORY_OR_CREATED_H
