#ifndef GNEISS_IMAGEREADER_H
#define GNEISS_IMAGEREADER_H
#pragma once
/** @file */

#include "gneiss/utils/c_resource.h"

#include <stb_image.h>

#include <cstdint>

namespace gneiss::executable_utils {

/**
 * @addtogroup executable-utils
 *
 * @{
 */

/**
 * @brief wrapper of stbi_image_free that converts unsigned char* to void* to work with stdext::c_resource
 * @param imageData
 */
void freeCharImage(uint8_t* imageData);

/**
 * @brief wrapper of stbi_image_free that converts unsigned short* to void* to work with stdext::c_resource
 * @param imageData
 */
void freeShortImage(uint16_t* imageData);

template <typename PixelType_>
  requires(std::is_same_v<PixelType_, uint8_t> || std::is_same_v<PixelType_, uint16_t>)
using ImageData =
    std::conditional_t<std::is_same_v<PixelType_, uint8_t>, stdext::c_resource<uint8_t, stbi_load, freeCharImage>,
                       stdext::c_resource<uint16_t, stbi_load_16, freeShortImage>>;

/** @} */  // end of executable-utils

}  // namespace gneiss::executable_utils

#endif  // GNEISS_IMAGEREADER_H
