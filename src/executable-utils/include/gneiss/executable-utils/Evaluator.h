#ifndef GNEISS_EVALUATOR_H
#define GNEISS_EVALUATOR_H
#pragma once
/** @file */

#include "gneiss/evaluation/calculate_optical_flow_se.h"
#include "gneiss/evaluation/print_optical_flow_evaluation.h"
#include "gneiss/executable-utils/StreamTee.h"
#include "gneiss/executable-utils/is_directory_or_created.h"
#include "gneiss/image/KeypointsEstimate.h"
#include "gneiss/io/Dataset.h"
#include "gneiss/profiling/ProfilingStats.h"
#include "gneiss/utils/ranges.h"

#include <oneapi/tbb/concurrent_vector.h>

#include <filesystem>
#include <memory>

namespace gneiss::executable_utils {

/**
 * @addtogroup executable-utils
 *
 * @{
 */

class Evaluator {
 public:
  explicit Evaluator(std::shared_ptr<gneiss::io::Dataset> dataset) : m_dataset(std::move(dataset)) {}

  void push_back(const gneiss::KeypointsEstimate& estimate) { m_keypointsEstimates.push_back(estimate); }

  void push_back(const gneiss::ProfilingStats& stats) { m_profilingStats.push_back(stats); }

  void summarize(const std::filesystem::path& evaluationFile = std::filesystem::path()) const {
    gneiss::executable_utils::StreamTee evaluationStream(std::cout);
    std::unique_ptr<std::fstream> evaluationFileStream;
    if (!evaluationFile.empty() && gneiss::executable_utils::is_directory_or_created(evaluationFile.parent_path())) {
      evaluationFileStream = std::make_unique<std::fstream>(evaluationFile, std::fstream::out);

      if (evaluationFileStream->is_open()) {
        evaluationStream.link_stream(*evaluationFileStream);
        *evaluationFileStream << "---\n";
      } else {
        fmt::print("Could not open {}.\n", evaluationFile.string());
      }
    }
    summarize_optical_flow_evaluation(evaluationStream);
    summarize_runtimes(evaluationStream);
  }

  void summarize_optical_flow_evaluation(std::ostream& out) const {
    constexpr static std::string_view optical_flow_scalar =
        gneiss::parameters::parameters["optical_flow_scalar"sv].template get<std::string_view>();
    using Scalar =
        gneiss::string_to_type_t<gneiss::FixedString<optical_flow_scalar.size()>(optical_flow_scalar.data())>;

    std::array<std::vector<std::vector<float>>, cameras.size()> cameraIdxFrameIdxKeypointIdxErrors_pixel2;
    gneiss::constexpr_for<0, cameras.size(), 1>([&](auto cameraIdx) {
      const auto& opticalFlowGroundTruthForCamera = m_dataset->opticalFlow.at(cameraIdx);
      cameraIdxFrameIdxKeypointIdxErrors_pixel2.at(cameraIdx) =
          gneiss::ranges::views::iota(std::size_t{0}, m_dataset->cameraTimestamps.size() - 1) |
          gneiss::ranges::views::transform([&](const auto frameIdx) -> std::vector<float> {
            const auto& opticalFlowGroundTruth = *opticalFlowGroundTruthForCamera.at(frameIdx);
            return gneiss::calculate_optical_flow_se_pixel2<Scalar>(
                m_keypointsEstimates.at(frameIdx).template get<cameraIdx>(),
                m_keypointsEstimates.at(frameIdx + 1).template get<cameraIdx>(), opticalFlowGroundTruth);
          }) |
          gneiss::ranges::to<std::vector<std::vector<float>>>();
    });

    std::array<std::vector<float>, cameras.size()> cameraIdxFrameIdxRmseMapping;
    gneiss::constexpr_for<0, cameras.size(), 1>([&](auto cameraIdx) {
      cameraIdxFrameIdxRmseMapping.at(cameraIdx) =
          cameraIdxFrameIdxKeypointIdxErrors_pixel2.at(cameraIdx) |
          gneiss::ranges::views::transform([&](const auto& errors_pixel2) -> float {
            const float sum = std::reduce(errors_pixel2.cbegin(), errors_pixel2.cend());
            const float mse = 1.f / static_cast<float>(errors_pixel2.size()) * sum;
            return std::sqrt(mse);
          }) |
          gneiss::ranges::to<std::vector<float>>();
    });

    std::array<std::vector<float>, cameras.size()> cameraIdxFrameIdxNumValidMapping;
    gneiss::constexpr_for<0, cameras.size(), 1>([&](auto cameraIdx) {
      cameraIdxFrameIdxNumValidMapping.at(cameraIdx) =
          cameraIdxFrameIdxKeypointIdxErrors_pixel2.at(cameraIdx) |
          gneiss::ranges::views::transform([&](const auto& errors_pixel2) -> float {
            return static_cast<float>(std::count_if(
                errors_pixel2.begin(), errors_pixel2.end(),
                [validThreshold_pixel2 = 4.f](const auto error) { return error < validThreshold_pixel2; }));
          }) |
          gneiss::ranges::to<std::vector<float>>();
    });

    gneiss::print_optical_flow_evaluation(out, cameraIdxFrameIdxRmseMapping, cameraIdxFrameIdxNumValidMapping);
  }

  void summarize_runtimes(std::ostream& out) const {
    out << "runtime_ms:\n";
    for (const auto& [target, string] :
         {std::make_tuple(gneiss::ProfilingTarget::OPTICAL_FLOW_ESTIMATOR, "optical_flow_estimator"),
          std::make_tuple(gneiss::ProfilingTarget::OPTICAL_FLOW_PYRAMID_CREATION, "optical_flow_pyramid_creation"),
          std::make_tuple(gneiss::ProfilingTarget::OPTICAL_FLOW_POINTS_TRACKING, "optical_flow_points_tracking"),
          std::make_tuple(gneiss::ProfilingTarget::OPTICAL_FLOW_KEYPOINT_DETECTION,
                          "optical_flow_keypoint_detection")}) {
      const auto runtimes_ms =
          m_profilingStats |
          gneiss::ranges::views::filter([target](const auto& stats) noexcept { return stats.target == target; }) |
          gneiss::ranges::views::transform([](const auto& stats) noexcept { return stats.executionTime.count(); }) |
          gneiss::ranges::to<std::vector<int64_t>>();

      out << " " << string << ":\n";
      out << "    min: " << gneiss::ranges::min(runtimes_ms) << '\n';
      out << "    max: " << gneiss::ranges::max(runtimes_ms) << '\n';
      out << "    average: "
          << (!runtimes_ms.empty()
                  ? static_cast<double>(gneiss::ranges::fold_left(runtimes_ms, 0, std::plus<int64_t>{})) /
                        static_cast<double>(runtimes_ms.size())
                  : 0)
          << '\n';
    }
  }

 private:
  constexpr static auto& cameras = gneiss::parameters::parameters["cameras"sv].array_data();

  std::shared_ptr<gneiss::io::Dataset> m_dataset;

  tbb::concurrent_vector<gneiss::KeypointsEstimate> m_keypointsEstimates;
  tbb::concurrent_vector<gneiss::ProfilingStats> m_profilingStats;
};

/** @} */  // end of executable-utils

}  // namespace gneiss::executable_utils
#endif  // GNEISS_EVALUATOR_H
