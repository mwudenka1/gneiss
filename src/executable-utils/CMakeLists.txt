find_package(stb REQUIRED CONFIG)

add_library(gneiss-executable-utils STATIC src/ImageData.cpp src/is_directory_or_created.cpp)

target_include_directories(gneiss-executable-utils PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>)

target_link_libraries(gneiss-executable-utils PUBLIC gneiss::utils stb::stb)

add_library(gneiss::executable-utils ALIAS gneiss-executable-utils)
