# Gneiss

Work in progress. Documentation will follow soon.

## Documentation

Documentation corresponding to the master branch will be at [gitlab.io](https://mwudenka.gitlab.io/gneiss/).

Run architecture vis
```shell
docker pull structurizr/lite:latest
docker run -it --rm -p 8080:8080 -v $PWD:/usr/local/structurizr structurizr/lite
```
