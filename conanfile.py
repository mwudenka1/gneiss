import os
import re
import json
from typing import Dict

from conan import ConanFile
from conan.tools.cmake import CMakeToolchain, CMake, CMakeDeps, cmake_layout
from conan.tools.files import load, copy
from conan.tools.build import check_min_cppstd
from conan.errors import ConanInvalidConfiguration


class GneissConan(ConanFile):
    name = "gneiss"

    package_type = "header-library"

    # Optional metadata
    license = "MIT"
    author = "Martin Wudenka Martin.Wudenka@gmx.de"
    url = ""
    description = "<Description of Gneiss here>"
    topics = ("Computer Vision", "Visual Odometry")

    # Binary configuration
    settings = "os", "compiler", "build_type", "arch"
    options = {"parallelization": ["off", "tbb"],
               "with_executables": [True, False],
               "with_gui": [True, False],
               "sanitizer": ["None", "Address", "Thread", "Leak", "Memory"]}
    default_options = {
        "parallelization": "tbb",
        "with_executables": True,
        "with_gui": True,
        "sanitizer": "None"}

    generators = "VirtualBuildEnv"

    exports = ["LICENSE.md"]
    # Sources are located in the same place as this recipe, copy them to the
    # recipe
    exports_sources = "CMakeLists.txt", "src/*", "cmake/*", "test/*", "docs/*", "scripts/*", ".clang-tidy"
    no_copy_source = False

    @property
    def _parameter_file(self):
        return self.conf.get(
            "user.build:parameter_file",
            default=str(
                os.path.join(
                    "data",
                    "parameters.json")))

    @property
    def _abs_parameter_file(self):
        if os.path.isabs(self._parameter_file):
            return self._parameter_file

        return os.path.join(
            self.source_folder if self.source_folder else "",
            "src",
            self._parameter_file)

    @property
    def _build_all(self):
        return bool(self.conf.get("user.build:all", default=True))

    @property
    def _skip_docs(self):
        return bool(self.conf.get("user.build:skip_docs", default=True))

    @property
    def _tests(self):
        return bool(self.conf.get("user.build:tests", default=True))

    @property
    def _benchmarks(self):
        return bool(self.conf.get("user.build:benchmarks", default=False))

    def set_version(self):
        content = load(
            self,
            os.path.join(
                self.recipe_folder,
                "src/CMakeLists.txt"))
        version = re.search(
            r"project\([^\)]+VERSION (\d+\.\d+\.\d+)[^\)]*\)", content
        ).group(1)
        self.version = version.strip()

    # def config_options(self):
    #     if not self.options.with_executables:
    #         self.options.rm_safe("with_gui")

    def configure(self):
        self._add_parameter_to_conf(self.conf)

        if self.options.parallelization == "tbb" or (
                self.options.with_executables and self.options.with_gui):
            self.options["onetbb/*"].shared = True
            if self.settings.compiler == "clang":
                # requires hwloc, does not work with clang 17
                self.options["onetbb/*"].tbbbind = False

        if self.options.with_executables and self.options.with_gui:
            self.options["imgui/*"].shared = False
            self.options["vsg/*"].shared = False
            self.options["vsgimgui/*"].shared = False
            self.options["vsgxchange/*"].shared = False

    def validate(self):
        if self.settings.compiler.get_safe("cppstd"):
            check_min_cppstd(self, 23)

    def requirements(self):
        self.requires("eigen/3.4.0", transitive_headers=True)
        self.requires("sophus/1.22.10", transitive_headers=True)
        self.requires("range-v3/0.12.0", transitive_headers=True)
        self.requires(
            "json2cpp/0.0.1@mwudenka/snapshot",
            transitive_headers=True)

        if self.options.parallelization == "tbb" or (
                self.options.with_executables and self.options.with_gui):
            self.requires("onetbb/2021.10.0")

        if self.options.with_executables:
            self.requires("stb/cci.20220909", visible=False)
            self.requires("cli11/2.3.2", visible=False)
            self.requires("fmt/10.1.0", visible=False)

        if self.options.with_executables and self.options.with_gui:
            self.requires("imgui/1.90.8-docking", force=True, visible=False)
            self.requires(
                "vsg/1.1.4@mwudenka/snapshot",
                visible=False,
                force=True)
            self.requires(
                "vsgimgui/0.4.0@mwudenka/snapshot",
                visible=False)
            self.requires("vsgxchange/1.1.2@mwudenka/snapshot", visible=False)
            self.requires(
                "crill/4b5f65be96489ecf6e3516955943bddfba4fd219@mwudenka/snapshot",
                visible=False)
            self.requires("implot/0.16")

    def build_requirements(self):
        self.tool_requires("cmake/[>=3.21 <4]")
        self.tool_requires("structurizr/2024.03.03@mwudenka/snapshot")
        self.tool_requires("poetry/1.8.3@mwudenka/snapshot")
        self.tool_requires("json2cpp/0.0.1@mwudenka/snapshot")
        self.tool_requires("ccache/[>=4.5.1]")

        if self._build_all:
            self.test_requires("catch2/3.3.0")

        if self._build_all and not self._skip_docs:
            self.tool_requires("doxygen/1.9.4")

    def layout(self):
        cmake_layout(self, src_folder=".")

    def generate(self):
        if not os.path.isfile(self._abs_parameter_file):
            raise ConanInvalidConfiguration(
                f"{self._abs_parameter_file} is not a file!")
        tc = CMakeToolchain(self)
        tc.variables["GNEISS_PARALLELIZATION"] = self.options.parallelization
        tc.variables["SANITIZER"] = str(self.options.sanitizer)
        tc.variables["GNEISS_PARAMETER_FILE"] = str(self._abs_parameter_file)
        tc.variables["GNEISS_BUILD_DOCS"] = self._build_all and not self._skip_docs
        tc.variables["GNEISS_WITH_EXECUTABLES"] = bool(
            self.options.with_executables)
        tc.variables["GNEISS_WITH_GUI"] = bool(self.options.with_gui)
        tc.variables["GNEISS_WITH_TESTS"] = bool(self._tests)
        tc.variables["GNEISS_WITH_BENCHMARKS"] = bool(self._benchmarks)
        tc.generate()
        deps = CMakeDeps(self)
        if self._build_all and not self._skip_docs:
            deps.build_context_activated = ["doxygen"]
        deps.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure(build_script_folder=None if self._build_all else "src")
        cmake.build()
        if self._tests or self._benchmarks:
            cmake.test()

    def package(self):
        copy(
            self,
            "LICENSE.md",
            self.source_folder,
            self.package_folder
        )
        cmake = CMake(self)
        cmake.install()

    def package_id(self):
        # the package exports an exe and a header-only library, so compiler and
        # build type don't matter downstream
        self.info.settings.rm_safe("compiler")
        if not self.info.options.with_executables:
            self.info.settings.rm_safe("build_type")
            self.info.settings.rm_safe("arch")
            self.info.settings.rm_safe("os")

        self._add_parameter_to_conf(self.info.conf)
        self.info.conf.unset("user.build:parameter_file")

    def _add_parameter_to_conf(self, conf):
        def add_dict_to_options(param_value, flattened_keys: str = ""):
            if isinstance(param_value, dict):
                for pk, pv in param_value.items():
                    add_dict_to_options(pv, flattened_keys + pk + ".")
            elif isinstance(param_value, list):
                for idx, pi in enumerate(param_value):
                    add_dict_to_options(pi, flattened_keys + str(idx) + ".")
            else:
                conf.define(
                    "user.parameter:" +
                    flattened_keys.rstrip('.'),
                    str(param_value))

        if os.path.isfile(self._abs_parameter_file):
            with open(self._abs_parameter_file) as parameter_file:
                parameters = json.load(parameter_file)
                add_dict_to_options(parameters)

    def package_info(self):
        self.cpp_info.set_property(
            "cmake_find_mode",
            "none")  # Do NOT generate any files
        self.cpp_info.builddirs.append(os.path.join("lib", "cmake", "gneiss"))
