#include "gneiss/utils/constexpr_enumerate.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstdint>

SCENARIO("constexpr_enumerate applies a function to every element in a range", "[utils]") {
  GIVEN("An input range") {
    const uint32_t length = 5;
    constexpr std::array<int32_t, length> input = {0, 1, 2, 3, 4};

    WHEN("constexpr_enumerate is used to copy the values to another container at compile time") {
      std::array<int32_t, length> target{};

      gneiss::constexpr_enumerate<input>([&target](auto idx, auto value) {
        target[idx] = value;
        STATIC_REQUIRE(idx == value);
      });

      THEN("the values have been copied and can be used at runtime") { CHECK(target.at(3) == 3); }
    }
  }
}
