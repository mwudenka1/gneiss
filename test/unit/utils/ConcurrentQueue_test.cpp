#include "gneiss/utils/ConcurrentQueue.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <memory>
#include <thread>

SCENARIO("Values can be pushed and popped from ConcurrentQueue", "[utils]") {
  GIVEN("a ConcurrentQueue object of capacity 1") {
    auto testQueue = gneiss::ConcurrentQueue<double, 2>();

    WHEN("a value is stored") {
      testQueue.push(948.2928);

      THEN("the value can be popped") {
        const auto poppedOptional = testQueue.pop();
        CHECK(poppedOptional.has_value());
        if (poppedOptional.has_value()) {
          CHECK(poppedOptional.value() == 948.2928);
        }
      }
    }
  }
}

SCENARIO("Values can be pushed and popped from ConcurrentQueue in a multi threaded context", "[utils]") {
  GIVEN("a ConcurrentQueue object and a list of trivial values") {
    auto testQueue = gneiss::ConcurrentQueue<float, 3>();
    const std::array<float, 8> inputs = {0.733f, 1e5f, 84.726f, 4e-6f, 73.923f, -732.029f, -4e-4f, 744.9282f};

    WHEN("Values are pushed from on thread") {
      auto producer = std::thread([&]() noexcept {
        for (const auto value : inputs) {
          testQueue.push(value);
        }
      });

      THEN("the values can be popped in correct order") {
        auto consumer = std::thread([&]() {
          for (const auto value : inputs) {
            const auto poppedOptional = testQueue.pop();
            CHECK(poppedOptional.has_value());
            if (poppedOptional.has_value()) {
              CHECK(poppedOptional.value() == value);
            }
          }
        });
        producer.join();
        consumer.join();
      }
    }
  }

  GIVEN("a ConcurrentQueue object for non-trivial objects and a list of values") {
    auto testQueue = gneiss::ConcurrentQueue<std::unique_ptr<float>, 3>();
    const std::array<float, 8> inputs = {0.733f, 1e5f, 84.726f, 4e-6f, 73.923f, -732.029f, -4e-4f, 744.9282f};

    WHEN("Values are pushed from on thread") {
      auto producer = std::thread([&]() noexcept {
        for (const auto value : inputs) {
          testQueue.push(std::make_unique<float>(value));
        }
      });

      THEN("the values can be popped in correct order") {
        auto consumer = std::thread([&]() {
          for (const auto value : inputs) {
            const auto poppedOptional = testQueue.pop();
            CHECK(poppedOptional.has_value());
            if (poppedOptional.has_value()) {
              CHECK(poppedOptional.value());
              if (poppedOptional.value()) {
                CHECK(*(poppedOptional.value()) == value);
              }
            }
          }
        });
        producer.join();
        consumer.join();
      }
    }
  }
}
