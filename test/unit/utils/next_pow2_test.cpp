#include "gneiss/utils/next_pow2.h"

#include "gneiss/utils/constexpr_enumerate.h"
#include "gneiss/utils/ranges.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstddef>
#include <cstdint>
#include <limits>

namespace {
struct NextPowData {
  uint32_t input;
  uint32_t expected;
};
}  // namespace

SCENARIO("32bit unsigned integers can be rounded to the next power of 2", "[utils]") {
  GIVEN("A 32bit unsigned integer not equal a power of 2") {
    constexpr static std::array<NextPowData, 4> data = {{{5, 8}, {12, 16}, {33, 64}, {99, 128}}};

    const static auto idxs = gneiss::ranges::views::iota(0ul, data.size());
    gneiss::ranges::for_each(gneiss::ranges::views::zip(idxs, data), [](const auto& zipped) {
      const auto& [idx, datum] = zipped;
      WHEN("the integer is rounded to the next power of 2 at runtime, input #: " << idx) {
        const auto result = gneiss::next_pow2(datum.input);
        THEN("it equals the expected value") { CHECK(result == datum.expected); }
      }
    });

    gneiss::constexpr_enumerate<data>([&](auto idx, auto datumConstant) {
      constexpr const typename decltype(datumConstant)::value_type datum = datumConstant;
      WHEN("the integer is rounded to the next power of 2 at runtime, input #: " << idx) {
        const auto result = gneiss::next_pow2(datum.input);
        THEN("it equals the expected value") { STATIC_REQUIRE(result == datum.expected); }
      }
    });
  }

  GIVEN("A 32bit unsigned integer equal a power of 2") {
    constexpr static std::size_t num_tests = 4;
    constexpr static std::array<uint32_t, num_tests> inputs = {4, 8, 16, 512};

    const static auto idxs = gneiss::ranges::views::iota(0ul, inputs.size());
    gneiss::ranges::for_each(gneiss::ranges::views::zip(idxs, inputs), [](const auto& zipped) {
      const auto& [idx, input] = zipped;
      WHEN("the integer is rounded to the next power of 2 at runtime, input #: " << idx) {
        const auto result = gneiss::next_pow2(input);
        THEN("the integer is not rounded up (result equals input)") { CHECK(result == input); }
      }
    });

    gneiss::constexpr_enumerate<inputs>([&](auto idx, auto input) {
      WHEN("the integer is rounded to the next power of 2 at runtime, input #: " << idx) {
        const auto result = gneiss::next_pow2(input);
        THEN("the integer is not rounded up (result equals input)") { STATIC_REQUIRE(result == input); }
      }
    });
  }

  GIVEN("A 32bit unsigned integer equal to 0") {
    const uint32_t input = 0u;
    WHEN("it is rounded up at runtime") {
      const auto result = gneiss::next_pow2(input);

      THEN("the result is 0") { CHECK(result == 0u); }
    }
    WHEN("it is rounded up at compile time") {
      const auto result = gneiss::next_pow2(input);

      THEN("the result is 0") { STATIC_REQUIRE(result == 0u); }
    }
  }

  GIVEN("A 32bit unsigned integer equal to int max") {
    const auto input = std::numeric_limits<uint32_t>::max();
    WHEN("it is rounded up at runtime") {
      const auto result = gneiss::next_pow2(input);

      THEN("the result is 0") { CHECK(result == 0u); }
    }

    WHEN("it is rounded up at compile time") {
      const auto result = gneiss::next_pow2(input);

      THEN("the result is 0 ") { STATIC_REQUIRE(result == 0u); }
    }
  }
}
