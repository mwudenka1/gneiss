#include "gneiss/utils/NamedTuple.h"

#include "gneiss/utils/Named.h"

#include <catch2/catch_test_macros.hpp>

#include <sstream>
#include <string>

SCENARIO("Objects can be named and stored in NamedTuple", "[utils]") {
  GIVEN("A NamedTuple") {
    auto testNamedTuple = gneiss::make_named_tuple(gneiss::make_named<"first">(5.f), gneiss::make_named<"second">(6));

    WHEN("an element is queried") {
      const auto second = testNamedTuple.get<"second">();
      THEN("the correct value is returned") { CHECK(second == 6); }
    }

    WHEN("the value of an element is updated") {
      testNamedTuple.get<"first">() = 7.f;
      THEN("the new value is stored") { CHECK(testNamedTuple.get<"first">() == 7.f); }
    }
  }
}

SCENARIO("NamedTuple elements can be accessed at compile time", "[utils]") {
  GIVEN("A NamedTuple") {
    constexpr auto testNamedTuple =
        gneiss::make_named_tuple(gneiss::make_named<"first">(5.f), gneiss::make_named<"second">(6));

    WHEN("an element is queried") {
      const auto second = testNamedTuple.get<"second">();
      THEN("the correct value is returned") { STATIC_REQUIRE(second == 6); }
    }
  }
}

SCENARIO("NamedTuple can be printed", "[utils]") {
  GIVEN("A NamedTuple") {
    constexpr auto testNamedTuple =
        gneiss::make_named_tuple(gneiss::make_named<"first">(5.f), gneiss::make_named<"second">(6));

    WHEN("it is piped into a ostream") {
      std::stringstream stream;
      stream << testNamedTuple;
      THEN("the tuple is nicely formatted") { CHECK(stream.str() == std::string("(first: 5, second: 6)")); }
    }
  }
}
