#include "gneiss/utils/StaticVector.h"

#include "gneiss/utils/ranges.h"

#include <catch2/catch_test_macros.hpp>

#include <cstdint>
#include <iterator>
#include <type_traits>
#include <vector>

TEST_CASE("StaticVector is nothrow destructible", "[utils]") {
  STATIC_REQUIRE(std::is_nothrow_destructible_v<gneiss::StaticVector<float, 3>>);
}

SCENARIO("StaticVector can be filled with items", "[utils]") {
  GIVEN("An instance of StaticVector") {
    gneiss::StaticVector<uint8_t, 5> vector;

    WHEN("before values are added") {
      THEN("the vector is empty") { CHECK(vector.is_empty()); }
    }

    WHEN("Values are added") {
      vector.push_back(1);
      vector.push_back(2);
      vector.push_back(3);
      vector.push_back(4);
      vector.push_back(5);

      THEN("The size of the vector increases") { CHECK(vector.size() == 5); }

      AND_THEN("The values can be retrieved") {
        CHECK(vector.at(0) == 1);
        CHECK(vector.at(1) == 2);
        CHECK(vector.at(2) == 3);
        CHECK(vector.at(3) == 4);
        CHECK(vector.at(4) == 5);
      }
    }
  }
}

SCENARIO("StaticVector iterators can be traversed", "[utils]") {
  GIVEN("An instance of StaticVector with values") {
    gneiss::StaticVector<uint8_t, 5> vector;
    vector.push_back(1);
    vector.push_back(2);
    vector.push_back(3);
    vector.push_back(4);
    vector.push_back(5);

    WHEN("it is iterated over") {
      uint8_t expected_val = 1;
      for (const auto &val : vector) {
        AND_THEN("the correct value is represented by the iterator; iter: " << expected_val) {
          CHECK(val == expected_val);
        }
        ++expected_val;
      }
    }

    WHEN("it is iterated over as reversed range") {
      uint8_t expected_val = 5;
      for (const auto &val : vector | gneiss::ranges::views::reverse) {
        AND_THEN("the correct value is represented by the iterator; iter: " << expected_val) {
          CHECK(val == expected_val);
        }
        --expected_val;
      }
    }
  }
}

SCENARIO("Values can be assigned to StaticVector via iterators", "[utils]") {
  GIVEN("An instance of StaticVector") {
    gneiss::StaticVector<uint8_t, 5> vector;

    WHEN("Values are assigned using iterators") {
      std::vector<uint8_t> input_values{1, 2, 3, 4, 5};
      std::copy(input_values.begin(), input_values.end(), std::back_inserter(vector));

      THEN("The size of the vector is correct") { CHECK(vector.size() == input_values.size()); }

      AND_THEN("The values can be retrieved correctly") {
        CHECK(std::equal(vector.begin(), vector.end(), input_values.begin()));
      }
    }
  }
}

SCENARIO("Values can be erased from StaticVector", "[utils]") {
  GIVEN("An instance of StaticVector with some values") {
    gneiss::StaticVector<uint8_t, 5> vector;
    vector.push_back(1);
    vector.push_back(2);
    vector.push_back(3);
    vector.push_back(4);
    vector.push_back(5);

    WHEN("A value is erased") {
      vector.erase(std::next(vector.begin()));

      THEN("The size of the vector decreases") { CHECK(vector.size() == 4); }

      AND_THEN("The value is removed from the vector") {
        for (const auto &val : vector) {
          CHECK(val != 2);
        }
      }
    }
  }
}
