#include "gneiss/utils/Specializes.h"  // IWYU pragma: keep

#include <catch2/catch_test_macros.hpp>

#include <cstddef>
#include <cstdint>

namespace {
template <typename Test_>
struct StructTemplate {
  Test_ value;
};

template <typename Test_>
struct AnotherStructTemplate {
  Test_ value;
};

// Test that the concept can be used inline in a template statement
template <gneiss::SpecializesWithType<StructTemplate> StructTemplate_>
struct InlineConcept {
  StructTemplate_ value;
};

template <std::size_t n_>
struct StructValueTemplate {
  static constexpr std::size_t number = n_;
};

template <int64_t n_>
struct AnotherStructValueTemplate {
  static constexpr float number = n_;
};

// Test that the concept can be used inline in a template statement
template <gneiss::SpecializesWithValue<StructValueTemplate> StructValueTemplate_>
struct InlineValueConcept {
  StructValueTemplate_ value;
};
}  // namespace

TEST_CASE("The SpecializesWithType concept yields a true positive", "[utils]") {
  STATIC_REQUIRE(gneiss::SpecializesWithType<StructTemplate<int>, StructTemplate>);
}

TEST_CASE("The SpecializesWithType concept yields a true negative", "[utils]") {
  STATIC_REQUIRE(!gneiss::SpecializesWithType<StructTemplate<int>, AnotherStructTemplate>);
}

TEST_CASE("The SpecializesWithValue concept yields a true positive", "[utils]") {
  STATIC_REQUIRE(gneiss::SpecializesWithValue<StructValueTemplate<5>, StructValueTemplate>);
}

TEST_CASE("The SpecializesWithValue concept yields a true negative", "[utils]") {
  STATIC_REQUIRE(!gneiss::SpecializesWithValue<StructValueTemplate<5>, AnotherStructValueTemplate>);
}
