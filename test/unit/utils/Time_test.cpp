#include "gneiss/utils/Duration.h"
#include "gneiss/utils/Timestamp.h"

#include <catch2/catch_test_macros.hpp>

#include <chrono>
#include <type_traits>

using namespace std::literals::chrono_literals;

SCENARIO("Timestamps and Durations can be added and subtracted", "[utils]") {
  GIVEN("a timestamp") {
    // NOLINTNEXTLINE(misc-include-cleaner)
    const gneiss::Timestamp testTimestamp(3s);
    WHEN("a duration is added") {
      // NOLINTNEXTLINE(misc-include-cleaner)
      auto newTimestamp = testTimestamp + gneiss::Duration(500ms);
      THEN("the resulting type is again a Timestamp") {
        STATIC_REQUIRE(std::is_same_v<gneiss::Timestamp, decltype(newTimestamp)>);
      }
    }

    WHEN("it is subtracted from another Timestamp") {
      // NOLINTNEXTLINE(misc-include-cleaner)
      auto newDuration = gneiss::Timestamp(10h) - testTimestamp;
      THEN("the resulting type is again a Timestamp") {
        STATIC_REQUIRE(std::is_same_v<gneiss::Duration, decltype(newDuration)>);
      }
    }
  }
}