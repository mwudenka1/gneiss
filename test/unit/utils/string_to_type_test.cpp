#include "gneiss/utils/string_to_type.h"

#include <catch2/catch_test_macros.hpp>

#include <type_traits>

TEST_CASE("string_to_type correctly resolves types", "[utils]") {
  STATIC_REQUIRE(std::is_same_v<float, gneiss::string_to_type_t<"float">>);
  STATIC_REQUIRE(std::is_same_v<double, gneiss::string_to_type_t<"double">>);
}
