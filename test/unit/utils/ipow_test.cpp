#include "gneiss/utils/ipow.h"

#include "gneiss/utils/constexpr_enumerate.h"
#include "gneiss/utils/ranges.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstdint>

namespace {
template <typename Type_, typename Power_>
struct PowData {
  Type_ base;
  Power_ exponent;
  Type_ expected;
};
}  // namespace

SCENARIO("the integer power can be computed from 64bit integers", "[utils]") {
  GIVEN("A 64bit integer base and an unsigned integer exponent") {
    constexpr static std::array<PowData<int64_t, uint32_t>, 5> data = {
        {{2, 3, 8}, {-2, 3, -8}, {-2, 4, 16}, {5, 10, 9765625}, {34938, 2, 1220663844}}};

    const static auto idxs = gneiss::ranges::views::iota(0ul, data.size());
    gneiss::ranges::for_each(gneiss::ranges::views::zip(idxs, data), [](const auto& zipped) {
      const auto& [idx, datum] = zipped;
      WHEN("the integer power is computed at runtime, input #: " << idx) {
        const auto result = gneiss::ipow(datum.base, datum.exponent);
        THEN("it equals the expected value") { CHECK(result == datum.expected); }
      }
    });

    gneiss::constexpr_enumerate<data>([&](auto idx, auto datumConstant) {
      constexpr const typename decltype(datumConstant)::value_type datum = datumConstant;
      WHEN("the integer power is computed at compile time, input #:" << idx) {
        const auto result = gneiss::ipow(datum.base, datum.exponent);
        THEN("it equals the expected value") { STATIC_REQUIRE(result == datum.expected); }
      }
    });
  }
}