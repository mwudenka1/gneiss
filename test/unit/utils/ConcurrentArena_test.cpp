#include "gneiss/utils/ConcurrentArena.h"

#include <catch2/catch_test_macros.hpp>

#include <cstddef>
#include <memory>
#include <ostream>
#include <thread>
#include <type_traits>
#include <utility>

struct NonTrivialType {
 public:
  explicit constexpr NonTrivialType(float initialValue) noexcept : value(initialValue) {}

  auto operator<=>(const NonTrivialType&) const = default;

  friend std::ostream& operator<<(std::ostream& outStream, const NonTrivialType& self) {
    outStream << "NonTrivialType{" << self.value << '}';
    return outStream;
  }

  [[nodiscard]] constexpr float getValue() const noexcept { return value; }

 private:
  float value;
};

static_assert(!std::is_trivially_default_constructible_v<NonTrivialType>);

// TEST_CASE("UniqueArenaPtr is movable", "[utils]") {
//     STATIC_REQUIRE(std::is_move_assignable_v<gneiss::UniqueArenaPtr<double>>);
//     STATIC_REQUIRE(std::is_move_constructible_v<gneiss::UniqueArenaPtr<double>>);
//     STATIC_REQUIRE(std::is_move_assignable_v<gneiss::UniqueArenaPtr<NonTrivialType>>);
//     STATIC_REQUIRE(std::is_move_constructible_v<gneiss::UniqueArenaPtr<NonTrivialType>>);
// }

SCENARIO("A value can be stored inside an ConcurrentArena and retrieved", "[utils]") {
  GIVEN("a ConcurrentArena object for non-const trivial objects") {
    auto testArena = gneiss::ConcurrentArena<float, 5>();

    WHEN("a value is stored") {
      auto testItem = testArena.emplace(5.f);

      THEN("it can be retrieved again") { CHECK(*testItem == 5.f); }
    }
  }

  GIVEN("a ConcurrentArena object for non-const non-trivial objects") {
    auto testArena = gneiss::ConcurrentArena<NonTrivialType, 5>();

    WHEN("a value is stored") {
      auto testItem = testArena.emplace(NonTrivialType{5674.56f});

      THEN("it can be retrieved again") { CHECK(testItem->getValue() == 5674.56f); }
    }
  }
}

SCENARIO("An UniqueArenaPtr frees its space after going out of scope", "[utils]") {
  GIVEN("an ConcurrentArena object of capacity 1") {
    auto testArena = gneiss::ConcurrentArena<std::size_t, 1>();

    WHEN("a value is stored and the UniqueArenaPtr goes out of scope") {
      { auto testItem = testArena.emplace(5457345); }

      THEN("again a value can be stores") { auto testItem = testArena.emplace(3637390389); }
    }
  }
}

SCENARIO("A ConcurrentArena waits until there is enough space to emplace a new element", "[utils]") {
  GIVEN("a full ConcurrentArena object of capacity 1") {
    auto testArena = gneiss::ConcurrentArena<size_t, 1>();
    auto firstItem = testArena.emplace(5457345);
    std::unique_ptr<size_t, gneiss::ArenaPtrDeleter<size_t>> secondItem = nullptr;

    WHEN("another value is enqueued to be inserted and the first one is destroyed") {
      auto emplaceThread = std::thread([&]() noexcept { secondItem = testArena.emplace(89352); });

      THEN("the first item is in the arena") { CHECK(*firstItem == 5457345); }

      firstItem.reset();
      emplaceThread.join();

      THEN("the second item is in the arena") { CHECK(*secondItem == 89352); }
    }
  }
}

SCENARIO("A ConcurrentArena can be shutdown gracefully", "[utils]") {
  GIVEN("a full ConcurrentArena object of capacity 1") {
    auto testArena = gneiss::ConcurrentArena<std::size_t, 1>();
    auto firstItem = testArena.emplace(5457345);
    std::unique_ptr<std::size_t, gneiss::ArenaPtrDeleter<std::size_t>>* secondItemPtr = nullptr;

    WHEN("another value is enqueued to be inserted and the arena is asked to shutdown") {
      auto emplaceThread = std::thread([&]() {
        auto secondItem = testArena.emplace(89352);
        secondItemPtr = new std::unique_ptr<std::size_t, gneiss::ArenaPtrDeleter<std::size_t>>(std::move(secondItem));
      });

      testArena.shutdown();
      THEN("no deadlock happens") { emplaceThread.join(); }
      delete secondItemPtr;
    }
  }
}
