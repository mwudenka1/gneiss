#include "gneiss/utils/division_power_2.h"

#include "gneiss/utils/constexpr_enumerate.h"
#include "gneiss/utils/ranges.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstdint>

namespace {
template <typename Type_, typename Power_>
struct DivisionData {
  Type_ dividend;
  Power_ power;
  Type_ expected;
};
}  // namespace

SCENARIO("integers can by divided by a power of 2", "[utils]") {
  GIVEN("A integer base and an unsigned integer exponent") {
    constexpr static std::array<DivisionData<int32_t, uint32_t>, 5> data = {
        {{2, 1, 1}, {-2, 1, -1}, {0, 5, 0}, {513, 7, 4}, {65964, 8, 258}}};

    const static auto idxs = gneiss::ranges::views::iota(0ul, data.size());
    gneiss::ranges::for_each(gneiss::ranges::views::zip(idxs, data), [](const auto& zipped) {
      const auto& [idx, datum] = zipped;
      WHEN("the division is computed at runtime, input #: " << idx) {
        const auto result = gneiss::division_power_2(datum.dividend, datum.power);
        THEN("it equals the expected value") { CHECK(result == datum.expected); }
      }
    });

    gneiss::constexpr_enumerate<data>([&](auto idx, auto datumConstant) {
      constexpr const typename decltype(datumConstant)::value_type datum = datumConstant;
      WHEN("the division is computed at compile time, input #:" << idx) {
        const auto result = gneiss::division_power_2(datum.dividend, datum.power);
        THEN("it equals the expected value") { STATIC_REQUIRE(result == datum.expected); }
      }
    });
  }
}