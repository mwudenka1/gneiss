#include "gneiss/utils/ConditionalTuple.h"

#include <catch2/catch_test_macros.hpp>

#include <tuple>
#include <type_traits>

TEST_CASE("ConditionalTuple returns the correct type when all conditions are true", "[utils]") {
  STATIC_REQUIRE(
      std::is_same_v<gneiss::ConditionalTuple<gneiss::Typelist<int, float>, gneiss::Boollist<true, true>>::type,
                     std::tuple<int, float>>);
}

TEST_CASE("ConditionalTuple returns the correct type when all conditions are false", "[utils]") {
  STATIC_REQUIRE(
      std::is_same_v<gneiss::ConditionalTuple<gneiss::Typelist<int, float>, gneiss::Boollist<false, false>>::type,
                     void>);
}

TEST_CASE("ConditionalTuple returns the correct type when one condition is true", "[utils]") {
  STATIC_REQUIRE(
      std::is_same_v<
          gneiss::ConditionalTuple<gneiss::Typelist<int, float, double>, gneiss::Boollist<false, true, false>>::type,
          float>);
}

TEST_CASE("ConditionalTuple returns the correct type when one condition is false", "[utils]") {
  STATIC_REQUIRE(
      std::is_same_v<
          gneiss::ConditionalTuple<gneiss::Typelist<int, float, double>, gneiss::Boollist<true, true, false>>::type,
          std::tuple<int, float>>);
}

TEST_CASE("ConditionalTuple returns the correct type when no type is given", "[utils]") {
  STATIC_REQUIRE(std::is_same_v<gneiss::ConditionalTuple<gneiss::Typelist<>, gneiss::Boollist<>>::type, void>);
}

TEST_CASE("ConditionalTuple returns the correct type when one type is given", "[utils]") {
  STATIC_REQUIRE(std::is_same_v<gneiss::ConditionalTuple<gneiss::Typelist<int>, gneiss::Boollist<true>>::type, int>);
}
