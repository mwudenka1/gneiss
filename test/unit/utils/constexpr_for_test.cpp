#include "gneiss/utils/constexpr_for.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstdint>

SCENARIO("constexpr_for can be used to unroll a loop at compile time", "[utils]") {
  GIVEN("a number to count until ('length')") {
    const uint32_t length = 5;

    WHEN("constexpr_for is used to iterate from 0 to 'length'") {
      constexpr std::array<int32_t, length> idxList = {0, 1, 2, 3, 4};

      gneiss::constexpr_for<0, length, 1>([&idxList](auto idx) {
        THEN("the loop is unrolled at compile time #" << idx) { STATIC_REQUIRE(idx == idxList.at(idx)); }
      });
    }
  }
}
