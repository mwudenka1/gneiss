#include "gneiss/utils/transform_to_array.h"

#include "gneiss/utils/constexpr_enumerate.h"
#include "gneiss/utils/ranges.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstdint>

namespace {
struct AddFunctor {
  constexpr inline int32_t operator()(const int32_t value) const { return value + offset; }

  // NOLINTNEXTLINE(misc-non-private-member-variables-in-classes)
  int32_t offset;
};
}  // namespace

SCENARIO("A predicate can be applied to a source array and the result returned in a new array", "[utils]") {
  GIVEN("A predicate and a array") {
    constexpr const static std::array<int32_t, 5> source = {-74628, 0, 4546, 938, 342};
    constexpr const static AddFunctor adder{4};

    WHEN("the data is transformed by the predicate at compile time") {
      constexpr auto results = gneiss::transform_to_array(source, adder);

      THEN("the predicate was correctly applied") {
        gneiss::constexpr_enumerate<results>([&](auto idx, auto resultConstant) {
          constexpr const typename decltype(resultConstant)::value_type result = resultConstant;
          STATIC_REQUIRE(result == adder(source.at(idx)));
        });
      }
    }

    WHEN("the data is transformed by the predicate at runtime") {
      const auto results = gneiss::transform_to_array(source, adder);

      THEN("the predicate was correctly applied") {
        for (const auto& [result, inValue] : gneiss::ranges::views::zip(results, source)) {
          CHECK(result == adder(inValue));
        }
      }
    }
  }
}