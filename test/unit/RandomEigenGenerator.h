#ifndef GNEISS_RANDOMEIGENGENERATOR_H
#define GNEISS_RANDOMEIGENGENERATOR_H
#pragma once
#include <Eigen/Core>
#include <catch2/generators/catch_generators.hpp>
#include <catch2/generators/catch_generators_adapters.hpp>

#include <random>

template <typename EigenType_, typename Dist_ = std::uniform_real_distribution<typename EigenType_::Scalar>>
class RandomEigenGenerator final : public Catch::Generators::IGenerator<EigenType_> {
 public:
  explicit RandomEigenGenerator(Dist_&& dist) : m_distr{std::forward<Dist_>(dist)} { fillRandom(); }

  bool next() override {
    fillRandom();
    return true;
  }

  const EigenType_& get() const override { return m_value; }

  void fillRandom() {
    for (auto& elem : m_value.reshaped()) {
      elem = m_distr(m_randGen);
    }
  }

 private:
  EigenType_ m_value;
  std::default_random_engine m_randGen{};  // NOLINT(cert-msc32-c,cert-msc51-cpp)
  Dist_ m_distr;
};

template <typename EigenType_, typename Dist_ = std::uniform_real_distribution<typename EigenType_::Scalar>>
Catch::Generators::GeneratorWrapper<EigenType_> randomEigen(Dist_&& dist) {
  return Catch::Generators::GeneratorWrapper<EigenType_>(
      new RandomEigenGenerator<EigenType_, Dist_>(std::forward<Dist_>(dist)));
}

#endif  // GNEISS_RANDOMEIGENGENERATOR_H
