#include "gneiss/image/FastScoreView.h"

#include "gneiss/image/Image.h"

#include <catch2/catch_test_macros.hpp>

#include <cstdint>

SCENARIO("The FAST score can be computed for images", "[image]") {
  GIVEN("a small toy image") {
    static constexpr uint8_t bright = 120;
    // clang-format off
    static constexpr gneiss::Image<uint8_t, 7, 7> image({
      0,0,0,0,bright,bright,bright,
      0,0,0,0,bright,bright,bright,
      0,0,0,0,bright,bright,bright,
      0,0,0,0,bright,bright,bright,
      bright,bright,bright,bright,bright,bright,bright,
      bright,bright,bright,bright,bright,bright,bright,
      bright,bright,bright,bright,bright,bright,bright,
    });
    // clang-format on
    WHEN("the FAST score is calculated") {
      static constexpr auto fastScores = image | gneiss::view::FastScore;

      THEN("the score for the center pixel is 'bright'") { REQUIRE(fastScores({3, 3}) == bright); }
    }
  }
}
