#!/usr/bin/env python3
from argparse import ArgumentParser

import os, sys

import cv2


def main():
    argument_parser = ArgumentParser(
        description="Print the bits of an image")
    argument_parser.add_argument(
        "--image_path_name",
        type=str,
        help="Path and name of the image to be opened",
        required=True)

    args = argument_parser.parse_args()

    if not os.path.isfile(args.image_path_name):
        print(f"Error: {args.image_path_name} is not a file.")
        sys.exit(1)

    image = cv2.imread(args.image_path_name, cv2.IMREAD_GRAYSCALE)

    height = image.shape[0]
    width = image.shape[1]

    print('{')
    for y in range(0, height):
        #print('{', end='')
        for x in range(0, width):
            print(image[y, x], end='')
            if x != width - 1: print(',', end='')
        #print('}')
        if y != height - 1: print(',', end='')
    print('}')


if __name__ == "__main__":
    main()
