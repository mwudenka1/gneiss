import numpy as np

kernel = np.array([[1, 4, 6, 4, 1],
                   [4, 16, 24, 16, 4],
                   [6, 24, 36, 24, 6],
                   [4, 16, 24, 16, 4],
                   [1, 4, 6, 4, 1]])

image = np.arange(50).reshape(5, 10)
padded_image = np.pad(image, (2, 2), 'reflect')

for y in np.arange(int(image.shape[0] / 2)):
    for x in np.arange(int(image.shape[1] / 2)):
        new_val = 0
        for kernel_y in np.arange(5):
            for kernel_x in np.arange(5):
                new_val += padded_image[2 * y + 1 + kernel_y, 2 * x + 1 + kernel_x] * kernel[kernel_y, kernel_x]

        new_val /= 256
        print(int(np.round(new_val)), end=", ")

    print("")
