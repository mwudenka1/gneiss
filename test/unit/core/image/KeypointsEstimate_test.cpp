#include "gneiss/image/KeypointsEstimate.h"

#include "gneiss/image/KeypointDescriptorAtLevel.h"

#include <Eigen/Dense>  // IWYU pragma: keep
#include <catch2/catch_test_macros.hpp>

SCENARIO("Keypoints of KeypointsEstimate can be assigned and read from", "[optical-flow]") {
  GIVEN("an KeypointsEstimate") {
    auto testEstimate = gneiss::KeypointsEstimate();
    using Scalar = gneiss::KeypointsEstimate::Scalar;
    const auto testKeypointDescriptorAtLevel = gneiss::KeypointDescriptorAtLevel<Scalar>{
        .keypoint =
            {
                // NOLINTNEXTLINE(misc-include-cleaner)
                .transform = Eigen::AffineCompact2d::Identity().cast<Scalar>(),
                .id = 5,
            },
        .level = 0};

    WHEN("data is written to idx 0") {
      testEstimate.get<0>().push_back(testKeypointDescriptorAtLevel);

      THEN("it can be read again") { CHECK(testEstimate.get<0>().at(0).keypoint.id == 5); }
    }
  }
}