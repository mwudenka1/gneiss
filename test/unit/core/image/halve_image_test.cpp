#include "gneiss/image/halve_image.h"

#include "core/image/data/landscape_gray.h"
#include "core/image/data/landscape_gray_halve.h"
#include "gneiss/image/Image.h"
#include "gneiss/utils/constexpr_enumerate.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstddef>
#include <cstdint>

SCENARIO("images can be subsampled in halve", "[image]") {
  GIVEN("a small toy image") {
    constexpr static gneiss::Image<uint8_t, 10, 5> image(
        {0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
         25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49});

    WHEN("the image is halved") {
      constexpr static auto halved_image = halve_image(image);
      THEN("the image dimensions are halved and the image is subsampled and smoothed") {
        STATIC_REQUIRE(halved_image.width == 5);
        STATIC_REQUIRE(halved_image.height == 2);

        constexpr static std::array<uint8_t, 10> expectedData = {12, 14, 16, 18, 20, 30, 32, 34, 36, 37};

        gneiss::constexpr_enumerate<expectedData>([&](auto idx, auto expectedConstant) {
          constexpr typename decltype(expectedConstant)::value_type expected = expectedConstant;
          STATIC_REQUIRE(halved_image(idx) == expected);
        });
      }
    }
  }

  GIVEN("a real image with even dimensions") {
    const gneiss::Image<uint8_t, 128, 96> image(landscape_gray);
    WHEN("the image is halved") {
      const auto halved_image = halve_image(image);
      THEN("the image dimensions are halved and the image is subsampled and smoothed") {
        STATIC_REQUIRE(halved_image.width == 64);
        STATIC_REQUIRE(halved_image.height == 48);

        for (std::size_t idx = 0; idx < 64ul * 48ul; idx++) {
          CHECK(halved_image(idx) == landscape_gray_halve.at(idx));
        }
      }
    }
  }
}
