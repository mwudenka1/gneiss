#include "gneiss/image/ImageMeasurement.h"

#include <catch2/catch_test_macros.hpp>

SCENARIO("Images of ImageMeasurement can be assigned and read from", "[image]") {
  GIVEN("an ImageMeasurement") {
    auto testMeasurement = gneiss::ImageMeasurement();

    WHEN("data is written to image 0") {
      testMeasurement.get<0>()(4, 8) = 64;

      THEN("it can be read again") { CHECK(testMeasurement.get<0>()(4, 8) == 64); }
    }
  }
}
