#include "gneiss/image/Image.h"

#include "gneiss/fields-2d/FieldCoordinate.h"

#include <Eigen/Dense>  // IWYU pragma: keep
#include <catch2/catch_message.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>

#include <cstdint>

SCENARIO("Instantiate an image and access individual pixels", "[image]") {
  GIVEN("An image object") {
    gneiss::Image<uint8_t, 200, 200> image{};

    WHEN("a pixel is assigned a value") {
      const uint8_t value = 10;
      image(5, 2) = value;
      THEN("the pixel value can be retrieved later on") { CHECK(image(5, 2) == value); }
    }
  }

  GIVEN("A constant expression image object") {
    constexpr gneiss::Image<uint8_t, 4, 2> image({0, 1, 2, 3, 10, 11, 12, 13});

    WHEN("at compile time") {
      THEN("the pixel value can be retrieved") { STATIC_REQUIRE(image(2, 1) == 12); }
    }
  }
}

SCENARIO("Intensity values can be linear interpolated", "[image]") {
  GIVEN("An image object") {
    static constexpr gneiss::Image<uint8_t, 2, 2> image{{35, 17, 105, 64}};

    WHEN("an intensity value in between pixels is requested as double") {
      const auto optionalIntensity =
          image(gneiss::fields_2d::FieldCoordinate<double>{0.3, 0.6}, gneiss::interpolate_linear).value();

      THEN("it is correctly interpolated") {
        REQUIRE(optionalIntensity);
        const auto intensity = optionalIntensity.value();
        CHECK_THAT(intensity, Catch::Matchers::WithinULP(67.46, 1));
      }
    }

    WHEN("an intensity value in between pixels is requested as float") {
      const auto optionalIntensity =
          image(gneiss::fields_2d::FieldCoordinate<float>{0.3f, 0.6f}, gneiss::interpolate_linear).value();

      THEN("it is correctly interpolated") {
        REQUIRE(optionalIntensity);
        const auto intensity = optionalIntensity.value();
        CHECK_THAT(intensity, Catch::Matchers::WithinULP(67.46f, 1));
      }
    }

    WHEN("an intensity value on the upper bounds is requested") {
      const auto optionalIntensity =
          image(gneiss::fields_2d::FieldCoordinate<double>{1.0, 0.7}, gneiss::interpolate_linear).value();

      THEN("it is correctly interpolated") {
        REQUIRE(optionalIntensity);
        const auto intensity = optionalIntensity.value();
        CHECK_THAT(intensity, Catch::Matchers::WithinULP(49.9, 1));
      }
    }

    WHEN("an intensity value on the lower bounds is requested") {
      const auto optionalIntensity =
          image(gneiss::fields_2d::FieldCoordinate<double>{0.56, 0.0}, gneiss::interpolate_linear).value();

      THEN("it is correctly interpolated") {
        REQUIRE(optionalIntensity);
        const auto intensity = optionalIntensity.value();
        CHECK_THAT(intensity, Catch::Matchers::WithinULP(24.92, 1));
      }
    }
  }
}

SCENARIO("The gradient of interpolated intensity values can be obtained", "[image]") {
  GIVEN("An image object") {
    static constexpr gneiss::Image<uint8_t, 4, 4> image{{
        // clang-format off
        129, 245,  26,  25,
        202,  80, 213, 128,
        198, 253, 118,  95,
         80, 150,  10,  27
        // clang-format on
    }};

    WHEN("the gradient is taken") {
      const auto optionalGradient =
          image(gneiss::fields_2d::FieldCoordinate<float>{1.039f, 1.826f}, gneiss::interpolate_linear).gradient();
      THEN("the optional has a value") { REQUIRE(optionalGradient.has_value()); }
      // NOLINTNEXTLINE(misc-include-cleaner)
      const Eigen::Vector2f should(-88.368f, 162.548f);
      INFO("should:\n" << should << "\n\nis:\n" << optionalGradient.value());
      THEN("the gradient is correct") { CHECK(optionalGradient.value().isApprox(should)); }
    }

    WHEN("the gradient on the upper bounds is requested") {
      const auto optionalGradient =
          image(gneiss::fields_2d::FieldCoordinate<float>{1.87f, 3.f}, gneiss::interpolate_linear).gradient();
      THEN("the optional has no value") { REQUIRE(!optionalGradient.has_value()); }
    }

    WHEN("the gradient on the lower bounds is requested") {
      const auto optionalGradient =
          image(gneiss::fields_2d::FieldCoordinate<float>{0.f, 1.4583f}, gneiss::interpolate_linear).gradient();
      THEN("the optional has a value") { REQUIRE(optionalGradient.has_value()); }
    }

    WHEN("the smoothed gradient is taken") {
      const auto optionalGradient =
          image(gneiss::fields_2d::FieldCoordinate<float>{1.039f, 1.826f}, gneiss::interpolate_linear)
              .smoothed_gradient();
      THEN("the optional has a value") { REQUIRE(optionalGradient.has_value()); }
      // NOLINTNEXTLINE(misc-include-cleaner)
      const Eigen::Vector2f should(-33.2138f, 25.4938f);
      INFO("should:\n" << should << "\n\nis:\n" << optionalGradient.value());
      THEN("the gradient is correct") { CHECK(optionalGradient.value().isApprox(should)); }
    }

    WHEN("the smoothed gradient on the upper bounds is requested") {
      const auto optionalGradient =
          image(gneiss::fields_2d::FieldCoordinate<float>{2.f, 2.f}, gneiss::interpolate_linear).smoothed_gradient();
      THEN("the optional has a value") { REQUIRE(optionalGradient.has_value()); }
    }

    WHEN("the smoothed gradient above the upper bounds is requested") {
      const auto optionalGradient =
          image(gneiss::fields_2d::FieldCoordinate<float>{2.001f, 2.f}, gneiss::interpolate_linear).smoothed_gradient();
      THEN("the optional has no value") { CHECK(!optionalGradient.has_value()); }
    }

    WHEN("the smoothed gradient on the lower bounds is requested") {
      const auto optionalGradient =
          image(gneiss::fields_2d::FieldCoordinate<float>{1.f, 1.f}, gneiss::interpolate_linear).smoothed_gradient();
      THEN("the optional has a value") { REQUIRE(optionalGradient.has_value()); }
    }

    WHEN("the smoothed gradient below the lower bounds is requested") {
      const auto optionalGradient =
          image(gneiss::fields_2d::FieldCoordinate<float>{1.f, 0.99f}, gneiss::interpolate_linear).smoothed_gradient();
      THEN("the optional has no value") { CHECK(!optionalGradient.has_value()); }
    }
  }
}
