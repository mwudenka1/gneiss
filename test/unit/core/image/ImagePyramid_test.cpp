#include "gneiss/image/ImagePyramid.h"

#include "gneiss/fields-2d/FieldCoordinate.h"
#include "gneiss/image/Image.h"
#include "gneiss/utils/constexpr_enumerate.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstddef>
#include <cstdint>

SCENARIO("Instantiate an image pyramid with the right dimensions", "[image]") {
  GIVEN("An image pyramid object with base dimensions of a power of 2") {
    constexpr gneiss::ImagePyramid<4, uint8_t, 512, 256> image_pyramid;

    WHEN("the height and width of the levels are computed") {
      THEN("every level is exactly half the level below") {
        STATIC_REQUIRE(image_pyramid.get_level<0>().height == 256);
        STATIC_REQUIRE(image_pyramid.get_level<1>().height == 128);
        STATIC_REQUIRE(image_pyramid.get_level<2>().height == 64);
        STATIC_REQUIRE(image_pyramid.get_level<3>().height == 32);

        STATIC_REQUIRE(image_pyramid.get_level<0>().width == 512);
        STATIC_REQUIRE(image_pyramid.get_level<1>().width == 256);
        STATIC_REQUIRE(image_pyramid.get_level<2>().width == 128);
        STATIC_REQUIRE(image_pyramid.get_level<3>().width == 64);
      }
    }
  }

  GIVEN("An image pyramid object with uneven dimensions") {
    constexpr gneiss::ImagePyramid<4, uint8_t, 255, 87> image_pyramid{};

    WHEN("the height and width of the levels are computed") {
      THEN("every level is half the level below and rounded down") {
        STATIC_REQUIRE(image_pyramid.get_level<0>().height == 87);
        STATIC_REQUIRE(image_pyramid.get_level<1>().height == 43);
        STATIC_REQUIRE(image_pyramid.get_level<2>().height == 21);
        STATIC_REQUIRE(image_pyramid.get_level<3>().height == 10);

        STATIC_REQUIRE(image_pyramid.get_level<0>().width == 255);
        STATIC_REQUIRE(image_pyramid.get_level<1>().width == 127);
        STATIC_REQUIRE(image_pyramid.get_level<2>().width == 63);
        STATIC_REQUIRE(image_pyramid.get_level<3>().width == 31);
      }
    }
  }
}

SCENARIO("An image pyramid can be instantiated given a base image", "[image]") {
  GIVEN("A dummy image") {
    // constexpr gneiss::Image<uint8_t, 10, 5> image(gneiss::ranges::views::iota(0u, 50u));
    constexpr gneiss::Image<uint8_t, 10, 5> image({0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15, 16,
                                                   17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33,
                                                   34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49});

    WHEN("the height and width of the levels are computed") {
      constexpr gneiss::ImagePyramid<2, uint8_t, 10, 5> image_pyramid(image);

      THEN("every level is exactly half the level below") {
        REQUIRE(image_pyramid.get_level<0>().height == 5);
        REQUIRE(image_pyramid.get_level<1>().height == 2);

        REQUIRE(image_pyramid.get_level<0>().width == 10);
        REQUIRE(image_pyramid.get_level<1>().width == 5);
      }

      THEN("the second level is correctly subsampled") {
        constexpr static std::array<uint8_t, 10> expected = {12, 14, 16, 18, 20, 30, 32, 34, 36, 37};

        gneiss::constexpr_enumerate<expected>([&](auto idx, auto datumConstant) {
          constexpr const typename decltype(datumConstant)::value_type datum = datumConstant;
          STATIC_REQUIRE(image_pyramid.get_level<1>()(idx) == datum);
        });
      }
    }
  }
}

struct CoordinateLevelPair {
  gneiss::fields_2d::FieldCoordinate<std::size_t> coordinateInPyramidLevel;
  std::size_t level = 0;
  gneiss::fields_2d::FieldCoordinate<std::size_t> coordinateInBaseImage;
};

SCENARIO("Coordinates of a pyramid level can be converted to coordinates of the base image", "[image]") {
  GIVEN("coordinates in a pyramid level") {
    constexpr static std::array<CoordinateLevelPair, 4> data = {
        {{{0, 0}, 0, {0, 0}}, {{2, 4}, 0, {2, 4}}, {{0, 0}, 1, {0, 0}}, {{6, 8}, 2, {24, 32}}}};

    gneiss::constexpr_enumerate<data>([&](auto idx, auto datumConstant) {
      constexpr const typename decltype(datumConstant)::value_type datum = datumConstant;
      WHEN("the coordinate of the base image is calculated, input #:" << idx) {
        constexpr auto result = gneiss::coordinate_in_base_image(datum.coordinateInPyramidLevel, datum.level);
        THEN("it equals the expected value") { STATIC_REQUIRE(result == datum.coordinateInBaseImage); }
      }
    });
  }
}
