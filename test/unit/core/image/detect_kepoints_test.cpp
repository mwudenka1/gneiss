#include "gneiss/image/detect_keypoints.h"

#include "gneiss/image/Image.h"
#include "gneiss/image/ImagePyramid.h"

#include <Eigen/Dense>  // IWYU pragma: keep
#include <catch2/catch_message.hpp>
#include <catch2/catch_test_macros.hpp>

#include <cstddef>
#include <cstdint>

SCENARIO("Evenly spaced keypoints can be detected on an image", "[image]") {
  GIVEN("a small image pyramid") {
    constexpr static auto image = gneiss::Image<uint8_t, 20, 20>({
        // clang-format off
        169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,
        169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,
        169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,
        169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,
        169,169, 65, 65, 65, 65, 65, 65, 65, 65,169,169,169,169,169,169,169,169,169,169,
        169,169, 65, 65, 65, 65, 65, 65, 65, 65,169,169,169,169,169,169,169,169,169,169,
        169,169, 65, 65, 65, 65, 65, 65, 65, 65,169,169,169,169,169,169,169,169,169,169,
        169,169, 65, 65, 65, 65, 65, 65, 65, 65,169,169,169,169,169,169,169,169,169,169,
        169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,
        169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,
        169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,
        169,169,169, 10, 10, 10, 10, 10, 10, 10, 10,169,169,169,169,169,169,169,169,169,
        169,169,169, 10, 10, 10, 10, 10, 10, 10, 10, 65, 65, 65, 65, 65, 65,169,169,169,
        169,169,169, 10, 10, 10, 10, 10, 10, 10, 10, 65, 65, 65, 65, 65, 65,169,169,169,
        169,169,169,169,169,169, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65,169,169,169,
        169,169,169,169,169,169, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65,169,169,169,
        169,169,169,169,169,169, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65,169,169,169,
        169,169,169,169,169,169, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65, 65,169,169,169,
        169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,
        169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169,169
        // clang-format on
    });

    constexpr auto imagePyramid = gneiss::ImagePyramid<2, uint8_t, 20, 20>(image);
    WHEN("keypoints are detected") {
      static constexpr std::size_t gridSize = 10;
      static constexpr std::size_t padding = 0;
      static constexpr std::size_t gridCellCapacity = 3;
      gneiss::KeypointPyramid<float,
                              gneiss::GridPyramidParameters{.baseImageWidth = decltype(imagePyramid)::width_0,
                                                            .baseImageHeight = decltype(imagePyramid)::height_0,
                                                            .gridSize = gridSize,
                                                            .padding = padding,
                                                            .gridCapacity = gridCellCapacity},
                              2>
          keypointPyramid;
      gneiss::detect_keypoints<float, gridSize, padding, gridCellCapacity, decltype(imagePyramid), 2>(imagePyramid,
                                                                                                      keypointPyramid);

      THEN("the returned size is correct") {
        CHECK(keypointPyramid.get_level<0>().height == 2);
        CHECK(keypointPyramid.get_level<0>().width == 2);
        CHECK(keypointPyramid.get_level<1>().height == 1);
        CHECK(keypointPyramid.get_level<1>().width == 1);
      }
      THEN("the best keypoint of the top left block is correct") {
        // NOLINTNEXTLINE(misc-include-cleaner)
        Eigen::AffineCompact2f should(Eigen::AffineCompact2f::TranslationType(3.f, 4.f));
        const auto actual = keypointPyramid.get_level<0>()({0, 0}).at(0).transform;

        INFO("should:\n" << should.matrix() << "\nactual:\n" << actual.matrix() << "\n");
        CHECK(actual.isApprox(should));
      }
    }
  }
}
