#include "gneiss/optical-flow/solve_optical_flow.h"

#include "gneiss/image/Image.h"
#include "gneiss/image/ImagePyramid.h"
#include "gneiss/optical-flow/OpticalFlowProblem.h"
#include "gneiss/optical-flow/PatchPatterns.h"

#include <Eigen/Dense>     // IWYU pragma: keep
#include <Eigen/Geometry>  // IWYU pragma: keep
#include <catch2/catch_message.hpp>
#include <catch2/catch_test_macros.hpp>

#include <cstdint>

SCENARIO("A toy optical flow problem can be solved by a Kanade-Lucas-Tomasi (KLT) tracker", "[optical-flow]") {
  GIVEN("A toy optical flow problem") {
    static constexpr gneiss::Image<uint8_t, 15, 15> fromImage{{
        // clang-format off
         12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,
         12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,
         12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,
         12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,
         12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,
         12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,
         70,  70,  70,  70,  70,  70,  70,  70,  12,  12,  12,  12,  12,  12,  12,
        164, 164, 164, 164, 164, 164, 164,  70,  12,  12,  12,  12,  12,  12,  12,
        164, 164, 164, 164, 164, 164, 164,  70,  12,  12,  12,  12,  12,  12,  12,
        164, 164, 164, 164, 164, 164, 164,  70,  12,  12,  12,  12,  12,  12,  12,
        164, 164, 164, 164, 164, 164, 164,  70,  12,  12,  12,  12,  12,  12,  12,
        164, 164, 164, 164, 164, 164, 164,  70,  12,  12,  12,  12,  12,  12,  12,
        164, 164, 164, 164, 164, 164, 164,  70,  12,  12,  12,  12,  12,  12,  12,
        164, 164, 164, 164, 164, 164, 164,  70,  12,  12,  12,  12,  12,  12,  12,
        164, 164, 164, 164, 164, 164, 164,  70,  12,  12,  12,  12,  12,  12,  12
        // clang-format on
    }};
    static constexpr auto fromPyramid = gneiss::make_image_pyramid<1>(fromImage);
    static constexpr gneiss::Image<uint8_t, 15, 15> toImage{{
        // clang-format off
         12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,
         12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,
         12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,
         12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,
         12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,
         12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,
         12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,  12,
         70,  70,  70,  70,  70,  70,  70,  12,  12,  12,  12,  12,  12,  12,  12,
        164, 164, 164, 164, 164, 164,  70,  12,  12,  12,  12,  12,  12,  12,  12,
        164, 164, 164, 164, 164, 164,  70,  12,  12,  12,  12,  12,  12,  12,  12,
        164, 164, 164, 164, 164, 164,  70,  12,  12,  12,  12,  12,  12,  12,  12,
        164, 164, 164, 164, 164, 164,  70,  12,  12,  12,  12,  12,  12,  12,  12,
        164, 164, 164, 164, 164, 164,  70,  12,  12,  12,  12,  12,  12,  12,  12,
        164, 164, 164, 164, 164, 164,  70,  12,  12,  12,  12,  12,  12,  12,  12,
        164, 164, 164, 164, 164, 164,  70,  12,  12,  12,  12,  12,  12,  12,  12
        // clang-format on
    }};
    static constexpr auto toPyramid = gneiss::make_image_pyramid<1>(toImage);
    const auto problem =
        gneiss::OpticalFlowProblem<float, decltype(fromPyramid)>{.fromPyramid = fromPyramid,
                                                                 .toPyramid = toPyramid,
                                                                 // NOLINTNEXTLINE(misc-include-cleaner)
                                                                 .fromPosition = Eigen::Vector2f(6.f, 7.f)};

    WHEN("the problem is solved by the KLT tracker") {
      const auto optionalTransform =
          gneiss::solve_optical_flow_klt<float, decltype(fromPyramid), gneiss::Pattern24>(problem, 7);

      THEN("the transform is correctly estimated") {
        REQUIRE(optionalTransform.has_value());

        // NOLINTNEXTLINE(misc-include-cleaner)
        const Eigen::AffineCompact2f shouldTransform(Eigen::AffineCompact2f::TranslationType(5.f, 8.f));
        INFO("should:\n" << shouldTransform.matrix() << "\nis:\n" << optionalTransform->matrix());
        CHECK(optionalTransform->isApprox(shouldTransform, 0.05f));
      }
    }

    WHEN("the problem is solved by the robust KLT tracker") {
      const auto optionalTransform =
          gneiss::solve_optical_flow_klt_robust<float, decltype(fromPyramid), gneiss::Pattern24>(problem, 7, 0.04f);

      THEN("the transform is correctly estimated") {
        REQUIRE(optionalTransform.has_value());

        // NOLINTNEXTLINE(misc-include-cleaner)
        const Eigen::AffineCompact2f shouldTransform(Eigen::AffineCompact2f::TranslationType(5.f, 8.f));
        INFO("should:\n" << shouldTransform.matrix() << "\nis:\n" << optionalTransform->matrix());
        CHECK(optionalTransform->isApprox(shouldTransform, 0.05f));
      }
    }
  }
}
