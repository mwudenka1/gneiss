#include "gneiss/camera/Pinhole.h"

#include "gneiss/camera/camera.h"  // IWYU pragma: keep

#include "RandomEigenGenerator.h"
#include "test_jacobian_numerically.h"

#include <Eigen/Dense>  // IWYU pragma: keep
#include <catch2/catch_message.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <catch2/generators/catch_generators_adapters.hpp>

#include <random>

TEST_CASE("Pinhole fulfills the camera concept", "[camera]") {
  STATIC_REQUIRE(gneiss::camera<gneiss::Pinhole<double>>);
}

SCENARIO("Pinhole camera only projects valid 3D points", "[camera]") {
  GIVEN("a camera and a valid 3D point") {
    // NOLINTNEXTLINE(misc-include-cleaner)
    const static auto camera = gneiss::Pinhole<double>(Eigen::Vector4d(200, 200, 256, 256));
    const auto validPoint =
        GENERATE(take(4, filter([](const auto& point) { return point.z() > 0.001; },
                                // NOLINTNEXTLINE(misc-include-cleaner)
                                randomEigen<Eigen::Vector3d>(std::uniform_real_distribution(-1000.0, 1000.0)))));
    WHEN("projecting it") {
      const auto projectedPoint = camera.project(validPoint);
      THEN("the projected point is valid") {
        INFO("projected 3D point " << validPoint);
        CHECK(projectedPoint.has_value());
      }
    }
  }

  GIVEN("A camera and an invalid 3D point") {
    // NOLINTNEXTLINE(misc-include-cleaner)
    const static auto camera = gneiss::Pinhole<double>(Eigen::Vector4d(200, 200, 256, 256));
    // NOLINTNEXTLINE(misc-include-cleaner)
    const auto invalidPoint =
        GENERATE(take(4, filter([](auto& point) { return point.z() < 0.; },
                                // NOLINTNEXTLINE(misc-include-cleaner)
                                randomEigen<Eigen::Vector3d>(std::uniform_real_distribution(-1000.0, 1000.0)))));
    WHEN("projecting it") {
      const auto projectedPoint = camera.project(invalidPoint);
      THEN("the projected point is invalid") {
        INFO("projected 3D point " << invalidPoint);
        CHECK(!projectedPoint.has_value());
      }
    }
  }
}

SCENARIO("Pinhole camera projects trivial cases correctly", "[camera]") {
  GIVEN("A camera and a valid 3D point") {
    // NOLINTNEXTLINE(misc-include-cleaner)
    const static auto camera = gneiss::Pinhole<double>(Eigen::Vector4d(200, 200, 256, 256));
    WHEN("projecting it") {
      const auto validPoint = Eigen::Vector3d(0, 0, 1);  // NOLINT(misc-include-cleaner)
      const auto projection = camera.project(validPoint);
      THEN("the projection is correct") {
        const auto should = Eigen::Vector2d(256, 256);  // NOLINT(misc-include-cleaner)
        REQUIRE(projection.has_value());
        INFO("projection:\n" << projection.value() << "\nshould:\n" << should);
        CHECK(projection.value().isApprox(should));
      }
    }
  }
}

SCENARIO("The jacobians of the Pinhole project method are numerically correct", "[camera]") {
  GIVEN("a camera and a valid 3D point") {
    // NOLINTNEXTLINE(misc-include-cleaner)
    const static auto camera = gneiss::Pinhole<double>(Eigen::Vector4d(200, 200, 256, 256));
    WHEN("projecting it") {
      // NOLINTNEXTLINE(misc-include-cleaner)
      const Eigen::Vector3d validPoint =
          GENERATE(take(4, filter([](const auto& i) { return i[2] > 0.001; },
                                  // NOLINTNEXTLINE(misc-include-cleaner)
                                  randomEigen<Eigen::Vector3d>(std::uniform_real_distribution(-1000.0, 1000.0)))));
      const auto projection = camera.project<true, true>(validPoint);
      THEN("the jacobian by point is correct") {
        REQUIRE(projection.has_value());
        test_jacobian_numerically(
            std::get<1>(projection.value()),
            // NOLINTNEXTLINE(misc-include-cleaner)
            [&](const Eigen::Vector3d& point) { return camera.project(point).value(); }, validPoint);
      }

      THEN("the jacobian by parameter is correct") {
        REQUIRE(projection.has_value());
        test_jacobian_numerically(
            std::get<2>(projection.value()),
            // NOLINTNEXTLINE(misc-include-cleaner)
            [&](const Eigen::Vector4d& params) {
              const auto tmpCamera = gneiss::Pinhole<double>(params);
              return tmpCamera.project(validPoint).value();
            },
            // NOLINTNEXTLINE(misc-include-cleaner)
            Eigen::Vector4d(200, 200, 256, 256));
      }
    }
  }
}

SCENARIO("Pinhole camera unprojects trivial cases correctly", "[camera]") {
  GIVEN("A camera and a valid 2D point") {
    // NOLINTNEXTLINE(misc-include-cleaner)
    const static auto camera = gneiss::Pinhole<double>(Eigen::Vector4d(200, 200, 256, 256));
    WHEN("unprojecting it") {
      const auto validPoint = Eigen::Vector2d(256, 256);  // NOLINT(misc-include-cleaner)
      const auto unprojection = camera.unproject(validPoint);
      THEN("the projection is correct") {
        const auto should = Eigen::Vector3d(0, 0, 1);  // NOLINT(misc-include-cleaner)
        REQUIRE(unprojection.has_value());
        INFO("unprojection:\n" << unprojection.value() << "\nshould:\n" << should);
        CHECK(unprojection.value().isApprox(should));
      }
    }
  }
}

SCENARIO("The jacobians of the Pinhole unproject method are numerically correct", "[camera]") {
  GIVEN("a camera and a valid 2D point") {
    // NOLINTNEXTLINE(misc-include-cleaner)
    const static auto camera = gneiss::Pinhole<double>(Eigen::Vector4d(200, 200, 256, 256));
    WHEN("unprojecting it") {
      // NOLINTNEXTLINE(misc-include-cleaner)
      const Eigen::Vector2d validPoint =
          // NOLINTNEXTLINE(misc-include-cleaner)
          GENERATE(take(4, randomEigen<Eigen::Vector2d>(std::uniform_real_distribution(-1000.0, 1000.0))));
      const auto unprojection = camera.unproject<true, true>(validPoint);
      THEN("the jacobian by point is correct") {
        REQUIRE(unprojection.has_value());
        test_jacobian_numerically(
            std::get<1>(unprojection.value()),
            // NOLINTNEXTLINE(misc-include-cleaner)
            [&](const Eigen::Vector2d& point) { return camera.unproject(point).value(); }, validPoint);
      }

      THEN("the jacobian by parameter is correct") {
        REQUIRE(unprojection.has_value());
        test_jacobian_numerically(
            std::get<2>(unprojection.value()),
            // NOLINTNEXTLINE(misc-include-cleaner)
            [&](const Eigen::Vector4d& params) {
              const auto tmpCamera = gneiss::Pinhole<double>(params);
              return tmpCamera.unproject(validPoint).value();
            },
            // NOLINTNEXTLINE(misc-include-cleaner)
            Eigen::Vector4d(200, 200, 256, 256));
      }
    }
  }
}

SCENARIO("The unproject method of the Pinhole camera is the inverse of the project method", "[camera]") {
  GIVEN("a camera and a valid 3D point") {
    // NOLINTNEXTLINE(misc-include-cleaner)
    const static auto camera = gneiss::Pinhole<double>(Eigen::Vector4d(200, 200, 256, 256));
    WHEN("projecting and unprojecting it") {
      const auto validPoint =
          GENERATE(take(4, filter([](const auto& point) { return point.z() > 0.001; },
                                  // NOLINTNEXTLINE(misc-include-cleaner)
                                  randomEigen<Eigen::Vector3d>(std::uniform_real_distribution(-1000.0, 1000.0)))));
      const auto projection = camera.project(validPoint);
      const auto unprojection = camera.unproject(projection.value());
      THEN("the unprojection is the bearing vector to the point") {
        REQUIRE(unprojection.has_value());
        INFO("unprojection:\n" << unprojection.value() << "\nshould:\n" << validPoint.normalized());
        CHECK(unprojection.value().isApprox(validPoint.normalized()));
      }
    }
  }
}
