#include "gneiss/camera/DoubleSphere.h"

#include "gneiss/camera/camera.h"  // IWYU pragma: keep

#include "RandomEigenGenerator.h"
#include "test_jacobian_numerically.h"

#include <Eigen/Dense>  // IWYU pragma: keep
#include <catch2/catch_message.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>
#include <catch2/generators/catch_generators_adapters.hpp>

#include <cmath>
#include <random>

TEST_CASE("DoubleSphere fulfills the camera concept", "[camera]") {
  STATIC_REQUIRE(gneiss::camera<gneiss::DoubleSphere<double>>);
}

// NOLINTNEXTLINE(misc-include-cleaner)
using Vector6d = Eigen::Matrix<double, 6, 1>;
const auto defaultParams = Vector6d(0.5 * 805, 0.5 * 800, 505, 509, 0.5 * -0.150694, 0.5 * 1.48785);

[[nodiscard]] bool is_valid(const auto& point) {
  const auto w1 = defaultParams(5) <= 0.5 ? defaultParams(5) / (1.0 - defaultParams(5))
                                          : (1.0 - defaultParams(5)) / defaultParams(5);
  const auto w2 =
      (w1 + defaultParams(4)) / std::sqrt(2.0 * w1 * defaultParams(4) + defaultParams(4) * defaultParams(4) + 1.0);
  const auto d1 = std::sqrt(point.x() * point.x() + point.y() * point.y() + point.z() * point.z());
  return point.z() > -w2 * d1;
}

[[nodiscard]] bool is_valid_projected(const auto& point) {
  if (defaultParams(5) <= 0.5) {
    return true;
  }

  const auto mx = (point.x() - defaultParams(2)) / defaultParams(0);
  const auto my = (point.y() - defaultParams(3)) / defaultParams(1);
  const auto r2 = mx * mx + my * my;

  return r2 <= 1.0 / (2.0 * defaultParams(5) - 1.0);
}

SCENARIO("DoubleSphere camera only projects valid 3D points", "[camera]") {
  GIVEN("a camera and a valid 3D point") {
    const static auto camera = gneiss::DoubleSphere<double>(defaultParams);
    const auto validPoint =
        GENERATE(take(4, filter([](const auto& point) { return is_valid(point); },
                                // NOLINTNEXTLINE(misc-include-cleaner)
                                randomEigen<Eigen::Vector3d>(std::uniform_real_distribution(-1000.0, 1000.0)))));
    WHEN("projecting it") {
      const auto projectedPoint = camera.project(validPoint);
      THEN("the projected point is valid") {
        INFO("projected 3D point " << validPoint);
        CHECK(projectedPoint.has_value());
      }
    }
  }

  GIVEN("A camera and an invalid 3D point") {
    const static auto camera = gneiss::DoubleSphere<double>(defaultParams);
    const auto invalidPoint =
        GENERATE(take(4, filter([](const auto& point) { return !is_valid(point); },
                                // NOLINTNEXTLINE(misc-include-cleaner)
                                randomEigen<Eigen::Vector3d>(std::uniform_real_distribution(-1000.0, 1000.0)))));
    WHEN("projecting it") {
      const auto projectedPoint = camera.project(invalidPoint);
      THEN("the projected point is invalid") {
        INFO("projected 3D point " << invalidPoint);
        CHECK(!projectedPoint.has_value());
      }
    }
  }
}

SCENARIO("DoubleSphere camera projects trivial cases correctly", "[camera]") {
  GIVEN("A camera and a valid 3D point") {
    const static auto camera = gneiss::DoubleSphere<double>(defaultParams);
    WHEN("projecting it") {
      const auto validPoint = Eigen::Vector3d(0, 0, 1);  // NOLINT(misc-include-cleaner)
      const auto projection = camera.project(validPoint);
      THEN("the projection is correct") {
        const auto should = Eigen::Vector2d(505, 509);  // NOLINT(misc-include-cleaner)
        REQUIRE(projection.has_value());
        INFO("projection:\n" << projection.value() << "\nshould:\n" << should);
        CHECK(projection.value().isApprox(should));
      }
    }
  }
}

SCENARIO("The jacobians of the DoubleSphere project method are numerically correct", "[camera]") {
  GIVEN("a camera and a valid 3D point") {
    const static auto camera = gneiss::DoubleSphere<double>(defaultParams);
    WHEN("projecting it") {
      // NOLINTNEXTLINE(misc-include-cleaner)
      const Eigen::Vector3d validPoint =
          GENERATE(take(4, filter([](const auto& point) { return is_valid(point); },
                                  // NOLINTNEXTLINE(misc-include-cleaner)
                                  randomEigen<Eigen::Vector3d>(std::uniform_real_distribution(-1000.0, 1000.0)))));
      const auto projection = camera.project<true, true>(validPoint);
      THEN("the jacobian by point is correct") {
        REQUIRE(projection.has_value());
        test_jacobian_numerically(
            std::get<1>(projection.value()),
            // NOLINTNEXTLINE(misc-include-cleaner)
            [&](const Eigen::Vector3d& point) { return camera.project(point).value(); }, validPoint);
      }

      THEN("the jacobian by parameter is correct") {
        REQUIRE(projection.has_value());
        test_jacobian_numerically(
            std::get<2>(projection.value()),
            [&](const Vector6d& params) {
              const auto tmpCamera = gneiss::DoubleSphere<double>(params);
              return tmpCamera.project(validPoint).value();
            },
            // NOLINTNEXTLINE(misc-include-cleaner)
            defaultParams);
      }
    }
  }
}

SCENARIO("DoubleSphere camera unprojects trivial cases correctly", "[camera]") {
  GIVEN("A camera and a valid 2D point") {
    const static auto camera = gneiss::DoubleSphere<double>(defaultParams);
    WHEN("unprojecting it") {
      const auto validPoint = Eigen::Vector2d(505, 509);  // NOLINT(misc-include-cleaner)
      const auto unprojection = camera.unproject(validPoint);
      THEN("the projection is correct") {
        const auto should = Eigen::Vector3d(0, 0, 1);  // NOLINT(misc-include-cleaner)
        REQUIRE(unprojection.has_value());
        INFO("unprojection:\n" << unprojection.value() << "\nshould:\n" << should);
        CHECK(unprojection.value().isApprox(should));
      }
    }
  }
}

SCENARIO("The jacobians of the DoubleSphere unproject method are numerically correct", "[camera]") {
  GIVEN("a camera and a valid 2D point") {
    const static auto camera = gneiss::DoubleSphere<double>(defaultParams);
    WHEN("unprojecting it") {
      // NOLINTNEXTLINE(misc-include-cleaner)
      const Eigen::Vector2d validPoint =
          GENERATE(take(4, filter([](const auto& point) { return is_valid_projected(point); },
                                  // NOLINTNEXTLINE(misc-include-cleaner)
                                  randomEigen<Eigen::Vector2d>(std::uniform_real_distribution(-1000.0, 1000.0)))));
      const auto unprojection = camera.unproject<true, true>(validPoint);
      THEN("the jacobian by point is correct") {
        REQUIRE(unprojection.has_value());
        test_jacobian_numerically(
            std::get<1>(unprojection.value()),
            // NOLINTNEXTLINE(misc-include-cleaner)
            [&](const Eigen::Vector2d& point) { return camera.unproject(point).value(); }, validPoint);
      }

      THEN("the jacobian by parameter is correct") {
        REQUIRE(unprojection.has_value());
        test_jacobian_numerically(
            std::get<2>(unprojection.value()),
            [&](const Vector6d& params) {
              const auto tmpCamera = gneiss::DoubleSphere<double>(params);
              return tmpCamera.unproject(validPoint).value();
            },
            // NOLINTNEXTLINE(misc-include-cleaner)
            defaultParams);
      }
    }
  }
}

SCENARIO("The unproject method of the DoubleSphere camera is the inverse of the project method", "[camera]") {
  GIVEN("a camera and a valid 3D point") {
    const static auto camera = gneiss::DoubleSphere<double>(defaultParams);
    WHEN("projecting and unprojecting it") {
      const auto validPoint =
          GENERATE(take(4, filter([](const auto& point) { return is_valid(point); },
                                  // NOLINTNEXTLINE(misc-include-cleaner)
                                  randomEigen<Eigen::Vector3d>(std::uniform_real_distribution(-1000.0, 1000.0)))));
      const auto projection = camera.project(validPoint);
      const auto unprojection = camera.unproject(projection.value());
      THEN("the unprojection is the bearing vector to the point") {
        REQUIRE(unprojection.has_value());
        INFO("unprojection:\n" << unprojection.value() << "\nshould:\n" << validPoint.normalized());
        CHECK(unprojection.value().isApprox(validPoint.normalized()));
      }
    }
  }
}
