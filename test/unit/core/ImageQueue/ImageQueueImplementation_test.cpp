#include "gneiss/ImageQueue/ImageQueueImplementation.h"

#include "gneiss/image/ImageMeasurement.h"
#include "gneiss/utils/ConcurrentArena.h"

#include <catch2/catch_test_macros.hpp>

#include <utility>

SCENARIO("Instantiate an ImageQueueImplementation and call a member function", "[ImageQueue]") {
  GIVEN(
      "An ImageQueueImplementation object that statically inherits (CRTP) from "
      "ImageQueue") {
    gneiss::ConcurrentArena<gneiss::ImageMeasurement, 2> arena;
    gneiss::ImageQueueImplementation imageQueue;
    auto testImageMeasurement = arena.emplace(gneiss::ImageMeasurement());

    WHEN("a method of the interface is called") {
      imageQueue.push(std::move(testImageMeasurement));
      THEN("static polymorphism is correctly resolved") {
        // TODO(mwudenka) add check
      }
    }
  }
}
