#include "gneiss/gui/Redux.h"

#include "gneiss/utils/Named.h"

#include <catch2/catch_test_macros.hpp>

#include <cstdint>
#include <iostream>
#include <memory>

#include <sstream>
#include <string>

namespace {
enum class Addition : uint8_t { INCREMENT, DECREMENT };

class AdditionAction : public gneiss::gui::Action {
 public:
  AdditionAction(Addition inType, int inPayload) : type(inType), payload(inPayload) {}

  [[nodiscard]] std::string to_string() const override { return "AdditionAction"; }

  Addition type;
  int payload;
};

enum class Multiplication : uint8_t { MULTIPLY, DIVIDE };

class MultiplicationAction : gneiss::gui::Action {
 public:
  MultiplicationAction(Multiplication inType, int inPayload) : type(inType), payload(inPayload) {}

  [[nodiscard]] std::string to_string() const override { return "MultiplicationAction"; }

  Multiplication type;
  int payload;
};

struct AdditionUpdater {
  template <typename State_>
  static void update(State_&, const gneiss::gui::Action&) {
    std::cout << "not specialized!" << '\n';
  }

  static void update(int& state, const gneiss::gui::Action& action) {
    if (const auto* const additionAction = dynamic_cast<const AdditionAction*>(&action)) {
      switch (additionAction->type) {
        case Addition::INCREMENT:
          state += additionAction->payload;
          break;
        case Addition::DECREMENT:
          state -= additionAction->payload;
          break;
      }
      std::cout << state << '\n';
    }
  }
};

struct MultiplicationUpdater {
  template <typename State_>
  static void update(State_&, const gneiss::gui::Action&) {
    std::cout << "not specialized!" << '\n';
  }

  static void update(int& state, const gneiss::gui::Action& action) {
    if (const auto* const multiplicationAction = dynamic_cast<const MultiplicationAction*>(&action)) {
      switch (multiplicationAction->type) {
        case Multiplication::MULTIPLY:
          state *= multiplicationAction->payload;
          break;
        case Multiplication::DIVIDE:
          state /= multiplicationAction->payload;
          break;
      }
      std::cout << state << '\n';
    }
  }
};

}  // namespace

SCENARIO("Basic redux store setup and manipulation", "[gui]") {
  GIVEN("a redux store") {
    auto myStorePart1 = gneiss::gui::StorePart<AdditionUpdater, int>(5);
    auto myStorePart2 = gneiss::gui::StorePart<MultiplicationUpdater, int>(6);

    auto rootStorePart = gneiss::gui::combine_store_parts(gneiss::make_named<"first">(myStorePart1),
                                                          gneiss::make_named<"second">(myStorePart2));

    auto myStore = gneiss::gui::Store(rootStorePart);
    auto myReader = myStore.get_reader();

    WHEN("an element is queried") {
      auto firstValue = myReader.read_lock()->template get<"first">();

      THEN("the correct value is returned") { CHECK(firstValue == 5); }
    }

    WHEN("an action is dispatched") {
      myStore.dispatch(std::make_unique<AdditionAction>(Addition::INCREMENT, 2));
      myStore.process_actions();
      THEN("the value is correctly updated") { CHECK(myReader.read_lock()->template get<"first">() == 7); }
    }
  }
}

SCENARIO("A logger middleware can be used with a redux store", "[gui]") {
  GIVEN("a redux store") {
    auto myStorePart1 = gneiss::gui::StorePart<AdditionUpdater, int>(5);
    auto myStorePart2 = gneiss::gui::StorePart<MultiplicationUpdater, int>(6);

    auto rootStorePart = gneiss::gui::combine_store_parts(gneiss::make_named<"first">(myStorePart1),
                                                          gneiss::make_named<"second">(myStorePart2));

    auto myStore = gneiss::gui::Store(rootStorePart);
    std::stringstream logStream;
    myStore.register_middleware(gneiss::gui::logger_middleware<decltype(myStore)::StateType>(logStream));
    auto myReader = myStore.get_reader();

    WHEN("an action is dispatched") {
      std::stringstream stateBefore;
      stateBefore << *(myReader.read_lock());
      auto action = AdditionAction(Addition::INCREMENT, 2);
      myStore.dispatch(std::make_unique<decltype(action)>(action));
      myStore.process_actions();
      std::stringstream stateAfter;
      stateAfter << *(myReader.read_lock());
      THEN("the correct message is logged") {
        std::stringstream shouldLog;
        shouldLog << "---------------------------\n"
                     "State before update:\n";
        shouldLog << stateBefore.str() << '\n';
        shouldLog << "Applying action " << action.to_string() << '\n';
        shouldLog << "State after update:\n";
        shouldLog << stateAfter.str() << '\n';
        shouldLog << "---------------------------\n";
        CHECK(logStream.str() == shouldLog.str());
      }
    }
  }
}
