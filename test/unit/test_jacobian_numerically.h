#ifndef GNEISS_TEST_JACOBIAN_NUMERICALLY_H
#define GNEISS_TEST_JACOBIAN_NUMERICALLY_H
#pragma once

#include <Eigen/Eigen>
#include <catch2/catch_test_macros.hpp>

#include <string>

// Code taken from
// https://gitlab.com/VladyslavUsenko/basalt-headers/-/blob/master/test/include/test_utils.h?ref_type=heads

template <typename Scalar_>
struct TestConstants;

template <>
struct TestConstants<double> {
  static constexpr double epsilon = 1e-8;
  static constexpr double max_norm = 1e-3;
};

template <>
struct TestConstants<float> {
  static constexpr double epsilon = 1e-2;
  static constexpr double max_norm = 1e-2;
};

template <typename Derived1, typename Derived2, typename F>
void test_jacobian_numerically(const Eigen::MatrixBase<Derived1> &analyticJacobian, F func,
                               const Eigen::MatrixBase<Derived2> &x0,  // NOLINT(readability-identifier-length)
                               double eps = TestConstants<typename Derived1::Scalar>::epsilon,
                               double max_norm = TestConstants<typename Derived1::Scalar>::max_norm) {
  typedef typename Derived1::Scalar Scalar;

  Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> numericJacobian = analyticJacobian;
  numericJacobian.setZero();

  Eigen::Matrix<Scalar, Eigen::Dynamic, 1> inc = x0;
  for (int i = 0; i < numericJacobian.cols(); i++) {
    inc.setZero();
    inc[i] += eps;

    const Eigen::Matrix<Scalar, Eigen::Dynamic, 1> fpe = func(x0 + inc);
    const Eigen::Matrix<Scalar, Eigen::Dynamic, 1> fme = func(x0 - inc);

    numericJacobian.col(i) = (fpe - fme);
  }

  numericJacobian /= (2 * eps);

  CHECK(analyticJacobian.allFinite());
  CHECK(numericJacobian.allFinite());

  if (numericJacobian.isZero(max_norm) && analyticJacobian.isZero(max_norm)) {
    INFO("analyticJacobian not equal to Jn(diff norm:"
         << (numericJacobian - analyticJacobian).norm() << ")\nanalyticJacobian: (norm: " << analyticJacobian.norm()
         << ")\n"
         << analyticJacobian << "\nJn: (norm: " << numericJacobian.norm() << ")\n"
         << numericJacobian);
    CHECK((numericJacobian - analyticJacobian).isZero(max_norm));

  } else {
    INFO("analyticJacobian not equal to numericJacobian (diff norm:"
         << (numericJacobian - analyticJacobian).norm() << ")\nanalyticJacobian: (norm: " << analyticJacobian.norm()
         << ")\n"
         << analyticJacobian << "\nnumericJacobian: (norm: " << numericJacobian.norm() << ")\n"
         << numericJacobian);
    CHECK(numericJacobian.isApprox(analyticJacobian, max_norm));
  }
}

#endif  // GNEISS_TEST_JACOBIAN_NUMERICALLY_H
