#include "gneiss/executable-utils/StreamTee.h"

#include <catch2/catch_test_macros.hpp>

#include <sstream>
#include <string_view>

SCENARIO("Content can be streamed to multiple streams by using StreamTee", "[executable-utils]") {
  GIVEN("two output streams an a StreamTee") {
    std::ostringstream testOutStreamA;
    std::ostringstream testOutStreamB;
    gneiss::executable_utils::StreamTee testStreamTee(testOutStreamA, testOutStreamB);

    WHEN("a some content is streamed to the tee it is forwarded to the linked streams") {
      static constexpr std::string_view testString = "42";
      testStreamTee << testString;
      THEN("the linked streams both contain the streamed content") {
        CHECK(testOutStreamA.str() == testString);
        CHECK(testOutStreamB.str() == testString);
      }
    }
  }
}