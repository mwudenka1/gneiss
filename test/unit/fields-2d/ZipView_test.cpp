#include "gneiss/fields-2d/ZipView.h"

#include "gneiss/fields-2d/ArrayAdapterView.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstdint>

SCENARIO("2D fields can be zipped", "[fields-2d]") {
  GIVEN("Two toy 2D field") {
    constexpr static std::array<uint8_t, 5ul * 3ul> testDataA{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    constexpr static std::array<int16_t, 5ul * 3ul> testDataB{0,  -1, -2,  -3,  -4,  -5,  -6, -7,
                                                              -8, -9, -10, -11, -12, -13, -14};
    constexpr auto testFieldA = testDataA | gneiss::fields_2d::view::ArrayAdapter<5, 3>;
    constexpr auto testFieldB = testDataB | gneiss::fields_2d::view::ArrayAdapter<3, 5>;

    WHEN("a ZipView is applied") {
      constexpr auto zipped = gneiss::fields_2d::view::Zip(testFieldA, testFieldB);

      THEN("the width and height are correct") {
        STATIC_REQUIRE(zipped.width == 3ul);
        STATIC_REQUIRE(zipped.height == 3ul);
      }

      THEN("the fields are element wise zipped") {
        constexpr auto first = zipped(0, 0);
        STATIC_REQUIRE(std::get<0>(first) == 0);
      }
    }
  }
}
