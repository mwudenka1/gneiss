#include "gneiss/fields-2d/BlockView.h"

#include "gneiss/fields-2d/ArrayAdapterView.h"
#include "gneiss/fields-2d/operators.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstdint>

TEST_CASE("BlockView fulfills the field-2d concept", "[fields-2d]") {
  STATIC_REQUIRE(
      gneiss::fields_2d::field_2d<
          gneiss::fields_2d::BlockView<gneiss::fields_2d::ArrayAdapterView<2, 2, std::array<uint8_t, 4>>, 1, 1>>);
}

SCENARIO("A sub field can be taken from a 2d field with the BlockView", "[fields-2d]") {
  GIVEN("A 2d field") {
    const std::array<uint8_t, 5ul * 3ul> testData{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    const auto testField = testData | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    WHEN("a BlockView is applied to the 2d field") {
      const auto block = testField | gneiss::fields_2d::view::Block<3, 2>({2, 1});

      THEN("the right block is extracted") {
        const std::array<uint8_t, 3ul * 2ul> expectedData{7, 8, 9, 12, 13, 14};
        const auto expectedField = expectedData | gneiss::fields_2d::view::ArrayAdapter<3, 2>;

        CHECK(block == expectedField);
      }
    }
  }
}
