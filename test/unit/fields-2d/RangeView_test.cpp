#include "gneiss/fields-2d/RangeView.h"

#include "gneiss/fields-2d/ArrayAdapterView.h"
#include "gneiss/utils/ranges.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstddef>
#include <cstdint>
#include <execution>  // IWYU pragma: keep
#include <tuple>

SCENARIO("2D fields can be iterated over with a RangeView", "[fields-2d]") {
  GIVEN("A 2d field") {
    const std::array<uint8_t, 3ul * 5ul> testData{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    const auto testField = testData | gneiss::fields_2d::view::ArrayAdapter<3, 5>;

    WHEN("a RangeView is applied to the field") {
      auto rangeView = testField | gneiss::fields_2d::view::Range;

      THEN("RangeView::begin returns a valid random access iterator") {
        STATIC_REQUIRE(std::random_access_iterator<decltype(rangeView.begin())>);
      }

      THEN("RangeView is a valid random access range") {
        STATIC_REQUIRE(gneiss::ranges::random_access_range<decltype(rangeView)>);
      }

      THEN("the iterators operator+= is correct") {
        using difference_type = decltype(rangeView.begin())::difference_type;
        const std::array<std::tuple<std::size_t, difference_type>, 4> iteratorTestData = {
            {{1, 7}, {3, 3}, {7, -6}, {6, -3}}};

        for (const auto& [start, diff] : iteratorTestData) {
          auto testIter = rangeView.begin();
          gneiss::ranges::for_each(gneiss::ranges::views::iota(std::size_t{0}, start),
                                   [&](const auto&) noexcept { ++testIter; });

          auto expectedIter = rangeView.begin();
          gneiss::ranges::for_each(
              gneiss::ranges::views::iota(std::size_t{0},
                                          static_cast<std::size_t>(static_cast<difference_type>(start) + diff)),
              [&](const auto&) noexcept { ++expectedIter; });

          CHECK((testIter += diff) == expectedIter);
        }
      }

      THEN("the iterators operator-= is correct") {
        using difference_type = decltype(rangeView.begin())::difference_type;
        const std::array<std::tuple<size_t, difference_type>, 4> iteratorTestData = {
            {{8, 7}, {6, 3}, {1, -6}, {3, -3}}};

        for (const auto& [start, diff] : iteratorTestData) {
          auto testIter = rangeView.begin();
          gneiss::ranges::for_each(gneiss::ranges::views::iota(std::size_t{0}, start),
                                   [&](const auto&) noexcept { ++testIter; });

          auto expectedIter = rangeView.begin();
          gneiss::ranges::for_each(
              gneiss::ranges::views::iota(std::size_t{0},
                                          static_cast<std::size_t>(static_cast<difference_type>(start) - diff)),
              [&](const auto&) noexcept { ++expectedIter; });

          CHECK((testIter -= diff) == expectedIter);
        }
      }

      THEN("the RangeView can be used in a range based for loop") {
        constexpr static std::array<uint8_t, 15> expecteds = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
        {
          uint32_t idx = 0;
          // NOLINTNEXTLINE(modernize-loop-convert)
          for (auto rangeIt = rangeView.begin(); rangeIt != rangeView.end(); ++rangeIt) {
            CHECK(*rangeIt == expecteds.at(idx));
            ++idx;
          }
        }
        {
          uint32_t idx = 0;
          for (auto item : rangeView) {
            CHECK(item == expecteds.at(idx));
            ++idx;
          }
        }
      }

      THEN("the RangeView can be used in a reversed range based for loop") {
        constexpr static std::array<uint8_t, 15> expecteds = {14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        // does not work with libstdc++
        // {
        //   uint32_t idx = 0;
        //   // NOLINTNEXTLINE(modernize-loop-convert)
        //   for (auto rowsIt = rowsView.rbegin(); rowsIt != rowsView.rend(); ++rowsIt) {
        //     // NOLINTNEXTLINE(modernize-loop-convert)
        //     for (auto resultIt = (*rowsIt).rbegin();
        //          resultIt != (*rowsIt).rend();
        //          ++resultIt) {
        //       CHECK(*resultIt == expecteds.at(idx));
        //       ++idx;
        //     }
        //
        //   }
        // }
        {
          uint32_t idx = 0;
          for (auto item : gneiss::ranges::views::reverse(rangeView)) {
            CHECK(item == expecteds.at(idx));
            ++idx;
          }
        }
      }
    }
  }
}

#if __cpp_lib_execution >= 201902L
SCENARIO("RangeViews can be zipped and used in parallel for_each", "[fields-2d]") {
  GIVEN("Two 2d fields") {
    const std::array<uint8_t, 5ul * 3ul> testDataA{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    const auto testFieldA = testDataA | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    const std::array<uint8_t, 5ul * 3ul> testDataB{0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28};
    const auto testFieldB = testDataB | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    WHEN("the fields can be zipped as ranges") {
      const auto zippedRanges = gneiss::ranges::views::zip(testFieldA | gneiss::fields_2d::view::Range,
                                                           testFieldB | gneiss::fields_2d::view::Range);
      THEN("parallel for_each can be applied to it") {
        std::for_each(std::execution::par, zippedRanges.begin(), zippedRanges.end(), [](const auto& valuePair) {
          const auto& [valA, valB] = valuePair;
          CHECK(valA * 2 == valB);
        });
      }
    }
  }
}
#endif
