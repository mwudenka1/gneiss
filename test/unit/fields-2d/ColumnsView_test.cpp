#include "gneiss/fields-2d/ColumnsView.h"

#include "gneiss/fields-2d/ArrayAdapterView.h"
#include "gneiss/utils/ranges.h"

#include <catch2/catch_test_macros.hpp>

#include <algorithm>  // IWYU pragma: keep
#include <array>
#include <cstdint>
#include <execution>  // IWYU pragma: keep

SCENARIO("2D fields can be iterated over with a ColumnsView", "[fields-2d]") {
  GIVEN("A 2d field") {
    const std::array<uint8_t, 3ul * 5ul> testData{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    const auto testField = testData | gneiss::fields_2d::view::ArrayAdapter<3, 5>;

    WHEN("a ColumnsView is applied to the field") {
      auto columnsView = testField | gneiss::fields_2d::view::Columns;

      THEN("ColumnsView::begin returns a valid random access iterator") {
        STATIC_REQUIRE(std::random_access_iterator<decltype(columnsView.begin())>);
      }

      THEN("ColumnsView is a valid random access range") {
        STATIC_REQUIRE(gneiss::ranges::random_access_range<decltype(columnsView)>);
      }

      THEN("the ColumnsView and ColumnViews can be used in a range based for loop") {
        constexpr static std::array<uint8_t, 15> expecteds = {0, 3, 6, 9, 12, 1, 4, 7, 10, 13, 2, 5, 8, 11, 14};
        {
          uint32_t idx = 0;
          // NOLINTNEXTLINE(modernize-loop-convert)
          for (auto columnsIt = columnsView.begin(); columnsIt != columnsView.end(); ++columnsIt) {
            for (auto result : *columnsIt) {
              CHECK(result == expecteds.at(idx));
              ++idx;
            }
          }
        }
        {
          uint32_t idx = 0;
          for (auto column : columnsView) {
            for (auto result : column) {
              CHECK(result == expecteds.at(idx));
              ++idx;
            }
          }
        }
      }

      THEN("the ColumnsView and ColumnViews can be used in a reversed range based for loop") {
        constexpr static std::array<uint8_t, 15> expecteds = {2, 5, 8, 11, 14, 1, 4, 7, 10, 13, 0, 3, 6, 9, 12};
        {
          uint32_t idx = 0;
          // NOLINTNEXTLINE(modernize-loop-convert)
          for (auto columnsIt = columnsView.rbegin(); columnsIt != columnsView.rend(); ++columnsIt) {
            for (auto result : *columnsIt) {
              CHECK(result == expecteds.at(idx));
              ++idx;
            }
          }
        }

        {
          uint32_t idx = 0;
          for (auto column : columnsView | gneiss::ranges::views::reverse) {
            for (auto result : column) {
              CHECK(result == expecteds.at(idx));
              ++idx;
            }
          }
        }
      }

      THEN("the ColumnsView can be used in a range based for loop and the ColumnViews reversed") {
        constexpr static std::array<uint8_t, 15> expecteds = {12, 9, 6, 3, 0, 13, 10, 7, 4, 1, 14, 11, 8, 5, 2};
        // Does not work with libstdc++
        // {
        //   uint32_t idx = 0;
        //   for (auto column : columnsView) {
        //     // NOLINTNEXTLINE(modernize-loop-convert)
        //     for (auto resultIt = column.rbegin(); resultIt != column.rend(); ++resultIt) {
        //       CHECK(*resultIt == expecteds.at(idx));
        //       ++idx;
        //     }
        //   }
        // }

        {
          uint32_t idx = 0;
          for (auto column : columnsView) {
            for (auto result : gneiss::ranges::views::reverse(column)) {
              CHECK(result == expecteds.at(idx));
              ++idx;
            }
          }
        }
      }

      THEN("the ColumnsView can be used in a reversed range based for loop and the ColumnViews reversed") {
        constexpr static std::array<uint8_t, 15> expecteds = {14, 11, 8, 5, 2, 13, 10, 7, 4, 1, 12, 9, 6, 3, 0};
        // does not work with libstdc++
        // {
        //   uint32_t idx = 0;
        //   // NOLINTNEXTLINE(modernize-loop-convert)
        //   for (auto columnsIt = columnsView.rbegin(); columnsIt != columnsView.rend(); ++columnsIt) {
        //     // NOLINTNEXTLINE(modernize-loop-convert)
        //     for (auto resultIt = (*columnsIt).rbegin();
        //          resultIt != (*columnsIt).rend();
        //          ++resultIt) {
        //       CHECK(*resultIt == expecteds.at(idx));
        //       ++idx;
        //     }
        //
        //   }
        // }
        {
          uint32_t idx = 0;
          for (auto column : gneiss::ranges::views::reverse(columnsView)) {
            for (auto result : gneiss::ranges::views::reverse(column)) {
              CHECK(result == expecteds.at(idx));
              ++idx;
            }
          }
        }
      }
    }
  }
}

#if __cpp_lib_execution >= 201902L
SCENARIO("ColumnsViews can be zipped and used in parallel for_each", "[fields-2d]") {
  GIVEN("Two 2d fields") {
    const std::array<uint8_t, 5ul * 3ul> testDataA{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    const auto testFieldA = testDataA | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    const std::array<uint8_t, 5ul * 3ul> testDataB{0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28};
    const auto testFieldB = testDataB | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    WHEN("the rows of each are zipped") {
      const auto zippedColumns = gneiss::ranges::views::zip(testFieldA | gneiss::fields_2d::view::Columns,
                                                            testFieldB | gneiss::fields_2d::view::Columns);
      THEN("parallel for_each can be applied to it") {
        std::for_each(std::execution::par, zippedColumns.begin(), zippedColumns.end(), [](const auto& columnPair) {
          const auto& [columnA, columnB] = columnPair;
          for (const auto& valuePair : gneiss::ranges::views::zip(columnA, columnB)) {
            const auto& [valA, valB] = valuePair;
            CHECK(valA * 2 == valB);
          }
        });
      }
    }
  }
}
#endif
