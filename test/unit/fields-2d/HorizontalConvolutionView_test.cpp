#include "gneiss/fields-2d/HorizontalConvolutionView.h"

#include "gneiss/fields-2d/ArrayAdapterView.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstddef>
#include <cstdint>

TEST_CASE("HorizontalConvolutionView fulfills the field-2d concept", "[fields-2d]") {
  constexpr static std::array<int, 5> kernel{1, 4, 6, 4, 1};

  STATIC_REQUIRE(gneiss::fields_2d::field_2d<gneiss::fields_2d::HorizontalConvolutionView<
                     2, kernel, gneiss::fields_2d::ArrayAdapterView<2, 2, std::array<uint8_t, 4>>>>);
}

SCENARIO("2D fields can be horizontally convoluted", "[fields-2d]") {
  GIVEN("a toy 2D field") {
    const std::array<uint8_t, 5ul * 3ul> testData{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    const auto testField = testData | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    WHEN("a HorizontalConvolutionView is applied") {
      constexpr static std::array<int, 5> kernel{1, 4, 6, 4, 1};
      const auto view = testField | gneiss::fields_2d::view::HorizontalConvolution<2, kernel>;

      THEN("the width and height are correct") {
        STATIC_REQUIRE(view.width == testField.width - 2 - 2);
        STATIC_REQUIRE(view.height == testField.height);
      }

      THEN("the convolution is correctly applied") {
        constexpr static std::array<std::array<uint8_t, decltype(view)::width>, decltype(view)::height> expecteds = {
            {{32}, {112}, {192}}};

        for (std::size_t xIdx = 0; xIdx < decltype(view)::width; ++xIdx) {
          for (std::size_t yIdx = 0; yIdx < decltype(view)::height; ++yIdx) {
            CHECK(view(xIdx, yIdx) == expecteds.at(yIdx).at(xIdx));
          }
        }
      }
    }
  }
}
