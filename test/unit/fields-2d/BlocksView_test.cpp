#include "gneiss/fields-2d/BlocksView.h"

#include "gneiss/fields-2d/ArrayAdapterView.h"
#include "gneiss/fields-2d/operators.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstdint>

TEST_CASE("BlocksView fulfills the field-2d concept", "[fields-2d]") {
  STATIC_REQUIRE(
      gneiss::fields_2d::field_2d<
          gneiss::fields_2d::BlocksView<gneiss::fields_2d::ArrayAdapterView<2, 2, std::array<uint8_t, 4>>, 1, 1>>);
}

TEST_CASE("The size calculation of a BlocksView is correct", "[fields-2d]") {
  const std::array<uint8_t, 5ul * 3ul> testData{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
  [[maybe_unused]] const auto testField = testData | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

  // default stride
  STATIC_REQUIRE(gneiss::fields_2d::BlocksView<decltype(testField), 3, 3>::width == 1);
  STATIC_REQUIRE(gneiss::fields_2d::BlocksView<decltype(testField), 3, 3>::height == 1);

  // stride 1
  STATIC_REQUIRE(gneiss::fields_2d::BlocksView<decltype(testField), 3, 3, 0, 0, 1, 1>::width == 3);
  STATIC_REQUIRE(gneiss::fields_2d::BlocksView<decltype(testField), 3, 2, 0, 0, 1, 1>::height == 2);

  // padding 2 , 1
  STATIC_REQUIRE(gneiss::fields_2d::BlocksView<decltype(testField), 1, 1, 2, 1>::width == 1);
  STATIC_REQUIRE(gneiss::fields_2d::BlocksView<decltype(testField), 1, 1, 2, 1>::height == 1);
}

SCENARIO("A fields can be divided into consecutive sub fields", "[fields-2d]") {
  GIVEN("A 2d field") {
    static constexpr std::array<uint8_t, 5ul * 3ul> testData{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    constexpr auto testField = testData | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    WHEN("a BlocksView is applied to the 2d field") {
      constexpr auto blocks = testField | gneiss::fields_2d::view::Blocks<3, 2, 0, 0, 2, 1>;

      THEN("the block at position (1,1) is the correct one") {
        static constexpr std::array<uint8_t, 3ul * 2ul> expectedData{7, 8, 9, 12, 13, 14};
        constexpr auto expectedField = expectedData | gneiss::fields_2d::view::ArrayAdapter<3, 2>;
        STATIC_REQUIRE(blocks(1, 1) == expectedField);
      }
    }
  }
}