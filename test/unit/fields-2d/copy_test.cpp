#include "gneiss/fields-2d/copy.h"

#include "gneiss/fields-2d/ArrayAdapterView.h"
#include "gneiss/utils/ranges.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstddef>
#include <cstdint>

SCENARIO("2D fields can be copied to another", "[fields-2d]") {
  GIVEN("two fields") {
    using value_type = uint8_t;
    constexpr static std::size_t width = 3;
    constexpr static std::size_t height = 2;
    const std::array<value_type, width * height> source{0, 1, 2, 3, 4, 5};
    std::array<value_type, width * height> sink{};
    const auto sourceField2d = source | gneiss::fields_2d::view::ArrayAdapter<width, height>;
    auto sinkField2d = sink | gneiss::fields_2d::view::ArrayAdapter<width, height>;

    WHEN("the data is copied from source to sink") {
      gneiss::fields_2d::copy(sourceField2d, sinkField2d);

      THEN("the data is identical in both") {
        for (const auto [sourceVal, sinkVal] : gneiss::ranges::views::zip(source, sink)) {
          CHECK(sourceVal == sinkVal);
        }
      }
    }
  }
}
