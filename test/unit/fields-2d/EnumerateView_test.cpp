#include "gneiss/fields-2d/EnumerateView.h"

#include "gneiss/fields-2d/ArrayAdapterView.h"
#include "gneiss/fields-2d/FieldCoordinate.h"
#include "gneiss/fields-2d/operators.h"
#include "gneiss/utils/output.h"  // IWYU pragma: keep

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstddef>
#include <cstdint>
#include <tuple>

TEST_CASE("EnumerateView fulfills the field-2d concept", "[fields-2d]") {
  STATIC_REQUIRE(
      gneiss::fields_2d::field_2d<
          const gneiss::fields_2d::EnumerateView<gneiss::fields_2d::ArrayAdapterView<2, 2, std::array<uint8_t, 4>>>>);
}

SCENARIO("2D fields can be enumerated", "[fields-2d]") {
  GIVEN("A toy 2D field") {
    const std::array<int, 5ul * 3ul> testData{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    const auto testField = testData | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    WHEN("a Enumerate is applied") {
      const auto view = testField | gneiss::fields_2d::view::Enumerate;

      THEN("the correct coordinates are attached") {
        using Coordinate = gneiss::fields_2d::FieldCoordinate<std::size_t>;
        const std::array<std::tuple<Coordinate, int>, 5ul * 3ul> expectedData{{{Coordinate{0, 0}, 0},
                                                                               {Coordinate{1, 0}, 1},
                                                                               {Coordinate{2, 0}, 2},
                                                                               {Coordinate{3, 0}, 3},
                                                                               {Coordinate{4, 0}, 4},
                                                                               {Coordinate{0, 1}, 5},
                                                                               {Coordinate{1, 1}, 6},
                                                                               {Coordinate{2, 1}, 7},
                                                                               {Coordinate{3, 1}, 8},
                                                                               {Coordinate{4, 1}, 9},
                                                                               {Coordinate{0, 2}, 10},
                                                                               {Coordinate{1, 2}, 11},
                                                                               {Coordinate{2, 2}, 12},
                                                                               {Coordinate{3, 2}, 13},
                                                                               {Coordinate{4, 2}, 14}}};

        const auto expectedField = expectedData | gneiss::fields_2d::view::ArrayAdapter<5, 3>;
        CHECK(view == expectedField);
      }
    }
  }
}