#include "gneiss/fields-2d/ColumnView.h"

#include "gneiss/fields-2d/ArrayAdapterView.h"
#include "gneiss/utils/ranges.h"

#include <catch2/catch_test_macros.hpp>

#include <algorithm>  // IWYU pragma: keep
#include <array>

#include <cstdint>
#include <execution>  // IWYU pragma: keep

SCENARIO("Columns of 2D fields can be iterated over with a ColumnView", "[fields-2d]") {
  GIVEN("A 2d field") {
    const std::array<uint8_t, 5ul * 3ul> testData{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    const auto testField = testData | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    WHEN("a ColumnView is applied to the 2d field") {
      const auto columnView = testField | gneiss::fields_2d::view::Column(0);

      THEN("ColumnView::begin returns a valid random access iterator iterator") {
        STATIC_REQUIRE(std::random_access_iterator<decltype(columnView.begin())>);
      }

      THEN("ColumnView is a valid random access range") {
        STATIC_REQUIRE(gneiss::ranges::random_access_range<decltype(columnView)>);
      }

      THEN("the ColumnView can be used in a range based for loop") {
        constexpr static std::array<uint8_t, 3> expecteds = {0, 5, 10};
        uint32_t idx = 0;
        for (auto result : columnView) {
          CHECK(result == expecteds.at(idx));
          ++idx;
        }
      }

      THEN("the ColumnView can be used in a reversed range based for loop") {
        constexpr static std::array<uint8_t, 3> expecteds = {10, 5, 0};
        uint32_t idx = 0;
        for (auto result : columnView | gneiss::ranges::views::reverse) {
          CHECK(result == expecteds.at(idx));
          ++idx;
        }
      }
    }
  }
}

SCENARIO("Columns of 2D fields can be iterated over with a ColumnView and values assigned", "[fields-2d]") {
  GIVEN("A 2d field") {
    std::array<int, 5ul * 3ul> testData{};
    const auto testField = testData | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    WHEN("a ColumnView is applied to the 2d field") {
      const auto columnView = testField | gneiss::fields_2d::view::Column(0);
      THEN("values can be assigned to the element behind the iterator") {
        for (const auto& value : columnView) {
          CHECK(value == 0);
        }
        for (auto idxValuePair : gneiss::ranges::views::enumerate(columnView)) {
          auto& [idx, value] = idxValuePair;
          value = static_cast<int>(idx);
        }
        for (auto idxValuePair : gneiss::ranges::views::enumerate(columnView)) {
          auto& [idx, value] = idxValuePair;
          CHECK(value == static_cast<int>(idx));
        }
      }
    }
  }
}

#if __cpp_lib_execution >= 201902L
SCENARIO("Columns can be zipped and used in parallel for_each", "[fields-2d]") {
  GIVEN("Two 2d fields") {
    const std::array<uint8_t, 5ul * 3ul> testDataA{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    const auto testFieldA = testDataA | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    const std::array<uint8_t, 5ul * 3ul> testDataB{0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28};
    const auto testFieldB = testDataB | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    WHEN("the Columns of each are zipped") {
      const auto zippedColumns = gneiss::ranges::views::zip(testFieldA | gneiss::fields_2d::view::Column(0),
                                                            testFieldB | gneiss::fields_2d::view::Column(0));
      THEN("parallel for_each can be applied to it") {
        std::for_each(std::execution::par, zippedColumns.begin(), zippedColumns.end(), [](const auto& pair) {
          auto& [valA, valB] = pair;
          CHECK(valA * 2 == valB);
        });
      }
    }
  }
}
#endif
