#include "gneiss/fields-2d/concepts.h"  // IWYU pragma: keep

#include "gneiss/image/Image.h"

#include <catch2/catch_test_macros.hpp>

#include <cstdint>

TEST_CASE("Images fulfill the field_2d concept", "[fields-2d]") {
  STATIC_REQUIRE(gneiss::fields_2d::field_2d<gneiss::Image<uint8_t, 5, 5>>);
}
