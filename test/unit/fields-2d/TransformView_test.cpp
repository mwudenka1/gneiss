#include "gneiss/fields-2d/TransformView.h"

#include "gneiss/fields-2d/ArrayAdapterView.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstdint>

TEST_CASE("TransformView fulfill the field_2d concept", "[fields-2d]") {
  constexpr auto exampleLambda = [](const auto value) { return value; };
  STATIC_REQUIRE(gneiss::fields_2d::field_2d<gneiss::fields_2d::TransformView<
                     gneiss::fields_2d::ArrayAdapterView<5, 5, std::array<uint8_t, 25>>, decltype(exampleLambda)>>);
}

constexpr inline uint8_t doubleValue(const uint8_t &inValue) { return inValue * 3u; }

SCENARIO("The data of 2d fields can be transformed", "[fields-2d]") {
  GIVEN("some toy data in a 2d field") {
    constexpr static std::array<uint8_t, 4> data{0, 1, 2, 3};
    constexpr auto inField2d = data | gneiss::fields_2d::view::ArrayAdapter<2, 2>;
    WHEN("a lambda is applied via view::Transform") {
      constexpr auto doubleLambda = [](const auto value) -> uint8_t { return value * 2u; };
      constexpr auto transformedData = inField2d | gneiss::fields_2d::view::Transform(doubleLambda);

      THEN("the lambda is correctly applied") {
        STATIC_REQUIRE(transformedData(1, 1) == doubleLambda(inField2d(1, 1)));
      }
    }

    WHEN("a function is applied via view::Transform") {
      constexpr auto transformedData = inField2d | gneiss::fields_2d::view::Transform(doubleValue);

      THEN("the function is correctly applied") {
        STATIC_REQUIRE(transformedData(1, 1) == doubleValue(inField2d(1, 1)));
      }
    }
  }
}