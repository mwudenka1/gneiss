#include "gneiss/fields-2d/ArrayAdapterView.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstdint>

TEST_CASE("ArrayAdapters fulfill the field_2d concept", "[fields-2d]") {
  STATIC_REQUIRE(gneiss::fields_2d::field_2d<gneiss::fields_2d::ArrayAdapterView<5, 5, std::array<uint8_t, 25>>>);
}
