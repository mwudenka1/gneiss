#include "gneiss/fields-2d/FlipXYView.h"

#include "gneiss/fields-2d/ArrayAdapterView.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstddef>
#include <cstdint>

TEST_CASE("FlipXYView fulfills the field-2d concept", "[fields-2d]") {
  STATIC_REQUIRE(gneiss::fields_2d::field_2d<
                 gneiss::fields_2d::FlipXYView<gneiss::fields_2d::ArrayAdapterView<2, 2, std::array<uint8_t, 4>>>>);
}

SCENARIO("2D fields can be mirrored along the x-y-axis (flipped)", "[fields-2d]") {
  GIVEN("A toy 2D field") {
    const std::array<uint8_t, 5ul * 3ul> testData{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    const auto testField = testData | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    WHEN("a FlipXYView is applied") {
      const auto view = testField | gneiss::fields_2d::view::FlipXY;

      THEN("the width and height are correct") {
        STATIC_REQUIRE(view.width == testField.height);
        STATIC_REQUIRE(view.height == testField.width);
      }

      THEN("the mirroring operation is correctly applied") {
        for (std::size_t xIdx = 0; xIdx < decltype(view)::width; ++xIdx) {
          for (std::size_t yIdx = 0; yIdx < decltype(view)::height; ++yIdx) {
            // NOLINTNEXTLINE(readability-suspicious-call-argument)
            CHECK(view(xIdx, yIdx) == testField(yIdx, xIdx));
          }
        }
      }
    }
  }
}
