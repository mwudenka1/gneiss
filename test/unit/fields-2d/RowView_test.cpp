#include "gneiss/fields-2d/RowView.h"

#include "gneiss/fields-2d/ArrayAdapterView.h"
#include "gneiss/utils/ranges.h"

#include <catch2/catch_test_macros.hpp>

#include <algorithm>  // IWYU pragma: keep
#include <array>
#include <cstdint>
#include <execution>  // IWYU pragma: keep

SCENARIO("Rows of 2D fields can be iterated over with a RowView", "[fields-2d]") {
  GIVEN("A 2d field") {
    const std::array<uint8_t, 5ul * 3ul> testData{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    const auto testField = testData | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    WHEN("a RowView is applied to the 2d field") {
      const auto rowView = testField | gneiss::fields_2d::view::Row(0);

      THEN("RowView::begin returns a valid random access iterator") {
        STATIC_REQUIRE(std::random_access_iterator<decltype(rowView.begin())>);
      }

      THEN("RowView is a valid random access range") {
        STATIC_REQUIRE(gneiss::ranges::random_access_range<decltype(rowView)>);
      }

      THEN("the RowView can be used in a range based for loop") {
        constexpr static std::array<uint8_t, 5> expecteds = {0, 1, 2, 3, 4};
        uint32_t idx = 0;
        for (auto result : rowView) {
          CHECK(result == expecteds.at(idx));
          ++idx;
        }
      }

      THEN("the RowView can be used in a reversed range based for loop") {
        constexpr static std::array<uint8_t, 5> expecteds = {4, 3, 2, 1, 0};
        uint32_t idx = 0;
        for (auto result : rowView | gneiss::ranges::views::reverse) {
          CHECK(result == expecteds.at(idx));
          ++idx;
        }
      }
    }
  }
}

SCENARIO("Rows of 2D fields can be iterated over with a RowView and values assigned", "[fields-2d]") {
  GIVEN("A 2d field") {
    std::array<int, 5ul * 3ul> testData{};
    const auto testField = testData | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    WHEN("a RowView is applied to the 2d field") {
      const auto rowView = testField | gneiss::fields_2d::view::Row(0);
      THEN("values can be assigned to the element behind the iterator") {
        for (const auto& value : rowView) {
          CHECK(value == 0);
        }

        for (auto idxValuePair : gneiss::ranges::views::enumerate(rowView)) {
          auto& [idx, value] = idxValuePair;
          value = static_cast<int>(idx);
        }
        for (auto idxValuePair : gneiss::ranges::views::enumerate(rowView)) {
          auto& [idx, value] = idxValuePair;
          CHECK(value == static_cast<int>(idx));
        }
      }
    }
  }
}

#if __cpp_lib_execution >= 201902L
SCENARIO("Rows can be zipped and used in parallel for_each", "[fields-2d]") {
  GIVEN("Two 2d fields") {
    const std::array<uint8_t, 5ul * 3ul> testDataA{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    const auto testFieldA = testDataA | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    const std::array<uint8_t, 5ul * 3ul> testDataB{0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28};
    const auto testFieldB = testDataB | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    WHEN("the rows of each are zipped") {
      const auto zippedRows = gneiss::ranges::views::zip(testFieldA | gneiss::fields_2d::view::Row(0),
                                                         testFieldB | gneiss::fields_2d::view::Row(0));
      THEN("parallel for_each can be applied to it") {
        std::for_each(std::execution::par, zippedRows.begin(), zippedRows.end(), [](const auto& pair) {
          auto& [valA, valB] = pair;
          CHECK(valA * 2 == valB);
        });
      }
    }
  }
}
#endif
