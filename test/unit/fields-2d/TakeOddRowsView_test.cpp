#include "gneiss/fields-2d/TakeOddRowsView.h"

#include "gneiss/fields-2d/ArrayAdapterView.h"

#include <catch2/catch_test_macros.hpp>

#include <array>
#include <cstddef>
#include <cstdint>

TEST_CASE("TakeOddRowsView fulfills the field-2d concept", "[fields-2d]") {
  STATIC_REQUIRE(
      gneiss::fields_2d::field_2d<
          gneiss::fields_2d::TakeOddRowsView<gneiss::fields_2d::ArrayAdapterView<2, 2, std::array<uint8_t, 4>>>>);
}

SCENARIO("Even rows of a 2D field can be skipped", "[fields-2d]") {
  GIVEN("a toy 2D field") {
    const std::array<uint8_t, 3ul * 3ul> testData{0, 1, 2, 3, 4, 5, 6, 7, 8};
    const auto testField = testData | gneiss::fields_2d::view::ArrayAdapter<3, 3>;

    WHEN("a TakeOddRowsView is applied") {
      const auto view = testField | gneiss::fields_2d::view::TakeOddRows;

      THEN("the width and height are correct") {
        STATIC_REQUIRE(view.width == testField.width);
        STATIC_REQUIRE(view.height == testField.height / 2);
      }

      THEN("the even rows are skipped") {
        constexpr static std::array<std::array<uint8_t, decltype(view)::width>, decltype(view)::height> expecteds = {
            {{3, 4, 5}}};

        for (std::size_t xIdx = 0; xIdx < decltype(view)::width; ++xIdx) {
          for (std::size_t yIdx = 0; yIdx < decltype(view)::height; ++yIdx) {
            CHECK(view(xIdx, yIdx) == expecteds.at(yIdx).at(xIdx));
          }
        }
      }
    }
  }
}
