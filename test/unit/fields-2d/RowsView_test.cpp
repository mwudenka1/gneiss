#include "gneiss/fields-2d/RowsView.h"

#include "gneiss/fields-2d/ArrayAdapterView.h"
#include "gneiss/utils/ranges.h"

#include <catch2/catch_test_macros.hpp>

#include <algorithm>  // IWYU pragma: keep
#include <array>
#include <cstdint>
#include <execution>  // IWYU pragma: keep

SCENARIO("2D fields can be iterated over with a RowsView", "[fields-2d]") {
  GIVEN("A 2d field") {
    const std::array<uint8_t, 3ul * 5ul> testData{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    const auto testField = testData | gneiss::fields_2d::view::ArrayAdapter<3, 5>;

    WHEN("a RowsView is applied to the field") {
      auto rowsView = testField | gneiss::fields_2d::view::Rows;

      THEN("RowsView::begin returns a valid random access iterator") {
        STATIC_REQUIRE(std::random_access_iterator<decltype(rowsView.begin())>);
      }

      THEN("RowsView is a valid random access range") {
        STATIC_REQUIRE(gneiss::ranges::random_access_range<decltype(rowsView)>);
      }

      THEN("the RowsView and RowViews can be used in a range based for loop") {
        constexpr static std::array<uint8_t, 15> expecteds = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
        {
          uint32_t idx = 0;
          // NOLINTNEXTLINE(modernize-loop-convert)
          for (auto rowsIt = rowsView.begin(); rowsIt != rowsView.end(); ++rowsIt) {
            for (auto result : *rowsIt) {
              CHECK(result == expecteds.at(idx));
              ++idx;
            }
          }
        }
        {
          uint32_t idx = 0;
          for (auto row : rowsView) {
            for (auto result : row) {
              CHECK(result == expecteds.at(idx));
              ++idx;
            }
          }
        }
      }

      THEN("the RowsView and RowViews can be used in a reversed range based for loop") {
        constexpr static std::array<uint8_t, 15> expecteds = {12, 13, 14, 9, 10, 11, 6, 7, 8, 3, 4, 5, 0, 1, 2};
        {
          uint32_t idx = 0;
          // NOLINTNEXTLINE(modernize-loop-convert)
          for (auto rowsIt = rowsView.rbegin(); rowsIt != rowsView.rend(); ++rowsIt) {
            for (auto result : *rowsIt) {
              CHECK(result == expecteds.at(idx));
              ++idx;
            }
          }
        }

        {
          uint32_t idx = 0;
          for (auto row : rowsView | gneiss::ranges::views::reverse) {
            for (auto result : row) {
              CHECK(result == expecteds.at(idx));
              ++idx;
            }
          }
        }
      }

      THEN("the RowsView can be used in a range based for loop and the RowViews reversed") {
        constexpr static std::array<uint8_t, 15> expecteds = {2, 1, 0, 5, 4, 3, 8, 7, 6, 11, 10, 9, 14, 13, 12};
        // Does not work with libstdc++
        // {
        //   uint32_t idx = 0;
        //   for (auto row : rowsView) {
        //     // NOLINTNEXTLINE(modernize-loop-convert)
        //     for (auto resultIt = row.rbegin(); resultIt != row.rend(); ++resultIt) {
        //       CHECK(*resultIt == expecteds.at(idx));
        //       ++idx;
        //     }
        //   }
        // }

        {
          uint32_t idx = 0;
          for (auto row : rowsView) {
            for (auto result : gneiss::ranges::views::reverse(row)) {
              CHECK(result == expecteds.at(idx));
              ++idx;
            }
          }
        }
      }

      THEN("the RowsView can be used in a reversed range based for loop and the RowViews reversed") {
        constexpr static std::array<uint8_t, 15> expecteds = {14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        // does not work with libstdc++
        // {
        //   uint32_t idx = 0;
        //   // NOLINTNEXTLINE(modernize-loop-convert)
        //   for (auto rowsIt = rowsView.rbegin(); rowsIt != rowsView.rend(); ++rowsIt) {
        //     // NOLINTNEXTLINE(modernize-loop-convert)
        //     for (auto resultIt = (*rowsIt).rbegin();
        //          resultIt != (*rowsIt).rend();
        //          ++resultIt) {
        //       CHECK(*resultIt == expecteds.at(idx));
        //       ++idx;
        //     }
        //
        //   }
        // }
        {
          uint32_t idx = 0;
          for (auto row : gneiss::ranges::views::reverse(rowsView)) {
            for (auto result : gneiss::ranges::views::reverse(row)) {
              CHECK(result == expecteds.at(idx));
              ++idx;
            }
          }
        }
      }
    }
  }
}

#if __cpp_lib_execution >= 201902L
SCENARIO("RowsViews can be zipped and used in parallel for_each", "[fields-2d]") {
  GIVEN("Two 2d fields") {
    const std::array<uint8_t, 5ul * 3ul> testDataA{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
    const auto testFieldA = testDataA | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    const std::array<uint8_t, 5ul * 3ul> testDataB{0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28};
    const auto testFieldB = testDataB | gneiss::fields_2d::view::ArrayAdapter<5, 3>;

    WHEN("the rows of each are zipped") {
      const auto zippedRows = gneiss::ranges::views::zip(testFieldA | gneiss::fields_2d::view::Rows,
                                                         testFieldB | gneiss::fields_2d::view::Rows);
      THEN("parallel for_each can be applied to it") {
        std::for_each(std::execution::par, zippedRows.begin(), zippedRows.end(), [](const auto& rowPair) {
          const auto& [rowA, rowB] = rowPair;
          for (const auto& valuePair : gneiss::ranges::views::zip(rowA, rowB)) {
            const auto& [valA, valB] = valuePair;
            CHECK(valA * 2 == valB);
          }
        });
      }
    }
  }
}
#endif
