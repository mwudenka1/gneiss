#include "gneiss/image/detect_keypoints.h"

#include "gneiss/image/Image.h"
#include "gneiss/image/ImagePyramid.h"
#include "gneiss/utils/parallelize.h"
#include "gneiss/utils/ranges.h"

#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/benchmark/catch_chronometer.hpp>
#include <catch2/catch_test_macros.hpp>

#include <cstddef>
#include <cstdint>
#include <limits>
#include <random>
#include <type_traits>
#include <vector>

auto randomPyramids(const int amount) {
  static std::random_device dev;
  static std::mt19937 rng(dev());
  std::uniform_int_distribution<uint8_t> dist(std::numeric_limits<uint8_t>::min(), std::numeric_limits<uint8_t>::max());

  std::vector<gneiss::ImagePyramid<4, uint8_t, 1242, 375>> result;
  result.resize(static_cast<std::size_t>(amount));
  for ([[maybe_unused]] const auto idx : gneiss::ranges::views::iota(0, amount)) {
    // result.emplace_back();
    auto baseImage = gneiss::Image<uint8_t, 1242, 375>();
    for (auto& pixel : baseImage) {
      pixel = dist(rng);
    }
    result.push_back(gneiss::make_image_pyramid<4, gneiss::do_parallelize>(baseImage));
  }

  return result;
}

TEST_CASE("compare parallelized and sequential keypoint detection", "[image]") {
  constexpr static std::size_t gridCellSize = 50;
  constexpr static std::size_t padding = 1;
  constexpr static std::size_t upToLevel = 3;
  constexpr static std::size_t gridCellCapacity = 3;

  BENCHMARK_ADVANCED("parallelized")(Catch::Benchmark::Chronometer meter) {
    const auto pyramids = randomPyramids(meter.runs());
    meter.measure([&pyramids](int idx) {
      const auto& imagePyramid = pyramids.at(static_cast<std::size_t>(idx));

      using ImagePyramid = std::remove_cvref_t<decltype(imagePyramid)>;

      gneiss::KeypointPyramid<float,
                              gneiss::GridPyramidParameters{.baseImageWidth = ImagePyramid::width_0,
                                                            .baseImageHeight = ImagePyramid::height_0,
                                                            .gridSize = gridCellSize,
                                                            .padding = padding,
                                                            .gridCapacity = gridCellCapacity},
                              upToLevel>
          keypointPyramid;
      return gneiss::detect_keypoints<float, gridCellSize, padding, gridCellCapacity, ImagePyramid, upToLevel,
                                      gneiss::do_parallelize>(imagePyramid, keypointPyramid);
    });
  };

  BENCHMARK_ADVANCED("sequential")(Catch::Benchmark::Chronometer meter) {
    const auto pyramids = randomPyramids(meter.runs());
    meter.measure([&pyramids](int idx) {
      const auto& imagePyramid = pyramids.at(static_cast<std::size_t>(idx));

      using ImagePyramid = std::remove_cvref_t<decltype(imagePyramid)>;

      gneiss::KeypointPyramid<float,
                              gneiss::GridPyramidParameters{.baseImageWidth = ImagePyramid::width_0,
                                                            .baseImageHeight = ImagePyramid::height_0,
                                                            .gridSize = gridCellSize,
                                                            .padding = padding,
                                                            .gridCapacity = gridCellCapacity},
                              upToLevel>
          keypointPyramid;
      return gneiss::detect_keypoints<float, gridCellSize, padding, gridCellCapacity, ImagePyramid, upToLevel,
                                      gneiss::do_not_parallelize>(imagePyramid, keypointPyramid);
    });
  };
}