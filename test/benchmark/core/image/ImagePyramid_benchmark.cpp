#include "gneiss/image/ImagePyramid.h"

#include "gneiss/image/Image.h"
#include "gneiss/utils/parallelize.h"
#include "gneiss/utils/ranges.h"

#include <catch2/benchmark/catch_benchmark.hpp>
#include <catch2/benchmark/catch_chronometer.hpp>
#include <catch2/catch_test_macros.hpp>

#include <cstddef>
#include <cstdint>
#include <limits>
#include <random>
#include <vector>

std::vector<gneiss::Image<uint8_t, 1242, 375>> randomImages(const int amount) {
  static std::random_device dev;
  static std::mt19937 rng(dev());
  std::uniform_int_distribution<uint8_t> dist(std::numeric_limits<uint8_t>::min(), std::numeric_limits<uint8_t>::max());

  std::vector<gneiss::Image<uint8_t, 1242, 375>> result;
  result.resize(static_cast<std::size_t>(amount));
  for ([[maybe_unused]] const auto idx : gneiss::ranges::views::iota(0, amount)) {
    result.emplace_back();
    for (auto& pixel : result.back()) {
      pixel = dist(rng);
    }
  }

  return result;
}

TEST_CASE("compare parallelized and sequential image down sampling", "[image]") {
  BENCHMARK_ADVANCED("parallelized")(Catch::Benchmark::Chronometer meter) {
    const auto images = randomImages(meter.runs());
    meter.measure([&images](int idx) {
      return gneiss::make_image_pyramid<4, gneiss::do_parallelize>(images.at(static_cast<std::size_t>(idx)));
    });
  };

  BENCHMARK_ADVANCED("sequential")(Catch::Benchmark::Chronometer meter) {
    const auto images = randomImages(meter.runs());
    meter.measure([&images](int idx) {
      return gneiss::make_image_pyramid<4, gneiss::do_not_parallelize>(images.at(static_cast<std::size_t>(idx)));
    });
  };
}
