#include "gneiss/VioSystem.h"

#include "gneiss/utils/Timestamp.h"
#include "random_image_measurement.h"

#include <catch2/catch_test_macros.hpp>

#include <chrono>
#include <utility>

SCENARIO("Instantiate a VioSystem", "[System]") {
  GIVEN("An auto generated system assembler for vio tasks") {
    using namespace std::literals::chrono_literals;

    gneiss::VioSystem vioSystem{};
    auto testImageMeasurement =
        // NOLINTNEXTLINE(misc-include-cleaner)
        vioSystem.get_storage().storeImageMeasurement(random_image_measurement(gneiss::Timestamp(1s)));

    WHEN("the class is instantiated and a method is called") {
      vioSystem.get_image_queue().push(std::move(testImageMeasurement));
      THEN("nothing crashes") {}
    }
  }
}