#include "gneiss/VioSystem.h"

#include "gneiss/utils/Timestamp.h"
#include "random_image_measurement.h"

#include <catch2/catch_test_macros.hpp>

#include <chrono>
#include <utility>

SCENARIO("Instantiate a VioSystem without odometry", "[System]") {
  GIVEN("An auto generated system assembler for optical flow tasks") {
    using namespace std::literals::chrono_literals;

    gneiss::VioSystem opticalFlowSystem{};
    opticalFlowSystem.get_vio().shutdown();

    auto testImageMeasurementA =
        // NOLINTNEXTLINE(misc-include-cleaner)
        opticalFlowSystem.get_storage().storeImageMeasurement(random_image_measurement(gneiss::Timestamp(1s)));
    auto testImageMeasurementB =
        // NOLINTNEXTLINE(misc-include-cleaner)
        opticalFlowSystem.get_storage().storeImageMeasurement(random_image_measurement(gneiss::Timestamp(2s)));

    WHEN("when two image measurements are pushed into the queue") {
      opticalFlowSystem.get_image_queue().push(std::move(testImageMeasurementA));
      opticalFlowSystem.get_image_queue().push(std::move(testImageMeasurementB));
      THEN("optical flow estimates are coming in") {
        [[maybe_unused]] const auto keypointsEstimateA = opticalFlowSystem.get_keypoints_queue().pop();
        [[maybe_unused]] const auto keypointsEstimateB = opticalFlowSystem.get_keypoints_queue().pop();
      }
    }
  }
}
