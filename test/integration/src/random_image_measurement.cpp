#include "random_image_measurement.h"

#include "gneiss/image/ImageMeasurement.h"
#include "gneiss/utils/Timestamp.h"
#include "gneiss/utils/constexpr_for.h"

#include <cstddef>
#include <limits>
#include <random>
#include <type_traits>

gneiss::ImageMeasurement random_image_measurement(const gneiss::Timestamp timestamp) {
  static std::random_device dev;
  static std::mt19937 rng(dev());

  gneiss::ImageMeasurement randomMeasurement;
  randomMeasurement.timestamp() = timestamp;

  gneiss::constexpr_for<std::size_t{0}, gneiss::ImageMeasurement::numCameras, std::size_t{1}>([&](auto cameraIdx) {
    auto& image = randomMeasurement.get<cameraIdx>();

    using PixelType = std::remove_cvref_t<decltype(image)>::PixelType;

    std::uniform_int_distribution<PixelType> dist(std::numeric_limits<PixelType>::min(),
                                                  std::numeric_limits<PixelType>::max());
    for (auto& pixel : image) {
      pixel = dist(rng);
    }
  });

  return randomMeasurement;
}
