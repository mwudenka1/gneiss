#ifndef GNEISS_RANDOM_IMAGE_MEASUREMENT_H
#define GNEISS_RANDOM_IMAGE_MEASUREMENT_H
#pragma once

#include "gneiss/image/ImageMeasurement.h"
#include "gneiss/utils/Timestamp.h"

gneiss::ImageMeasurement random_image_measurement(gneiss::Timestamp timestamp);

#endif  // GNEISS_RANDOM_IMAGE_MEASUREMENT_H
