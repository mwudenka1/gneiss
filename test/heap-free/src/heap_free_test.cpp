#include <atomic>
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <new>

std::atomic<int> newCounter = 0;

// NOLINTNEXTLINE(readability-redundant-declaration)
void* operator new(std::size_t sz) {
  // extern void* bare_new_erroneously_called();
  // return bare_new_erroneously_called();

  std::cout << "new called" << '\n';
  newCounter++;

  if (sz == 0) {
    ++sz;
  }

  if (void* ptr = std::malloc(sz)) {
    return ptr;
  }

  throw std::bad_alloc{};
}

void* operator new[](std::size_t sz) {
  std::cout << "new[] called" << '\n';
  newCounter++;

  if (sz == 0) {
    ++sz;
  }

  if (void* ptr = std::malloc(sz)) {
    return ptr;
  }

  throw std::bad_alloc{};
}

#include "gneiss/VioSystem.h"

#include <limits>
#include <random>

template <typename Precision_>
Precision_ get_random_number() {
  // NOLINTNEXTLINE(cert-msc32-c,cert-msc51-cpp)
  std::default_random_engine generator{};
  static std::uniform_int_distribution<Precision_> distribution(std::numeric_limits<Precision_>::min(),
                                                                std::numeric_limits<Precision_>::max());

  return distribution(generator);
}

int main() {
  gneiss::VioSystem vioSystem{};
  vioSystem.get_vio().shutdown();  // not yet implemented

  gneiss::ImageMeasurement imageMeasurement;

  auto imageMeasurementPtr = vioSystem.get_storage().storeImageMeasurement(imageMeasurement);

  for (auto& pixel : imageMeasurementPtr->get<0>()) {
    pixel = get_random_number<std::remove_reference_t<decltype(imageMeasurementPtr->get<0>())>::PixelType>();
  }

  vioSystem.get_image_queue().push(std::move(imageMeasurementPtr));
  [[maybe_unused]] const auto keypointsEstimate = vioSystem.get_keypoints_queue().pop();

  // at the moment one heap allocation is done during the initialization phase for the optical flow worker std::thread
  if (newCounter == 1) {
    return 0;
  }

  return 1;
}
