def yes_no_dialog(question: str, default_answer="yes") -> bool:
    answers = {"yes": True, "y": True, "ye": True,
               "no": False, "n": False}
    if default_answer is None:
        tip = " [y/n] "
    elif default_answer == "yes":
        tip = " [Y/n] "
    elif default_answer == "no":
        tip = " [y/N] "
    else:
        raise ValueError(f'Invalid value: {default_answer = }')
    while True:
        print(question + tip + ": ", end="")
        user_answer = input().lower()
        if default_answer is not None and user_answer == '':
            return answers[default_answer]
        elif user_answer in answers:
            return answers[user_answer]
        else:
            print("Please enter yes/y or no/nn")
