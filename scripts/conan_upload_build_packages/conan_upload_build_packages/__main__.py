# adapted from
# https://github.com/conan-io/conan/issues/3225#issuecomment-408052372

import json
import sys
import os
from argparse import ArgumentParser
from pathlib import Path
from typing import Dict
from .yes_no_dialog import yes_no_dialog


def upload_packages(
        json_data: Dict,
        conan_remote: str,
        interactive: bool = True):
    for installed in json_data['installed']:
        reference = installed['recipe']['id']
        for package in installed['packages']:
            if package['built']:
                if interactive:
                    should_upload = yes_no_dialog(f"Upload {reference}?")
                else:
                    should_upload = True

                if should_upload:
                    os.system(
                        f"conan upload {reference}:{package['id']} -r {conan_remote} -c --retry 5 --retry-wait 5")


def main():
    argument_parser = ArgumentParser(
        description="Parses the json output from the conan install command and uploads all built packages")
    argument_parser.add_argument(
        "--remote",
        type=str,
        help="Name of the conan remote to which the packages should be uploaded",
        required=True)
    argument_parser.add_argument(
        "--json_file_path",
        type=str,
        help="Path and name of the json file created by the conan install command",
        required=True)
    argument_parser.add_argument('--non_interactive', action='store_true')
    argument_parser.set_defaults(non_interactive=False)

    args = argument_parser.parse_args()

    if not os.path.isfile(args.json_file_path):
        print(f"Error: {args.json_file_path} is not a file.")
        sys.exit(1)
    json_file_path = Path(args.json_file_path)

    with open(json_file_path) as json_file:
        json_data = json.load(json_file)

    upload_packages(
        json_data,
        args.remote,
        interactive=not args.non_interactive)


if __name__ == "__main__":
    main()
