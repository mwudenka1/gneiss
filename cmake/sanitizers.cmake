cmake_minimum_required(VERSION 3.15)

macro(_add_sanitizer add-sanitizer)
  string(TOLOWER ${add-sanitizer} add-sanitizer-lower)

  if(${SANITIZER} STREQUAL ${add-sanitizer})
    message(STATUS "${add-sanitizer} sanitizer enabled!")
    add_compile_options("-fsanitize=${add-sanitizer-lower}")
    add_link_options("-fsanitize=${add-sanitizer-lower}")
  endif()
endmacro()

function(enable_sanitizer)
  set(SANITIZER
      None
      CACHE STRING "Sanitizer to be used"
  )
  set_property(CACHE SANITIZER PROPERTY STRINGS None Address Thread Leak Memory)

  set(sanitizer_options None Address Thread Leak Memory)
  list(FIND sanitizer_options "${SANITIZER}" sanitizer-user)
  if(sanitizer-user EQUAL -1)
    message(FATAL_ERROR "'SANITIZER' should be one of ${sanitizer_options} ('${SANITIZER}' received)")

  else()
    message(STATUS "SANITIZER: ${SANITIZER}")
    _add_sanitizer("Address")
    _add_sanitizer("Thread")
    _add_sanitizer("Leak")
    _add_sanitizer("Memory")
  endif()
endfunction()
