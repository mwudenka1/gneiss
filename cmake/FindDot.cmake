# Look for an executable called dot
find_program(
  DOT_EXECUTABLE
  NAMES dot dot.exe
  DOC "Path to dot executable"
)

include(FindPackageHandleStandardArgs)

# Handle standard arguments to find_package like REQUIRED and QUIET
find_package_handle_standard_args(Dot "Failed to find dot executable" DOT_EXECUTABLE)
