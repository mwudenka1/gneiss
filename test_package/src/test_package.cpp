#include <gneiss/VioSystem.h>

int main() {
  gneiss::VioSystem vioSystem;
  vioSystem.get_vio().shutdown();
  auto testImageMeasurementPtr = vioSystem.get_storage().storeImageMeasurement(gneiss::ImageMeasurement{});
  vioSystem.get_image_queue().push(std::move(testImageMeasurementPtr));
  [[maybe_unused]] const auto keypointsEstimate = vioSystem.get_keypoints_queue().pop();
}
